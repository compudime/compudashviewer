unit ColumnUnboundThreadUnit;

interface

uses
  System.Classes, System.Generics.Collections, System.Variants, ColumnHelperTypesUnit, ColumnUnboundHelperUnit;

type
  TColumnUnboundThread = class(TThread)
  private
    threadResult: TDictionary<Variant, TDictionary<String, Variant>>;
    threadColumns: TDictionary<String, TUnboundColumn>;
    threadValues: TDictionary<Variant, TDictionary<String, Variant>>;
    threadGridKey: String;

    function checkValue(const rowIdx: Variant; const colIdx: String; item: TConditionUnbound;
      var expVal: Variant): Boolean;
    function checkListValues(const rowIdx: Variant; const colIdx: String; var expVal: Variant): Boolean;

    function CountOccurences(const SubText: string; const Text: string): Integer;
    function isValidExp(const Exp: string; const rowIdx: Variant; const colIdx: String): Boolean;
    function isFunction(const Exp: string): Boolean;
    function parse(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
    function parseBrace(Exp: string; const rowIdx: Variant; const colIdx: String; var lastIdx: Integer): Variant;
    function parseValue(Exp: string; const rowIdx: Variant; const colIdx: String; var lastIdx: Integer): Variant;
    function parseString(Exp: string; const rowIdx: Variant; const colIdx: String; var lastIdx: Integer): Variant;
    function parseField(Exp: string; const rowIdx: Variant; const colIdx: String; var lastIdx: Integer)
      : Variant; Overload;
    function parseField(Exp: string; const rowIdx: Variant; const colIdx: String): Variant; Overload;
    function parseFunc(Exp: string; const rowIdx: Variant; const colIdx: String; var lastIdx: Integer): Variant;
    function parseSQL(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
    function parseWS(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
    function calcValue(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
    function calcField(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
    function calcFunc(Exp1: string; Exp2: string; const rowIdx: Variant; const colIdx: String): Variant;
    function calcSQL(Exp1: string; Exp2: string; const rowIdx: Variant; const colIdx: String): Variant;
    function calcWS(Exp1: string; Exp2: string; const rowIdx: Variant; const colIdx: String): Variant;
    function interpretResult(resValue: Variant; const rowIdx: Variant; const colIdx: String): Variant;
  protected
    procedure Execute; override;

  public
    property ColumnUnboundResult: TDictionary < Variant, TDictionary < String, Variant >> read threadResult;

    property GridKey: String read threadGridKey;

    // property HadException: TDictionary<Integer, Boolean> read threadHadException;

    constructor Create(values: TDictionary<Variant, TDictionary<String, Variant>>;
      columns: TDictionary<String, TUnboundColumn>; key: String);

    destructor Destroy; override;
  end;

implementation

uses Winapi.ActiveX, System.SysUtils, ColumnHelperDataUnit;

constructor TColumnUnboundThread.Create(values: TDictionary<Variant, TDictionary<String, Variant>>;
  columns: TDictionary<String, TUnboundColumn>; key: String);
var
  column: TPair<String, TUnboundColumn>;
  row: TPair<Variant, TDictionary<String, Variant>>;
begin
  inherited Create(true);
  threadResult := TDictionary < Variant, TDictionary < String, Variant >>.Create;
  // threadHadException:= TDictionary<Integer,Boolean>.Create;
  for row in values do
  begin
    threadResult.Add(row.key, TDictionary<String, Variant>.Create);
    for column in columns do
    begin
      threadResult[row.key].Add(column.key, Null);
      // threadHadException.Add(pair.Key,false);
    end;
  end;

  threadValues := values;
  threadColumns := columns;
  threadGridKey := key;
  FreeOnTerminate := false;
  Suspended := true;
end;

destructor TColumnUnboundThread.Destroy;
begin
  FreeAndNil(threadResult);

  inherited;
end;

procedure TColumnUnboundThread.Execute;
var
  row: TPair<Variant, TDictionary<String, Variant>>;
  column: TUnboundColumn;
  colName: String;
  varValue: Variant;
  columnNames: TList<String>;
begin
  inherited;
  CoInitialize(nil); // Needed for ADSComponents

  columnNames := ColumnUnboundHelper.sortColumns(threadColumns.values.ToArray());
  for row in threadValues do
  begin
    if (Terminated) then
      break;
    for colName in columnNames do
    begin
      try
        if (Terminated) then
          break;
        column := threadColumns[colName];
        if (column.mode in [umValue, umSQLEdit, umWSEdit]) and isValidExp(column.expression, row.key, colName) then
        begin
          threadResult[row.key][colName] := parse(column.expression, row.key, colName);
          row.Value[colName] := threadResult[row.key][colName];
        end
        else if column.mode in [umValueByCondition, umSQLEditByCondition, umWSEditByCondition] then
        begin
          varValue := Null;
          if checkListValues(row.key, colName, varValue) then
          begin
            threadResult[row.key][colName] := varValue;
            row.Value[colName] := threadResult[row.key][colName];
          end;
        end;
      except
        // self.threadHadException[pair.Key] := true;
      end;
    end;
  end;
  columnNames.Free;

  // if not Terminated then
  // FreeOnTerminate := True;

  CoUninitialize;
end;

function TColumnUnboundThread.parse(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  I: Integer;
  item: String;
  lastSym: String;
  itemType: TDataTypes;
  varValue: Variant;
begin
  Result := Null;
  I := 0;
  lastSym := String.Empty;
  while I < Exp.Length do
  begin
    item := Exp.Substring(I, 1);
    if item = '' then
    begin
      Inc(I);
      continue;
    end;
    itemType := getStrType(item, varValue);
    if itemType = dtString then
    begin
      if (item = '+') or (item = '-') or (item = '*') then
      begin
        lastSym := item;
      end
      else if item = '(' then
      begin
        if lastSym.IsEmpty then
          Result := parseBrace(Exp.Substring(I + 1), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseBrace(Exp.Substring(I + 1), rowIdx, colIdx, I)
          else if lastSym = '-' then
            Result := Result - parseBrace(Exp.Substring(I + 1), rowIdx, colIdx, I)
          else if lastSym = '*' then
            Result := Result * parseBrace(Exp.Substring(I + 1), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end
      else if item = '[' then
      begin
        if lastSym.IsEmpty then
          Result := parseField(Exp.Substring(I + 1), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseField(Exp.Substring(I + 1), rowIdx, colIdx, I)
          else if lastSym = '-' then
            Result := Result - parseField(Exp.Substring(I + 1), rowIdx, colIdx, I)
          else if lastSym = '*' then
            Result := Result * parseField(Exp.Substring(I + 1), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end
      else if item = '"' then
      begin
        if lastSym.IsEmpty then
          Result := parseString(Exp.Substring(I + 1), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseString(Exp.Substring(I + 1), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end
      else if isFunction(Exp.Substring(I)) then
      begin
        if lastSym.IsEmpty then
          Result := parseFunc(Exp.Substring(I), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseFunc(Exp.Substring(I), rowIdx, colIdx, I)
          else if lastSym = '-' then
            Result := Result - parseFunc(Exp.Substring(I), rowIdx, colIdx, I)
          else if lastSym = '*' then
            Result := Result * parseFunc(Exp.Substring(I), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end;
    end
    else if itemType = dtInt then
    begin
      if lastSym.IsEmpty then
        Result := parseValue(Exp.Substring(I), rowIdx, colIdx, I)
      else
      begin
        if lastSym = '+' then
          Result := Result + parseValue(Exp.Substring(I), rowIdx, colIdx, I)
        else if lastSym = '-' then
          Result := Result - parseValue(Exp.Substring(I), rowIdx, colIdx, I)
        else if lastSym = '*' then
          Result := Result * parseValue(Exp.Substring(I), rowIdx, colIdx, I);
        lastSym := String.Empty;
      end;
    end;
    Inc(I);
  end;
end;

function TColumnUnboundThread.parseBrace(Exp: string; const rowIdx: Variant; const colIdx: String;
  var lastIdx: Integer): Variant;
var
  I: Integer;
  item: String;
  lastSym: String;
  itemType: TDataTypes;
  varValue: Variant;
begin
  Result := Null;
  I := 0;
  lastSym := String.Empty;
  while I < Exp.Length do
  begin
    item := Exp.Substring(I, 1);
    if item = ' ' then
    begin
      Inc(I);
      continue;
    end;
    itemType := getStrType(item, varValue);
    if itemType = dtString then
    begin
      if (item = '+') or (item = '-') or (item = '*') then
      begin
        lastSym := item;
      end
      else if item = '(' then
      begin
        Result := parseBrace(Exp.Substring(I + 1), rowIdx, colIdx, I);
      end
      else if item = '[' then
      begin
        if lastSym.IsEmpty then
          Result := parseField(Exp.Substring(I + 1), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseField(Exp.Substring(I + 1), rowIdx, colIdx, I)
          else if lastSym = '-' then
            Result := Result - parseField(Exp.Substring(I + 1), rowIdx, colIdx, I)
          else if lastSym = '*' then
            Result := Result * parseField(Exp.Substring(I + 1), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end
      else if isFunction(Exp.Substring(I)) then
      begin
        if lastSym.IsEmpty then
          Result := parseFunc(Exp.Substring(I), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseFunc(Exp.Substring(I), rowIdx, colIdx, I)
          else if lastSym = '-' then
            Result := Result - parseFunc(Exp.Substring(I), rowIdx, colIdx, I)
          else if lastSym = '*' then
            Result := Result * parseFunc(Exp.Substring(I), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end
      else if item = '"' then
      begin
        if lastSym.IsEmpty then
          Result := parseString(Exp.Substring(I + 1), rowIdx, colIdx, I)
        else
        begin
          if lastSym = '+' then
            Result := Result + parseString(Exp.Substring(I + 1), rowIdx, colIdx, I);
          lastSym := String.Empty;
        end;
      end
      else if item = ')' then
      begin
        lastIdx := lastIdx + I + 1;
        break;
      end;
    end
    else if itemType = dtInt then
    begin
      if lastSym.IsEmpty then
        Result := parseValue(Exp.Substring(I), rowIdx, colIdx, I)
      else
      begin
        if lastSym = '+' then
          Result := Result + parseValue(Exp.Substring(I), rowIdx, colIdx, I)
        else if lastSym = '-' then
          Result := Result - parseValue(Exp.Substring(I), rowIdx, colIdx, I)
        else if lastSym = '*' then
          Result := Result * parseValue(Exp.Substring(I), rowIdx, colIdx, I);
        lastSym := String.Empty;
      end;
    end;
    Inc(I);
  end;

end;

function TColumnUnboundThread.parseString(Exp: string; const rowIdx: Variant; const colIdx: String;
  var lastIdx: Integer): Variant;
var
  I: Integer;
  item: String;
  valStr: String;
begin
  Result := Null;
  valStr := String.Empty;
  for I := 0 to Exp.Length - 1 do
  begin
    item := Exp.Substring(I, 1);
    if item = '"' then
    begin
      lastIdx := lastIdx + I + 1;
      Result := valStr;
      break;
    end
    else
      valStr := valStr + item;
  end;
end;

function TColumnUnboundThread.parseValue(Exp: string; const rowIdx: Variant; const colIdx: String;
  var lastIdx: Integer): Variant;
var
  I: Integer;
  item: String;
  valStr: String;
  itemType: TDataTypes;
  varValue: Variant;
begin
  Result := Null;
  valStr := String.Empty;
  for I := 0 to Exp.Length - 1 do
  begin
    item := Exp.Substring(I, 1);
    if item = ' ' then
    begin
      continue;
    end;
    itemType := getStrType(item, varValue);
    if itemType = dtInt then
    begin
      valStr := valStr + item;
      if I = (Exp.Length - 1) then
      begin
        lastIdx := lastIdx + I;
        Result := calcValue(valStr, rowIdx, colIdx);
      end;
    end
    else if (itemType = dtString) then
    begin
      if ((item = '.') or (item = ',')) then
        valStr := valStr + item
      else
      begin
        lastIdx := lastIdx + I - 1;
        Result := calcValue(valStr, rowIdx, colIdx);
        break;
      end;
    end
    else
    begin
      lastIdx := lastIdx + I - 1;
      Result := calcValue(valStr, rowIdx, colIdx);
      break;
    end;
  end;
end;

function TColumnUnboundThread.parseField(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  idx: Integer;
begin
  Result := parseField(Exp, rowIdx, colIdx, idx);
end;

function TColumnUnboundThread.parseField(Exp: string; const rowIdx: Variant; const colIdx: String;
  var lastIdx: Integer): Variant;
var
  I: Integer;
  item: String;
  valStr: String;
  itemType: TDataTypes;
  varValue: Variant;
begin
  Result := Null;
  valStr := String.Empty;
  for I := 0 to Exp.Length - 1 do
  begin
    item := Exp.Substring(I, 1);
    if ((item = ' ') or (item = '.') or (item = ',')) then
    begin
      break;
    end;
    itemType := getStrType(item, varValue);
    if itemType = dtInt then
    begin
      valStr := valStr + item;
    end
    else if itemType = dtString then
    begin
      if (item = ']') then
      begin
        Result := calcField(valStr, rowIdx, colIdx);
        lastIdx := lastIdx + I + 1;
        break;
      end
      else
        valStr := valStr + item;
    end
    else
      break;
  end;
end;

function TColumnUnboundThread.parseFunc(Exp: string; const rowIdx: Variant; const colIdx: String;
  var lastIdx: Integer): Variant;
var
  I: Integer;
  item: String;
  nameStr, valStr: String;
  itemType: TDataTypes;
  varValue: Variant;
  startVal: Boolean;
begin
  Result := Null;

  nameStr := String.Empty;
  valStr := String.Empty;
  startVal := false;
  for I := 0 to Exp.Length - 1 do
  begin
    item := Exp.Substring(I, 1);
    if (item = ' ') then
    begin
      continue;
    end;
    itemType := getStrType(item, varValue);
    if itemType = dtString then
    begin
      if (item = '(') then
      begin
        startVal := true;
      end
      else if (item = ')') then
      begin
        Result := calcFunc(nameStr, valStr, rowIdx, colIdx);
        lastIdx := lastIdx + I;
        break;
      end
      else
      begin
        if startVal then
          valStr := valStr + item
        else
          nameStr := nameStr + item;
      end;
    end
    else if itemType = dtInt then
    begin
      if startVal then
        valStr := valStr + item
      else
        nameStr := nameStr + item;
    end;
  end;
end;

function TColumnUnboundThread.parseSQL(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  fieldName: String;
  idx: Integer;
begin
  fieldName := String.Empty;
  idx := Exp.IndexOf('_');
  if idx > 0 then
    fieldName := Exp.Substring(idx + 1);
  Result := calcSQL(Exp, fieldName, rowIdx, colIdx);
end;

function TColumnUnboundThread.parseWS(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  fieldName: String;
  idx: Integer;
begin
  fieldName := String.Empty;
  idx := Exp.IndexOf('_');
  if idx > 0 then
    fieldName := Exp.Substring(idx + 1);
  Result := calcWS(Exp, fieldName, rowIdx, colIdx);
end;

function TColumnUnboundThread.calcValue(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  itemType: TDataTypes;
  varValue: Variant;
begin
  Result := Null;
  itemType := getStrType(Exp, varValue);
  if (itemType <> dtUnknown) and (itemType <> dtString) then
    Result := varValue;
end;

function TColumnUnboundThread.calcField(Exp: string; const rowIdx: Variant; const colIdx: String): Variant;
begin
  Result := Null;
  if Exp.StartsWith('SQL') then
    Result := parseSQL(Exp, rowIdx, colIdx)
  else if Exp.StartsWith('WS') then
    Result := parseWS(Exp, rowIdx, colIdx)
  else if threadValues[rowIdx].ContainsKey(Exp) then
    Result := threadValues[rowIdx][Exp];
end;

function TColumnUnboundThread.calcFunc(Exp1: string; Exp2: string; const rowIdx: Variant; const colIdx: String)
  : Variant;
var
  argValue: Variant;
begin
  Result := Null;
  if Exp2.IsEmpty then
  begin
    if Exp1.StartsWith('Date', true) then
    begin
      if threadColumns[colIdx].ctype = dtDate then
        Result := Date()
      else
        Result := DateToStr(Date())
    end;
  end
  else
  begin
    argValue := Null;
    if Exp2.Substring(0, 1) = '[' then
      argValue := parseField(Exp2.Substring(1), rowIdx, colIdx)
    else
      argValue := calcValue(Exp2, rowIdx, colIdx);
    if Exp1.StartsWith('Date', true) then
    begin
      if threadColumns[colIdx].ctype = dtDate then
        Result := StrToDate(argValue)
      else
        Result := argValue
    end
    else if Exp1.StartsWith('IntToStr', true) then
      Result := IntToStr(argValue)
    else if Exp1.StartsWith('FloatToStr', true) then
      Result := FloatToStr(argValue)
    else if Exp1.StartsWith('DateToStr', true) then
      Result := DateToStr(argValue)
    else if Exp1.StartsWith('StrToDate', true) then
      Result := StrToDate(argValue)
    else if Exp1.StartsWith('StrToFloat', true) then
      Result := StrToFloat(argValue)
    else if Exp1.StartsWith('StrToInt', true) then
      Result := StrToInt(argValue);
  end;
end;

function TColumnUnboundThread.calcSQL(Exp1: string; Exp2: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  sql: String;
  names: TDictionary<String, Variant>;
  fields: TList<String>;
  exception: String;
  resValue: Variant;
begin
  Result := Null;

  if not threadColumns[colIdx].sqlStatements.ContainsKey(Exp1) then
    exit;
  names := TDictionary<String, Variant>.Create;
  sql := ColumnUnboundHelperData.prepareSQLText(threadColumns[colIdx].sqlStatements[Exp1], threadValues[rowIdx], names);
  if not sql.IsEmpty then
  begin
    fields := TList<String>.Create;
    resValue := ColumnUnboundHelperData.runSQLRequest(sql, Exp2, names, fields, exception);
    names.Free;
    fields.Free;
    Result := interpretResult(resValue, rowIdx, colIdx);
  end;
end;

function TColumnUnboundThread.calcWS(Exp1: string; Exp2: string; const rowIdx: Variant; const colIdx: String): Variant;
var
  url: String;
  fields: TList<String>;
  exception: String;
  resValue: Variant;
begin
  Result := Null;

  if not threadColumns[colIdx].wsStatements.ContainsKey(Exp1) then
    exit;
  url := ColumnUnboundHelperData.prepareWSText(threadColumns[colIdx].wsStatements[Exp1], threadValues[rowIdx]);
  if not url.IsEmpty then
  begin
    fields := TList<String>.Create;
    resValue := ColumnUnboundHelperData.runWSRequest(url, Exp2, fields, exception);
    fields.Free;
    Result := interpretResult(resValue, rowIdx, colIdx);
  end;
end;

function TColumnUnboundThread.interpretResult(resValue: Variant; const rowIdx: Variant; const colIdx: String): Variant;
var
  resType: TDataTypes;
begin
  try
    resType := valueType(resValue);
    Result := Null;
    if resType = dtUnknown then
      Result := resValue
    else if threadColumns[colIdx].ctype = dtString then
    begin
      case resType of
        dtFloat:
          Result := FloatToStr(resValue);
        dtCurrency:
          Result := FloatToStr(resValue);
        dtInt:
          Result := IntToStr(resValue);
        dtDate:
          Result := DateToStr(resValue);
        dtString:
          Result := resValue;
      end;
    end
    else if (threadColumns[colIdx].ctype = dtFloat) or (threadColumns[colIdx].ctype = dtCurrency) then
    begin
      case resType of
        dtString:
          Result := StrToFloat(resValue);
      else
        Result := resValue;
      end;
    end
    else if threadColumns[colIdx].ctype = dtInt then
    begin
      case resType of
        dtString:
          Result := StrToInt(resValue);
      else
        Result := resValue;
      end;
    end
    else if threadColumns[colIdx].ctype = dtDate then
    begin
      case resType of
        dtString:
          Result := StrToDate(resValue);
      else
        Result := resValue;
      end;
    end;
  finally
  end;
end;

function TColumnUnboundThread.isFunction(const Exp: string): Boolean;
begin
  Result := Exp.StartsWith('Date', true) or Exp.StartsWith('IntToStr', true) or Exp.StartsWith('FloatToStr', true) or
    Exp.StartsWith('DateToStr', true) or Exp.StartsWith('StrToDate', true) or Exp.StartsWith('StrToFloat', true) or
    Exp.StartsWith('StrToInt', true);
end;

function TColumnUnboundThread.isValidExp(const Exp: string; const rowIdx: Variant; const colIdx: String): Boolean;
begin
  if Exp.IsEmpty or (CountOccurences(Exp, '(') <> CountOccurences(Exp, ')')) or
    (CountOccurences(Exp, '[') <> CountOccurences(Exp, ']')) then
    Result := false
  else if threadColumns[colIdx].ctype = dtString then
    Result := ((not Exp.Contains('*')) and (not Exp.Contains('-')))
  else
    Result := true;
end;

function TColumnUnboundThread.CountOccurences(const SubText: string; const Text: string): Integer;
begin
  if SubText.IsEmpty or (not Text.Contains(SubText)) then
    Result := 0
  else
    Result := (Length(Text) - Length(StringReplace(Text, SubText, '', [rfReplaceAll, rfIgnoreCase])))
      div Length(SubText);
end;

function TColumnUnboundThread.checkListValues(const rowIdx: Variant; const colIdx: String; var expVal: Variant)
  : Boolean;
var
  I: Integer;
  item, previtem: TConditionUnbound;
begin
  if threadColumns[colIdx].conditions.Count = 0 then
  begin
    Result := false;
    exit;
  end;
  Result := checkValue(rowIdx, colIdx, threadColumns[colIdx].conditions[0], expVal);
  for I := 1 to threadColumns[colIdx].conditions.Count - 1 do
  begin
    previtem := threadColumns[colIdx].conditions[I - 1];
    item := threadColumns[colIdx].conditions[I];
    if previtem.operand = opNone then
      break
    else if previtem.operand = opAnd_Op then
      Result := (Result AND checkValue(rowIdx, colIdx, item, expVal))
    else
    begin
      if Result then
        break;
      Result := (Result OR checkValue(rowIdx, colIdx, item, expVal))
    end;
  end;
end;

function TColumnUnboundThread.checkValue(const rowIdx: Variant; const colIdx: String; item: TConditionUnbound;
  var expVal: Variant): Boolean;
var
  idx: Integer;
  columnValue, otherColumnValue: Variant;
begin
  Result := false;
  idx := indexOfArray(conditions, item.condition);
  if item.mode = cmColumns then
  begin
    if threadValues[rowIdx].ContainsKey(item.columnName1) and threadValues[rowIdx].ContainsKey(item.columnName2) then
    begin
      columnValue := threadValues[rowIdx][item.columnName1];
      otherColumnValue := threadValues[rowIdx][item.columnName2];
      if idx = 0 then
        Result := columnValue = (otherColumnValue + item.Value)
      else if idx = 1 then
        Result := columnValue <> (otherColumnValue + item.Value)
      else if idx = 2 then
        Result := columnValue < (otherColumnValue + item.Value)
      else if idx = 3 then
        Result := columnValue <= (otherColumnValue + item.Value)
      else if idx = 4 then
        Result := columnValue > (otherColumnValue + item.Value)
      else if idx = 5 then
        Result := columnValue >= (otherColumnValue + item.Value)
      else
        Result := containsString(columnValue, otherColumnValue + item.Value);
    end
  end
  else if item.mode = cmOtherColumn then
  begin
    if threadValues[rowIdx].ContainsKey(item.columnName1) then
    begin
      columnValue := threadValues[rowIdx][item.columnName1];
      if idx = 0 then
        Result := columnValue = item.Value
      else if idx = 1 then
        Result := columnValue <> item.Value
      else if idx = 2 then
        Result := columnValue < item.Value
      else if idx = 3 then
        Result := columnValue <= item.Value
      else if idx = 4 then
        Result := columnValue > item.Value
      else if idx = 5 then
        Result := columnValue >= item.Value
      else
        Result := containsString(columnValue, item.Value);
    end
  end;

  if Result and isValidExp(item.expression, rowIdx, colIdx) then
    expVal := parse(item.expression, rowIdx, colIdx);
end;

end.
