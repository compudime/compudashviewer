unit ColumnHelperDataUnit;

interface

uses System.Generics.Collections, System.Variants, Classes, cxGridTableView, cxGridCustomView, cxGridCustomTableView,cxTextEdit,
  ColumnUnboundHelperUnit, cxEdit;

type
  TColumnUnboundHelperData = class
  private
    databaseConnection: TComponent;

    function checkListValues(viewInfo: TcxGridTableDataCellViewInfo; unboundColumn: TUnboundColumn;
      var condition: TConditionUnbound): Boolean;
    function checkValue(viewInfo: TcxGridTableDataCellViewInfo; item: TConditionUnbound;
      var condition: TConditionUnbound): Boolean;
  public
    function findGridViewColumn(view: TcxCustomGridTableView; name: String;onlyVisible: Boolean = true): TcxGridColumn;

    function prepareSQLText(expression: String; fieldValues: TDictionary<String, Variant>;
      var params: TDictionary<String, Variant>): String; Overload;
    function prepareSQLText(expression: String; viewInfo: TcxGridTableDataCellViewInfo; updateField: String;
      updateValue: Variant; var params: TDictionary<String, Variant>): String; Overload;
    function prepareSQLUpdateText(selectstatement: String; updateField: String; updateValue: Variant;
      viewInfo: TcxGridTableDataCellViewInfo; var params: TDictionary<String, Variant>): String; Overload;
    function prepareSQLUpdateText(changedItem: TcxCustomGridTableItem; unboundColumn: TUnboundColumn;
      var params: TDictionary<String, Variant>; var updateField: String): String; Overload;
    function prepareSQLUpdateText(changedEdit: TcxCustomEdit; unboundColumn: TUnboundColumn;
      var params: TDictionary<String, Variant>; var updateField: String): String; Overload;
    function prepareSQLUpdateText(changedItem: TcxCustomGridTableItem; expression: String;
      unboundColumn: TUnboundColumn; var params: TDictionary<String, Variant>; var updateField: String)
      : String; Overload;
    function prepareWSText(expression: String; fieldValues: TDictionary<String, Variant>): String; Overload;
    function prepareWSText(expression: String; viewInfo: TcxGridTableDataCellViewInfo): String; Overload;
    function prepareWSPostText(changedItem: TcxCustomGridTableItem; unboundColumn: TUnboundColumn;
      var updateField: String): String; Overload;
    function prepareWSPostText(changedEdit: TcxCustomEdit; unboundColumn: TUnboundColumn;
      var updateField: String): String; Overload;
    function prepareWSPostText(changedItem: TcxCustomGridTableItem; expression: String; unboundColumn: TUnboundColumn;
      var updateField: String): String; Overload;
    function runSQLRequest(sqlquery: String; retrieveField: String; sqlParams: TDictionary<String, Variant>;
      var sqlFields: TList<String>; var exceptStr: String): Variant; Overload;
    procedure runSQLRequest(sqlquery: String; sqlParams: TDictionary<String, Variant>; var exceptStr: String); Overload;
    function runWSRequest(wsurl: String; retrieveField: String; var wsFields: TList<String>; var exceptStr: String)
      : Variant; Overload;
    procedure runWSRequest(wsurl: String; postedData: String; var exceptStr: String); Overload;

    procedure assignConnection(connection: TComponent = nil);

  end;

function ColumnUnboundHelperData: TColumnUnboundHelperData;

implementation

//{$UNDEF	NOADSGRIDS}

uses {$IFNDEF NOADSGRIDS}adstable, adscnnct, adsdata, adsfunc, ADSSET {$ELSE}Data.Win.ADODB{$ENDIF}, DB,
  IdHTTP, Xml.XMLIntf, Xml.XMLDoc, System.JSON, Vcl.Forms, System.SysUtils, ColumnHelperTypesUnit;

var
  _columnStyleHelperData: TColumnUnboundHelperData;

function ColumnUnboundHelperData: TColumnUnboundHelperData;
begin
  if not Assigned(_columnStyleHelperData) then
    _columnStyleHelperData := TColumnUnboundHelperData.Create;
  Result := _columnStyleHelperData;
end;

procedure TColumnUnboundHelperData.assignConnection(connection: TComponent);
var
  dbfFolder: String;
begin
  if Assigned(connection) then
  begin

    databaseConnection := connection
  end
  else
  begin
    dbfFolder := ExtractFilePath(Application.ExeName);
{$IFNDEF NOADSGRIDS}
    databaseConnection := TAdsConnection.Create(nil);
    TAdsConnection(databaseConnection).AdsServerTypes := [stADS_LOCAL];
    TAdsConnection(databaseConnection).ConnectPath := dbfFolder;
{$ELSE}
    TADOConnection(databaseConnection) := TADOConnection.Create(nil);
    TADOConnection(databaseConnection).ConnectionString :=
      Format('Provider=Microsoft.JET.OLEDB.4.0;Data Source=%s;Extended Properties=dBase IV;', [dbfFolder]);
{$ENDIF}
{$IFNDEF NOADSGRIDS}TAdsConnection(databaseConnection){$ELSE}TADOConnection(databaseConnection){$ENDIF}.LoginPrompt := false;
{$IFNDEF NOADSGRIDS}TAdsConnection(databaseConnection).IsConnected{$ELSE}TADOConnection(databaseConnection).Connected{$ENDIF} := True;
  end;
end;

function TColumnUnboundHelperData.prepareSQLText(expression: String; fieldValues: TDictionary<String, Variant>;
  var params: TDictionary<String, Variant>): String;
var
  I: Integer;
  item, name: String;
  isFieldParse: Boolean;
begin
  I := 0;
  Result := String.Empty;
  name := String.Empty;
  isFieldParse := false;
  while I < expression.Length do
  begin
    item := expression.Substring(I, 1);
    if item = '[' then
    begin
      isFieldParse := True;
    end
    else if item = ']' then
    begin
      isFieldParse := false;
      if fieldValues.ContainsKey(name) then
      begin
        Result := Result + Format(':%s', [name]);
        if VarIsNull(fieldValues[name]) then
        begin
          Result := String.Empty;
          exit;
        end;
        params.Add(name, fieldValues[name]);
      end
      else
      begin
        Result := Result + Format('[%s]', [name]);
      end;
      name := String.Empty;
    end
    else
    begin
      if not isFieldParse then
        Result := Result + item
      else
        name := name + item;
    end;
    Inc(I);
  end;
end;

function TColumnUnboundHelperData.prepareSQLText(expression: String; viewInfo: TcxGridTableDataCellViewInfo;
  updateField: String; updateValue: Variant; var params: TDictionary<String, Variant>): String;
var
  fieldValues: TDictionary<String, Variant>;
  I: Integer;
  column: TcxGridColumn;
  fieldName: String;
begin
  fieldValues := TDictionary<String, Variant>.Create;
  for I := 0 to viewInfo.GridRecord.ValueCount - 1 do
  begin
    column := viewInfo.GridView.Items[I] as TcxGridColumn;
    if column.Visible then
    begin
      fieldName := getFieldName(column);
      if fieldName.IsEmpty then
        fieldName := column.name;
      fieldValues.Add(fieldName, viewInfo.GridRecord.Values[I]);
    end;
  end;
  fieldValues.Add(updateField, updateValue);
  Result := prepareSQLText(expression, fieldValues, params);
  fieldValues.Free;
end;

function TColumnUnboundHelperData.checkListValues(viewInfo: TcxGridTableDataCellViewInfo; unboundColumn: TUnboundColumn;
  var condition: TConditionUnbound): Boolean;
var
  I: Integer;
  item, previtem: TConditionUnbound;
begin
  Result := false;
  if unboundColumn.conditions.Count = 0 then
    exit;
  Result := checkValue(viewInfo, unboundColumn.conditions[0], condition);
  for I := 1 to unboundColumn.conditions.Count - 1 do
  begin
    previtem := unboundColumn.conditions[I - 1];
    item := unboundColumn.conditions[I];
    if previtem.operand = opNone then
      break
    else if previtem.operand = opAnd_Op then
      Result := (Result AND checkValue(viewInfo, item, condition))
    else
    begin
      if Result then
        break;
      Result := (Result OR checkValue(viewInfo, item, condition))
    end;
  end;
end;

function TColumnUnboundHelperData.findGridViewColumn(view: TcxCustomGridTableView; name: String;onlyVisible: Boolean)
  : TcxGridColumn;
var
  I: Integer;
  fieldName: String;
  column: TcxGridColumn;
begin
  Result := view.FindItemByName(name) as TcxGridColumn;
  if Result = nil then
  begin
    for I := 0 to view.ItemCount - 1 do
    begin
      column := view.Items[I] as TcxGridColumn;
      if (not Assigned(column)) or (onlyVisible and (not column.Visible)) then
        continue;
      fieldName := getFieldName(column);
      if fieldName.ToLower() = name.ToLower() then
      begin
        Result := column;
        exit;
      end;
    end;
  end;
end;

function TColumnUnboundHelperData.checkValue(viewInfo: TcxGridTableDataCellViewInfo; item: TConditionUnbound;
  var condition: TConditionUnbound): Boolean;
var
  idx: Integer;
  columnValue, otherColumnValue: Variant;
  column1, column2: TcxGridColumn;
begin
  Result := false;
  idx := indexOfArray(conditions, item.condition);
  if item.mode = cmColumns then
  begin
    column1 := findGridViewColumn(viewInfo.GridView, item.columnName1);
    column2 := findGridViewColumn(viewInfo.GridView, item.columnName2);
    if (column1 <> nil) and (column2 <> nil) then
    begin
      columnValue := viewInfo.GridRecord.Values[column1.Index];
      otherColumnValue := viewInfo.GridRecord.Values[column2.Index];
      if idx = 0 then
        Result := columnValue = (otherColumnValue + item.value)
      else if idx = 1 then
        Result := columnValue <> (otherColumnValue + item.value)
      else if idx = 2 then
        Result := columnValue < (otherColumnValue + item.value)
      else if idx = 3 then
        Result := columnValue <= (otherColumnValue + item.value)
      else if idx = 4 then
        Result := columnValue > (otherColumnValue + item.value)
      else if idx = 5 then
        Result := columnValue >= (otherColumnValue + item.value)
      else
        Result := containsString(columnValue, otherColumnValue + item.value);
    end
  end
  else if item.mode = cmOtherColumn then
  begin

    column1 := findGridViewColumn(viewInfo.GridView, item.columnName1);
    if column1 <> nil then
    begin
      columnValue := viewInfo.GridRecord.Values[column1.Index];
      if idx = 0 then
        Result := columnValue = item.value
      else if idx = 1 then
        Result := columnValue <> item.value
      else if idx = 2 then
        Result := columnValue < item.value
      else if idx = 3 then
        Result := columnValue <= item.value
      else if idx = 4 then
        Result := columnValue > item.value
      else if idx = 5 then
        Result := columnValue >= item.value
      else
        Result := containsString(columnValue, item.value);
    end
  end;

  if Result then
    condition := item;
end;

function TColumnUnboundHelperData.prepareSQLUpdateText(selectstatement: String; updateField: String;
  updateValue: Variant; viewInfo: TcxGridTableDataCellViewInfo; var params: TDictionary<String, Variant>): String;
var
  posFrom, posWhere: Integer;
  sqlselect: String;
begin
  sqlselect := prepareSQLText(selectstatement, viewInfo, updateField, updateValue, params);
  if not sqlselect.IsEmpty then
  begin
    posFrom := ansipos('from', sqlselect.ToLower());
    sqlselect := sqlselect.Substring(posFrom - 1);
    sqlselect := StringReplace(sqlselect, 'from', 'update', [rfIgnoreCase]);
    posWhere := ansipos('where', sqlselect.ToLower());
    sqlselect := sqlselect.Insert(posWhere - 1, Format('set %s=:%s ', [updateField, updateField]));
  end;
  Result := sqlselect;
end;

function TColumnUnboundHelperData.prepareSQLUpdateText(changedItem: TcxCustomGridTableItem; expression: String;
  unboundColumn: TUnboundColumn; var params: TDictionary<String, Variant>; var updateField: String): String;
var
  sqlName: String;
  updateValue: Variant;
begin
  Result := string.Empty;
  sqlName := Copy(expression, 2, Length(expression) - 2);
  updateField := Copy(sqlName, sqlName.IndexOf('_') + 2, sqlName.Length);
  updateValue := changedItem.EditValue;
  if VarIsNull(updateValue) then
    updateValue := changedItem.FocusedCellViewInfo.DisplayValue;
  if unboundColumn.sqlUpdateStatements.ContainsKey(sqlName) and (not unboundColumn.sqlUpdateStatements[sqlName].IsEmpty)
  then
  begin
    Result := prepareSQLText(unboundColumn.sqlUpdateStatements[sqlName], changedItem.FocusedCellViewInfo, updateField,
      updateValue, params);
  end
  else if unboundColumn.sqlStatements.ContainsKey(sqlName) and (not unboundColumn.sqlStatements[sqlName].IsEmpty) then
  begin
    Result := prepareSQLUpdateText(unboundColumn.sqlStatements[sqlName], updateField, updateValue,
      changedItem.FocusedCellViewInfo, params);
  end;
end;

function TColumnUnboundHelperData.prepareSQLUpdateText(changedItem: TcxCustomGridTableItem;
  unboundColumn: TUnboundColumn; var params: TDictionary<String, Variant>; var updateField: String): String;
var
  condition: TConditionUnbound;
begin
  Result := string.Empty;
  if unboundColumn.mode = umSQLEdit then
  begin
    changedItem.GridView.DataController.Post;
    Result := prepareSQLUpdateText(changedItem, unboundColumn.expression, unboundColumn, params, updateField);
  end
  else if unboundColumn.mode = umSQLEditByCondition then
  begin
    if checkListValues(changedItem.FocusedCellViewInfo, unboundColumn, condition) then
    begin
      changedItem.GridView.DataController.Post;
      Result := prepareSQLUpdateText(changedItem, condition.expression, unboundColumn, params, updateField);
    end
    else
      changedItem.GridView.DataController.Cancel;
  end;
end;

function TColumnUnboundHelperData.prepareSQLUpdateText(changedEdit: TcxCustomEdit; unboundColumn: TUnboundColumn;
      var params: TDictionary<String, Variant>; var updateField: String): String;
var
  condition: TConditionUnbound;
  view: TcxGridTableView;
  itemName: String;
  changedItem: TcxCustomGridTableItem;
begin
  Result := string.Empty;
  view:=changedEdit.Owner as TcxGridTableView;
  itemName:= getFocusedItemName(view);
  changedItem:=view.FindItemByName(itemName);
  if not Assigned(changedItem) then
    exit;
  if unboundColumn.mode = umSQLEdit then
  begin
    ColumnUnboundHelper.postEditor(view,changedEdit);
    Result := prepareSQLUpdateText(changedItem, unboundColumn.expression, unboundColumn, params, updateField);
  end
  else if unboundColumn.mode = umSQLEditByCondition then
  begin
    if checkListValues(changedItem.FocusedCellViewInfo, unboundColumn, condition) then
    begin
      ColumnUnboundHelper.postEditor(view,changedEdit);
      Result := prepareSQLUpdateText(changedItem, condition.expression, unboundColumn, params, updateField);
    end
    else
      undoEdit(changedEdit);
  end;
end;

function TColumnUnboundHelperData.prepareWSText(expression: String; fieldValues: TDictionary<String, Variant>): String;
var
  I: Integer;
  item, name, value: String;
  isFieldParse: Boolean;
begin
  I := 0;
  Result := String.Empty;
  name := String.Empty;
  isFieldParse := false;
  while I < expression.Length do
  begin
    item := expression.Substring(I, 1);
    if item = '[' then
    begin
      isFieldParse := True;
    end
    else if item = ']' then
    begin
      isFieldParse := false;
      if fieldValues.ContainsKey(name) then
      begin
        value := VarToStr(fieldValues[name]);
        if String.IsNullOrEmpty(value) then
        begin
          Result := String.Empty;
          exit;
        end;
        Result := Result + value;
      end;
      name := String.Empty;
    end
    else
    begin
      if not isFieldParse then
        Result := Result + item
      else
        name := name + item;
    end;
    Inc(I);
  end;
end;

function TColumnUnboundHelperData.prepareWSText(expression: String; viewInfo: TcxGridTableDataCellViewInfo): String;
var
  fieldValues: TDictionary<String, Variant>;
  I: Integer;
  column: TcxGridColumn;
  fieldName: String;
begin
  fieldValues := TDictionary<String, Variant>.Create;
  for I := 0 to viewInfo.GridRecord.ValueCount - 1 do
  begin
    column := viewInfo.GridView.Items[I] as TcxGridColumn;
    if column.Visible then
    begin
      fieldName := getFieldName(column);
      if not fieldName.IsEmpty then
        fieldValues.Add(fieldName, viewInfo.GridRecord.Values[I])
      else
        fieldValues.Add(column.name, viewInfo.GridRecord.Values[I])
    end;
  end;
  Result := prepareWSText(expression, fieldValues);
  fieldValues.Free;
end;

function TColumnUnboundHelperData.prepareWSPostText(changedItem: TcxCustomGridTableItem; expression: String;
  unboundColumn: TUnboundColumn; var updateField: String): String;
var
  wsName: String;
  updateValue: Variant;
begin
  Result := string.Empty;
  wsName := Copy(expression, 2, Length(expression) - 2);
  updateField := Copy(wsName, wsName.IndexOf('_') + 2, wsName.Length);
  updateValue := changedItem.EditValue;
  if VarIsNull(updateValue) then
    updateValue := changedItem.FocusedCellViewInfo.DisplayValue;
  if unboundColumn.wsPostStatements.ContainsKey(wsName) and (not unboundColumn.wsPostStatements[wsName].IsEmpty) then
  begin
    Result := prepareWSText(unboundColumn.wsPostStatements[wsName], changedItem.FocusedCellViewInfo);
  end
  else if unboundColumn.wsStatements.ContainsKey(wsName) and (not unboundColumn.wsStatements[wsName].IsEmpty) then
  begin
    Result := prepareWSText(unboundColumn.wsStatements[wsName], changedItem.FocusedCellViewInfo);
  end;
end;

function TColumnUnboundHelperData.prepareWSPostText(changedItem: TcxCustomGridTableItem; unboundColumn: TUnboundColumn;
  var updateField: String): String;
var
  condition: TConditionUnbound;
begin
  Result := string.Empty;
  if unboundColumn.mode = umWSEdit then
  begin
    changedItem.GridView.DataController.Post;
    Result := prepareWSPostText(changedItem, unboundColumn.expression, unboundColumn, updateField);
  end
  else if unboundColumn.mode = umWSEditByCondition then
  begin
    if checkListValues(changedItem.FocusedCellViewInfo, unboundColumn, condition) then
    begin
      changedItem.GridView.DataController.Post;
      Result := prepareWSPostText(changedItem, condition.expression, unboundColumn, updateField);
    end
    else
      changedItem.GridView.DataController.Cancel;
  end;
end;

function TColumnUnboundHelperData.prepareWSPostText(changedEdit: TcxCustomEdit; unboundColumn: TUnboundColumn;
      var updateField: String): String;
var
  condition: TConditionUnbound;
  view: TcxGridTableView;
  itemName: String;
  changedItem: TcxCustomGridTableItem;
begin
  Result := string.Empty;
  view:=changedEdit.Owner as TcxGridTableView;
  itemName:= getFocusedItemName(view);
  changedItem:=view.FindItemByName(itemName);
  if not Assigned(changedItem) then
    exit;
  if unboundColumn.mode = umWSEdit then
  begin
    ColumnUnboundHelper.postEditor(view,changedEdit);
    Result := prepareWSPostText(changedItem, unboundColumn.expression, unboundColumn, updateField);
  end
  else if unboundColumn.mode = umWSEditByCondition then
  begin
    if checkListValues(changedItem.FocusedCellViewInfo, unboundColumn, condition) then
    begin
      ColumnUnboundHelper.postEditor(view,changedEdit);
      Result := prepareWSPostText(changedItem, condition.expression, unboundColumn, updateField);
    end
    else
      undoEdit(changedEdit);
  end;
end;

// Select data
function TColumnUnboundHelperData.runSQLRequest(sqlquery: String; retrieveField: String;
  sqlParams: TDictionary<String, Variant>; var sqlFields: TList<String>; var exceptStr: String): Variant;
var
{$IFNDEF NOADSGRIDS}
  AdsQuery: TAdsQuery;
{$ELSE}
  ADOQuery: TADOQuery;
{$ENDIF}
  field: TField;
  pair: TPair<String, Variant>;
  I: Integer;
begin
  Result := Null;

{$IFNDEF NOADSGRIDS}
  AdsQuery := TAdsQuery.Create(nil);
  //AdsQuery.SourceTableType := ttAdsVFP;
{$ELSE}
  ADOQuery := TADOQuery.Create(nil);
{$ENDIF}
  try
    try
{$IFNDEF NOADSGRIDS}
      AdsQuery.AdsConnection := TAdsConnection(databaseConnection);
{$ELSE}
      ADOQuery.connection := TADOConnection(databaseConnection);
{$ENDIF}
{$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.SQL.Add(sqlquery);
      for pair in sqlParams do
      begin
{$IFNDEF NOADSGRIDS}AdsQuery.params.{$ELSE}ADOQuery.Parameters.{$ENDIF}ParamByName(pair.Key).value := pair.value;
      end;
{$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.Open;
      for I := 0 to {$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.FieldCount - 1 do
        sqlFields.Add({$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.Fields[I].FullName);
      if {$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.RecordCount > 0 then
      begin
        if retrieveField.IsEmpty then
          Result := {$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.fieldValues
            [{$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.Fields[0].name]
        else
        begin
          field := {$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.FindField(retrieveField);
          if field <> nil then
            Result := field.AsVariant;
        end;
      end;
{$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.Close;
    finally
{$IFNDEF NOADSGRIDS}
      AdsQuery.Free;
{$ELSE}
      ADOQuery.Free;
{$ENDIF}
    end;
  except
    on E: Exception do
      exceptStr := E.Message;
  end;
end;

// Update data
procedure TColumnUnboundHelperData.runSQLRequest(sqlquery: String; sqlParams: TDictionary<String, Variant>;
  var exceptStr: String);
var
{$IFNDEF NOADSGRIDS}
  AdsQuery: TAdsQuery;
{$ELSE}
  ADOQuery: TADOQuery;
{$ENDIF}
  pair: TPair<String, Variant>;
begin
{$IFNDEF NOADSGRIDS}
  AdsQuery := TAdsQuery.Create(nil);
  //AdsQuery.SourceTableType := ttAdsADT;
{$ELSE}
  ADOQuery := TADOQuery.Create(nil);
{$ENDIF}
  try
    try
{$IFNDEF NOADSGRIDS}
      AdsQuery.AdsConnection := TAdsConnection(databaseConnection);
{$ELSE}
      ADOQuery.connection := TADOConnection(databaseConnection);
{$ENDIF}
{$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.SQL.Add(sqlquery);
      for pair in sqlParams do
      begin
{$IFNDEF NOADSGRIDS}AdsQuery.params.{$ELSE}ADOQuery.Parameters.{$ENDIF}ParamByName(pair.Key).value := pair.value;
      end;
{$IFNDEF NOADSGRIDS}AdsQuery{$ELSE}ADOQuery{$ENDIF}.ExecSQL;
    finally
{$IFNDEF NOADSGRIDS}
      AdsQuery.Free;
{$ELSE}
      ADOQuery.Free;
{$ENDIF}
    end;
  except
    on E: Exception do
      exceptStr := E.Message;
  end;
end;

// Get data
function TColumnUnboundHelperData.runWSRequest(wsurl: String; retrieveField: String; var wsFields: TList<String>;
  var exceptStr: String): Variant;
const
  WS_TIMEOUT = 25000;
var
  lHTTP: TIdHTTP;
  html: String;
  XMLDoc: IXmlDocument;
  mainNode: IXMLNode;
  tmpNode: IXMLNode;
  JSONObject: TJSONObject;
  JSONArray: TJSONArray;
  JSONPair: TJSONPair;
  I: Integer;
begin
  Result := Null;

  try
    lHTTP := TIdHTTP.Create(nil);
    try
      lHTTP.ConnectTimeout := WS_TIMEOUT;
      lHTTP.ReadTimeout := WS_TIMEOUT;
      html := lHTTP.Get(wsurl);
    finally
      lHTTP.Free;
    end;

    JSONObject := TJSONObject.ParseJSONValue(html) as TJSONObject;
    if Assigned(JSONObject) then
    begin
      JSONArray := JSONObject.Get('result').JsonValue as TJSONArray;
      if Assigned(JSONArray) and (JSONArray.Count > 0) then
      begin
        JSONObject := JSONArray.Items[0] as TJSONObject;
        for I := 0 to JSONObject.Count - 1 do
          wsFields.Add(JSONObject.Pairs[I].JsonString.value);
        if retrieveField.IsEmpty and (JSONObject.Count > 0) then
          Result := JSONObject.Pairs[0].JsonValue.value
        else
        begin
          JSONPair := JSONObject.Get(retrieveField);
          if Assigned(JSONPair) then
            Result := JSONPair.JsonValue.value
        end;
      end;
    end
    else
    begin
      XMLDoc := TXmlDocument.Create(nil);
      try
        XMLDoc.LoadFromXML(html);
        mainNode := XMLDoc.DocumentElement;
        for I := 0 to mainNode.ChildNodes.Count - 1 do
          wsFields.Add(mainNode.ChildNodes[I].NodeName);
        if retrieveField.IsEmpty and (mainNode.ChildNodes.Count > 0) then
          Result := mainNode.ChildNodes[0].NodeValue
        else
        begin
          tmpNode := mainNode.ChildNodes.FindNode(retrieveField, '');
          if tmpNode <> nil then
            Result := tmpNode.NodeValue;
        end;
      finally
        XMLDoc := nil;
      end;
    end;
  except
    on E: Exception do
      exceptStr := E.Message;
  end;
end;

// Post data
procedure TColumnUnboundHelperData.runWSRequest(wsurl: String; postedData: String; var exceptStr: String);
const
  WS_TIMEOUT = 25000;
var
  lHTTP: TIdHTTP;
  lRequest: TStringStream;
begin
  try
    lHTTP := TIdHTTP.Create(nil);
    lRequest := TStringStream.Create(postedData, TEncoding.UTF8);
    try
      lHTTP.ConnectTimeout := WS_TIMEOUT;
      lHTTP.ReadTimeout := WS_TIMEOUT;
      lHTTP.Post(wsurl, lRequest);
    finally
      lHTTP.Free;
      lRequest.Free;
    end;
  except
    on E: Exception do
      exceptStr := E.Message;
  end;
end;

initialization

_columnStyleHelperData := TColumnUnboundHelperData.Create;

finalization

if Assigned(_columnStyleHelperData) then
begin
  if Assigned(_columnStyleHelperData.databaseConnection) then
  begin
{$IFNDEF NOADSGRIDS}TAdsConnection(_columnStyleHelperData.databaseConnection).IsConnected{$ELSE}TADOConnection(_columnStyleHelperData.databaseConnection).Connected{$ENDIF} := false;
  end;
  FreeAndNil(_columnStyleHelperData);
end;

end.
