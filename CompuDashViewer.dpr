program CompuDashViewer;


uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {frmViewer},
  Unit2 in 'Unit2.pas' {frmActions},
  uCdMongo in 'uCdMongo.pas' {cdMongo: TDataModule},
  ufrmFilterSettings in 'ufrmFilterSettings.pas' {frmFilterSettings},
  uFilter in 'uFilter.pas',
  View.ConnectionManager in 'ConnectionManager\View.ConnectionManager.pas' {FormConnectionManager},
  DM.ConnectionManager in 'ConnectionManager\DM.ConnectionManager.pas' {DMConnectionManager: TDataModule},
  VisualOptionList in 'Components\VisualOptionList.pas',
  frmLayoutSettings in 'frmLayoutSettings.pas' {SetupForm},
  dmLayouts in 'dmLayouts.pas' {LayoutsData: TDataModule},
  Utils in 'Utils.pas',
  frGrid in 'frGrid.pas' {GridFrame: TFrame},
  Unit3 in 'Unit3.pas' {frmEditColumnValue},
  ufrmVisibilityModules in 'ufrmVisibilityModules.pas' {frmVisibilityModules},
  uNotifyCenter in 'Libs\uNotifyCenter.pas',
  CDV.Consts in 'CDV.Consts.pas',
  CDV.GridFrameArray in 'CDV.GridFrameArray.pas',
  AMR.AutoObject in 'Libs\AMR.AutoObject.pas',
  frReportGrid in 'frReportGrid.pas' {ReportGridFrame: TFrame};

{$R *.res}

begin
  Application.Initialize;
//  {$IFDEF DEBUG}
//    ReportMemoryLeaksOnShutdown := True;
//  {$ENDIF}
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmViewer, frmViewer);
  Application.CreateForm(TfrmActions, frmActions);
  Application.CreateForm(TcdMongo, cdMongo);
  Application.CreateForm(TfrmFilterSettings, frmFilterSettings);
  Application.CreateForm(TSetupForm, SetupForm);
  Application.CreateForm(TLayoutsData, LayoutsData);
  Application.CreateForm(TfrmEditColumnValue, frmEditColumnValue);
  Application.CreateForm(TfrmVisibilityModules, frmVisibilityModules);
  Application.Run;
end.
