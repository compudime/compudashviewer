unit ColumnStyleHelperUnit;

interface

uses
  System.Classes, System.Generics.Collections, System.Types, System.Variants, System.SysUtils, Vcl.Graphics,
  Vcl.Controls, cxGridCustomTableView, cxGridTableView, cxGraphics, cxGridCustomView, DB, ColumnHelperTypesUnit;

const
  itemsStyleShiftLength = 2;
  countConditionStyleItems = 7;
  defaultStyleColors: TArray<TColor> = [clRed, clWhite, clGreen, clWhite];

type
  TConditionStyle = record
    Mode: TCompareModes;
    ColumnName: string;
    ColumnCaption: string;
    Condition: string;
    Value: Variant;
    Operand: TOperands;
    Color: TConditionColors;
  end;

  TColumnStyleHelper = class
  private
    gridViews: TDictionary<string, TcxGridTableView>;
    columnStyles: TDictionary<string, TDictionary<string, TList<TConditionStyle>>>;
    isEmptyStyles: TDictionary<string, Boolean>;

    function findFieldCaption(view: TcxGridTableView; ColumnName: string): string;
    function checkValue(viewInfo: TcxGridTableDataCellViewInfo; item: TConditionStyle;
      var colorVal: TConditionColors): Boolean;
    function checkListValues(viewInfo: TcxGridTableDataCellViewInfo; items: TList<TConditionStyle>;
      var colorVal: TConditionColors): Boolean;
    function checkValues(viewInfo: TcxGridTableDataCellViewInfo; var colorVal: TConditionColors): Boolean;
    procedure closeDialogs();
    function isFoundStyles(view: TcxGridTableView): Boolean;
    function listToConditionStyles(view: TcxGridTableView; conditionList: TStringList): TList<TConditionStyle>;
  protected
    frmColumnStyleList: TDictionary<string, TWinControl>;

    procedure setFormBounds(viewInfo: TcxGridTableDataCellViewInfo); Virtual;
  public
    OnCompleted: TWorkCompetedEvent;
    listConditions: TStringList;

    function conditionStylesTostring(view: TcxGridTableView; ColumnName: string): string;
    procedure addOrSetFieldStyle(view: TcxGridTableView; fieldName: string; fieldValue: Variant);
    function isStyledColumn(view: TcxGridTableView; ColumnName: string): Boolean;
    procedure clearCondition(view: TcxGridTableView; ColumnName: string);
    procedure updateStyle(view: TcxGridTableView; ColumnName: string; styleList: TStringList);
    function getConditionsByColumn(view: TcxGridTableView; ColumnName: string): TList<TConditionStyle>;
    procedure removeDialog(view: TcxCustomGridTableView);

    procedure openDialog(ACellViewInfo: TcxGridTableDataCellViewInfo);
    procedure closeDialog(ACellViewInfo: TcxGridTableDataCellViewInfo);

    function TrySetColumnStyles(ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo): Boolean;

    procedure addTableView(gridView: TcxGridTableView);
    function HasStyleColumns(gridView: TcxGridTableView): Boolean;
  end;

function ColumnStyleHelper: TColumnStyleHelper;

implementation

uses frmColumnStyleUnit, ColumnUnboundHelperUnit, System.TypInfo;

var
  _columnStyleHelper: TColumnStyleHelper;

function ColumnStyleHelper: TColumnStyleHelper;
begin
  if not Assigned(_columnStyleHelper) then
    _columnStyleHelper := TColumnStyleHelper.Create;
  Result := _columnStyleHelper;
end;

// Add new grid view
procedure TColumnStyleHelper.addTableView(gridView: TcxGridTableView);
var
  gridKey: string;
begin
  gridKey := gridView.Owner.Name + gridView.Name;
  self.gridViews.AddOrSetValue(gridView.Owner.Name + gridView.Name, gridView);
  self.columnStyles.AddOrSetValue(gridKey, TDictionary < string, TList < TConditionStyle >>.Create);
  self.isEmptyStyles.AddOrSetValue(gridView.Owner.Name + gridView.Name, True);
end;

{ Open form of dialog*********************************************************** }
procedure TColumnStyleHelper.openDialog(ACellViewInfo: TcxGridTableDataCellViewInfo);
var
  vtype: TDataTypes;
  frmColumnStyle: TfrmColumnStyle;
  Sender: TcxCustomGridTableView;
  gridKey: string;
  column: TcxGridColumn;
  cellValue: Variant;
begin
  cellValue := ACellViewInfo.Value;
  if VarIsNull(cellValue) then
    cellValue := ACellViewInfo.DisplayValue;
  if checkTypeByValue(cellValue) then
  begin
    column := ACellViewInfo.item as TcxGridColumn;
    Sender := ACellViewInfo.gridView;
    gridKey := Sender.Owner.Name + Sender.Name;
    frmColumnStyle := nil;
    if frmColumnStyleList.ContainsKey(gridKey) then
      frmColumnStyle := frmColumnStyleList[gridKey] as TfrmColumnStyle;

    vtype := valueType(ACellViewInfo.Value, ACellViewInfo.gridView.items[column.Index].DataBinding.valueType);

    if frmColumnStyle <> nil then
      frmColumnStyle.Close;
    frmColumnStyle := TfrmColumnStyle.Create(Sender.Owner);
    if not frmColumnStyleList.ContainsKey(gridKey) then
      frmColumnStyleList.Add(gridKey, frmColumnStyle);
    frmColumnStyle.cellValue := cellValue;
    frmColumnStyle.ColumnName := column.Name;
    frmColumnStyle.columnType := vtype;
    frmColumnStyle.gridView := gridViews[gridKey];
    frmColumnStyle.initFields(ACellViewInfo);

    setFormBounds(ACellViewInfo);

    frmColumnStyle.lblColumn.Caption := frmColumnStyle.allFieldCapts[frmColumnStyle.ColumnName];
    frmColumnStyle.Show;
  end;
end;

// close dialog
procedure TColumnStyleHelper.closeDialog(ACellViewInfo: TcxGridTableDataCellViewInfo);
var
  frmColumnStyle: TfrmColumnStyle;
  Sender: TcxCustomGridTableView;
begin
  if not IsPublishedProp(ACellViewInfo, 'GridView') then
    Exit;

  Sender := ACellViewInfo.gridView;
  frmColumnStyle := nil;
  if frmColumnStyleList.ContainsKey(Sender.Owner.Name + Sender.Name) then
    frmColumnStyle := frmColumnStyleList[Sender.Owner.Name + Sender.Name] as TfrmColumnStyle;
  if frmColumnStyle <> nil then
  begin
    frmColumnStyle.Close;
    removeDialog(Sender);
  end;
end;

// Set bounds form of dialog
procedure TColumnStyleHelper.setFormBounds(viewInfo: TcxGridTableDataCellViewInfo);
var
  ownner, dialog: TWinControl;
  fpoint: TPoint;
  bolder, header: Integer;
begin
  ownner := viewInfo.gridView.Owner as TWinControl;
  dialog := frmColumnStyleList[ownner.Name + viewInfo.gridView.Name];

  fpoint := Point(viewInfo.Bounds.Right, viewInfo.Bounds.Top);
  fpoint := ownner.ClientToScreen(fpoint);
  bolder := (dialog.Width - dialog.ClientWidth) div 2;
  header := dialog.Height - dialog.ClientHeight - bolder;
  dialog.Left := fpoint.X + 2;
  dialog.Top := fpoint.Y + header - 2;
  dialog.Height := dialog.Height - 70;
end;

procedure TColumnStyleHelper.removeDialog(view: TcxCustomGridTableView);
begin
  if (view.Owner <> nil) and frmColumnStyleList.ContainsKey(view.Owner.Name + view.Name) then
    frmColumnStyleList.Remove(view.Owner.Name + view.Name);
end;
{ ****************************************************************************** }

{ Draw styles******************************************************************* }
function TColumnStyleHelper.TrySetColumnStyles(ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
var
  colorValue: TConditionColors;
  Sender: TcxCustomGridTableView;
  gridKey: string;
  cellValue: Variant;
begin
  Sender := AViewInfo.gridView;
  gridKey := Sender.Owner.Name + Sender.Name;
  cellValue := AViewInfo.Value;
  if VarIsNull(cellValue) then
    cellValue := AViewInfo.DisplayValue;
  if isEmptyStyles.ContainsKey(gridKey) and (not isEmptyStyles[gridKey]) and checkTypeByValue(cellValue) then
  begin
    colorValue.Color := defaultStyleColors[0];
    colorValue.textColor := defaultStyleColors[1];
    if checkValues(AViewInfo, colorValue) then
    begin
      ACanvas.Brush.Color := colorValue.Color;
      ACanvas.Font.Color := colorValue.textColor;
      Result := False;
    end
    else
      Result := True;
  end
  else
    Result := True;
end;

function TColumnStyleHelper.checkValues(viewInfo: TcxGridTableDataCellViewInfo; var colorVal: TConditionColors)
  : Boolean;
var
  column: TcxGridColumn;
  view: TcxGridTableView;
  colName: string;
begin
  column := viewInfo.item as TcxGridColumn;
  view := viewInfo.gridView as TcxGridTableView;
  colName := getFieldName(column);
  if colName.IsEmpty then
    colName := column.Name;
  if not isStyledColumn(view, colName) then
    Result := False
  else
    Result := checkListValues(viewInfo, getConditionsByColumn(view, colName), colorVal);
end;

function TColumnStyleHelper.checkListValues(viewInfo: TcxGridTableDataCellViewInfo; items: TList<TConditionStyle>;
  var colorVal: TConditionColors): Boolean;
var
  I: Integer;
  item, previtem: TConditionStyle;
begin
  Result := False;
  if items.Count = 0 then
    Exit;
  Result := checkValue(viewInfo, items[0], colorVal);
  for I := 1 to items.Count - 1 do
  begin
    previtem := items[I - 1];
    item := items[I];
    if previtem.Operand = opNone then
      Break
    else if previtem.Operand = opAnd_Op then
      Result := (Result and checkValue(viewInfo, item, colorVal))
    else
    begin
      if Result then
        Break;
      Result := (Result OR checkValue(viewInfo, item, colorVal))
    end;
  end;
end;

function TColumnStyleHelper.checkValue(viewInfo: TcxGridTableDataCellViewInfo; item: TConditionStyle;
  var colorVal: TConditionColors): Boolean;
var
  idx, I, fromLastUpdate: Integer;
  otherColumnValue: Variant;
  column: TcxGridColumn;
  colName, DisplayValue: string;
  cellValue: Variant;
begin
  Result := False;
  if VarIsNull(viewInfo.Value) and (viewInfo.DisplayValue = string.Empty) then
    Exit;
  cellValue := viewInfo.Value;
  if VarIsNull(cellValue) then
    cellValue := viewInfo.DisplayValue;
  idx := listConditions.IndexOf(item.Condition);
  if item.Mode = cmValue then
  begin
    if idx = 0 then
      Result := cellValue = item.Value
    else if idx = 1 then
      Result := cellValue <> item.Value
    else if idx = 2 then
      Result := cellValue < item.Value
    else if idx = 3 then
      Result := cellValue <= item.Value
    else if idx = 4 then
      Result := cellValue > item.Value
    else if idx = 5 then
      Result := cellValue >= item.Value
    else
      Result := containsstring(cellValue, item.Value);
  end
  else if item.Mode = cmColumns then
  begin
    for I := 0 to viewInfo.gridView.ItemCount - 1 do
    begin
      column := viewInfo.gridView.items[I] as TcxGridColumn;
      colName := getFieldName(column);
      if colName.IsEmpty then
        colName := column.Name;
      if colName = item.ColumnName then
      begin
        otherColumnValue := viewInfo.GridRecord.Values[I];
        DisplayValue := viewInfo.GridRecord.DisplayTexts[I];
        if VarIsNull(otherColumnValue) and (not DisplayValue.IsEmpty) then
          getStrType(DisplayValue, otherColumnValue);
        { if idx = 0 then
          Result := cellValue = (otherColumnValue + item.Value)
          else if idx = 1 then
          Result := cellValue <> (otherColumnValue + item.Value)
          else if idx = 2 then
          Result := cellValue < (otherColumnValue + item.Value)
          else if idx = 3 then
          Result := cellValue <= (otherColumnValue + item.Value)
          else if idx = 4 then
          Result := cellValue > (otherColumnValue + item.Value)
          else if idx = 5 then
          Result := cellValue >= (otherColumnValue + item.Value)
          else
          Result := containsstring(cellValue, otherColumnValue + item.Value); }
        // --Sol 2/15/17 refering here to item.value seems buggy, we should compare 2 columns
        if idx = 0 then
          Result := cellValue = otherColumnValue
        else if idx = 1 then
          Result := cellValue <> otherColumnValue
        else if idx = 2 then
          Result := cellValue < otherColumnValue
        else if idx = 3 then
          Result := cellValue <= otherColumnValue
        else if idx = 4 then
          Result := cellValue > otherColumnValue
        else if idx = 5 then
          Result := cellValue >= otherColumnValue
        else
          Result := containsstring(cellValue, otherColumnValue);
        Break;
      end;
    end;
  end
  else if item.Mode = cmOtherColumn then
  begin
    for I := 0 to viewInfo.gridView.ItemCount - 1 do
    begin
      column := viewInfo.gridView.items[I] as TcxGridColumn;
      colName := getFieldName(column);
      if colName.IsEmpty then
        colName := column.Name;
      if colName = item.ColumnName then
      begin
        otherColumnValue := viewInfo.GridRecord.Values[I];
        DisplayValue := viewInfo.GridRecord.DisplayTexts[I];
        if VarIsNull(otherColumnValue) and (not DisplayValue.IsEmpty) then
          getStrType(DisplayValue, otherColumnValue);
        if idx = 0 then
          Result := otherColumnValue = item.Value
        else if idx = 1 then
          Result := otherColumnValue <> item.Value
        else if idx = 2 then
          Result := otherColumnValue < item.Value
        else if idx = 3 then
          Result := otherColumnValue <= item.Value
        else if idx = 4 then
          Result := otherColumnValue > item.Value
        else if idx = 5 then
          Result := otherColumnValue >= item.Value
        else
          Result := containsstring(otherColumnValue, item.Value);
        Break;
      end;
    end;
  end
  else if item.Mode = cmUpdateTime then
  begin
    fromLastUpdate := 0;
    if ColumnUnboundHelper.getLastUpdate(viewInfo.gridView as TcxGridTableView, viewInfo.GridRecord, fromLastUpdate)
    then
    begin
      if idx = 0 then
        Result := fromLastUpdate = item.Value
      else if idx = 1 then
        Result := fromLastUpdate <> item.Value
      else if idx = 2 then
        Result := fromLastUpdate < item.Value
      else if idx = 3 then
        Result := fromLastUpdate <= item.Value
      else if idx = 4 then
        Result := fromLastUpdate > item.Value
      else if idx = 5 then
        Result := fromLastUpdate >= item.Value
    end;
  end;

  if Result then
    colorVal := item.Color;
end;

{ ****************************************************************************** }

procedure TColumnStyleHelper.addOrSetFieldStyle(view: TcxGridTableView; fieldName: string; fieldValue: Variant);
var
  gridKey: string;
  memo: TStringList;
  pair: TPair<string, TList<TConditionStyle>>;
begin
  gridKey := view.Owner.Name + view.Name;
  if columnStyles.ContainsKey(gridKey) and (not VarIsNull(fieldValue)) and (not fieldName.IsEmpty) then
  begin
    for pair in columnStyles[gridKey] do
    begin
      memo := TStringList.Create;
      memo.Text := conditionStylesTostring(view, pair.Key);
      columnStyles[gridKey][pair.Key] := listToConditionStyles(view, memo);
      memo.Free;
    end;
    memo := TStringList.Create;
    memo.Text := fieldValue;
    if not string.isNullOrEmpty(memo.Text) then
    begin
      columnStyles[gridKey].AddOrSetValue(fieldName, listToConditionStyles(view, memo));
      isEmptyStyles[gridKey] := False;
    end;
    memo.Free;
  end;
end;

// Update column style
procedure TColumnStyleHelper.updateStyle(view: TcxGridTableView; ColumnName: string; styleList: TStringList);
var
  gridKey: string;
begin
  gridKey := view.Owner.Name + view.Name;
  if columnStyles.ContainsKey(gridKey) then
  begin
    columnStyles[gridKey].AddOrSetValue(ColumnName, listToConditionStyles(view, styleList));
    gridViews[gridKey].Painter.Invalidate;
    isEmptyStyles[gridKey] := False;
  end;
end;

// Clear column condition
procedure TColumnStyleHelper.clearCondition(view: TcxGridTableView; ColumnName: string);
var
  gridKey: string;
begin
  gridKey := view.Owner.Name + view.Name;
  if columnStyles.ContainsKey(gridKey) and columnStyles[gridKey].ContainsKey(ColumnName) then
  begin
    columnStyles[gridKey].Remove(ColumnName);
    gridViews[gridKey].Painter.Invalidate;
    isEmptyStyles[gridKey] := not isFoundStyles(gridViews[gridKey]);
  end;
end;

// Get column conditions
function TColumnStyleHelper.getConditionsByColumn(view: TcxGridTableView; ColumnName: string): TList<TConditionStyle>;
var
  gridKey: string;
begin
  Result := TList<TConditionStyle>.Create;
  gridKey := view.Owner.Name + view.Name;
  if columnStyles.ContainsKey(gridKey) and columnStyles[gridKey].ContainsKey(ColumnName) then
    Result := columnStyles[gridKey][ColumnName]
end;

function TColumnStyleHelper.HasStyleColumns(gridView: TcxGridTableView): Boolean;
var
  gridKey: string;
begin
  Result := False;
  gridKey := gridView.Owner.Name + gridView.Name;
  if isEmptyStyles.ContainsKey(gridKey) then
    Result := not isEmptyStyles[gridKey];
end;

// Find column caption
function TColumnStyleHelper.findFieldCaption(view: TcxGridTableView; ColumnName: string): string;
var
  colName: string;
  column: TcxGridColumn;
  I: Integer;
begin
  Result := string.Empty;
  for I := 0 to view.ColumnCount - 1 do
  begin
    column := view.Columns[I];
    colName := getFieldName(column);
    if colName.IsEmpty then
      colName := column.Name;
    if colName = ColumnName then
    begin
      Result := column.Caption;
      Break;
    end;
  end;
end;

function TColumnStyleHelper.listToConditionStyles(view: TcxGridTableView; conditionList: TStringList)
  : TList<TConditionStyle>;
var
  I: Integer;
  item: TConditionStyle;
  id, strval: string;
  varValue: Variant;
begin
  Result := TList<TConditionStyle>.Create;
  for I := 0 to (conditionList.Count div countConditionStyleItems) - 1 do
  try
    id := IntToStr(I + 1);
    item.Mode := TCompareModes(StrToInt(conditionList.Values['mode' + id]));
    item.ColumnName := conditionList.Values['column' + id];
    if not item.ColumnName.IsEmpty then
      item.ColumnCaption := findFieldCaption(view, item.ColumnName);

    item.Condition := conditionList.Values['condition' + id];

    strval := conditionList.Values['value' + id];
    getStrType(strval, varValue);
    item.Value := varValue;

    item.Operand := TOperands(StrToInt(conditionList.Values['op' + id]));

    item.Color.Color := StrToInt(conditionList.Values['color' + id]);
    item.Color.textColor := StrToInt(conditionList.Values['textcolor' + id]);

    Result.Add(item);
  except
    //maybe log somewhere
  end;
end;

function TColumnStyleHelper.conditionStylesTostring(view: TcxGridTableView; ColumnName: string): string;
var
  conditionStyles: TList<TConditionStyle>;
  I: Integer;
  idx: string;
  styleList: TStringList;
  Condition: TConditionStyle;
begin
  conditionStyles := getConditionsByColumn(view, ColumnName);
  Result := string.Empty;
  if conditionStyles.Count > 0 then
  begin
    styleList := TStringList.Create;
    for I := 0 to conditionStyles.Count - 1 do
    begin
      Condition := conditionStyles[I];
      idx := IntToStr(I + 1);
      styleList.Add('mode' + idx + '=' + IntToStr(Ord(Condition.Mode)));
      styleList.Add('column' + idx + '=' + Condition.ColumnName);
      styleList.Add('condition' + idx + '=' + Condition.Condition);
      case valueType(Condition.Value) of
        dtInt:
          styleList.Add('value' + idx + '=' + IntToStr(Condition.Value));
        dtFloat .. dtCurrency:
          styleList.Add('value' + idx + '=' + FloatToStr(Condition.Value));
        dtDate:
          styleList.Add('value' + idx + '=' + DateTimeToStr(Condition.Value));
        dtstring:
          styleList.Add('value' + idx + '=' + Condition.Value);
      else
        styleList.Add('value' + idx + '=');
      end;
      styleList.Add('op' + idx + '=' + IntToStr(Ord(Condition.Operand)));
      styleList.Add('color' + idx + '=' + IntToStr(Condition.Color.Color));
      styleList.Add('textcolor' + idx + '=' + IntToStr(Condition.Color.textColor));
    end;
    Result := styleList.Text;
    styleList.Free;
  end;
end;

function TColumnStyleHelper.isStyledColumn(view: TcxGridTableView; ColumnName: string): Boolean;
var
  gridKey: string;
begin
  Result := False;
  gridKey := view.Owner.Name + view.Name;
  if columnStyles.ContainsKey(gridKey) then
    Result := columnStyles[gridKey].ContainsKey(ColumnName);
end;

function TColumnStyleHelper.isFoundStyles(view: TcxGridTableView): Boolean;
var
  I: Integer;
  column: TcxGridColumn;
  colName, gridKey: string;
begin
  Result := False;
  gridKey := view.Owner.Name + view.Name;
  for I := 0 to view.ColumnCount - 1 do
  begin
    column := view.Columns[I];
    colName := getFieldName(column);
    if colName.IsEmpty then
      colName := column.Name;
    if columnStyles.ContainsKey(gridKey) and columnStyles[gridKey].ContainsKey(colName) then
    begin
      Result := True;
      Break;
    end;
  end;
end;

procedure TColumnStyleHelper.closeDialogs();
var
  pair: TPair<string, TWinControl>;
  form: TfrmColumnStyle;
begin
  for pair in frmColumnStyleList do
  begin
    form := pair.Value as TfrmColumnStyle;
    if form <> nil then
      form.Close;
  end;
end;

initialization

_columnStyleHelper := TColumnStyleHelper.Create;
_columnStyleHelper.listConditions := TStringList.Create;
_columnStyleHelper.listConditions.Addstrings(conditions);
_columnStyleHelper.gridViews := TDictionary<string, TcxGridTableView>.Create;
_columnStyleHelper.columnStyles := TDictionary < string, TDictionary < string, TList<TConditionStyle> >>.Create;
_columnStyleHelper.frmColumnStyleList := TDictionary<string, TWinControl>.Create;
_columnStyleHelper.isEmptyStyles := TDictionary<string, Boolean>.Create;

finalization

if Assigned(_columnStyleHelper) then
begin
  FreeAndNil(_columnStyleHelper.listConditions);
  FreeAndNil(_columnStyleHelper.gridViews);
  FreeAndNil(_columnStyleHelper.columnStyles);
  _columnStyleHelper.closeDialogs();
  FreeAndNil(_columnStyleHelper.frmColumnStyleList);
  FreeAndNil(_columnStyleHelper.isEmptyStyles);
  FreeAndNil(_columnStyleHelper);
end;

end.
