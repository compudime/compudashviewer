unit frmLayoutSettings;

interface

uses
  Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls,
  Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Menus, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, cxGroupBox, dxLayoutContainer, cxClasses,
  dxLayoutControl, dxLayoutcxEditAdapters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, dxDateRanges,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxGridCustomTableView,
  cxDBData, cxTextEdit, cxMemo, cxGridLevel, cxGridCustomView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxLayoutLookAndFeels, dxLayoutControlAdapters,
  cxButtons, dxScrollbarAnnotations, cxDBEdit, TagEditor, dxTokenEdit, cxDBLookupComboBox, FireDAC.Comp.Client,
  dxUIAClasses, dxSkinWXI;

type
  TSetupForm = class(TForm)
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutGroup1: TdxLayoutGroup;
    dxLayoutSplitterItem1: TdxLayoutSplitterItem;
    tvLayouts: TcxGridDBTableView;
    gdLayoutsLevel1: TcxGridLevel;
    gdLayouts: TcxGrid;
    dxLayoutItem3: TdxLayoutItem;
    edtLayoutTag: TDBTagEditor;
    dxLayoutItem1: TdxLayoutItem;
    dxLayoutLabeledItem1: TdxLayoutLabeledItem;
    tvLayoutsName: TcxGridDBColumn;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutGroup2: TdxLayoutGroup;
    dxLayoutGroup3: TdxLayoutGroup;
    dxLayoutSplitterItem2: TdxLayoutSplitterItem;
    dxLayoutSplitterItem3: TdxLayoutSplitterItem;
    tvComps: TcxGridDBTableView;
    gdCompsLevel1: TcxGridLevel;
    gdComps: TcxGrid;
    dxLayoutItem2: TdxLayoutItem;
    tvUsers: TcxGridDBTableView;
    gdUsersLevel1: TcxGridLevel;
    gdUsers: TcxGrid;
    dxLayoutItem4: TdxLayoutItem;
    edtCompTag: TDBTagEditor;
    dxLayoutItem5: TdxLayoutItem;
    edtUserTag: TDBTagEditor;
    dxLayoutItem6: TdxLayoutItem;
    dxLayoutSplitterItem4: TdxLayoutSplitterItem;
    dxLayoutSplitterItem5: TdxLayoutSplitterItem;
    dxLayoutLabeledItem2: TdxLayoutLabeledItem;
    dxLayoutLabeledItem3: TdxLayoutLabeledItem;
    tvCompsName: TcxGridDBColumn;
    tvCompsLayout: TcxGridDBColumn;
    tvUsersName: TcxGridDBColumn;
    tvUsersLayout: TcxGridDBColumn;
    dxLayoutGroup4: TdxLayoutGroup;
    dxLayoutGroup5: TdxLayoutGroup;
    btnSave: TcxButton;
    dxLayoutItem7: TdxLayoutItem;
    btnCancel: TcxButton;
    dxLayoutItem8: TdxLayoutItem;
    btnAddComp: TcxButton;
    dxLayoutItem9: TdxLayoutItem;
    btnDelComp: TcxButton;
    dxLayoutItem11: TdxLayoutItem;
    dxLayoutGroup7: TdxLayoutGroup;
    dxLayoutGroup6: TdxLayoutGroup;
    btnAddUser: TcxButton;
    dxLayoutItem12: TdxLayoutItem;
    btnDelUser: TcxButton;
    dxLayoutItem14: TdxLayoutItem;
    dxLayoutGroup8: TdxLayoutGroup;
    btnDelLayout: TcxButton;
    dxLayoutItem16: TdxLayoutItem;
    dxLayoutGroup9: TdxLayoutGroup;
    dxLayoutGroup10: TdxLayoutGroup;
    dxLayoutGroup11: TdxLayoutGroup;
    dxLayoutGroup12: TdxLayoutGroup;
    dxLayoutGroup13: TdxLayoutGroup;
    dxLayoutGroup14: TdxLayoutGroup;
    procedure btnSaveClick(Sender: TObject);
    procedure btnDelLayoutClick(Sender: TObject);
    procedure btnAddCompClick(Sender: TObject);
    procedure btnDelCompClick(Sender: TObject);
    procedure btnAddUserClick(Sender: TObject);
    procedure btnDelUserClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tvLayoutsNamePropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvCompsNameCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvUsersCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  end;

var
  SetupForm: TSetupForm;

implementation

{$R *.dfm}

uses dmLayouts, FireDAC.Comp.DataSet, Variants;

procedure TSetupForm.btnAddCompClick(Sender: TObject);
var
  _names: array of String;
begin
  SetLength(_names, 1);
  if InputQuery('Add computer', ['Name:'], _names, function(const ANames: array of string): Boolean
    begin
      Result := not Trim(ANames[0]).IsEmpty;
      if not Result then
        ShowMessage('Computer name can''t be empty!');
    end) then

  with LayoutsData, memComps do
  begin
    if not Active then
      Open;
    Append;
    memCompscomp_name.AsString := Trim(_names[0]);
//    Post;
  end;
end;

procedure TSetupForm.btnAddUserClick(Sender: TObject);
var
  _names: array of String;
begin
  SetLength(_names, 1);
  if InputQuery('Add user', ['Name:'], _names, function(const ANames: array of string): Boolean
    begin
      Result := not Trim(ANames[0]).IsEmpty;
      if not Result then
        ShowMessage('User name can''t be empty!');
    end) then

  with LayoutsData, memUsers do
  begin
    if not Active then
      Open;
    Append;
    memUsersuser_name.AsString := Trim(_names[0]);
//    Post;
  end;
end;

procedure TSetupForm.btnDelCompClick(Sender: TObject);
begin
  if Application.MessageBox(PChar(Format('Delete ''%s''?', [tvCompsName.EditValue])),
    'Confirm', MB_ICONQUESTION + MB_YESNO) = IDYES then
    with LayoutsData.memComps do
      Delete;
end;

procedure TSetupForm.btnDelLayoutClick(Sender: TObject);
begin
  if Application.MessageBox(PChar(Format('Delete ''%s''?', [tvLayoutsName.EditValue])),
    'Confirm', MB_ICONQUESTION + MB_YESNO) = IDYES then
    LayoutsData.dsViews.DataSet.Delete;
end;

procedure TSetupForm.btnDelUserClick(Sender: TObject);
begin
  if Application.MessageBox(PChar(Format('Delete ''%s''?', [tvUsersName.EditValue])),
    'Confirm', MB_ICONQUESTION + MB_YESNO) = IDYES then
    LayoutsData.dsViews.DataSet.Delete;
end;

procedure TSetupForm.btnSaveClick(Sender: TObject);
begin
  with LayoutsData do
  begin
    // Post all changes
    if dsViews.DataSet.Active and (dsViews.DataSet.State in [dsEdit, dsInsert]) then
      dsViews.DataSet.Post;
    if memComps.Active and (memComps.State in [dsEdit, dsInsert]) then
      memComps.Post;
    if memUsers.Active and (memUsers.State in [dsEdit, dsInsert]) then
      memUsers.Post;

    // Write to files
    DoSaveMemViewsToFile;
    DoSaveMemUsersToFile;
    DoSaveMemCompsToFile;
  end;
end;

procedure TSetupForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrCancel then
    with LayoutsData do
    if (dsViews.DataSet.State in dsEditModes) or
       (memComps.State in dsEditModes) or
       (memUsers.State in dsEditModes) then
      if Application.MessageBox('Cancel ALL changes?',
        'Confirm', MB_ICONQUESTION + MB_YESNO) = IDYES then
        with LayoutsData do
        begin
          DoLoadMemViewsFromFile;
          DoLoadMemUsersFromFile;
          DoLoadMemCompsFromFile;
        end
      else
        ModalResult := mrNone;

  CanClose := ModalResult <> mrNone;
end;

procedure TSetupForm.FormShow(Sender: TObject);
var
  _mult: Double;
begin
  // Workaround for TagEditor HighDPI bug
  _mult := Screen.PixelsPerInch / 96;
  edtLayoutTag.Properties.TagHeight := Round(edtLayoutTag.Properties.TagHeight * _mult);
  edtCompTag.Properties.TagHeight := Round(edtCompTag.Properties.TagHeight * _mult);
  edtUserTag.Properties.TagHeight := Round(edtUserTag.Properties.TagHeight * _mult);
end;

procedure TSetupForm.tvCompsNameCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  _comp: String;
begin
  if not VarIsNull(AViewInfo.DisplayValue) then
  begin
    _comp := AViewInfo.DisplayValue;
    if SameText(LayoutsData.ComputerName, _comp) then
      ACanvas.Font.Style := [TFontStyle.fsBold];
  end;
end;

procedure TSetupForm.tvLayoutsNamePropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
    if (LayoutsData.dsViews.DataSet as TFDMemTable).LocateEx('view_name', DisplayValue, [lxoCheckOnly, lxoFromCurrent, lxoBackward]) or
      (LayoutsData.dsViews.DataSet as TFDMemTable).LocateEx('view_name', DisplayValue, [lxoCheckOnly, lxoFromCurrent]) then
    begin
      ErrorText := Format('Error: duplicate layout name - ''%s''!', [DisplayValue]);
      Error := True;
    end;
end;

procedure TSetupForm.tvUsersCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
  _user: String;
begin
  if not VarIsNull(AViewInfo.DisplayValue) then
  begin
    _user := AViewInfo.DisplayValue;
    if SameText(LayoutsData.UserName, _user) then
      ACanvas.Font.Style := [TFontStyle.fsBold];
  end;
end;

end.

