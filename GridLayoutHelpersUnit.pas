unit GridLayoutHelpersUnit;

interface

uses
  SysUtils, Classes, JvSimpleXml, JvComponentBase, JvAppStorage, JvAppXMLStorage, cxGridTableView, cxGridDBTableView,
  cxGridDBBandedTableView, cxCustomPivotGrid, cxDBPivotGrid, cxCustomData, DB, dxSpreadSheetConditionalFormatting,
  cxGridCustomTableView;

type
  TcxGridConditionalFormattingProviderAccess = class(TcxGridConditionalFormattingProvider);

  TGridLayoutHelpers = class(TDataModule)
    AppXMLFileStorage: TJvAppXMLFileStorage;
    XMLLoader: TJvSimpleXML;
  private
    procedure KeyNamePropPath(const Path: string; const PersObj: TPersistent; const PropName: string;
      var KeyName, PropPath: string);
    procedure DoWriteProperty(const Path: string; const PersObj: TPersistent; const PropName: string);
    procedure DoReadProperty(const Path: string; const PersObj: TPersistent; const PropName: string);

    procedure DoWriteData(const Path: string; const Name: string; const Data: string);
    function DoReadData(const Path: string; const Name: string): string;

    procedure DoWriteInteger(const Path: string; const PersObj: TPersistent; const PropName: string;
      const Value: Integer);
    function DoReadInteger(const Path: string; const PersObj: TPersistent; const PropName: string;
      Default: Integer): Integer;
    function SaveConditionalFormattingToString(AView: TcxGridTableView): string; overload;
    function SaveConditionalFormattingToString(AView: TcxGridDBBandedTableView): string; overload;
    procedure LoadConditionalFormattingFromString(AView: TcxGridTableView; AData: string); overload;
    procedure SaveConditionalFormattingToXML(AView: TcxGridTableView);
    procedure LoadConditionalFormattingFromXML(AView: TcxGridTableView; AXml: TJvSimpleXMLElem);
    // function SummaryKindToString(AKind: string): TcxSummaryKind;
    // function SummaryStringToKind(AKind: TcxSummaryKind): string;
  public
{$IFNDEF NOPROGPREFS}
    procedure SaveColumnLayoutToProgPrefs(AView: TcxGridTableView; const AContainerName, AStationID: string); overload;
    procedure SaveColumnLayoutToProgPrefs(AView: TcxGridDBBandedTableView;
      const AContainerName, AStationID: string); overload;
    procedure LoadColumnLayoutFromProgPrefs(AView: TcxGridDBTableView; const AContainerName: string); overload;
    procedure LoadColumnLayoutFromProgPrefs(AView: TcxGridDBBandedTableView; const AContainerName: string); overload;

    function SaveViewFeaturesToProgPrefs(AView: TcxGridTableView; const AContainerName, AStationID: string): string;
    procedure LoadViewFeaturesFromProgPrefs(AView: TcxGridTableView; const AContainerName, AKeyFieldNames: string);
{$ENDIF}
    procedure SaveColumnLayoutToStream(AView: TcxGridTableView; var AStream: TStream); overload;
    procedure SaveColumnLayoutToStream(AView: TcxGridDBBandedTableView; var AStream: TStream); overload;
    procedure LoadColumnLayoutFromStream(AView: TcxGridDBTableView; AStream: TStream); overload;
    procedure LoadColumnLayoutFromStream(AView: TcxGridDBBandedTableView; AStream: TStream); overload;

    function SaveColumnLayoutToString(AView: TcxGridTableView; AStoreConditionalFormatting: Boolean = False): string; overload;
    function SaveColumnLayoutToString(AView: TcxGridDBBandedTableView; AStoreConditionalFormatting: Boolean = False): string; overload;
    procedure LoadColumnLayoutFromString(AView: TcxGridDBTableView; AXmlData: string); overload;
    procedure LoadColumnLayoutFromString(AView: TcxGridDBBandedTableView; AXmlData: string); overload;

    function SaveViewFeaturesToString(AView: TcxGridTableView): string;
    procedure LoadViewFeaturesFromString(AView: TcxGridTableView; AKeyFieldNames, AXmlData: string);
    function SaveViewFeaturesToStream(AView: TcxGridTableView; var AStream: TStream): string;
    procedure LoadViewFeaturesFromStream(AView: TcxGridTableView; AStream: TStream; const AKeyFieldNames: string);

    procedure SaveColumnLayoutToStream(AView: TcxDBPivotGrid; var AStream: TStream); overload;
    procedure LoadColumnLayoutFromStream(AView: TcxDBPivotGrid; AStream: TStream); overload;
    function SaveColumnLayoutToString(AView: TcxDBPivotGrid): string; overload;
    procedure LoadColumnLayoutFromString(AView: TcxDBPivotGrid; AXmlData: string); overload;

    procedure ColumnLayoutFinalize(AView: TcxCustomGridTableView);
  end;

function GridLayoutHelpers: TGridLayoutHelpers;

{$IFDEF TISLIBDLL}

var
  __GridLayoutHelpers: TGridLayoutHelpers;
{$ENDIF}

implementation

uses {$IFNDEF NOPROGPREFS} GeneralHelpersUnit, {$ENDIF} StrUtils, cxControls, Forms, TypInfo, JclStreams,
  ColumnStyleHelperUnit,
  ColumnUnboundHelperUnit,
  ColumnsHelperGuiUnit,
  System.NetEncoding, cxVariants, System.Types,
  System.Generics.Collections{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};
{$R *.dfm}
{$IFNDEF TISLIBDLL}

var
  __GridLayoutHelpers: TGridLayoutHelpers;
{$ENDIF}

function GridLayoutHelpers: TGridLayoutHelpers;
begin
  if not Assigned(__GridLayoutHelpers) then
    __GridLayoutHelpers := TGridLayoutHelpers.Create(Application);
  Result := __GridLayoutHelpers;
end;

{ TGridLayoutHelpers }

procedure TGridLayoutHelpers.DoWriteData(const Path: string; const Name: string; const Data: string);
var
  DataPath: string;
begin
  DataPath := AppXMLFileStorage.ConcatPaths([Path, Name]);
  AppXMLFileStorage.WriteString(DataPath, Data);
end;

procedure TGridLayoutHelpers.ColumnLayoutFinalize(AView: TcxCustomGridTableView);
begin
  if AView is TcxGridDBTableView then
  begin
    if TcxGridDBTableView(AView).GroupedColumnCount>0 then
      TcxGridDBTableView(AView).DataController.Groups.FullExpand;
  end
  else if AView is TcxGridDBBandedTableView then
  begin
    if TcxGridDBBandedTableView(AView).GroupedColumnCount>0 then
      TcxGridDBBandedTableView(AView).DataController.Groups.FullExpand;
  end;
end;

function TGridLayoutHelpers.DoReadData(const Path: string; const Name: string): string;
var
  DataPath: string;
begin
  DataPath := AppXMLFileStorage.ConcatPaths([Path, Name]);
  Result := AppXMLFileStorage.ReadString(DataPath);
end;

function TGridLayoutHelpers.DoReadInteger(const Path: string; const PersObj: TPersistent; const PropName: string;
  Default: Integer): Integer;
var
  KeyName: string;
  PropPath: string;
begin
  KeyNamePropPath(Path, PersObj, PropName, KeyName, PropPath);
  Result := AppXMLFileStorage.ReadInteger(PropPath, Default);
end;

procedure TGridLayoutHelpers.DoReadProperty(const Path: string; const PersObj: TPersistent; const PropName: string);
var
  KeyName: string;
  PropPath: string;
begin
  KeyNamePropPath(Path, PersObj, PropName, KeyName, PropPath);
  AppXMLFileStorage.ReadProperty(PropPath, PersObj, PropName, True, True);
end;

procedure TGridLayoutHelpers.DoWriteInteger(const Path: string; const PersObj: TPersistent; const PropName: string;
  const Value: Integer);
var
  KeyName: string;
  PropPath: string;
begin
  KeyNamePropPath(Path, PersObj, PropName, KeyName, PropPath);
  AppXMLFileStorage.WriteInteger(PropPath, Value);
end;

procedure TGridLayoutHelpers.DoWriteProperty(const Path: string; const PersObj: TPersistent; const PropName: string);
var
  KeyName: string;
  PropPath: string;
begin
  KeyNamePropPath(Path, PersObj, PropName, KeyName, PropPath);
  AppXMLFileStorage.WriteProperty(PropPath, PersObj, PropName, True, nil);
end;

procedure TGridLayoutHelpers.KeyNamePropPath(const Path: string; const PersObj: TPersistent; const PropName: string;
  var KeyName, PropPath: string);
begin
  KeyName := AppXMLFileStorage.TranslatePropertyName(PersObj, PropName, False);
  PropPath := AppXMLFileStorage.ConcatPaths([Path, KeyName]);
end;
{$IFNDEF NOPROGPREFS}

procedure TGridLayoutHelpers.LoadColumnLayoutFromProgPrefs(AView: TcxGridDBTableView; const AContainerName: string);
var
  AStream: TMemoryStream;
  AKeyFieldName: string;
begin
  AStream := TMemoryStream.Create;
  try
    AStream := nil;
    GenHelpers.ProgPref(AContainerName + AView.Name + 'Columns', AStream);
    LoadColumnLayoutFromStream(AView, AStream);
  finally
    AStream.Free;
  end;

  AKeyFieldName := AView.DataController.KeyFieldNames;
  LoadViewFeaturesFromProgPrefs(AView, AContainerName, AKeyFieldName);
end;

procedure TGridLayoutHelpers.LoadColumnLayoutFromProgPrefs(AView: TcxGridDBBandedTableView;
  const AContainerName: string);
var
  AStream: TMemoryStream;
  AKeyFieldName: string;
begin
  AStream := TMemoryStream.Create;
  try
    AStream := nil;
    GenHelpers.ProgPref(AContainerName + AView.Name + 'Columns', AStream);
    LoadColumnLayoutFromStream(AView, AStream);
  finally
    AStream.Free;
  end;

  AKeyFieldName := AView.DataController.KeyFieldNames;
  LoadViewFeaturesFromProgPrefs(AView, AContainerName, AKeyFieldName);
end;
{$ENDIF}

procedure TGridLayoutHelpers.LoadColumnLayoutFromStream(AView: TcxGridDBTableView; AStream: TStream);
begin
  if Assigned(AStream) then
  begin
    XMLLoader.LoadFromStream(AStream); // Using XmlLoader so we could set the Trim options
    LoadColumnLayoutFromString(AView, XMLLoader.XMLData);
  end;
end;

procedure TGridLayoutHelpers.LoadColumnLayoutFromStream(AView: TcxGridDBBandedTableView; AStream: TStream);
begin
  if Assigned(AStream) then
  begin
    XMLLoader.LoadFromStream(AStream); // Using XmlLoader so we could set the Trim options
    LoadColumnLayoutFromString(AView, XMLLoader.XMLData);
  end;
end;

procedure TGridLayoutHelpers.LoadColumnLayoutFromString(AView: TcxGridDBTableView; AXmlData: string);
var
  AXml: TJvSimpleXMLElem;
  I, x, AColIndex: Integer;
  AColumn: TcxGridDBColumn;
  AName, AColumnName, AValue, AKeyName, APropPath, ACondFormat: string;
  ANewColumnsVisible: Boolean;
  AStoredColumnList: TStringList;
begin
  ACondFormat := '';
  AStoredColumnList := TStringList.Create;
  AStoredColumnList.Sorted := True;
  ANewColumnsVisible := False;
  AView.BeginUpdate(lsimImmediate);
  try
    AppXMLFileStorage.Xml.XMLData := AXmlData;

    AXml := AppXMLFileStorage.Xml.Root;
    for x := 0 to AXml.Items.Count - 1 do
    begin
      AName := AXml.Items[x].Name;
      if StartsText('_Column_', AName) then
      begin
        AColumnName := RightStr(AName, Length(AName) - 8);

        if ContainsText(AColumnName, 'Unbound') and (AView.FindItemByName(AColumnName) = nil) then
        begin
          if (AView.DataController.KeyFieldNames <> '') and (not ColumnUnboundHelper.TableViewInList(AView)) then
            ColumnsGuiHelper.addTableView(AView, AView.DataController.KeyFieldNames);
          ColumnUnboundHelper.createUnboundColumn(AView, AColumnName);
          AStoredColumnList.Add(AColumnName);
        end;

        AColumn := nil;
        for I := 0 to AView.ColumnCount - 1 do
          if SameText(AView.Columns[I].Name, AColumnName) then
          begin
            AColumn := AView.Columns[I];
            Break;
          end;

        if Assigned(AColumn) then
        begin
          AStoredColumnList.Add(AColumnName);
          DoReadProperty(AXml.Items[x].Name, AColumn, 'Caption');
          if AColumn.VisibleForCustomization then // see notes in Save method
            DoReadProperty(AXml.Items[x].Name, AColumn, 'Visible');
          // DoReadProperty(AXml.Items[x].Name, AColumn, 'VisibleForCustomization'); //see notes in Save method
          DoReadProperty(AXml.Items[x].Name, AColumn, 'Width');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'SortOrder');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'SortIndex');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'GroupIndex');

          if ContainsText(AColumnName, 'Unbound') then
          begin
            ColumnUnboundHelper.addOrSetFieldUnbound(AView, AColumnName, DoReadData(AXml.Items[x].Name, 'Unbound'));
            ColumnUnboundHelper.restoreColumn(AView, AColumn);
          end;

          AValue := DoReadData(AXml.Items[x].Name, 'Style');
          if AValue <> '' then
            ColumnStyleHelper.addOrSetFieldStyle(AView, AColumn.DataBinding.FieldName, AValue);

          AColIndex := DoReadInteger(AXml.Items[x].Name, AColumn, 'Index', -10);
          if AColIndex <> -10 then
            AColumn.Index := AColIndex;
          KeyNamePropPath(AXml.Items[x].Name, AColumn, 'DisplayFormat', AKeyName, APropPath);
          AValue := AppXMLFileStorage.ReadString(APropPath);
          if (AValue <> '') and IsPublishedProp(AColumn.Properties, 'DisplayFormat') then
            SetPropValue(AColumn.Properties, 'DisplayFormat', AValue);
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'FooterKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'FooterFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFooterKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFooterFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'SortByGroupFooterSummary');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'SortByGroupSummary');
        end;
      end
      else if SameText(AName, '_OptionsView_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.OptionsView)
      else if SameText(AName, '_OptionsBehavior_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.OptionsBehavior)
      else if SameText(AName, '_DateTimeHandling_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.DateTimeHandling)
      else if SameText(AName, 'NewColumnsVisible') then
        ANewColumnsVisible := StrToBool(AXml.Items[x].Value)
        { else if SameText(AName, '_ConditionalFormatting_') then
          ACondFormat := DoReadData('_ConditionalFormatting_', 'ConditionalFormatting') };

      // DoWriteProperty('_OptionsView_', AProps, 'ColumnAutoWidth');
      // DoWriteProperty('_OptionsView_', AProps, 'Header');
    end;
    if (not ANewColumnsVisible) and (AStoredColumnList.Count > 0) then
    begin
      for I := 0 to AView.ColumnCount - 1 do
      begin
        if not AStoredColumnList.Find(AView.Columns[I].Name, AColIndex) then
          AView.Columns[I].Visible := False;
      end;
    end;
  finally
    AView.EndUpdate;
    AStoredColumnList.Free;
  end;

  ColumnLayoutFinalize(AView);
  { if ACondFormat <> '' then
    LoadConditionalFormattingFromString(AView, ACondFormat); }
  LoadConditionalFormattingFromXML(AView, AXml);
end;

procedure TGridLayoutHelpers.LoadColumnLayoutFromString(AView: TcxGridDBBandedTableView; AXmlData: string);
var
  AXml: TJvSimpleXMLElem;
  I, x, AColIndex: Integer;
  AColumn: TcxGridDBBandedColumn;
  AName, AColumnName, AValue, AKeyName, APropPath, ACondFormat: string;
  ANewColumnsVisible: Boolean;
  AStoredColumnList: TStringList;
begin
  ACondFormat := '';
  AStoredColumnList := TStringList.Create;
  AStoredColumnList.Sorted := True;
  ANewColumnsVisible := False;
  AView.BeginUpdate(lsimImmediate);
  try
    AppXMLFileStorage.Xml.XMLData := AXmlData;

    AXml := AppXMLFileStorage.Xml.Root;
    for x := 0 to AXml.Items.Count - 1 do
    begin
      AName := AXml.Items[x].Name;
      if StartsText('_Column_', AName) then
      begin
        AColumnName := RightStr(AName, Length(AName) - 8);

        if ContainsText(AColumnName, 'Unbound') and (AView.FindItemByName(AColumnName) = nil) then
        begin
          if (AView.DataController.KeyFieldNames <> '') and (not ColumnUnboundHelper.TableViewInList(AView)) then
            ColumnsGuiHelper.addTableView(AView, AView.DataController.KeyFieldNames);
          ColumnUnboundHelper.createUnboundColumn(AView, AColumnName);
          AStoredColumnList.Add(AColumnName);
        end;

        AColumn := nil;
        for I := 0 to AView.ColumnCount - 1 do
          if SameText(AView.Columns[I].Name, AColumnName) then
          begin
            AColumn := AView.Columns[I];
            Break;
          end;
        if Assigned(AColumn) then
        begin
          AStoredColumnList.Add(AColumnName);
          DoReadProperty(AXml.Items[x].Name, AColumn, 'Caption');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'Visible');
          if AColumn.VisibleForCustomization then // see notes in Save method
            DoReadProperty(AXml.Items[x].Name, AColumn, 'Visible');
          // DoReadProperty(AXml.Items[x].Name, AColumn, 'VisibleForCustomization'); //see notes in Save method
          DoReadProperty(AXml.Items[x].Name, AColumn, 'Width');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'SortOrder');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'SortIndex');
          DoReadProperty(AXml.Items[x].Name, AColumn, 'GroupIndex');

          if ContainsText(AColumnName, 'Unbound') then
          begin
            ColumnUnboundHelper.addOrSetFieldUnbound(AView, AColumnName, DoReadData(AXml.Items[x].Name, 'Unbound'));
            ColumnUnboundHelper.restoreColumn(AView, AColumn);
          end;

          AValue := DoReadData(AXml.Items[x].Name, 'Style');
          if AValue <> '' then
            ColumnStyleHelper.addOrSetFieldStyle(AView, AColumn.DataBinding.FieldName, AValue);

          AColIndex := DoReadInteger(AXml.Items[x].Name, AColumn, 'BandIndex', -10);
          if AColIndex <> -10 then
            AColumn.Position.BandIndex := AColIndex;
          AColIndex := DoReadInteger(AXml.Items[x].Name, AColumn, 'ColIndex', -10);
          if AColIndex <> -10 then
            AColumn.Position.ColIndex := AColIndex;
          AColIndex := DoReadInteger(AXml.Items[x].Name, AColumn, 'RowIndex', -10);
          if AColIndex <> -10 then
            AColumn.Position.RowIndex := AColIndex;
          KeyNamePropPath(AXml.Items[x].Name, AColumn, 'DisplayFormat', AKeyName, APropPath);
          AValue := AppXMLFileStorage.ReadString(APropPath);
          if (AValue <> '') and IsPublishedProp(AColumn.Properties, 'DisplayFormat') then
            SetPropValue(AColumn.Properties, 'DisplayFormat', AValue);
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'FooterKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'FooterFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFooterKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFooterFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'SortByGroupFooterSummary');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'SortByGroupSummary');
        end;
      end
      else if SameText(AName, '_OptionsView_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.OptionsView)
      else if SameText(AName, '_OptionsBehavior_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.OptionsBehavior)
      else if SameText(AName, '_DateTimeHandling_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.DateTimeHandling)
      else if SameText(AName, 'NewColumnsVisible') then
        ANewColumnsVisible := StrToBool(AXml.Items[x].Value)
        { else if SameText(AName, '_ConditionalFormatting_') then
          ACondFormat := DoReadData('_ConditionalFormatting_', 'ConditionalFormatting') };
      if (not ANewColumnsVisible) and (AStoredColumnList.Count > 0) then
      begin
        for I := 0 to AView.ColumnCount - 1 do
        begin
          if not AStoredColumnList.Find(AView.Columns[I].Name, AColIndex) then
            AView.Columns[I].Visible := False;
        end;
      end;
    end;
  finally
    AView.EndUpdate;
  end;
  { if ACondFormat <> '' then
    LoadConditionalFormattingFromString(AView, ACondFormat); }

  ColumnLayoutFinalize(AView);
  LoadConditionalFormattingFromXML(AView, AXml);
end;
{$IFNDEF NOPROGPREFS}

procedure TGridLayoutHelpers.SaveColumnLayoutToProgPrefs(AView: TcxGridTableView;
  const AContainerName, AStationID: string);
var
  AStream: TStream;
begin
  AStream := TMemoryStream.Create;
  try
    SaveColumnLayoutToStream(AView, AStream);
    GenHelpers.ProgPrefStore(AContainerName + AView.Name + 'Columns', AStationID, TMemoryStream(AStream));
  finally
    AStream.Free;
  end;
end;

procedure TGridLayoutHelpers.SaveColumnLayoutToProgPrefs(AView: TcxGridDBBandedTableView;
  const AContainerName, AStationID: string);
var
  AStream: TStream;
begin
  AStream := TMemoryStream.Create;
  try
    SaveColumnLayoutToStream(AView, AStream);
    GenHelpers.ProgPrefStore(AContainerName + AView.Name + 'Columns', AStationID, TMemoryStream(AStream));
  finally
    AStream.Free;
  end;
end;
{$ENDIF}

procedure TGridLayoutHelpers.SaveColumnLayoutToStream(AView: TcxGridTableView; var AStream: TStream);
begin
  SaveColumnLayoutToString(AView);
  AppXMLFileStorage.Xml.SaveToStream(AStream, TJclStringEncoding.seAnsi);
end;

procedure TGridLayoutHelpers.SaveColumnLayoutToStream(AView: TcxGridDBBandedTableView; var AStream: TStream);
begin
  SaveColumnLayoutToString(AView);
  AppXMLFileStorage.Xml.SaveToStream(AStream, TJclStringEncoding.seAnsi);
end;

function TGridLayoutHelpers.SaveColumnLayoutToString(AView: TcxGridDBBandedTableView; AStoreConditionalFormatting: Boolean): string;
var
  I: Integer;
  AProps: TPersistent;
  AStoreDef: Boolean;
  colName, AValue: string;
begin
  AppXMLFileStorage.Xml.Root.Clear;
  AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
  AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
  AProps := AView.DateTimeHandling;
  DoWriteProperty('_DateTimeHandling_', AProps, 'Grouping');
  AProps := AView.OptionsView;
  DoWriteProperty('_OptionsView_', AProps, 'CellAutoHeight');
  DoWriteProperty('_OptionsView_', AProps, 'ColumnAutoWidth');
  DoWriteProperty('_OptionsView_', AProps, 'Header');
  DoWriteProperty('_OptionsView_', AProps, 'Footer');
  DoWriteProperty('_OptionsView_', AProps, 'GroupFooters');
  AProps := AView.OptionsBehavior;
  DoWriteProperty('_OptionsBehavior_', AProps, 'FixedGroups');
  AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;

  for I := 0 to AView.VisibleColumnCount - 1 do
  begin
    // Changes 1.1.0.10 lists adding support to save VisibleForCustomization property
    // This is causing issues when a previously unused featiure or custom field is enabled
    // so now we are adding this line to stop saving and loading VisibleForCustomization
    if (not AView.VisibleColumns[I].VisibleForCustomization) then
      Continue;

    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Caption');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Visible');
    if not AView.VisibleColumns[I].VisibleForCustomization then
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'VisibleForCustomization');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Width');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'SortOrder');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'SortIndex');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'GroupIndex');
    DoWriteInteger('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'BandIndex',
      AView.VisibleColumns[I].Position.BandIndex);
    DoWriteInteger('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'ColIndex',
      AView.VisibleColumns[I].Position.ColIndex);
    DoWriteInteger('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'RowIndex',
      AView.VisibleColumns[I].Position.RowIndex);

    colName := (AView.VisibleColumns[I] as TcxGridDBBandedColumn).DataBinding.FieldName;
    if ColumnUnboundHelper.isUnbound(AView.VisibleColumns[I]) then
    begin
      colName := AView.VisibleColumns[I].Name;
      DoWriteData('_Column_' + AView.VisibleColumns[I].Name, 'Unbound', ColumnUnboundHelper.unboundColumnToString(AView,
        colName));
    end;
    AValue := ColumnStyleHelper.conditionStylesToString(AView, colName);
    if AValue <> '' then
      DoWriteData('_Column_' + AView.VisibleColumns[I].Name, 'Style', AValue);

    AProps := AView.VisibleColumns[I].Properties;
    if Assigned(AProps) and IsPublishedProp(AProps, 'DisplayFormat') then
    begin
      AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
      AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
      // AValue := GetPropValue(AProps, 'DisplayFormat', True);
      // if (AValue<>'') and (not (SameText(AValue, 'True') or SameText(AValue, 'False'))) then
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AProps, 'DisplayFormat');
      AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;
    end;
    if AView.VisibleColumns[I].Summary.FooterKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'FooterKind');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'FooterFormat');
    end;
    if AView.VisibleColumns[I].Summary.GroupFooterKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupFooterKind');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupFooterFormat');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary,
        'SortByGroupFooterSummary');
    end;
    if AView.VisibleColumns[I].Summary.GroupKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupKind');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupFormat');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'SortByGroupSummary');
    end;
  end;

  for I := 0 to AView.ColumnCount - 1 do
    if not AView.Columns[I].Visible then
    begin
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Caption');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Visible');
      if not AView.Columns[I].VisibleForCustomization then
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'VisibleForCustomization');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Width');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'SortOrder');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'SortIndex');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'GroupIndex');
      DoWriteInteger('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'BandIndex',
        AView.Columns[I].Position.BandIndex);
      DoWriteInteger('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'ColIndex',
        AView.Columns[I].Position.ColIndex);
      DoWriteInteger('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'RowIndex',
        AView.Columns[I].Position.RowIndex);

      colName := (AView.Columns[I] as TcxGridDBBandedColumn).DataBinding.FieldName;
      if ColumnUnboundHelper.isUnbound(AView.Columns[I]) then
      begin
        colName := AView.Columns[I].Name;
        DoWriteData('_Column_' + AView.Columns[I].Name, 'Unbound', ColumnUnboundHelper.unboundColumnToString(AView,
          colName));
      end;
      AValue := ColumnStyleHelper.conditionStylesToString(AView, colName);
      if AValue <> '' then
        DoWriteData('_Column_' + AView.VisibleColumns[I].Name, 'Style', AValue);

      AProps := AView.Columns[I].Properties;
      if Assigned(AProps) and IsPublishedProp(AProps, 'DisplayFormat') then
      begin
        AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
        AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
        // AValue := GetPropValue(AProps, 'DisplayFormat', True);
        // if (AValue<>'') and (not (SameText(AValue, 'True') or SameText(AValue, 'False'))) then
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AProps, 'DisplayFormat');
        AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;
      end;
      if AView.Columns[I].Summary.FooterKind <> skNone then
      begin
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'FooterKind');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'FooterFormat');
      end;
      if AView.Columns[I].Summary.GroupFooterKind <> skNone then
      begin
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFooterKind');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFooterFormat');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'SortByGroupFooterSummary');
      end;
      if AView.Columns[I].Summary.GroupKind <> skNone then
      begin
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupKind');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFormat');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'SortByGroupSummary');
      end;
    end;

  if AStoreConditionalFormatting and (AView.ConditionalFormatting.RuleCount > 0) then
    SaveConditionalFormattingToXML(AView);
  // DoWriteData('_ConditionalFormatting_', 'ConditionalFormatting', SaveConditionalFormattingToString(AView));

  Result := AppXMLFileStorage.Xml.XMLData;
end;

function TGridLayoutHelpers.SaveColumnLayoutToString(AView: TcxGridTableView;
  AStoreConditionalFormatting: Boolean): string;
var
  I: Integer;
  AProps: TPersistent;
  AStoreDef: Boolean;
  colName, AValue: string;
begin
  AppXMLFileStorage.Xml.Root.Items.Clear; // XMLData := '';
  AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
  AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
  AProps := AView.DateTimeHandling;
  DoWriteProperty('_DateTimeHandling_', AProps, 'Grouping');
  AProps := AView.OptionsView;
  DoWriteProperty('_OptionsView_', AProps, 'CellAutoHeight');
  DoWriteProperty('_OptionsView_', AProps, 'ColumnAutoWidth');
  DoWriteProperty('_OptionsView_', AProps, 'Header');
  DoWriteProperty('_OptionsView_', AProps, 'Footer');
  DoWriteProperty('_OptionsView_', AProps, 'GroupFooters');
  AProps := AView.OptionsBehavior;
  DoWriteProperty('_OptionsBehavior_', AProps, 'FixedGroups');
  AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;

  for I := 0 to AView.VisibleColumnCount - 1 do
  begin
    // Changes 1.1.0.10 lists adding support to save VisibleForCustomization property
    // This is causing issues when a previously unused featiure or custom field is enabled
    // so now we are adding this line to stop saving and loading VisibleForCustomization
    if (not AView.VisibleColumns[I].VisibleForCustomization) then
      Continue;

    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Caption');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Visible');
    if not AView.VisibleColumns[I].VisibleForCustomization then
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'VisibleForCustomization');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Width');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'SortOrder');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'SortIndex');
    DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'GroupIndex');
    DoWriteInteger('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I], 'Index',
      AView.VisibleColumns[I].Index);

    colName := (AView.VisibleColumns[I] as TcxGridDBColumn).DataBinding.FieldName;
    if ColumnUnboundHelper.isUnbound(AView.VisibleColumns[I]) then
    begin
      colName := AView.VisibleColumns[I].Name;
      DoWriteData('_Column_' + AView.VisibleColumns[I].Name, 'Unbound',
        ColumnUnboundHelper.unboundColumnToString(AView as TcxGridDBTableView, colName));
    end;
    AValue := ColumnStyleHelper.conditionStylesToString(AView, colName);
    if AValue <> '' then
      DoWriteData('_Column_' + AView.VisibleColumns[I].Name, 'Style', AValue);

    AProps := AView.VisibleColumns[I].Properties;
    if Assigned(AProps) and IsPublishedProp(AProps, 'DisplayFormat') then
    begin
      AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
      AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
      // AValue := GetPropValue(AProps, 'DisplayFormat', True);
      // if (AValue<>'') and (not (SameText(AValue, 'True') or SameText(AValue, 'False'))) then
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AProps, 'DisplayFormat');
      AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;
    end;
    if AView.VisibleColumns[I].Summary.FooterKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'FooterKind');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'FooterFormat');
    end;
    if AView.VisibleColumns[I].Summary.GroupFooterKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupFooterKind');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupFooterFormat');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary,
        'SortByGroupFooterSummary');
    end;
    if AView.VisibleColumns[I].Summary.GroupKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupKind');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'GroupFormat');
      DoWriteProperty('_Column_' + AView.VisibleColumns[I].Name, AView.VisibleColumns[I].Summary, 'SortByGroupSummary');
    end;
  end;

  for I := 0 to AView.ColumnCount - 1 do
    if not AView.Columns[I].Visible then
    begin
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Caption');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Visible');
      if not AView.Columns[I].VisibleForCustomization then
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'VisibleForCustomization');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Width');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'SortOrder');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'SortIndex');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'GroupIndex');
      DoWriteInteger('_Column_' + AView.Columns[I].Name, AView.Columns[I], 'Index', AView.Columns[I].Index);

      colName := (AView.Columns[I] as TcxGridDBColumn).DataBinding.FieldName;
      if ColumnUnboundHelper.isUnbound(AView.Columns[I]) then
      begin
        colName := AView.Columns[I].Name;
        DoWriteData('_Column_' + AView.Columns[I].Name, 'Unbound',
          ColumnUnboundHelper.unboundColumnToString(AView as TcxGridDBTableView, colName));
      end;
      AValue := ColumnStyleHelper.conditionStylesToString(AView, colName);
      if AValue <> '' then
        DoWriteData('_Column_' + AView.Columns[I].Name, 'Style', AValue);

      AProps := AView.Columns[I].Properties;
      if Assigned(AProps) and IsPublishedProp(AProps, 'DisplayFormat') then
      begin
        AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
        AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
        // AValue := GetPropValue(AProps, 'DisplayFormat', True);
        // if (AValue<>'') and (not (SameText(AValue, 'True') or SameText(AValue, 'False'))) then
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AProps, 'DisplayFormat');
        AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;
      end;
      if AView.Columns[I].Summary.FooterKind <> skNone then
      begin
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'FooterKind');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'FooterFormat');
      end;
      if AView.Columns[I].Summary.GroupFooterKind <> skNone then
      begin
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFooterKind');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFooterFormat');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'SortByGroupFooterSummary');
      end;
      if AView.Columns[I].Summary.GroupKind <> skNone then
      begin
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupKind');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFormat');
        DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'SortByGroupSummary');
      end;
    end;

  if AStoreConditionalFormatting and (AView.ConditionalFormatting.RuleCount > 0) then
    SaveConditionalFormattingToXML(AView);
  // DoWriteData('_ConditionalFormatting_', 'ConditionalFormatting', SaveConditionalFormattingToString(AView));

  Result := AppXMLFileStorage.Xml.XMLData;
end;

procedure TGridLayoutHelpers.LoadColumnLayoutFromStream(AView: TcxDBPivotGrid; AStream: TStream);
begin
  if Assigned(AStream) then
  begin
    XMLLoader.LoadFromStream(AStream); // Using XmlLoader so we could set the Trim options
    LoadColumnLayoutFromString(AView, XMLLoader.XMLData);
  end;
end;

procedure TGridLayoutHelpers.LoadColumnLayoutFromString(AView: TcxDBPivotGrid; AXmlData: string);
var
  AXml: TJvSimpleXMLElem;
  I, x: Integer;
  AField: TcxPivotGridField;
  AName, AFieldName: string;
begin
  AView.BeginUpdate;
  try
    AppXMLFileStorage.Xml.XMLData := AXmlData;

    AXml := AppXMLFileStorage.Xml.Root;
    for x := 0 to AXml.Items.Count - 1 do
    begin
      AName := AXml.Items[x].Name;
      if StartsText('_Field_', AName) then
      begin
        AFieldName := RightStr(AName, Length(AName) - 7);
        AField := nil;
        for I := 0 to AView.FieldCount - 1 do
          if SameText(AView.Fields[I].Name, AFieldName) then
          begin
            AField := AView.Fields[I];
            Break;
          end;
        if Assigned(AField) then
        begin
          DoReadProperty(AXml.Items[x].Name, AField, 'Caption');
          DoReadProperty(AXml.Items[x].Name, AField, 'SortOrder');
          DoReadProperty(AXml.Items[x].Name, AField, 'GroupExpanded');
          DoReadProperty(AXml.Items[x].Name, AField, 'Visible');
          DoReadProperty(AXml.Items[x].Name, AField, 'Width');
          DoReadProperty(AXml.Items[x].Name, AField, 'Area');
          DoReadProperty(AXml.Items[x].Name, AField, 'AreaIndex');
          DoReadProperty(AXml.Items[x].Name, AField, 'DataVisibility');
          DoReadProperty(AXml.Items[x].Name, AField, 'GroupInterval');
          DoReadProperty(AXml.Items[x].Name, AField, 'SummaryType');
        end;
      end
      else if SameText(AName, '_OptionsView_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.OptionsView)
      else if SameText(AName, '_OptionsBehavior_') then
        AppXMLFileStorage.ReadPersistent(AName, AView.OptionsBehavior);
    end;
  finally
    AView.EndUpdate;
  end;
end;

function BinaryToStream(const Text: string; Value: TMemoryStream): Integer;
var
  Pos: Integer;
  dataLen: Integer;
  DataBytes: TBytes;
  Stream: TMemoryStream;
begin
  if Text <> '' then
  begin
    if Value is TMemoryStream then
      Stream := TMemoryStream(Value)
    else
      Stream := TMemoryStream.Create;

    try
      dataLen := Length(Text) div 2;
      SetLength(DataBytes, dataLen);
      Pos := Stream.Position;
      HexToBin(BytesOf(Text), 0, DataBytes, 0, dataLen);
      Stream.Write(DataBytes[0], dataLen);
      Stream.Position := Pos;
      if Value <> Stream then
        Value.CopyFrom(Stream, dataLen);
      Result := Stream.Size - Pos;
    finally
      if Value <> Stream then
        Stream.Free;
    end;
  end
  else
    Result := 0;
end;

function StreamToBinary(Value: TStream): string;
var
  Stream: TBytesStream;
  Buffer: TBytes;
begin
  SetLength(Result, (Value.Size - Value.Position) * 2);
  if Result.Length > 0 then
  begin
    if Value is TBytesStream then
      Stream := TBytesStream(Value)
    else
      Stream := TBytesStream.Create;

    try
      if Stream <> Value then
      begin
        Stream.CopyFrom(Value, Value.Size - Value.Position);
        Stream.Position := 0;
      end;
      SetLength(Buffer, Stream.Size * 2);
      BinToHex(Stream.Bytes, Stream.Position, Buffer, 0, Stream.Size - Stream.Position);
      Result := StringOf(Buffer);
    finally
      if Value <> Stream then
        Stream.Free;
    end;
  end;
end;

procedure StringToStream(const AData: string; Stream: TStream; const Encoding: TEncoding);
var
  StringBytes: TBytes;
begin
  StringBytes := Encoding.GetBytes(PChar(AData));
  Stream.WriteBuffer(StringBytes, SizeOf(StringBytes));
  Stream.Position := 0;
end;

function EncodeString(const Input: string): string;
var
  InStr, OutStr: TStringStream;
begin
  InStr := TStringStream.Create(Input, TEncoding.UTF8);
  try
    OutStr := TStringStream.Create('');
    try
      TNetEncoding.Base64.Encode(InStr, OutStr);
      // EncodeStream(InStr, OutStr);
      Result := OutStr.DataString;
    finally
      OutStr.Free;
    end;
  finally
    InStr.Free;
  end;
end;

function DecodeString(const Input: string): string;
var
  InStr, OutStr: TStringStream;
begin
  InStr := TStringStream.Create(Input);
  try
    OutStr := TStringStream.Create('', TEncoding.UTF8);
    try
      TNetEncoding.Base64.Decode(InStr, OutStr);
      // DecodeStream(InStr, OutStr);
      Result := OutStr.DataString;
    finally
      OutStr.Free;
    end;
  finally
    InStr.Free;
  end;
end;

procedure TGridLayoutHelpers.LoadConditionalFormattingFromString(AView: TcxGridTableView; AData: string);
var
  AStream: TStringStream;
  AOwner: IdxSpreadSheetConditionalFormattingOwner;
begin
  if AData = '' then
    Exit;

  AData := DecodeString(AData);

  if Length(AData) = 0 then
    Exit;

  AStream := TStringStream.Create;
  try
    AStream.WriteString(AData);
    AStream.Position := 0;
    AOwner := AView.ConditionalFormatting.Owner;
    TcxGridConditionalFormattingProviderAccess(AOwner as TObject).Read(AStream);
  finally
    AStream.Free;
  end;
end;

procedure TGridLayoutHelpers.LoadConditionalFormattingFromXML(AView: TcxGridTableView; AXml: TJvSimpleXMLElem);
var
  AStream: TMemoryStream;
  I: Integer;
  AColumnName, ARuleClassName, AStreamHex: string;
  AReader: TcxReader;
  ARule: TdxSpreadSheetConditionalFormattingCustomRule;
  AColumn: TcxGridColumn;
  ARuleClass: TdxSpreadSheetCustomConditionalFormattingRuleClass;
begin
  AView.ConditionalFormatting.BeginUpdate;
  try
    for I := 0 to AXml.Items.Count - 1 do
    begin
      if StartsText('_ConditionalFormatting_', AXml.Items[I].Name) then
      begin
        AColumnName := DoReadData(AXml.Items[I].Name, 'ColumnName');

        ARuleClassName := DoReadData(AXml.Items[I].Name, 'RuleClass');
        ARuleClass := nil;
        try
          ARuleClass := TdxSpreadSheetCustomConditionalFormattingRuleClass(FindClass(ARuleClassName));
        except
        end;

        if ARuleClass = nil then
          Continue;

        AColumn := AView.FindItemByName(AColumnName) as TcxGridColumn;
        if not Assigned(AColumn) then
          Continue;

        AStreamHex := DoReadData(AXml.Items[I].Name, 'RuleStream');
        AStream := TMemoryStream.Create;
        BinaryToStream(AStreamHex, AStream);
        AStream.Position := 0;
        AReader := TcxReader.Create(AStream);
        try
          AReader.Version := 10;
          AView.ConditionalFormatting.Add(ARuleClass, ARule);
          ARule.BeginUpdate;
          try
            try
              ARule.LoadFromStream(AReader);
              ARule.Area := TRect.Create(AColumn.Index, 0, AColumn.Index, MaxInt);
            except
            end;
          finally
            ARule.EndUpdate;
          end;
        finally
          AReader.Free;
          AStream.Free;
        end;
      end;
    end;
  finally
    AView.ConditionalFormatting.EndUpdate;
  end;
end;

{$IFNDEF NOPROGPREFS}

procedure TGridLayoutHelpers.LoadViewFeaturesFromProgPrefs(AView: TcxGridTableView;
  const AContainerName, AKeyFieldNames: string);
var
  AStream: TMemoryStream;
begin
  AStream := TMemoryStream.Create;
  try
    AStream := nil;
    GenHelpers.ProgPref(AContainerName + AView.Name + 'ViewFeatures', AStream);
    LoadViewFeaturesFromStream(AView, AStream, AKeyFieldNames);
  finally
    AStream.Free;
  end;
end;
{$ENDIF}

procedure TGridLayoutHelpers.LoadViewFeaturesFromStream(AView: TcxGridTableView; AStream: TStream;
  const AKeyFieldNames: string);
begin
  if Assigned(AStream) then
  begin
    XMLLoader.LoadFromStream(AStream); // Using XmlLoader so we could set the Trim options
    LoadViewFeaturesFromString(AView, AKeyFieldNames, XMLLoader.XMLData);
  end;
end;

procedure TGridLayoutHelpers.LoadViewFeaturesFromString(AView: TcxGridTableView; AKeyFieldNames, AXmlData: string);
var
  AXml: TJvSimpleXMLElem;
  I, x: Integer;
  AColumn: TcxGridColumn;
  AName, AColumnName, AValue, ACondFormat, AFieldName: string;
begin
  ACondFormat := '';
  AView.BeginUpdate(lsimImmediate);
  try
    AppXMLFileStorage.Xml.XMLData := AXmlData;

    AXml := AppXMLFileStorage.Xml.Root;
    for x := 0 to AXml.Items.Count - 1 do
    begin
      AName := AXml.Items[x].Name;
      if StartsText('_Column_', AName) then
      begin
        AColumnName := RightStr(AName, Length(AName) - 8);

        if ContainsText(AColumnName, 'Unbound') and (AView.FindItemByName(AColumnName) = nil) then
        begin
          if (AKeyFieldNames <> '') and (not ColumnUnboundHelper.TableViewInList(AView)) then
            ColumnsGuiHelper.addTableView(AView, AKeyFieldNames);
          ColumnUnboundHelper.createUnboundColumn(AView, AColumnName);
        end;

        AColumn := nil;
        for I := 0 to AView.ColumnCount - 1 do
          if SameText(AView.Columns[I].Name, AColumnName) then
          begin
            AColumn := AView.Columns[I];
            Break;
          end;

        if Assigned(AColumn) then
        begin
          if ContainsText(AColumnName, 'Unbound') then
          begin
            ColumnUnboundHelper.addOrSetFieldUnbound(AView, AColumnName, DoReadData(AXml.Items[x].Name, 'Unbound'));
            ColumnUnboundHelper.restoreColumn(AView, AColumn);
          end;

          AValue := DoReadData(AXml.Items[x].Name, 'Style');
          if AValue <> '' then
          begin
            if AColumn is TcxGridDBColumn then
              AFieldName := (AColumn as TcxGridDBColumn).DataBinding.FieldName
            else if AColumn is TcxGridDBBandedColumn then
              AFieldName := (AColumn as TcxGridDBBandedColumn).DataBinding.FieldName
            else
              AFieldName := '';
            if AFieldName <> '' then
              ColumnStyleHelper.addOrSetFieldStyle(AView, AFieldName, AValue);
          end;

          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'FooterKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'FooterFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFooterKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFooterFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'SortByGroupFooterSummary');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupKind');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'GroupFormat');
          DoReadProperty(AXml.Items[x].Name, AColumn.Summary, 'SortByGroupSummary');
        end;
      end
      { lse if SameText(AName, '_ConditionalFormatting_') then
        ACondFormat := DoReadData('_ConditionalFormatting_', 'ConditionalFormatting') }
    end;
  finally
    AView.EndUpdate;
  end;

  LoadConditionalFormattingFromXML(AView, AXml)
  { f ACondFormat <> '' then
    LoadConditionalFormattingFromString(AView, ACondFormat); }
end;

procedure TGridLayoutHelpers.SaveColumnLayoutToStream(AView: TcxDBPivotGrid; var AStream: TStream);
begin
  SaveColumnLayoutToString(AView);
  AppXMLFileStorage.Xml.SaveToStream(AStream, TJclStringEncoding.seAnsi);
end;

function TGridLayoutHelpers.SaveColumnLayoutToString(AView: TcxDBPivotGrid): string;
var
  x: Integer;
  AProps: TPersistent;
  AStoreDef: Boolean;
begin
  AppXMLFileStorage.Xml.Root.Clear;
  AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
  AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
  AProps := AView.OptionsView;
  DoWriteProperty('_OptionsView_', AProps, 'ColumnGrandTotals');
  DoWriteProperty('_OptionsView_', AProps, 'ColumnTotals');
  DoWriteProperty('_OptionsView_', AProps, 'RowGrandTotals');
  DoWriteProperty('_OptionsView_', AProps, 'RowTotals');
  AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;

  for x := 0 to AView.FieldCount - 1 do
  begin
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'Caption');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'SortOrder');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'GroupExpanded');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'Visible');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'Width');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'Area');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'AreaIndex');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'DataVisibility');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'GroupInterval');
    DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'SummaryType');
    // DoWriteProperty('_Field_' + AView.Fields[x].Name, AView.Fields[x], 'Filter');
    { AProps := AView.VisibleColumns[x].Properties;
      if Assigned(AProps) and IsPublishedProp(AProps, 'DisplayFormat') then
      begin
      AStoreDef := AppXMLFileStorage.StorageOptions.StoreDefaultValues;
      AppXMLFileStorage.StorageOptions.StoreDefaultValues := False;
      // AValue := GetPropValue(AProps, 'DisplayFormat', True);
      // if (AValue<>'') and (not (SameText(AValue, 'True') or SameText(AValue, 'False'))) then
      DoWriteProperty('_Column_' + AView.VisibleColumns[x].Name, AProps, 'DisplayFormat');
      AppXMLFileStorage.StorageOptions.StoreDefaultValues := AStoreDef;
      end; }
  end;
  Result := AppXMLFileStorage.Xml.XMLData;
end;

function StreamToString(const Stream: TStream; const Encoding: TEncoding): string;
var
  StringBytes: TBytes;
begin
  Stream.Position := 0;
  SetLength(StringBytes, Stream.Size);
  Stream.ReadBuffer(StringBytes, Stream.Size);
  Result := Encoding.GetString(StringBytes);
end;

function TGridLayoutHelpers.SaveConditionalFormattingToString(AView: TcxGridTableView): string;
var
  AStream: TStringStream;
  AOwner: IdxSpreadSheetConditionalFormattingOwner;
begin
  AStream := TStringStream.Create;
  try
    AOwner := AView.ConditionalFormatting.Owner;
    TcxGridConditionalFormattingProviderAccess(AOwner as TObject).Write(AStream);
    Result := EncodeString(AStream.DataString)
  finally
    AStream.Free;
  end;
end;

function TGridLayoutHelpers.SaveConditionalFormattingToString(AView: TcxGridDBBandedTableView): string;
var
  AStream: TStringStream;
  AOwner: IdxSpreadSheetConditionalFormattingOwner;
begin
  AStream := TStringStream.Create;
  try
    AOwner := AView.ConditionalFormatting.Owner;
    TcxGridConditionalFormattingProviderAccess(AOwner as TObject).Write(AStream);
    Result := EncodeString(AStream.DataString)
  finally
    AStream.Free;
  end;
end;

procedure TGridLayoutHelpers.SaveConditionalFormattingToXML(AView: TcxGridTableView);
var
  AStream: TMemoryStream;
  I: Integer;
  AWriter: TcxWriter;
  ARect: TRect;
  ACaption: string;
begin
  for I := 0 to AView.ConditionalFormatting.RuleCount - 1 do
  begin
    AStream := TMemoryStream.Create;
    AStream.Position := 0;
    AWriter := TcxWriter.Create(AStream);
    try
      AView.ConditionalFormatting.Rules[I].SaveToStream(AWriter);
      ARect := AView.ConditionalFormatting.Rules[I].Area;

      AStream.Position := 0;
      ACaption := IntToStr(I);

      DoWriteData('_ConditionalFormatting_' + ACaption, 'ColumnName', AView.Columns[ARect.Left].Name);
      DoWriteData('_ConditionalFormatting_' + ACaption, 'RuleClass', AView.ConditionalFormatting.Rules[I].ClassName);
      DoWriteData('_ConditionalFormatting_' + ACaption, 'RuleStream', StreamToBinary(AStream));
    finally
      AWriter.Free;
      AStream.Free;
    end;
  end;
end;

{$IFNDEF NOPROGPREFS}

function TGridLayoutHelpers.SaveViewFeaturesToProgPrefs(AView: TcxGridTableView;
  const AContainerName, AStationID: string): string;
var
  AStream: TStream;
begin
  AStream := TMemoryStream.Create;
  try
    SaveViewFeaturesToStream(AView, AStream);
    GenHelpers.ProgPrefStore(AContainerName + AView.Name + 'ViewFeatures', AStationID, TMemoryStream(AStream));
  finally
    AStream.Free;
  end;
end;
{$ENDIF}

function TGridLayoutHelpers.SaveViewFeaturesToStream(AView: TcxGridTableView; var AStream: TStream): string;
begin
  SaveViewFeaturesToString(AView);
  AppXMLFileStorage.Xml.SaveToStream(AStream, TJclStringEncoding.seAnsi);
end;

function TGridLayoutHelpers.SaveViewFeaturesToString(AView: TcxGridTableView): string;
var
  I: Integer;
  colName, AValue: string;
begin
  AppXMLFileStorage.Xml.Root.Clear;

  for I := 0 to AView.ColumnCount - 1 do
  begin
    if AView is TcxGridDBBandedTableView then
      colName := (AView.Columns[I] as TcxGridDBBandedColumn).DataBinding.FieldName
    else if AView is TcxGridDBTableView then
      colName := (AView.Columns[I] as TcxGridDBColumn).DataBinding.FieldName
    else
      colName := '';

    if ColumnUnboundHelper.isUnbound(AView.Columns[I]) then
    begin
      colName := AView.Columns[I].Name;
      DoWriteData('_Column_' + AView.Columns[I].Name, 'Unbound', ColumnUnboundHelper.unboundColumnToString(AView,
        colName));
    end;

    AValue := ColumnStyleHelper.conditionStylesToString(AView, colName);
    if AValue <> '' then
      DoWriteData('_Column_' + AView.VisibleColumns[I].Name, 'Style', AValue);

    if AView.Columns[I].Summary.FooterKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'FooterKind');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'FooterFormat');
    end;

    if AView.Columns[I].Summary.GroupFooterKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFooterKind');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFooterFormat');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'SortByGroupFooterSummary');
    end;

    if AView.Columns[I].Summary.GroupKind <> skNone then
    begin
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupKind');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'GroupFormat');
      DoWriteProperty('_Column_' + AView.Columns[I].Name, AView.Columns[I].Summary, 'SortByGroupSummary');
    end;
  end;

  if AView.ConditionalFormatting.RuleCount > 0 then
    SaveConditionalFormattingToXML(AView);
  // DoWriteData('_ConditionalFormatting_', 'ConditionalFormatting', SaveConditionalFormattingToString(AView));

  Result := AppXMLFileStorage.Xml.XMLData;
end;

{ function TGridLayoutHelpers.SummaryKindToString(AKind: string): TcxSummaryKind;
  begin
  Result := skNone;
  if SameText(AKind, 'skSum') then
  Result := skSum
  else if SameText(AKind, 'skMin') then
  Result := skMin
  else if SameText(AKind, 'skMax') then
  Result := skMax
  else if SameText(AKind, 'skCount') then
  Result := skCount
  else if SameText(AKind, 'skAverage') then
  Result := skAverage;
  end;

  function TGridLayoutHelpers.SummaryStringToKind(AKind: TcxSummaryKind): string;
  begin
  Result := '';
  if AKind = skSum then
  Result := 'skSum'
  else if AKind = skMin then
  Result := 'skMin'
  else if AKind = skMax then
  Result := 'skMax'
  else if AKind = skCount then
  Result := 'skCount'
  else if AKind = skAverage then
  Result := 'skAverage';
  end; }

end.

