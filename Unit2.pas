unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxLabel, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDataControllerConditionalFormattingRulesManagerDialog, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxMemo, dxSkinBasic, dxUIAClasses, dxSkinWXI,
  dxLayoutcxEditAdapters, dxLayoutControlAdapters, dxLayoutContainer, dxLayoutControl, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP, cxCheckBox;

type
  TfrmActions = class(TForm)
    cmbAction: TcxComboBox;
    btnApply: TcxButton;
    txtParams: TcxTextEdit;
    btnExtract: TcxButton;
    txtActionValue: TcxMemo;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutGroup3: TdxLayoutGroup;
    dxLayoutItem1: TdxLayoutItem;
    liTxtActionValue: TdxLayoutItem;
    liTxtParams: TdxLayoutItem;
    dxLayoutItem4: TdxLayoutItem;
    liBtnExtract: TdxLayoutItem;
    liCmbScripts: TdxLayoutItem;
    cmbScripts: TcxComboBox;
    IdFTP1: TIdFTP;
    dxLayoutAutoCreatedGroup2: TdxLayoutAutoCreatedGroup;
    liChkPostMQTT: TdxLayoutItem;
    chkPostMQTT: TcxCheckBox;
    dxLayoutAutoCreatedGroup1: TdxLayoutAutoCreatedGroup;
    procedure btnApplyClick(Sender: TObject);
    procedure cmbActionPropertiesEditValueChanged(Sender: TObject);
    procedure btnExtractClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure PostActionToDB;
    procedure SendShellExecuteMess(const AAction: string);
    procedure LoadScriptsFromFTP;
  public
    { Public declarations }
  end;

var
  frmActions: TfrmActions;

implementation

uses
  Unit1, uCdMongo, System.JSON, CryptLib, Vcl.Clipbrd;

{$R *.dfm}

procedure TfrmActions.btnApplyClick(Sender: TObject);
begin
  if SameText(cmbAction.Text, 'ShellExecute') then
    SendShellExecuteMess('Run')
  else
    PostActionToDB;
  Close;
end;

procedure TfrmActions.btnExtractClick(Sender: TObject);
begin
  SendShellExecuteMess('Clipboard');
end;

procedure TfrmActions.cmbActionPropertiesEditValueChanged(Sender: TObject);
begin
  liTxtActionValue.Visible := SameText(cmbAction.Text, 'DownloadFTP') or SameText(cmbAction.Text, 'SetKeyValue') or
                    SameText(cmbAction.Text, 'UpdateWicAPL') or SameText(cmbAction.Text, 'ExecSQL') or 
                    SameText(cmbAction.Text, 'ExecSQL_RepDefs') or SameText(cmbAction.Text, 'GetDataValue') or
                    SameText(cmbAction.Text, 'SetDTOS');
  liTxtParams.Visible := SameText(cmbAction.Text, 'ShellExecute');
  liBtnExtract.Visible := SameText(cmbAction.Text, 'ShellExecute');
  liCmbScripts.Visible := SameText(cmbAction.Text, 'RunScript');
  liChkPostMQTT.Visible := SameText(cmbAction.Text, 'RunScript') or SameText(cmbAction.Text, 'GetDataValue');

  if SameText(cmbAction.Text, 'SetKeyValue') then
    txtActionValue.Text := '{"Key":"Value","Key2":"Value2"}'
  else if SameText(cmbAction.Text, 'DownloadFTP') then
    txtActionValue.Text := '{"DownloadURL":"", "FileName":"", "RunInFolder":"", "RunApp":"", "Params":""}'
  else if SameText(cmbAction.Text, 'RunScript') and (cmbScripts.Properties.Items.Count=0) then
    LoadScriptsFromFTP;
end;

procedure TfrmActions.FormShow(Sender: TObject);
begin
  cmbAction.Text := 'RunScript';
  chkPostMQTT.Checked := True;
end;

procedure TfrmActions.LoadScriptsFromFTP;
begin
  IdFTP1.Host := 'connect.compudime.com';
  IdFTP1.Port := 2108;
  IdFTP1.Username := 'compudime';
  IdFTP1.Password := 'Mazel123$';
  IdFTP1.ConnectTimeout := 60000;
  IdFTP1.ReadTimeout := 60000;
  IdFTP1.Passive := True;
  IdFTP1.Connect;
  try
    IdFTP1.ChangeDir('/Scripts/');
    var AList: TStringList := TStringList.Create;
    IdFTP1.List(AList, '*.ps1', False);
    for var I := 0 to AList.Count - 1 do
      cmbScripts.Properties.Items.Add(AList[I]);
  finally
    IdFTP1.Disconnect;
  end;
end;

procedure TfrmActions.PostActionToDB;
var
  AColumn: TcxGridColumn;
  I, AEnterpriseI, AStoreI: Integer;
  AEnterprise, AStore, ACollectionName, AAction, AActionValue: string;
begin
  ACollectionName := 'CompuDashMain';

  AEnterpriseI := 0;
  AStoreI := 0;
  AColumn := frmViewer.TableView1.GetColumnByFieldName('enterprise_name');
  if Assigned(AColumn) then
    AEnterpriseI := AColumn.Index;

  AColumn := frmViewer.TableView1.GetColumnByFieldName('store_name');
  if Assigned(AColumn) then
    AStoreI := AColumn.Index;

  if (AEnterpriseI = 0) or (AStoreI = 0) then
    raise Exception.Create('Column Enterprise_name and store_name not found.');

  AAction := cmbAction.Text;
  AActionValue := txtActionValue.Text;
  if AAction = '' then
    raise Exception.Create('No Action selected.');

  if (AActionValue='') and SameText(AAction, 'UpdateWicAPL') then
    AActionValue := 'Download';

  if (AActionValue='') and SameText(AAction, 'PushEizer') then
    AActionValue := 'Upload';

  if SameText(AAction, 'RunScript') then
  begin
    AAction := 'DownloadFTP';
    AActionValue := '{"DownloadURL":"Scripts//'+cmbScripts.Text+'", "FileName":"C:\\RunPos\\Temp\\'+cmbScripts.Text+'", "RunApp":"'+cmbScripts.Text+'"}';
  end;

  cdMongo.DoConnect('compudime', ACollectionName);
  for I := 0 to frmViewer.TableView1.Controller.SelectedRecordCount - 1 do
  begin
    AEnterprise := frmViewer.TableView1.Controller.SelectedRecords[I].Values[AEnterpriseI];
    AStore := frmViewer.TableView1.Controller.SelectedRecords[I].Values[AStoreI];
    cdMongo.MyValueSetGet(ACollectionName, AEnterprise, AStore, 'action', AAction);
    cdMongo.MyValueSetGet(ACollectionName, AEnterprise, AStore, 'action_value', AActionValue);
  end;
  cdMongo.DoClose;
  if chkPostMQTT.Checked then
    frmViewer.MQTTRefresh;
end;

procedure TfrmActions.SendShellExecuteMess(const AAction: string);
const
  AKey = '8272835245346f4d9f8503266807f9cf30d34a6a5e511d49bac8a0854bcb7c3c';
var
  AColumn: TcxGridColumn;
  I, AEnterpriseI, AStoreI: Integer;
  AEnterprise, AStore, AJsonS: string;
  AJson: TJSONObject;
begin
  AEnterpriseI := 0;
  AStoreI := 0;
  AColumn := frmViewer.TableView1.GetColumnByFieldName('enterprise_name');
  if Assigned(AColumn) then
    AEnterpriseI := AColumn.Index;

  AColumn := frmViewer.TableView1.GetColumnByFieldName('store_name');
  if Assigned(AColumn) then
    AStoreI := AColumn.Index;

  if (AEnterpriseI = 0) or (AStoreI = 0) then
    raise Exception.Create('Column Enterprise_name and store_name not found.');

  if txtActionValue.Text = '' then
    raise Exception.Create('No Value Set - enter program to execute in Value prompt.');

  AJson := TJSONObject.Create;
  AJson.AddPair('Action', StrEncryptV3(txtActionValue.Text, AKey));
  if txtParams.Text<>'' then
    AJson.AddPair('Values', StrEncryptV3(txtParams.Text, AKey));
  AJsonS := AJson.ToString;
  AJson.Free;

  if SameText(AAction, 'Clipboard') then
  begin
    Clipboard.AsText := AJsonS;
    Exit;
  end;

  for I := 0 to frmViewer.TableView1.Controller.SelectedRecordCount - 1 do
  begin
    AEnterprise := frmViewer.TableView1.Controller.SelectedRecords[I].Values[AEnterpriseI];
    AStore := frmViewer.TableView1.Controller.SelectedRecords[I].Values[AStoreI];
    frmViewer.MQTTPublishToClient(AEnterprise, AStore, AJsonS);
  end;
end;

end.
