object frmColumnUnbound: TfrmColumnUnbound
  Left = 0
  Top = 0
  AlphaBlend = True
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  BorderWidth = 1
  Caption = 'unbound column'
  ClientHeight = 300
  ClientWidth = 623
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMode = pmAuto
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 623
    Height = 300
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = dxLayoutCxLookAndFeel1
    OptionsItem.AllowFloatingGroups = True
    OptionsItem.SizableHorz = True
    OptionsItem.SizableVert = True
    object btnSave: TcxButton
      Left = 457
      Top = 265
      Width = 75
      Height = 25
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 2
      OnClick = btnSaveClick
    end
    object btnClose: TcxButton
      Left = 538
      Top = 265
      Width = 75
      Height = 25
      Caption = 'Close'
      ModalResult = 2
      TabOrder = 3
    end
    object btnReset: TcxButton
      Left = 10
      Top = 265
      Width = 75
      Height = 25
      Caption = 'Delete'
      ModalResult = 14
      TabOrder = 1
    end
    object cxPageControl: TcxPageControl
      Left = 10
      Top = 10
      Width = 603
      Height = 249
      TabOrder = 0
      Properties.ActivePage = tshWSEditor
      Properties.CustomButtons.Buttons = <>
      Properties.HideTabs = True
      Properties.Style = 5
      ClientRectBottom = 249
      ClientRectRight = 603
      ClientRectTop = 0
      object tshExpressions: TcxTabSheet
        Caption = 'tshExpressions'
        ImageIndex = 0
        object dxLayoutControl1: TdxLayoutControl
          Left = 0
          Top = 0
          Width = 603
          Height = 249
          Align = alClient
          TabOrder = 0
          LayoutLookAndFeel = dxLayoutCxLookAndFeel1
          object editCaption: TcxTextEdit
            Left = 56
            Top = 10
            Style.HotTrack = False
            TabOrder = 0
            Width = 166
          end
          object editType: TcxComboBox
            Left = 294
            Top = 10
            Properties.DropDownListStyle = lsEditFixedList
            Properties.OnChange = editTypePropertiesChange
            Style.HotTrack = False
            TabOrder = 1
            Width = 140
          end
          object editExpModeValue: TcxRadioButton
            Left = 22
            Top = 93
            Width = 75
            Height = 17
            Caption = 'only value'
            Checked = True
            TabOrder = 5
            TabStop = True
            OnClick = editExpModeValueClick
            GroupIndex = 1
            Transparent = True
          end
          object editExpModeCondition: TcxRadioButton
            Left = 103
            Top = 93
            Width = 113
            Height = 17
            Caption = 'value by condition'
            TabOrder = 6
            OnClick = editExpModeValueClick
            GroupIndex = 1
            Transparent = True
          end
          object lblExpressions1: TcxLabel
            Left = 10
            Top = 128
            Caption = 'Set expression of unbound column where:'
            Style.HotTrack = False
            Transparent = True
          end
          object editMainExpression: TcxTextEdit
            Left = 10
            Top = 151
            Properties.ReadOnly = True
            Style.HotTrack = False
            TabOrder = 12
            Width = 505
          end
          object btnClearMainExp: TcxButton
            Left = 521
            Top = 151
            Width = 33
            Height = 25
            Caption = 'Edit'
            TabOrder = 13
            OnClick = btnEditMainExpClick
          end
          object btnEditMainExp: TcxButton
            Left = 560
            Top = 151
            Width = 33
            Height = 25
            Caption = 'Clear'
            TabOrder = 14
            OnClick = btnClearMainExpClick
          end
          object editExpModeSQLValue: TcxRadioButton
            Left = 222
            Top = 93
            Width = 69
            Height = 17
            Caption = 'only SQL'
            TabOrder = 7
            OnClick = editExpModeValueClick
            GroupIndex = 1
            Transparent = True
          end
          object editExpModeSQLCondition: TcxRadioButton
            Left = 297
            Top = 93
            Width = 104
            Height = 17
            Caption = 'SQL by condition'
            TabOrder = 8
            OnClick = editExpModeValueClick
            GroupIndex = 1
            Transparent = True
          end
          object editExpModeWSValue: TcxRadioButton
            Left = 407
            Top = 93
            Width = 65
            Height = 17
            Caption = 'only WS'
            TabOrder = 9
            OnClick = editExpModeValueClick
            GroupIndex = 1
            Transparent = True
          end
          object editExpModeWSCondition: TcxRadioButton
            Left = 478
            Top = 93
            Width = 102
            Height = 17
            Caption = 'WS by condition'
            TabOrder = 10
            OnClick = editExpModeValueClick
            GroupIndex = 1
            Transparent = True
          end
          object editMask: TcxTextEdit
            Left = 473
            Top = 10
            Style.HotTrack = False
            TabOrder = 3
            Width = 120
          end
          object editTargetField: TcxComboBox
            Left = 294
            Top = 37
            Properties.DropDownListStyle = lsEditFixedList
            Style.HotTrack = False
            TabOrder = 2
            Width = 140
          end
          object editWidth: TcxSpinEdit
            Left = 530
            Top = 37
            Properties.MaxValue = 1000.000000000000000000
            Properties.MinValue = 20.000000000000000000
            Style.HotTrack = False
            TabOrder = 4
            Value = 100
            Width = 63
          end
          object dxLayoutControl1Group_Root: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avTop
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            Hidden = True
            ShowBorder = False
            Index = -1
          end
          object dxLayoutItem1: TdxLayoutItem
            Parent = dxLayoutControl1Group1
            AlignHorz = ahClient
            CaptionOptions.Text = 'Caption:'
            Control = editCaption
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 121
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl1Item2: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup11
            AlignVert = avClient
            CaptionOptions.Text = 'Type:'
            Control = editType
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 120
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutGroup1: TdxLayoutGroup
            Parent = dxLayoutControl1Group_Root
            AlignHorz = ahClient
            CaptionOptions.Text = 'Expression mode'
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            Index = 2
          end
          object dxLayoutControl1Item4: TdxLayoutItem
            Parent = dxLayoutGroup1
            AlignHorz = ahLeft
            CaptionOptions.Text = 'cxRadioButton2'
            CaptionOptions.Visible = False
            Control = editExpModeValue
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl1Item3: TdxLayoutItem
            Parent = dxLayoutGroup1
            AlignHorz = ahLeft
            CaptionOptions.Text = 'cxRadioButton1'
            CaptionOptions.Visible = False
            Control = editExpModeCondition
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 113
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl1Item7: TdxLayoutItem
            Parent = dxLayoutControl1Group_Root
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = lblExpressions1
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 121
            ControlOptions.ShowBorder = False
            Index = 3
          end
          object dxLayoutControl1Group1: TdxLayoutGroup
            Parent = dxLayoutControl1Group_Root
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ItemIndex = 1
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object layoutExpression: TdxLayoutGroup
            Parent = dxLayoutControl1Group_Root
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 4
          end
          object dxLayoutControl1Item9: TdxLayoutItem
            Parent = layoutExpression
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editMainExpression
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 504
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl1Item1: TdxLayoutItem
            Parent = layoutExpression
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnClearMainExp
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 33
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl1Item6: TdxLayoutItem
            Parent = layoutExpression
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnEditMainExp
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 33
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object dxLayoutControl1Item8: TdxLayoutItem
            Parent = dxLayoutGroup1
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editExpModeSQLValue
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 69
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object dxLayoutControl1Item10: TdxLayoutItem
            Parent = dxLayoutGroup1
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editExpModeSQLCondition
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 104
            ControlOptions.ShowBorder = False
            Index = 3
          end
          object dxLayoutControl1Item11: TdxLayoutItem
            Parent = dxLayoutGroup1
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editExpModeWSValue
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 65
            ControlOptions.ShowBorder = False
            Index = 4
          end
          object dxLayoutControl1Item12: TdxLayoutItem
            Parent = dxLayoutGroup1
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editExpModeWSCondition
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 102
            ControlOptions.ShowBorder = False
            Index = 5
          end
          object layoutMask: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup8
            AlignHorz = ahRight
            CaptionOptions.Text = 'Mask:'
            Visible = False
            Control = editMask
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 120
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl1Group3: TdxLayoutGroup
            Parent = dxLayoutControl1Group_Root
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object layoutTargetField: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup11
            CaptionOptions.Text = 'Target Field:'
            Control = editTargetField
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 140
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl1Item5: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup8
            AlignHorz = ahRight
            CaptionOptions.Text = 'Width:'
            Control = editWidth
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 63
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutAutoCreatedGroup10: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutControl1Group1
            AlignHorz = ahRight
            AlignVert = avTop
            LayoutDirection = ldHorizontal
            Index = 1
          end
          object dxLayoutAutoCreatedGroup11: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutAutoCreatedGroup10
            AlignHorz = ahClient
            Index = 0
          end
          object dxLayoutAutoCreatedGroup8: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutAutoCreatedGroup10
            AlignHorz = ahRight
            Index = 1
          end
        end
      end
      object tshExpEditor: TcxTabSheet
        Caption = 'tshExpEditor'
        ImageIndex = 1
        object dxLayoutControl2: TdxLayoutControl
          Left = 0
          Top = 0
          Width = 603
          Height = 249
          Align = alClient
          TabOrder = 0
          LayoutLookAndFeel = dxLayoutCxLookAndFeel1
          object editExp: TcxRichEdit
            Left = 10
            Top = 98
            Properties.SelectionBar = True
            Properties.WantReturns = False
            Style.HotTrack = False
            TabOrder = 12
            Height = 110
            Width = 489
          end
          object editTypeValue: TcxComboBox
            Left = 245
            Top = 28
            Properties.DropDownListStyle = lsEditFixedList
            Properties.OnChange = editTypeValuePropertiesChange
            Style.HotTrack = False
            TabOrder = 2
            Width = 120
          end
          object editTextValue: TcxTextEdit
            Left = 212
            Top = 55
            Style.HotTrack = False
            TabOrder = 3
            Width = 31
          end
          object editNumberValue: TcxSpinEdit
            Left = 249
            Top = 55
            Style.HotTrack = False
            TabOrder = 4
            Width = 32
          end
          object editDateValue: TcxDateEdit
            Left = 287
            Top = 55
            Style.HotTrack = False
            TabOrder = 5
            Width = 31
          end
          object btnPasteValue: TcxButton
            Left = 324
            Top = 55
            Width = 41
            Height = 25
            Caption = 'Paste'
            TabOrder = 6
            OnClick = btnPasteValueClick
          end
          object btnPasteField: TcxButton
            Left = 141
            Top = 55
            Width = 41
            Height = 25
            Caption = 'Paste'
            TabOrder = 1
            OnClick = btnPasteFieldClick
          end
          object editFieldName: TcxComboBox
            Left = 58
            Top = 28
            Properties.DropDownListStyle = lsEditFixedList
            Style.HotTrack = False
            TabOrder = 0
            Width = 124
          end
          object editSQLName: TcxComboBox
            Left = 452
            Top = 28
            Properties.DropDownListStyle = lsEditFixedList
            Style.HotTrack = False
            TabOrder = 7
            Width = 124
          end
          object btnPasteSQL: TcxButton
            Left = 488
            Top = 55
            Width = 41
            Height = 25
            Caption = 'Paste'
            TabOrder = 8
            OnClick = btnPasteSQLClick
          end
          object btnEditSQL: TcxButton
            Left = 535
            Top = 55
            Width = 41
            Height = 25
            Caption = 'Edit'
            TabOrder = 9
            OnClick = btnEditSQLClick
          end
          object btnBackExp: TcxButton
            Left = 518
            Top = 214
            Width = 75
            Height = 25
            Caption = 'Back'
            TabOrder = 19
            OnClick = btnBackExpClick
          end
          object btnPasteBrace: TcxButton
            Left = 505
            Top = 98
            Width = 41
            Height = 25
            Caption = '(...)'
            TabOrder = 13
            OnClick = btnPasteBraceClick
          end
          object btnPasteDate: TcxButton
            Left = 552
            Top = 98
            Width = 41
            Height = 25
            Caption = 'date()'
            TabOrder = 14
            OnClick = btnPasteDateClick
          end
          object btnPasteAdd: TcxButton
            Left = 505
            Top = 129
            Width = 25
            Height = 25
            Caption = '+'
            TabOrder = 15
            OnClick = btnPasteAddClick
          end
          object btnPasteMin: TcxButton
            Left = 536
            Top = 129
            Width = 25
            Height = 25
            Caption = '-'
            TabOrder = 16
            OnClick = btnPasteMinClick
          end
          object bntPasteMul: TcxButton
            Left = 567
            Top = 129
            Width = 25
            Height = 25
            Caption = '*'
            TabOrder = 17
            OnClick = bntPasteMulClick
          end
          object rbSQL: TcxRadioButton
            Left = 395
            Top = 55
            Width = 40
            Height = 17
            Caption = 'SQL'
            Checked = True
            TabOrder = 10
            TabStop = True
            OnClick = rbSQLClick
            GroupIndex = 2
            Transparent = True
          end
          object rbWS: TcxRadioButton
            Left = 441
            Top = 55
            Width = 40
            Height = 17
            Caption = 'WS'
            TabOrder = 11
            OnClick = rbSQLClick
            GroupIndex = 2
            Transparent = True
          end
          object btnSetExp: TcxButton
            Left = 437
            Top = 214
            Width = 75
            Height = 25
            Caption = 'Set'
            TabOrder = 18
            OnClick = btnSetExpClick
          end
          object dxLayoutControl2Group_Root: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            Hidden = True
            ShowBorder = False
            Index = -1
          end
          object dxLayoutControl2Item1: TdxLayoutItem
            Parent = dxLayoutControl2Group3
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editExp
            ControlOptions.OriginalHeight = 94
            ControlOptions.OriginalWidth = 488
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Item2: TdxLayoutItem
            Parent = layoutAddValue
            CaptionOptions.Text = 'Type:'
            Control = editTypeValue
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 120
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutAddValue: TdxLayoutGroup
            Parent = dxLayoutControl2Group5
            AlignHorz = ahLeft
            CaptionOptions.Text = 'Add value'
            ButtonOptions.Buttons = <>
            Index = 1
          end
          object layoutExpFunc: TdxLayoutGroup
            Parent = dxLayoutControl2Group3
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Group3: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutControl2Group_Root
            AlignHorz = ahClient
            AlignVert = avClient
            LayoutDirection = ldHorizontal
            Index = 1
          end
          object layoutText: TdxLayoutItem
            Parent = dxLayoutControl2Group2
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = editTextValue
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 30
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutNumber: TdxLayoutItem
            Parent = dxLayoutControl2Group2
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Visible = False
            Control = editNumberValue
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 30
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Group2: TdxLayoutGroup
            Parent = layoutAddValue
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object layoutDate: TdxLayoutItem
            Parent = dxLayoutControl2Group2
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Visible = False
            Control = editDateValue
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 30
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object dxLayoutControl2Item8: TdxLayoutItem
            Parent = dxLayoutControl2Group2
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteValue
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 3
          end
          object dxLayoutControl2Item10: TdxLayoutItem
            Parent = dxLayoutControl2Group4
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteField
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Item9: TdxLayoutItem
            Parent = layoutAddField
            CaptionOptions.Text = 'Name:'
            Control = editFieldName
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 124
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Group4: TdxLayoutGroup
            Parent = layoutAddField
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ShowBorder = False
            Index = 1
          end
          object layoutAddField: TdxLayoutGroup
            Parent = dxLayoutControl2Group5
            AlignHorz = ahLeft
            CaptionOptions.Text = 'Add field'
            ButtonOptions.Buttons = <>
            Index = 0
          end
          object dxLayoutControl2Group5: TdxLayoutGroup
            Parent = dxLayoutControl2Group_Root
            AlignVert = avTop
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object layoutAddSQL: TdxLayoutGroup
            Parent = dxLayoutControl2Group5
            AlignHorz = ahLeft
            CaptionOptions.Text = 'Add SQL'
            ButtonOptions.Buttons = <>
            Index = 2
          end
          object layoutSQLName: TdxLayoutItem
            Parent = layoutAddSQL
            CaptionOptions.Text = 'SQL name:'
            Control = editSQLName
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 124
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Group7: TdxLayoutGroup
            Parent = layoutAddSQL
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object layoutPasteSQL: TdxLayoutItem
            Parent = dxLayoutControl2Group7
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutEditSQL: TdxLayoutItem
            Parent = dxLayoutControl2Group7
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnEditSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Item18: TdxLayoutItem
            Parent = dxLayoutControl2Group6
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnBackExp
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Group6: TdxLayoutGroup
            Parent = dxLayoutControl2Group_Root
            AlignVert = avBottom
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 2
          end
          object dxLayoutControl2Group9: TdxLayoutGroup
            Parent = layoutExpFunc
            AlignVert = avTop
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Item20: TdxLayoutItem
            Parent = dxLayoutControl2Group9
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteBrace
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Item19: TdxLayoutItem
            Parent = dxLayoutControl2Group9
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteDate
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Group10: TdxLayoutGroup
            Parent = layoutExpFunc
            AlignVert = avTop
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Item22: TdxLayoutItem
            Parent = dxLayoutControl2Group10
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteAdd
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 25
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl2Item21: TdxLayoutItem
            Parent = dxLayoutControl2Group10
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteMin
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 25
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl2Item23: TdxLayoutItem
            Parent = dxLayoutControl2Group10
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = bntPasteMul
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 25
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object layoutSelectSQL: TdxLayoutItem
            Parent = dxLayoutControl2Group7
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = rbSQL
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 40
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object layoutSelectWS: TdxLayoutItem
            Parent = dxLayoutControl2Group7
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = rbWS
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 17
            ControlOptions.OriginalWidth = 40
            ControlOptions.ShowBorder = False
            Index = 3
          end
          object dxLayoutControl2Item17: TdxLayoutItem
            Parent = dxLayoutControl2Group6
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnSetExp
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 0
          end
        end
      end
      object tshSQLEditor: TcxTabSheet
        Caption = 'tshSQLEditor'
        ImageIndex = 2
        object dxLayoutControl3: TdxLayoutControl
          Left = 0
          Top = 0
          Width = 603
          Height = 249
          Align = alClient
          TabOrder = 0
          LayoutLookAndFeel = dxLayoutCxLookAndFeel1
          object lstSQL: TcxListBox
            Left = 437
            Top = 41
            Width = 156
            Height = 136
            ItemHeight = 13
            TabOrder = 11
            OnClick = lstSQLClick
          end
          object btnPasteFieldToSQL: TcxButton
            Left = 197
            Top = 10
            Width = 41
            Height = 25
            Caption = 'Paste'
            TabOrder = 1
            OnClick = btnPasteFieldToSQLClick
          end
          object editFieldNameForSQL: TcxComboBox
            Left = 70
            Top = 10
            Properties.DropDownListStyle = lsEditFixedList
            Style.HotTrack = False
            TabOrder = 0
            Width = 121
          end
          object editSQLAsstField: TcxComboBox
            Left = 364
            Top = 10
            Style.HotTrack = False
            TabOrder = 2
            Width = 121
          end
          object btnSetSQLName: TcxButton
            Left = 491
            Top = 10
            Width = 41
            Height = 25
            Caption = 'Set'
            TabOrder = 3
            OnClick = btnSetSQLNameClick
          end
          object btnAddSQL: TcxButton
            Left = 437
            Top = 183
            Width = 75
            Height = 25
            Caption = 'Add'
            TabOrder = 6
            OnClick = btnAddSQLClick
          end
          object btnDelSQL: TcxButton
            Left = 518
            Top = 183
            Width = 75
            Height = 25
            Caption = 'Delete'
            TabOrder = 8
            OnClick = btnDelSQLClick
          end
          object btnBackFromSQL: TcxButton
            Left = 518
            Top = 214
            Width = 75
            Height = 25
            Caption = 'Back'
            TabOrder = 9
            OnClick = btnBackFromSQLClick
          end
          object btnSetSQL: TcxButton
            Left = 437
            Top = 214
            Width = 75
            Height = 25
            Caption = 'Set'
            TabOrder = 7
            OnClick = btnSetSQLClick
          end
          object btnTrySQL: TcxButton
            Left = 538
            Top = 10
            Width = 55
            Height = 25
            Caption = 'Try SQL'
            TabOrder = 4
            OnClick = btnTrySQLClick
          end
          object editSQLUpdateLimit: TcxSpinEdit
            Left = 331
            Top = 183
            Properties.AssignedValues.MinValue = True
            Properties.OnChange = editSQLUpdateLimitPropertiesChange
            Style.HotTrack = False
            TabOrder = 5
            Width = 100
          end
          object SQLPageControl: TcxPageControl
            Left = 11
            Top = 41
            Width = 420
            Height = 133
            TabOrder = 10
            Properties.ActivePage = tshSQLSelect
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            Properties.NavigatorPosition = npRightBottom
            Properties.TabPosition = tpBottom
            ClientRectBottom = 133
            ClientRectRight = 420
            ClientRectTop = 0
            object tshSQLSelect: TcxTabSheet
              Caption = 'Select'
              ImageIndex = 0
              OnShow = tshSQLUpdateShow
              object editSQLExp: TcxRichEdit
                Left = 0
                Top = 0
                Align = alClient
                Properties.OnChange = editSQLExpPropertiesChange
                Style.BorderColor = clWindowFrame
                Style.BorderStyle = ebsNone
                Style.HotTrack = False
                TabOrder = 0
                Height = 133
                Width = 420
              end
            end
            object tshSQLUpdate: TcxTabSheet
              Caption = 'Update'
              ImageIndex = 1
              OnShow = tshSQLUpdateShow
              object editSQLUpdate: TcxRichEdit
                Left = 0
                Top = 0
                Align = alClient
                Properties.OnChange = editSQLExpPropertiesChange
                Style.BorderColor = clWindowFrame
                Style.BorderStyle = ebsNone
                Style.HotTrack = False
                TabOrder = 0
                Height = 133
                Width = 420
              end
            end
          end
          object dxLayoutControl3Group_Root: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avClient
            ButtonOptions.Buttons = <>
            Hidden = True
            ItemIndex = 1
            ShowBorder = False
            Index = -1
          end
          object layoutSQLExp: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup7
            AlignHorz = ahRight
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Padding.Bottom = 3
            Padding.AssignedValues = [lpavBottom]
            Control = SQLPageControl
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 121
            ControlOptions.OriginalWidth = 420
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl3Item3: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup7
            AlignHorz = ahRight
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = lstSQL
            ControlOptions.OriginalHeight = 120
            ControlOptions.OriginalWidth = 156
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl3Group1: TdxLayoutGroup
            Parent = dxLayoutControl3Group_Root
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object layoutPasteToSQL: TdxLayoutItem
            Parent = layoutTopSQL
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteFieldToSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutControl3Item2: TdxLayoutItem
            Parent = layoutTopSQL
            AlignHorz = ahLeft
            CaptionOptions.Text = 'Field name:'
            Control = editFieldNameForSQL
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 121
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutTopSQL: TdxLayoutGroup
            Parent = dxLayoutControl3Group_Root
            AlignVert = avTop
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object dxLayoutControl3Item5: TdxLayoutItem
            Parent = layoutTopSQL
            AlignHorz = ahRight
            CaptionOptions.Text = 'SQL field:'
            Control = editSQLAsstField
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 121
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object layoutSQLSet: TdxLayoutItem
            Parent = layoutTopSQL
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnSetSQLName
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 3
          end
          object layoutAddSQLButton: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup6
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnAddSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutDeleteSQLButton: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup1
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnDelSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutControl3Item10: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup1
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnBackFromSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object layoutSetSQL: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup6
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Visible = False
            Control = btnSetSQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object layoutTrySQL: TdxLayoutItem
            Parent = layoutTopSQL
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnTrySQL
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 55
            ControlOptions.ShowBorder = False
            Index = 4
          end
          object layoutStaleData: TdxLayoutItem
            Parent = layoutBottomSQL
            AlignHorz = ahRight
            CaptionOptions.Text = 'Stale data after (sec):'
            Control = editSQLUpdateLimit
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 100
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutBottomSQL: TdxLayoutGroup
            Parent = dxLayoutControl3Group7
            AlignVert = avBottom
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ItemIndex = 2
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object dxLayoutControl3Group7: TdxLayoutGroup
            Parent = dxLayoutControl3Group1
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ShowBorder = False
            Index = 0
          end
          object dxLayoutLabeledItem1: TdxLayoutLabeledItem
            Parent = layoutBottomSQL
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 
              '[FieldName] will be converted into :Paramter, but will work only' +
              ' for Visible Columns'
            CaptionOptions.WordWrap = True
            Index = 3
          end
          object dxLayoutAutoCreatedGroup7: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutControl3Group7
            AlignVert = avClient
            LayoutDirection = ldHorizontal
            Index = 1
          end
          object dxLayoutAutoCreatedGroup1: TdxLayoutAutoCreatedGroup
            Parent = layoutBottomSQL
            AlignHorz = ahRight
            Index = 2
          end
          object dxLayoutAutoCreatedGroup6: TdxLayoutAutoCreatedGroup
            Parent = layoutBottomSQL
            AlignHorz = ahRight
            Index = 1
          end
        end
      end
      object tshWSEditor: TcxTabSheet
        Caption = 'tshWSEditor'
        ImageIndex = 3
        object dxLayoutControl4: TdxLayoutControl
          Left = 0
          Top = 0
          Width = 603
          Height = 249
          Align = alClient
          TabOrder = 0
          LayoutLookAndFeel = dxLayoutCxLookAndFeel1
          object lstWS: TcxListBox
            Left = 426
            Top = 41
            Width = 156
            Height = 136
            ItemHeight = 13
            TabOrder = 6
            OnClick = lstSQLClick
          end
          object btnPasteFieldToWS: TcxButton
            Left = 197
            Top = 10
            Width = 41
            Height = 25
            Caption = 'Paste'
            TabOrder = 1
            OnClick = btnPasteFieldToSQLClick
          end
          object editFieldNameForWS: TcxComboBox
            Left = 70
            Top = 10
            Properties.DropDownListStyle = lsEditFixedList
            Style.HotTrack = False
            TabOrder = 0
            Width = 121
          end
          object editWSAsstField: TcxComboBox
            Left = 364
            Top = 10
            Style.HotTrack = False
            TabOrder = 2
            Width = 121
          end
          object btnSetWSName: TcxButton
            Left = 491
            Top = 10
            Width = 41
            Height = 25
            Caption = 'Set'
            TabOrder = 3
            OnClick = btnSetSQLNameClick
          end
          object btnAddWS: TcxButton
            Left = 426
            Top = 183
            Width = 75
            Height = 25
            Caption = 'Add'
            TabOrder = 8
            OnClick = btnAddSQLClick
          end
          object btnDelWS: TcxButton
            Left = 507
            Top = 183
            Width = 75
            Height = 25
            Caption = 'Delete'
            TabOrder = 9
            OnClick = btnDelSQLClick
          end
          object btnBackFromWS: TcxButton
            Left = 507
            Top = 214
            Width = 75
            Height = 25
            Caption = 'Back'
            TabOrder = 11
            OnClick = btnBackFromSQLClick
          end
          object btnSetWS: TcxButton
            Left = 426
            Top = 214
            Width = 75
            Height = 25
            Caption = 'Set'
            TabOrder = 10
            OnClick = btnSetWSClick
          end
          object btnTryWS: TcxButton
            Left = 538
            Top = 10
            Width = 55
            Height = 25
            Caption = 'Try WS'
            TabOrder = 4
            OnClick = btnTrySQLClick
          end
          object editWSUpdateLimit: TcxSpinEdit
            Left = 320
            Top = 183
            Properties.AssignedValues.MinValue = True
            Properties.OnChange = editSQLUpdateLimitPropertiesChange
            Style.HotTrack = False
            TabOrder = 7
            Width = 100
          end
          object WSPageControl: TcxPageControl
            Left = 10
            Top = 41
            Width = 410
            Height = 133
            TabOrder = 5
            Properties.ActivePage = tshWSGet
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            Properties.TabPosition = tpBottom
            ClientRectBottom = 133
            ClientRectRight = 410
            ClientRectTop = 0
            object tshWSGet: TcxTabSheet
              Caption = 'Get'
              ImageIndex = 0
              OnShow = tshSQLUpdateShow
              object editWSExp: TcxRichEdit
                Left = 0
                Top = 0
                Align = alClient
                Properties.WantReturns = False
                Properties.OnChange = editSQLExpPropertiesChange
                Style.BorderColor = clWindowFrame
                Style.BorderStyle = ebsNone
                Style.HotTrack = False
                TabOrder = 0
                Height = 133
                Width = 410
              end
            end
            object tshWSPost: TcxTabSheet
              Caption = 'Post'
              ImageIndex = 1
              OnShow = tshSQLUpdateShow
              object editWSPost: TcxRichEdit
                Left = 0
                Top = 0
                Align = alClient
                Properties.WantReturns = False
                Properties.OnChange = editSQLExpPropertiesChange
                Style.BorderColor = clWindowFrame
                Style.BorderStyle = ebsNone
                Style.HotTrack = False
                TabOrder = 0
                Height = 133
                Width = 410
              end
            end
          end
          object dxLayoutGroup2: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avClient
            ButtonOptions.Buttons = <>
            Hidden = True
            ItemIndex = 1
            ShowBorder = False
            Index = -1
          end
          object layoutWSExp: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup4
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Padding.Bottom = 3
            Padding.AssignedValues = [lpavBottom]
            Control = WSPageControl
            ControlOptions.AutoColor = True
            ControlOptions.OriginalHeight = 121
            ControlOptions.OriginalWidth = 420
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object dxLayoutItem3: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup4
            AlignHorz = ahLeft
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = lstWS
            ControlOptions.OriginalHeight = 120
            ControlOptions.OriginalWidth = 156
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutGroup3: TdxLayoutGroup
            Parent = dxLayoutGroup2
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object layoutPasteToWS: TdxLayoutItem
            Parent = layoutTopWS
            AlignHorz = ahLeft
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnPasteFieldToWS
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutItem5: TdxLayoutItem
            Parent = layoutTopWS
            AlignHorz = ahLeft
            CaptionOptions.Text = 'Field name:'
            Control = editFieldNameForWS
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 121
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutTopWS: TdxLayoutGroup
            Parent = dxLayoutGroup2
            AlignVert = avTop
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object dxLayoutItem6: TdxLayoutItem
            Parent = layoutTopWS
            AlignHorz = ahRight
            CaptionOptions.Text = 'Web service field:'
            Control = editWSAsstField
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 121
            ControlOptions.ShowBorder = False
            Index = 2
          end
          object layoutWSSet: TdxLayoutItem
            Parent = layoutTopWS
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnSetWSName
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 41
            ControlOptions.ShowBorder = False
            Index = 3
          end
          object dxLayoutItem8: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup5
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnAddWS
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutDeleteWSButton: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup5
            AlignHorz = ahRight
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnDelWS
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutGroup5: TdxLayoutGroup
            Parent = dxLayoutGroup6
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 0
          end
          object dxLayoutGroup6: TdxLayoutGroup
            Parent = dxLayoutGroup3
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ShowBorder = False
            Index = 1
          end
          object dxLayoutItem10: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup3
            AlignVert = avClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnBackFromWS
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 1
          end
          object dxLayoutItem11: TdxLayoutItem
            Parent = dxLayoutAutoCreatedGroup3
            AlignHorz = ahClient
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Visible = False
            Control = btnSetWS
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 75
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutTryWS: TdxLayoutItem
            Parent = layoutTopWS
            AlignHorz = ahRight
            CaptionOptions.Text = 'New Item'
            CaptionOptions.Visible = False
            Control = btnTryWS
            ControlOptions.OriginalHeight = 25
            ControlOptions.OriginalWidth = 55
            ControlOptions.ShowBorder = False
            Index = 4
          end
          object dxLayoutControl4Item1: TdxLayoutItem
            Parent = layoutBottomWS
            AlignHorz = ahRight
            CaptionOptions.Text = 'Stale data after (sec):'
            Control = editWSUpdateLimit
            ControlOptions.OriginalHeight = 21
            ControlOptions.OriginalWidth = 100
            ControlOptions.ShowBorder = False
            Index = 0
          end
          object layoutBottomWS: TdxLayoutGroup
            Parent = dxLayoutControl4Group2
            AlignHorz = ahClient
            AlignVert = avBottom
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            LayoutDirection = ldHorizontal
            ShowBorder = False
            Index = 1
          end
          object dxLayoutControl4Group2: TdxLayoutGroup
            Parent = dxLayoutGroup3
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = 'New Group'
            CaptionOptions.Visible = False
            ButtonOptions.Buttons = <>
            ItemIndex = 1
            ShowBorder = False
            Index = 0
          end
          object dxLayoutAutoCreatedGroup4: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutControl4Group2
            AlignVert = avClient
            LayoutDirection = ldHorizontal
            Index = 0
          end
          object dxLayoutAutoCreatedGroup2: TdxLayoutAutoCreatedGroup
            Parent = layoutBottomWS
            AlignHorz = ahRight
            Index = 1
          end
          object dxLayoutAutoCreatedGroup3: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutAutoCreatedGroup2
            LayoutDirection = ldHorizontal
            Index = 1
          end
          object dxLayoutAutoCreatedGroup5: TdxLayoutAutoCreatedGroup
            Parent = dxLayoutAutoCreatedGroup2
            AlignVert = avClient
            LayoutDirection = ldHorizontal
            Index = 0
          end
        end
      end
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahParentManaged
      AlignVert = avParentManaged
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = True
      SizeOptions.SizableVert = True
      AllowQuickCustomize = True
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object layoutBottom: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      Padding.AssignedValues = [lpavBottom, lpavTop]
      ShowBorder = False
      Index = 1
    end
    object dxLayoutControlItem13: TdxLayoutItem
      Parent = layoutBottom
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton4'
      CaptionOptions.Visible = False
      Control = btnSave
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControlItem12: TdxLayoutItem
      Parent = layoutBottom
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnClose
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControlItem9: TdxLayoutItem
      Parent = layoutBottom
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnReset
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControlSpaceItem4: TdxLayoutEmptySpaceItem
      Parent = layoutBottom
      AlignHorz = ahClient
      CaptionOptions.Text = 'Empty Space Item'
      SizeOptions.Height = 10
      SizeOptions.Width = 10
      Index = 3
    end
    object dxLayoutControlItem1: TdxLayoutItem
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxPageControl1'
      CaptionOptions.Visible = False
      Control = cxPageControl
      ControlOptions.AutoColor = True
      ControlOptions.OriginalHeight = 233
      ControlOptions.OriginalWidth = 602
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    Left = 100
    Top = 100
    object dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
      PixelsPerInch = 96
    end
  end
end
