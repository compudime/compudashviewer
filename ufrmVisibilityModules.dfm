object frmVisibilityModules: TfrmVisibilityModules
  Left = 0
  Top = 0
  Caption = 'Modules visibility'
  ClientHeight = 363
  ClientWidth = 338
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TcxButton
    Left = 40
    Top = 329
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = btnOKClick
  end
  object btnApply: TcxButton
    Left = 121
    Top = 329
    Width = 75
    Height = 25
    Caption = 'Apply'
    TabOrder = 1
    OnClick = btnApplyClick
  end
  object btnCancel: TcxButton
    Left = 202
    Top = 329
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = btnCancelClick
  end
  object chkAutoStartScanForModules: TcxCheckBox
    Left = 8
    Top = 11
    Caption = 'Autoscan at app start'
    Style.TransparentBorder = False
    TabOrder = 3
  end
  object btnScanModules: TcxButton
    Left = 246
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Scan'
    TabOrder = 4
    OnClick = btnScanModulesClick
  end
  object gridModules: TcxGrid
    Left = 8
    Top = 39
    Width = 322
    Height = 283
    TabOrder = 5
    object gridModulesDBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      ScrollbarAnnotations.CustomAnnotations = <>
      DataController.DataSource = dsModules
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      object clmModuleName: TcxGridDBColumn
        Caption = 'Module name'
        DataBinding.FieldName = 'ModuleName'
        Options.Editing = False
      end
      object clmShowingSetting: TcxGridDBColumn
        Caption = 'Showing type'
        DataBinding.FieldName = 'ShowingType'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          'Show always'
          'Show when exists'
          'Not show')
      end
    end
    object gridModulesLevel1: TcxGridLevel
      GridView = gridModulesDBTableView1
    end
  end
  object mtblModules: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 40
    Top = 96
    object mtblModulesModuleName: TStringField
      FieldName = 'ModuleName'
    end
    object mtblModulesShowingType: TStringField
      FieldName = 'ShowingType'
    end
  end
  object dsModules: TDataSource
    DataSet = mtblModules
    Left = 112
    Top = 96
  end
end
