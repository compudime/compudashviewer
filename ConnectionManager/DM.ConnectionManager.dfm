object DMConnectionManager: TDMConnectionManager
  OldCreateOrder = False
  Height = 150
  Width = 215
  object memConnections: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 47
    Top = 40
    object memConnectionsdb_name: TStringField
      FieldName = 'db_name'
    end
    object memConnectionsuser_name: TStringField
      FieldName = 'user_name'
    end
    object memConnectionspassword: TStringField
      FieldName = 'password'
    end
    object memConnectionsserver: TStringField
      FieldName = 'server'
    end
    object memConnectionsis_default: TBooleanField
      FieldName = 'is_default'
    end
    object memConnectionsPort: TIntegerField
      FieldName = 'Port'
    end
    object memConnectionsdb_type: TStringField
      FieldName = 'db_type'
      Size = 255
    end
  end
  object dsConnections: TDataSource
    DataSet = memConnections
    Left = 135
    Top = 40
  end
end
