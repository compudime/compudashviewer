unit View.ConnectionManager;

interface

uses
  Vcl.Forms,
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Dialogs,
  cxGraphics,
  cxControls,
  cxLookAndFeels,
  cxLookAndFeelPainters,
  dxSkinsCore,
  cxStyles,
  cxCustomData,
  cxFilter,
  cxData,
  cxDataStorage,
  cxEdit,
  cxNavigator,
  dxDateRanges,
  dxScrollbarAnnotations,
  Data.DB,
  cxDBData,
  cxGridLevel,
  cxClasses,
  cxGridCustomView,
  cxGridCustomTableView,
  cxGridTableView,
  cxGridDBTableView,
  cxGrid,
  dxLayoutContainer,
  dxLayoutControlAdapters,
  Vcl.StdCtrls,
  dxLayoutControl,
  Vcl.ExtCtrls,
  cxMaskEdit,
  cxCheckBox,
  FireDAC.Stan.StorageJSON,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  cxDropDownEdit,
  DM.ConnectionManager, dxUIAClasses, dxSkinWXI;

type
  TFormConnectionManager = class(TForm)
    dsConnections: TDataSource;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutItem1: TdxLayoutItem;
    cxGridConnections: TcxGrid;
    cxViewConnections: TcxGridDBTableView;
    cxLevelConnections: TcxGridLevel;
    dxLayoutControl2Group_Root: TdxLayoutGroup;
    dxLayoutControl2: TdxLayoutControl;
    dxLayoutItem2: TdxLayoutItem;
    btnConnect: TButton;
    dxLayoutItem3: TdxLayoutItem;
    btnSaveAll: TButton;
    dxLayoutItem4: TdxLayoutItem;
    btnDelete: TButton;
    dxLayoutItem5: TdxLayoutItem;
    btnRestore: TButton;
    dxLayoutItem6: TdxLayoutItem;
    btnAddNew: TButton;
    dxLayoutItem7: TdxLayoutItem;
    cxViewConnectionsColumn1: TcxGridDBColumn;
    cxViewConnectionsColumn2: TcxGridDBColumn;
    cxViewConnectionsColumn3: TcxGridDBColumn;
    cxViewConnectionsColumn4: TcxGridDBColumn;
    cxViewConnectionsColumn5: TcxGridDBColumn;
    cxViewConnectionsColumn6: TcxGridDBColumn;
    cxViewConnectionsColumn7: TcxGridDBColumn;
    procedure btnAddNewClick(Sender: TObject);
    procedure btnSaveAllClick(Sender: TObject);
    procedure btnRestoreClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    class var
      FInstance: TFormConnectionManager;
  private
    FModalResult: TModalResult;
    FModel: IConnectionManager;
    FConnectionEvent: TProc<TConnectionData>;
    procedure InitModel;
    procedure InitTable;
  public
    class function GetInstance: TFormConnectionManager;
    function InitAndShow(AConnectionEvent: TProc<TConnectionData>): TModalResult;
  end;

implementation

const
  CONNECTION_SRC_FILE_PATH = 'connection_namager.json';
{$R *.dfm}

{ TFormConnectionManager }

procedure TFormConnectionManager.btnAddNewClick(Sender: TObject);
begin
  FModel.AddNew;
end;

procedure TFormConnectionManager.btnConnectClick(Sender: TObject);
begin
  FModel.ConnectTo;
  FModalResult := mrOk;
end;

procedure TFormConnectionManager.btnDeleteClick(Sender: TObject);
begin
  FModel.Delete;
end;

procedure TFormConnectionManager.btnSaveAllClick(Sender: TObject);
begin
  FModel.Save;
end;

procedure TFormConnectionManager.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ModalResult := FModalResult;
end;

procedure TFormConnectionManager.btnRestoreClick(Sender: TObject);
begin
  FModel.Restore;
end;

class function TFormConnectionManager.GetInstance: TFormConnectionManager;
begin
  if not Assigned(FInstance) then
    FInstance := TFormConnectionManager.Create(Application);

  FInstance.FModalResult := mrCancel;
  Result := FInstance;
end;

function TFormConnectionManager.InitAndShow(AConnectionEvent: TProc<TConnectionData>): TModalResult;
begin
  FConnectionEvent := AConnectionEvent;
  InitModel;
  InitTable;
  Result := Self.ShowModal;
end;

procedure TFormConnectionManager.InitModel;
begin
  FModel := TDMConnectionManager.Create(Application);
  FModel.SetEventOnConnectTo(FConnectionEvent);
end;

procedure TFormConnectionManager.InitTable;
begin
  dsConnections.DataSet := FModel.GetDataset;
end;

end.

