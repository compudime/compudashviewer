unit DM.ConnectionManager;

interface

uses
  System.SysUtils,
  System.Classes,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Param,
  FireDAC.Stan.Error,
  FireDAC.DatS,
  FireDAC.Phys.Intf,
  FireDAC.DApt.Intf,
  Data.DB,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

const
  DB_ATLAS_TYPE = 'Atlas';
  DB_SERVER_TYPE = 'Server';

type
  TConnectionData = class
    ConnectionType: string;
    Database: string;
    UserName: string;
    Password: string;
    Server: string;
    Port: Integer;
    IsConnected: Boolean;
  end;

  IConnectionManager = interface
    procedure AddNew;
    procedure Save;
    procedure Restore;
    procedure Delete;
    function GetDefault: TConnectionData;
    function GetDataset: TDataset;
    procedure ConnectTo;
    procedure SetEventOnConnectTo(AEvent: TProc<TConnectionData>);
    function ConnectToDefault: Boolean;
  end;

  IConnectionDataMapper = interface
    function MapToModel(ADataSet: TDataset): TConnectionData;
  end;

  TDMConnectionManager = class(TDataModule, IConnectionManager, IConnectionDataMapper)
    memConnections: TFDMemTable;
    memConnectionsdb_name: TStringField;
    memConnectionsuser_name: TStringField;
    memConnectionspassword: TStringField;
    memConnectionsserver: TStringField;
    memConnectionsis_default: TBooleanField;
    memConnectionsPort: TIntegerField;
    memConnectionsdb_type: TStringField;
    dsConnections: TDataSource;
  private
    FConnectionEvent: TProc<TConnectionData>;
    procedure ActiveMemTable;
    function GetSrcFilePath: string;
    function MapToModel(ADataSet: TDataset): TConnectionData;
    function FindByData(AData: TConnectionData): Boolean;
    procedure SetIsDefault(AValue: Boolean);
    procedure DoConnectionEvent;
  public
    procedure Save;
    procedure AddNew;
    procedure Delete;
    procedure Restore;
    procedure ConnectTo;
    function GetDataset: TDataset;
    function GetDefault: TConnectionData;
    procedure SetEventOnConnectTo(AEvent: TProc<TConnectionData>);
    function ConnectToDefault: Boolean;
  end;

implementation

const
  CONNECTION_SRC_FILE_PATH = 'connection_namager.json';
{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDMConnectionManager }

procedure TDMConnectionManager.ActiveMemTable;
begin
  if not memConnections.Active then
    memConnections.Active := True;
end;

procedure TDMConnectionManager.AddNew;
begin
  ActiveMemTable;
  memConnections.Insert;
end;

procedure TDMConnectionManager.ConnectTo;
var
  LFutureConnection: TConnectionData;
  LCurrentConnection: TConnectionData;
begin
  LFutureConnection := MapToModel(memConnections);
  LCurrentConnection := GetDefault;
  try
    if Assigned(LCurrentConnection) then
      SetIsDefault(False);

    if FindByData(LFutureConnection) then
      SetIsDefault(True);

    Save;

    DoConnectionEvent;
  finally
    FreeAndNil(LFutureConnection);
    FreeAndNil(LCurrentConnection);
  end;
end;

function TDMConnectionManager.ConnectToDefault: Boolean;
var
  LDefConnection: TConnectionData;
begin
  Result := True;
  LDefConnection := GetDefault;
  if Assigned(LDefConnection) then
    DoConnectionEvent
  else
    Result := False;
end;

procedure TDMConnectionManager.Delete;
begin
  memConnections.Delete;
end;

procedure TDMConnectionManager.DoConnectionEvent;
begin
  if Assigned(FConnectionEvent) then
    FConnectionEvent(GetDefault);
end;

function TDMConnectionManager.FindByData(AData: TConnectionData): Boolean;
var
  LValues: TArray<Variant>;
begin
  SetLength(LValues, 4);

  LValues[0] := AData.Server;
  LValues[1] := AData.ConnectionType;
  LValues[2] := AData.Database;

  Result := memConnections.Locate(memConnectionsserver.FieldName      {}
    + ';' + memConnectionsdb_type.FieldName                           {}
    + ';' + memConnectionsdb_name.FieldName  , LValues, []);         {}
end;

function TDMConnectionManager.GetDataset: TDataset;
begin
  Restore;
  ActiveMemTable;
  Result := memConnections;
end;

function TDMConnectionManager.GetDefault: TConnectionData;
begin
  Result := nil;

  if memConnections.Locate(memConnectionsis_default.FieldName, true, []) then
    Result := MapToModel(memConnections);
end;

function TDMConnectionManager.GetSrcFilePath: string;
begin
  Result := GetCurrentDir + '\' + CONNECTION_SRC_FILE_PATH;
end;

function TDMConnectionManager.MapToModel(ADataSet: TDataset): TConnectionData;
begin
  Result := TConnectionData.Create;

  Result.ConnectionType := ADataSet.FieldByName('db_type').AsString;
  Result.Database := ADataSet.FieldByName('db_name').AsString;
  Result.UserName := ADataSet.FieldByName('user_name').AsString;
  Result.Password := ADataSet.FieldByName('password').AsString;
  Result.Server := ADataSet.FieldByName('server').AsString;
  Result.Port := ADataSet.FieldByName('Port').AsInteger;
  Result.IsConnected := ADataSet.FieldByName('is_default').AsBoolean;
end;

procedure TDMConnectionManager.Restore;
begin
  if not FileExists(GetSrcFilePath) then
    Save;
  memConnections.LoadFromFile(GetSrcFilePath, TFDStorageFormat.sfJSON);
end;

procedure TDMConnectionManager.Save;
begin
  ActiveMemTable;
  memConnections.SaveToFile(GetSrcFilePath, TFDStorageFormat.sfJSON);
end;

procedure TDMConnectionManager.SetEventOnConnectTo(AEvent: TProc<TConnectionData>);
begin
  FConnectionEvent := AEvent;
end;

procedure TDMConnectionManager.SetIsDefault(AValue: Boolean);
begin
  memConnections.Edit;
  try
    memConnectionsis_default.Value := AValue;
    memConnections.Post;
  except
    memConnections.Cancel;
  end;
end;

end.

