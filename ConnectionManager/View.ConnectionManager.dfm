object FormConnectionManager: TFormConnectionManager
  Left = 0
  Top = 0
  Caption = 'Connection Manager'
  ClientHeight = 337
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 700
    Height = 337
    Align = alClient
    TabOrder = 0
    object cxGridConnections: TcxGrid
      Left = 10
      Top = 10
      Width = 680
      Height = 257
      TabOrder = 0
      object cxViewConnections: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsConnections
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object cxViewConnectionsColumn6: TcxGridDBColumn
          Caption = 'DB Type'
          DataBinding.FieldName = 'db_type'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Items.Strings = (
            'Atlas'
            'Server')
          Width = 83
        end
        object cxViewConnectionsColumn1: TcxGridDBColumn
          Caption = 'Database name'
          DataBinding.FieldName = 'db_name'
          DataBinding.IsNullValueType = True
          Width = 125
        end
        object cxViewConnectionsColumn2: TcxGridDBColumn
          Caption = 'Username'
          DataBinding.FieldName = 'user_name'
          DataBinding.IsNullValueType = True
          Width = 109
        end
        object cxViewConnectionsColumn3: TcxGridDBColumn
          Caption = 'Password'
          DataBinding.FieldName = 'password'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxMaskEditProperties'
          Properties.EchoMode = eemPassword
          Properties.MaxLength = 255
          Properties.PasswordChar = '*'
          Width = 103
        end
        object cxViewConnectionsColumn4: TcxGridDBColumn
          Caption = 'Server'
          DataBinding.FieldName = 'server'
          DataBinding.IsNullValueType = True
          Width = 145
        end
        object cxViewConnectionsColumn7: TcxGridDBColumn
          DataBinding.FieldName = 'Port'
          DataBinding.IsNullValueType = True
        end
        object cxViewConnectionsColumn5: TcxGridDBColumn
          Caption = 'Connected'
          DataBinding.FieldName = 'is_default'
          DataBinding.IsNullValueType = True
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.DisplayGrayed = 'False'
          Properties.ReadOnly = True
          Width = 61
        end
      end
      object cxLevelConnections: TcxGridLevel
        GridView = cxViewConnections
      end
    end
    object dxLayoutControl2: TdxLayoutControl
      Left = 10
      Top = 273
      Width = 680
      Height = 48
      TabOrder = 1
      object btnConnect: TButton
        Left = 127
        Top = 10
        Width = 103
        Height = 25
        Caption = 'Connect to'
        TabOrder = 1
        OnClick = btnConnectClick
      end
      object btnSaveAll: TButton
        Left = 375
        Top = 10
        Width = 110
        Height = 25
        Caption = 'Save All'
        TabOrder = 3
        OnClick = btnSaveAllClick
      end
      object btnDelete: TButton
        Left = 236
        Top = 10
        Width = 133
        Height = 25
        Caption = 'Delete'
        TabOrder = 2
        OnClick = btnDeleteClick
      end
      object btnRestore: TButton
        Left = 491
        Top = 10
        Width = 125
        Height = 25
        Caption = 'Restore'
        TabOrder = 4
        OnClick = btnRestoreClick
      end
      object btnAddNew: TButton
        Left = 10
        Top = 10
        Width = 111
        Height = 25
        Caption = 'Add New'
        TabOrder = 0
        OnClick = btnAddNewClick
      end
      object dxLayoutControl2Group_Root: TdxLayoutGroup
        AlignHorz = ahCenter
        AlignVert = avCenter
        ButtonOptions.Alignment = gbaLeft
        Hidden = True
        LayoutDirection = ldHorizontal
        ShowBorder = False
        Index = -1
      end
      object dxLayoutItem3: TdxLayoutItem
        Parent = dxLayoutControl2Group_Root
        AlignHorz = ahClient
        AlignVert = avClient
        SizeOptions.AssignedValues = [sovSizableHorz]
        SizeOptions.SizableHorz = False
        CaptionOptions.Text = 'Button1'
        CaptionOptions.Visible = False
        Control = btnConnect
        ControlOptions.OriginalHeight = 25
        ControlOptions.OriginalWidth = 103
        ControlOptions.ShowBorder = False
        Index = 1
      end
      object dxLayoutItem4: TdxLayoutItem
        Parent = dxLayoutControl2Group_Root
        AlignHorz = ahClient
        AlignVert = avClient
        SizeOptions.AssignedValues = [sovSizableHorz]
        SizeOptions.SizableHorz = False
        CaptionOptions.Text = 'Button2'
        CaptionOptions.Visible = False
        Control = btnSaveAll
        ControlOptions.OriginalHeight = 25
        ControlOptions.OriginalWidth = 110
        ControlOptions.ShowBorder = False
        Index = 3
      end
      object dxLayoutItem5: TdxLayoutItem
        Parent = dxLayoutControl2Group_Root
        AlignHorz = ahClient
        AlignVert = avClient
        SizeOptions.AssignedValues = [sovSizableHorz]
        SizeOptions.SizableHorz = False
        CaptionOptions.Text = 'Button3'
        CaptionOptions.Visible = False
        Control = btnDelete
        ControlOptions.OriginalHeight = 25
        ControlOptions.OriginalWidth = 133
        ControlOptions.ShowBorder = False
        Index = 2
      end
      object dxLayoutItem6: TdxLayoutItem
        Parent = dxLayoutControl2Group_Root
        AlignHorz = ahClient
        AlignVert = avClient
        SizeOptions.AssignedValues = [sovSizableHorz]
        SizeOptions.SizableHorz = False
        CaptionOptions.Text = 'Button4'
        CaptionOptions.Visible = False
        Control = btnRestore
        ControlOptions.OriginalHeight = 25
        ControlOptions.OriginalWidth = 125
        ControlOptions.ShowBorder = False
        Index = 4
      end
      object dxLayoutItem7: TdxLayoutItem
        Parent = dxLayoutControl2Group_Root
        AlignHorz = ahClient
        AlignVert = avClient
        SizeOptions.AssignedValues = [sovSizableHorz]
        SizeOptions.SizableHorz = False
        CaptionOptions.Text = 'Button1'
        CaptionOptions.Visible = False
        Control = btnAddNew
        ControlOptions.OriginalHeight = 25
        ControlOptions.OriginalWidth = 111
        ControlOptions.ShowBorder = False
        Index = 0
      end
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahParentManaged
      AlignVert = avTop
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = dxLayoutControl1Group_Root
      Control = cxGridConnections
      ControlOptions.OriginalHeight = 257
      ControlOptions.OriginalWidth = 617
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem2: TdxLayoutItem
      Parent = dxLayoutControl1Group_Root
      CaptionOptions.Text = 'dxLayoutControl2'
      CaptionOptions.Visible = False
      Control = dxLayoutControl2
      ControlOptions.OriginalHeight = 48
      ControlOptions.OriginalWidth = 300
      ControlOptions.ShowBorder = False
      Index = 1
    end
  end
  object dsConnections: TDataSource
    Left = 464
    Top = 48
  end
end
