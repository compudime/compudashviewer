unit ColumnUnboundHelperUnit;

interface

uses
  System.Classes, System.Generics.Collections, System.Types, System.UITypes, System.Variants, System.SysUtils,
  System.StrUtils, System.DateUtils, Vcl.Graphics, Vcl.Controls, Vcl.ExtCtrls, Vcl.Dialogs, Winapi.Windows,
  cxGridCustomTableView, cxGridTableView, cxGraphics, cxGridCustomView, cxDataStorage, cxButtonEdit, cxGrid, cxControls,
  dxCoreClasses, cxTextEdit, cxSpinEdit, cxCalendar, cxEdit, dxCore, cxCustomData, cxCurrencyEdit, cxMaskEdit, DB,
  ColumnHelperTypesUnit;

const
  itemsUConditionShiftLength = 5;
  countConditionUnboundItems = 7;
  unboundProcessingColor = clRed;

type
  TUnboundModes = (umValue, umValueByCondition, umSQLEdit, umSQLEditByCondition, umWSEdit, umWSEditByCondition);

  TConditionUnbound = record
    Mode: TCompareModes;
    ColumnName1: string;
    ColumnCaption1: string;
    Condition: string;
    ColumnName2: string;
    ColumnCaption2: string;
    Value: Variant;
    Operand: TOperands;
    Expression: string;
  end;

  TUnboundColumn = record
    Name: string;
    Caption: string;
    Width: Integer;
    cType: TDataTypes;
    Mask: string;
    Mode: TUnboundModes;
    Expression: string;
    TargetFieldName: string;
    Conditions: TList<TConditionUnbound>;
    sqlStatements: TDictionary<string, string>;
    sqlUpdateStatements: TDictionary<string, string>;
    wsStatements: TDictionary<string, string>;
    wsPostStatements: TDictionary<string, string>;
    updatePeriods: TDictionary<string, Integer>;
    procedure setFieldName(FieldName: string);
    function containsField(FieldName: string): Boolean;
    function containsInColumns(Columns: TArray<TUnboundColumn>): Boolean;
    function containsColumns(Columns: TArray<TUnboundColumn>): Boolean;
  end;

  TColumnUnboundHelper = class
  private
    gridViews: TDictionary<string, TcxGridTableView>;
    gridModes: TDictionary<string, TStoreMode>;
    gridViewKeys: TDictionary<string, string>;
    gridKeyUpEvents: TDictionary<string, TKeyEvent>;
    gridKeyDownEvents: TDictionary<string, TKeyEvent>;
    gridTopRowEvents: TDictionary<string, TNotifyEvent>;
    gridEditValueEvents: TDictionary<string, TcxGridCustomTableItemEvent>;
    gridDataControllerCompareEvents: TDictionary<string, TcxDataControllerCompareEvent>;
    gridTimers: TDictionary<string, TViewTimer>;
    processRows: TDictionary<string, TDictionary<Variant, TThread>>;
    processedRows: TDictionary<string, TDictionary<Variant, TDateTime>>;
    unboundColumns: TDictionary<string, TDictionary<string, TUnboundColumn>>;
    unboundColumnsData: TDictionary<string, TDictionary<string, TDictionary<Variant, Variant>>>;
    isEmptyUnbound: TDictionary<string, Boolean>;
    lastVScrollPos: TDictionary<string, Integer>;
    runFullUpdate: Boolean;

    function getKeyValue(row: TcxCustomGridRow; View: TcxGridTableView; KeyField: string): Variant;
    function findFieldCaption(View: TcxGridTableView; ColumnName: string): string;
    function isFoundUnbound(View: TcxGridTableView): Boolean;
    function getNewUnboundColumnName(View: TcxGridTableView): string;
    procedure createThread(View: TcxGridTableView; Rows: TList<TcxCustomGridRow>;
      uColumns: TDictionary<string, TUnboundColumn>);

    procedure TableViewTopRecordIndexChanged(Sender: TObject);
    procedure TableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TableViewKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TableViewEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
    procedure ColumnGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex: Integer; var AText: string);
    procedure ColumnGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
    procedure ColumnEditValueChanged(Sender: TObject);
    procedure TableViewDataControllerCompare(ADataController: TcxCustomDataController;
      ARecordIndex1, ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant; var Compare: Integer);

    function listToUnboundColumn(View: TcxGridTableView; ColumnList: TStringList): TUnboundColumn;
    procedure terminateThreads;
    procedure stoppingTimers;
    function isProcess(gridKey: string; keyValue: Variant): Boolean;
    function isProcessed(gridKey: string; keyValue: Variant): Boolean;
  protected
    frmColumnUnbound: TWinControl;
    frmColumnUnboundEdit: TWinControl;

    procedure setFormColumnBounds(viewInfo: TcxGridTableDataCellViewInfo); virtual;
    procedure OnTerminateThread(Sender: TObject);
  public
    OnCompleted: TWorkGridCompetedEvent;

    procedure assignFieldName(Column: TcxGridColumn; FieldName: string);
    function HasUnboundColumns(gridView: TcxGridTableView): Boolean;
    procedure resetGrid(View: TcxGridTableView);
    procedure postEditor(View: TcxGridTableView; edit: TcxCustomEdit);
    function isVScrollChange(View: TcxGridTableView): Boolean;
    function isUnbound(View: TcxCustomGridTableView; ColumnName: string): Boolean; overload;
    function isUnbound(View: TcxCustomGridTableView; columnIndex: Integer): Boolean; overload;
    function isUnbound(Column: TcxGridColumn): Boolean; overload;
    function addOrSetFieldUnbound(View: TcxGridTableView; FieldName: string; fieldValue: Variant): string;
    function unboundColumnToString(View: TcxGridTableView; ColumnName: string): string;
    procedure setVScrollPosition(View: TcxGridTableView);
    function sortColumns(Columns: TArray<TUnboundColumn>): TList<string>;
    function getLastUpdate(View: TcxGridTableView; row: TcxCustomGridRecord; var fromLastUpdate: Integer): Boolean;
    procedure deleteColumn(View: TcxGridTableView; ColumnName: string);
    procedure restoreColumn(View: TcxGridTableView; Column: TcxGridColumn);
    procedure updateUnbound(View: TcxGridTableView; ColumnName: string; Column: TUnboundColumn);
    procedure createUnboundColumn(View: TcxGridTableView; Column: TUnboundColumn; parentColumn: TcxGridColumn = nil);
      overload;
    procedure createUnboundColumn(View: TcxGridTableView; ColumnName: string; idx: Integer = -1); overload;
    procedure saveUnboundColumn(View: TcxGridTableView; Column: TUnboundColumn);

    procedure openDialog(ACellViewInfo: TcxGridTableDataCellViewInfo; forAdding: Boolean);

    function TryFindProcessingUnbound(ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
    function TryProcessUnbound(View: TcxGridTableView): Boolean;
    function TryProcessUnboundAll(View: TcxGridTableView): Boolean;
    function TryUpdateUnboundRow(View: TcxGridTableView; changedItem: TcxCustomGridTableItem): Boolean;

    procedure addTableView(gridView: TcxGridTableView; KeyField: string);
    function TableViewInList(View: TcxGridTableView): Boolean;
  end;

function ColumnUnboundHelper: TColumnUnboundHelper;

implementation

uses frmColumnUnboundUnit, ColumnUnboundThreadUnit, ColumnHelperDataUnit, System.JSON;

var
  _columnStyleHelper: TColumnUnboundHelper;

function ColumnUnboundHelper: TColumnUnboundHelper;
begin
  if not Assigned(_columnStyleHelper) then
    _columnStyleHelper := TColumnUnboundHelper.Create;
  Result := _columnStyleHelper;
end;

// Add new grid view
procedure TColumnUnboundHelper.addTableView(gridView: TcxGridTableView; KeyField: string);
var
  gridKey: string;
  storeMode: TStoreMode;
begin
  gridKey := gridView.Owner.Name + gridView.Name;
  Self.gridViews.AddOrSetValue(gridKey, gridView);

  storeMode := getColumnStoreMode(gridView);
  Self.gridModes.AddOrSetValue(gridKey, storeMode);

  Self.gridViewKeys.AddOrSetValue(gridKey, KeyField);

  if Assigned(gridView.OnKeyUp) then
    Self.gridKeyUpEvents.AddOrSetValue(gridKey, gridView.OnKeyUp);
  Self.gridViews[gridKey].OnKeyUp := TableViewKeyUp;

  if Assigned(gridView.DataController.OnCompare) then
    Self.gridDataControllerCompareEvents.AddOrSetValue(gridKey, gridView.DataController.OnCompare);
  Self.gridViews[gridKey].DataController.OnCompare := TableViewDataControllerCompare;

  if Assigned(gridView.OnKeyDown) then
    Self.gridKeyDownEvents.AddOrSetValue(gridKey, gridView.OnKeyDown);
  Self.gridViews[gridKey].OnKeyDown := TableViewKeyDown;

  if Assigned(gridView.OnTopRecordIndexChanged) then
    Self.gridTopRowEvents.AddOrSetValue(gridKey, gridView.OnTopRecordIndexChanged);
  Self.gridViews[gridKey].OnTopRecordIndexChanged := TableViewTopRecordIndexChanged;

  if Assigned(gridView.OnEditValueChanged) then
    Self.gridEditValueEvents.AddOrSetValue(gridKey, gridView.OnEditValueChanged);
  Self.gridViews[gridKey].OnEditValueChanged := TableViewEditValueChanged;

  Self.isEmptyUnbound.AddOrSetValue(gridKey, True);
  Self.processRows.AddOrSetValue(gridKey, TDictionary<Variant, TThread>.Create);
  Self.processedRows.AddOrSetValue(gridKey, TDictionary<Variant, TDateTime>.Create);
  Self.unboundColumns.AddOrSetValue(gridKey, TDictionary<string, TUnboundColumn>.Create);
  Self.unboundColumnsData.AddOrSetValue(gridKey, TDictionary < string, TDictionary < Variant, Variant >>.Create);
  Self.lastVScrollPos.AddOrSetValue(gridKey, 0);
  Self.gridTimers.AddOrSetValue(gridKey, TViewTimer.Create(gridView));
  Self.gridTimers[gridKey].Enabled := True;
  Self.runFullUpdate := False;
end;

{ Open form of dialog*********************************************************** }
procedure TColumnUnboundHelper.openDialog(ACellViewInfo: TcxGridTableDataCellViewInfo; forAdding: Boolean);
var
  Column: TcxGridColumn;
  ColumnName: string;
  Sender: TcxCustomGridTableView;
  res: TModalResult;
  gridKey: string;
begin
  Sender := ACellViewInfo.gridView;
  Column := ACellViewInfo.Item as TcxGridColumn;

  gridKey := Sender.Owner.Name + Sender.Name;

  frmColumnUnbound := TfrmColumnUnbound.Create(Sender.Owner);
  if forAdding then
    ColumnName := string.Empty
  else
    ColumnName := Column.Name;
  if unboundColumns[gridKey].ContainsKey(ColumnName) then
    TfrmColumnUnbound(frmColumnUnbound).unboundColumn := unboundColumns[gridKey][ColumnName]
  else
    TfrmColumnUnbound(frmColumnUnbound).unboundColumn := listToUnboundColumn(gridViews[gridKey], nil);

  TfrmColumnUnbound(frmColumnUnbound).gridView := gridViews[gridKey];
  TfrmColumnUnbound(frmColumnUnbound).initFields(ACellViewInfo);

  setFormColumnBounds(ACellViewInfo);

  res := TfrmColumnUnbound(frmColumnUnbound).ShowModal;
  if res = mrOk then
  begin
    if not ColumnName.IsEmpty then
      saveUnboundColumn(gridViews[gridKey], TfrmColumnUnbound(frmColumnUnbound).unboundColumn)
    else
    begin
      ColumnName := getNewUnboundColumnName(gridViews[gridKey]);
      TfrmColumnUnbound(frmColumnUnbound).unboundColumn.Name := ColumnName;
      unboundColumns[gridKey].AddOrSetValue(ColumnName, TfrmColumnUnbound(frmColumnUnbound).unboundColumn);
      createUnboundColumn(gridViews[gridKey], unboundColumns[gridKey][ColumnName], Column);
      processedRows[gridKey].Clear;
      TryProcessUnbound(gridViews[gridKey]);
    end;
  end
  else if res = mrYesToAll then
  begin
    deleteColumn(gridViews[gridKey], ColumnName);
  end;
  FreeAndNil(frmColumnUnbound);
end;

function TColumnUnboundHelper.getNewUnboundColumnName(View: TcxGridTableView): string;
const
  nameFormat = '%sUnbound%d';
var
  Count: Integer;
  Name: string;
begin
  Count := 1;
  name := Format(nameFormat, [View.Name, Count]);
  while View.FindItemByName(name) <> nil do
  begin
    Inc(Count);
    name := Format(nameFormat, [View.Name, Count]);
  end;
  Result := name;
end;

// Set bounds form of dialog
procedure TColumnUnboundHelper.setFormColumnBounds(viewInfo: TcxGridTableDataCellViewInfo);
var
  ownner, dialog: TWinControl;
  fpoint: TPoint;
  bolder, header: Integer;
begin
  ownner := viewInfo.gridView.Owner as TWinControl;
  dialog := frmColumnUnbound;

  fpoint := Point(viewInfo.Bounds.Right, viewInfo.Bounds.Top);
  fpoint := ownner.ClientToScreen(fpoint);
  bolder := (dialog.Width - dialog.ClientWidth) div 2;
  header := dialog.Height - dialog.ClientHeight - bolder;
  dialog.Left := fpoint.X + 2 - (dialog.Width div 2);
  dialog.Top := fpoint.Y - header - 2;
end;

{ ****************************************************************************** }

{ Proccesing columns************************************************************ }
function TColumnUnboundHelper.TryProcessUnbound(View: TcxGridTableView): Boolean;
  function getMinPeriod(Column: TUnboundColumn): Integer;
  var
    pairPeriod: TPair<string, Integer>;
  begin
    Result := -1;
    for pairPeriod in Column.updatePeriods do
    begin
      if (Result < 0) or (pairPeriod.Value < Result) then
      begin
        Result := pairPeriod.Value;
      end;
    end;
  end;

var
  gridKey, colName: string;
  I, minUnboundPeriod, minUnboundColumnPeriod: Integer;
  uColumns: TDictionary<string, TUnboundColumn>;
  timeNow: TDateTime;
  Rows: TList<TcxCustomGridRow>;
  keyValue: Variant;
begin
  Result := False;
  if not Assigned(View.Owner) then
    Exit;

  gridKey := View.Owner.Name + View.Name;
  if isEmptyUnbound.ContainsKey(gridKey) and (not isEmptyUnbound[gridKey]) and processRows.ContainsKey(gridKey) and
    processedRows.ContainsKey(gridKey) then
  begin
    if isVScrollChange(View) then
    begin
      lastVScrollPos[gridKey] := View.Site.VScrollBar.Position;
      Exit;
    end;
    lastVScrollPos[gridKey] := View.Site.VScrollBar.Position;

    timeNow := now;

    uColumns := TDictionary<string, TUnboundColumn>.Create;
    minUnboundPeriod := -1;
    for I := 0 to View.ColumnCount - 1 do
    begin
      if View.Columns[I].Visible and isUnbound(View.Columns[I]) then
      begin
        colName := View.Columns[I].Name;
        uColumns.Add(colName, unboundColumns[gridKey][View.Columns[I].Name]);
        minUnboundColumnPeriod := getMinPeriod(uColumns[colName]);
        if (minUnboundColumnPeriod >= 0) and (minUnboundPeriod < 0) then
        begin
          if (minUnboundPeriod < 0) or ((minUnboundPeriod >= 0) and (minUnboundColumnPeriod < minUnboundPeriod)) then
            minUnboundPeriod := minUnboundColumnPeriod;
        end;
      end
    end;

    Rows := TList<TcxCustomGridRow>.Create;

    for I := 0 to gridViews[gridKey].viewInfo.RecordsViewInfo.Count - 1 do
    begin
      keyValue := getKeyValue(gridViews[gridKey].viewInfo.RecordsViewInfo.Items[I].GridRecord, gridViews[gridKey],
        gridViewKeys[gridKey]);

      if isProcess(gridKey, keyValue) then
        Continue;

      if (minUnboundPeriod <= 0) and isProcessed(gridKey, keyValue) then
        Continue;
      if (minUnboundPeriod > 0) and isProcessed(gridKey, keyValue) and
        (SecondsBetween(timeNow, processedRows[gridKey][keyValue]) < minUnboundPeriod) then
        Continue;

      Rows.Add(gridViews[gridKey].viewInfo.RecordsViewInfo.Items[I].GridRecord);
    end;

    if Rows.Count > 0 then
      createThread(gridViews[gridKey], Rows, uColumns)
    else
    begin
      Rows.Free;
      uColumns.Free;
      gridViews[gridKey].Painter.Invalidate;
    end;

    Result := True;
  end;
end;

function TColumnUnboundHelper.TryUpdateUnboundRow(View: TcxGridTableView; changedItem: TcxCustomGridTableItem): Boolean;
var
  gridKey, colName, itemName, text, expTxt: string;
  I: Integer;
  uColumn, itemColumn: TUnboundColumn;
  uColumns: TDictionary<string, TUnboundColumn>;
  doChangeRow: Boolean;
  Rows: TList<TcxCustomGridRow>;
  params: TDictionary<string, Variant>;
  Column: TcxGridColumn;
  keyValue: Variant;
begin
  Result := False;
  gridKey := View.Owner.Name + View.Name;
  if isEmptyUnbound.ContainsKey(gridKey) and (not isEmptyUnbound[gridKey]) and processRows.ContainsKey(gridKey) and
    processedRows.ContainsKey(gridKey) then
  begin
    itemName := getFieldName(changedItem as TcxGridColumn);
    if itemName.IsEmpty and Assigned(changedItem) then
      itemName := changedItem.Name;
    doChangeRow := False;

    if isUnbound(View, itemName) then
    begin
      if gridModes[gridKey] = smDict then
        Exit;

      itemColumn := unboundColumns[gridKey][itemName];
      if itemColumn.Mode in [umSQLEdit, umSQLEditByCondition, umWSEdit, umWSEditByCondition] then
      begin
        keyValue := getKeyValue(TcxCustomGridRow(changedItem.FocusedCellViewInfo.GridRecord), gridViews[gridKey],
          gridViewKeys[gridKey]);
        if (not isProcessed(gridKey, keyValue)) or isProcess(gridKey, keyValue) then
        begin
          View.DataController.Cancel;
          Exit;
        end;

        text := string.Empty;
        params := TDictionary<string, Variant>.Create;
        if itemColumn.Mode in [umSQLEdit, umSQLEditByCondition] then
          text := ColumnUnboundHelperData.prepareSQLUpdateText(changedItem, itemColumn, params, colName)
        else
          text := ColumnUnboundHelperData.prepareWSPostText(changedItem, itemColumn, colName);
        if not text.IsEmpty then
        begin
          if itemColumn.Mode in [umSQLEdit, umSQLEditByCondition] then
            ColumnUnboundHelperData.runSQLRequest(text, params, expTxt)
          else
            ColumnUnboundHelperData.runWSRequest(text,
              TJSONObject.Create(TJSONPair.Create(colName, VarToStr(changedItem.EditValue))).ToString, expTxt);
          if not expTxt.IsEmpty then
          begin
            messagedlg(Format('Error while update: %s', [expTxt]), mtError, [mbOK], 0);
            params.Free;
            Exit;
          end
          else
          begin
            Column := ColumnUnboundHelperData.findGridViewColumn(View, colName);
            if (Column <> nil) and params.ContainsKey(colName) then
            begin
              View.DataController.Values[changedItem.FocusedCellViewInfo.GridRecord.RecordIndex, Column.Index] :=
                params[colName];
            end;
          end;
        end;
        params.Free;
        if text.IsEmpty or (not expTxt.IsEmpty) then
          Exit;
        doChangeRow := True;
      end
      else
        Exit;
    end
    else
      View.DataController.Post;

    uColumns := TDictionary<string, TUnboundColumn>.Create;
    for I := 0 to View.ColumnCount - 1 do
    begin
      if View.Columns[I].Visible and isUnbound(View.Columns[I]) then
      begin
        colName := View.Columns[I].Name;
        uColumn := unboundColumns[gridKey][View.Columns[I].Name];
        uColumns.Add(colName, uColumn);
        if (not doChangeRow) and uColumns[colName].containsField(itemName) then
          doChangeRow := True;
      end
    end;

    if doChangeRow and (not isProcess(gridKey, keyValue)) then
    begin
      Rows := TList<TcxCustomGridRow>.Create;
      Rows.Add(changedItem.FocusedCellViewInfo.GridRecord as TcxCustomGridRow);
      createThread(gridViews[gridKey], Rows, uColumns);
    end
    else
      uColumns.Free;

    Result := True;
  end;
end;

function TColumnUnboundHelper.TryProcessUnboundAll(View: TcxGridTableView): Boolean;
  function getMinPeriod(Column: TUnboundColumn): Integer;
  var
    pairPeriod: TPair<string, Integer>;
  begin
    Result := -1;
    for pairPeriod in Column.updatePeriods do
    begin
      if (Result < 0) or (pairPeriod.Value < Result) then
      begin
        Result := pairPeriod.Value;
      end;
    end;
  end;

var
  gridKey, colName: string;
  I, minUnboundPeriod, minUnboundColumnPeriod: Integer;
  uColumns: TDictionary<string, TUnboundColumn>;
  timeNow: TDateTime;
  Rows: TList<TcxCustomGridRow>;
  keyValue: Variant;
begin
  Result := False;
  gridKey := View.Owner.Name + View.Name;
  if isEmptyUnbound.ContainsKey(gridKey) and (not isEmptyUnbound[gridKey]) and processRows.ContainsKey(gridKey) and
    processedRows.ContainsKey(gridKey) then
  begin
    timeNow := now;

    uColumns := TDictionary<string, TUnboundColumn>.Create;
    minUnboundPeriod := -1;
    for I := 0 to View.ColumnCount - 1 do
    begin
      if View.Columns[I].Visible and isUnbound(View.Columns[I]) then
      begin
        colName := View.Columns[I].Name;
        uColumns.Add(colName, unboundColumns[gridKey][View.Columns[I].Name]);
        minUnboundColumnPeriod := getMinPeriod(uColumns[colName]);
        if (minUnboundColumnPeriod >= 0) and (minUnboundPeriod < 0) then
        begin
          if (minUnboundPeriod < 0) or ((minUnboundPeriod >= 0) and (minUnboundColumnPeriod < minUnboundPeriod)) then
            minUnboundPeriod := minUnboundColumnPeriod;
        end;
      end
    end;

    Rows := TList<TcxCustomGridRow>.Create;

    for I := 0 to gridViews[gridKey].ViewData.RowCount - 1 do
    begin
      keyValue := getKeyValue(gridViews[gridKey].ViewData.Rows[I], gridViews[gridKey], gridViewKeys[gridKey]);

      if isProcess(gridKey, keyValue) then
        Continue;

      if (minUnboundPeriod <= 0) and isProcessed(gridKey, keyValue) then
        Continue;

      if (minUnboundPeriod > 0) and isProcessed(gridKey, keyValue) and
        (SecondsBetween(timeNow, processedRows[gridKey][keyValue]) < minUnboundPeriod) then
        Continue;

      Rows.Add(gridViews[gridKey].ViewData.Rows[I]);
    end;

    if Rows.Count > 0 then
    begin
      runFullUpdate := True;
      createThread(gridViews[gridKey], Rows, uColumns)
    end
    else
    begin
      Rows.Free;
      uColumns.Free;
      gridViews[gridKey].Painter.Invalidate;
    end;

    Result := True;
  end;
end;

procedure TColumnUnboundHelper.createThread(View: TcxGridTableView; Rows: TList<TcxCustomGridRow>;
  uColumns: TDictionary<string, TUnboundColumn>);
var
  worker: TColumnUnboundThread;
  gridKey, AFiledName: string;
  I: Integer;
  tValues: TDictionary<Variant, TDictionary<string, Variant>>;
  Value, keyValue: Variant;
  row: TcxCustomGridRow;
begin
  gridKey := View.Owner.Name + View.Name;

  tValues := TDictionary < Variant, TDictionary < string, Variant >>.Create;
  for row in Rows do
  begin
    keyValue := getKeyValue(row, View, gridViewKeys[gridKey]);
    if VarIsNull(keyValue) then
      Continue;

    tValues.Add(keyValue, TDictionary<string, Variant>.Create);
    for I := 0 to View.ColumnCount - 1 do
    begin
      //if View.Columns[I].Visible then //do not check for Visible so we could use Paramters from non-visible columns
      begin
        Value := row.Values[I];

        if isUnbound(View.Columns[I]) then
        begin
          if gridModes[gridKey] = smDict then
          begin
            if unboundColumnsData[gridKey].ContainsKey(View.Columns[I].Name) and unboundColumnsData[gridKey]
              [View.Columns[I].Name].ContainsKey(keyValue) then
            begin
              Value := unboundColumnsData[gridKey][View.Columns[I].Name][keyValue];
            end;
          end;
          tValues[keyValue].Add(View.Columns[I].Name, Value)
        end
        else
        begin
          AFiledName := getFieldName(View.Columns[I]);
          if AFiledName <> '' then
            tValues[keyValue].Add(AFiledName, Value);
        end;
      end;
    end;
  end;

  if tValues.Count = 0 then
    Exit;

  worker := TColumnUnboundThread.Create(tValues, uColumns, gridKey);
  worker.OnTerminate := OnTerminateThread;
  for row in Rows do
  begin
    keyValue := getKeyValue(row, View, gridViewKeys[gridKey]);
    processRows[gridKey].Add(keyValue, worker);
  end;
  worker.Start;
  Rows.Free;
end;

procedure TColumnUnboundHelper.OnTerminateThread(Sender: TObject);
var
  worker: TColumnUnboundThread;
  tResult: TDictionary<Variant, TDictionary<string, Variant>>;
  row: TPair<Variant, TDictionary<string, Variant>>;
  Column: TPair<string, Variant>;
  timeNow: TDateTime;
  keyValue: Variant;
  tableItem: TcxCustomGridTableItem;
  FieldName: string;
begin
  timeNow := now;
  worker := TColumnUnboundThread(Sender);
  try
    // Write results
    tResult := worker.ColumnUnboundResult;
    if gridModes[worker.gridKey] = smGrid then
    begin
      gridViews[worker.gridKey].DataController.BeginFullUpdate;
      try
        for row in tResult do
        begin
          for Column in row.Value do
          begin
            tableItem := gridViews[worker.gridKey].FindItemByName(Column.Key);
            if Assigned(tableItem) then
            begin
              gridViews[worker.gridKey].DataController.Values[row.Key, tableItem.Index] := Column.Value;

              FieldName := getFieldName(TcxGridColumn(tableItem), True);
              if not FieldName.IsEmpty then
              begin
                setFieldValue(gridViews[worker.gridKey], row.Key, FieldName, Column.Value,
                  gridViewKeys[worker.gridKey]);
              end;
            end;
          end;
          processRows[worker.gridKey].Remove(row.Key);
          processedRows[worker.gridKey].AddOrSetValue(row.Key, timeNow);
        end;
      finally
        gridViews[worker.gridKey].DataController.EndFullUpdate;
        processRows[worker.gridKey].TrimExcess;
      end;
    end
    else
    begin
      try
        for row in tResult do
        begin
          keyValue := row.Key;

          if not VarIsNull(keyValue) then
          begin
            for Column in row.Value do
            begin
              if unboundColumnsData[worker.gridKey].ContainsKey(Column.Key) then
              begin
                unboundColumnsData[worker.gridKey][Column.Key].AddOrSetValue(keyValue, Column.Value);
              end;
            end;
          end;
          processRows[worker.gridKey].Remove(row.Key);
          processedRows[worker.gridKey].AddOrSetValue(row.Key, timeNow);
        end;
        gridViews[worker.gridKey].DataController.Refresh;
      finally
        processRows[worker.gridKey].TrimExcess;
      end;
    end;
  finally
    gridViews[worker.gridKey].Painter.Invalidate;
  end;

  if processRows[worker.gridKey].Count = 0 then
  begin
    if runFullUpdate then
    begin
      runFullUpdate := False;
      if Assigned(OnCompleted) then
        OnCompleted(gridViews[worker.gridKey]);
    end;
  end;
end;

function TColumnUnboundHelper.getLastUpdate(View: TcxGridTableView; row: TcxCustomGridRecord;
  var fromLastUpdate: Integer): Boolean;
var
  gridKey: string;
  timeNow: TDateTime;
  keyValue: Variant;
begin
  Result := False;
  gridKey := View.Owner.Name + View.Name;
  keyValue := getKeyValue(TcxCustomGridRow(row), View, gridViewKeys[gridKey]);
  if isProcessed(gridKey, keyValue) then
  begin
    timeNow := now;
    fromLastUpdate := SecondsBetween(timeNow, processedRows[gridKey][keyValue]);
    Result := True;
  end;
end;

procedure TColumnUnboundHelper.setVScrollPosition(View: TcxGridTableView);
begin
  lastVScrollPos.AddOrSetValue(View.Owner.Name + View.Name, View.Site.VScrollBar.Position);
end;

function TColumnUnboundHelper.isVScrollChange(View: TcxGridTableView): Boolean;
begin
  Result := Abs(View.Site.VScrollBar.Position - lastVScrollPos[View.Owner.Name + View.Name]) > 0;
end;

procedure TColumnUnboundHelper.postEditor(View: TcxGridTableView; edit: TcxCustomEdit);
var
  itemName, gridKey: string;
  keyValue: Variant;
begin
  gridKey := View.Owner.Name + View.Name;
  keyValue := getKeyValue(TcxCustomGridRow(View.ViewData.EditingRecord), View, gridViewKeys[gridKey]);
  itemName := getFocusedItemName(View);
  if not VarIsNull(keyValue) then
  begin
    if unboundColumnsData[gridKey].ContainsKey(itemName) then
    begin
      edit.PostEditValue;
      unboundColumnsData[gridKey][itemName].AddOrSetValue(keyValue, edit.EditValue);
    end;
  end;
end;

procedure TColumnUnboundHelper.resetGrid(View: TcxGridTableView);
var
  gridKey: string;
  Column: TPair<string, TDictionary<Variant, Variant>>;
  row: TPair<Variant, TDateTime>;
  unboundColumn: TPair<string, TUnboundColumn>;
  columnItem: TcxCustomGridTableItem;
begin
  gridKey := View.Owner.Name + View.Name;
  if not gridViews.ContainsKey(gridKey) then
    Exit;
  if processedRows.ContainsKey(gridKey) then
  begin
    if gridModes.ContainsKey(gridKey) and (gridModes[gridKey] = smGrid) and unboundColumns.ContainsKey(gridKey) then
    begin
      gridViews[gridKey].DataController.BeginFullUpdate;
      try
        for unboundColumn in unboundColumns[gridKey] do
        begin
          columnItem := gridViews[gridKey].FindItemByName(unboundColumn.Key);
          if Assigned(columnItem) then
            for row in processedRows[gridKey] do
              if row.Key < gridViews[gridKey].DataController.RecordCount then
                gridViews[gridKey].DataController.Values[row.Key, columnItem.Index] := Null;
        end;
      finally
        gridViews[gridKey].DataController.EndFullUpdate;
      end;
    end;
    processedRows[gridKey].Clear;
    processedRows[gridKey].TrimExcess;
  end;
  if gridModes.ContainsKey(gridKey) and (gridModes[gridKey] = smDict) and unboundColumnsData.ContainsKey(gridKey) then
  begin
    for Column in unboundColumnsData[gridKey] do
    begin
      Column.Value.Clear;
      Column.Value.TrimExcess;
    end;
  end;
  gridViews[gridKey].Painter.Invalidate;
  TryProcessUnbound(gridViews[gridKey]);
end;

function TColumnUnboundHelper.HasUnboundColumns(gridView: TcxGridTableView): Boolean;
var
  gridKey: string;
begin
  Result := False;
  gridKey := gridView.Owner.Name + gridView.Name;
  if isEmptyUnbound.ContainsKey(gridKey) then
    Result := not isEmptyUnbound[gridKey];
end;

procedure TColumnUnboundHelper.assignFieldName(Column: TcxGridColumn; FieldName: string);
var
  gridKey: string;
  ColumnName: string;
begin
  if isUnbound(Column) then
  begin
    gridKey := Column.gridView.Owner.Name + Column.gridView.Name;
    ColumnName := Column.Name;
    unboundColumns[gridKey][ColumnName].setFieldName(FieldName);
    setFieldName(Column, FieldName);
  end;
end;

{ ****************************************************************************** }

{ Events************************************************************************ }
procedure TColumnUnboundHelper.TableViewTopRecordIndexChanged(Sender: TObject);
var
  viewSender: TcxGridTableView;
  gridKey: string;
  Event1, Event2: TNotifyEvent;
begin
  try
    viewSender := Sender as TcxGridTableView;
    gridKey := viewSender.Owner.Name + viewSender.Name;
    if gridTopRowEvents.ContainsKey(gridKey) and Assigned(gridTopRowEvents[gridKey]) then
    begin
      Event1 := TableViewTopRecordIndexChanged;
      Event2 := gridTopRowEvents[gridKey];
      if @Event1 <> @Event2 then
        gridTopRowEvents[gridKey](Sender);
    end;
    setVScrollPosition(viewSender);
  finally
  end;
end;

procedure TColumnUnboundHelper.TableViewEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
var
  viewSender: TcxGridTableView;
  gridKey: string;
  Event1, Event2: TcxGridCustomTableItemEvent;
begin
  try
    viewSender := Sender as TcxGridTableView;
    gridKey := viewSender.Owner.Name + viewSender.Name;
    if gridEditValueEvents.ContainsKey(gridKey) and Assigned(gridEditValueEvents[gridKey]) then
    begin
      Event1 := TableViewEditValueChanged;
      Event2 := gridEditValueEvents[gridKey];
      if @Event1 <> @Event2 then
        gridEditValueEvents[gridKey](Sender, AItem);
    end;
    TryUpdateUnboundRow(viewSender, AItem);
  finally
  end;
end;

function TColumnUnboundHelper.TableViewInList(View: TcxGridTableView): Boolean;
var
  gridKey: string;
begin
  gridKey := View.Owner.Name + View.Name;
  Result := gridViews.ContainsKey(gridKey);
end;

procedure TColumnUnboundHelper.TableViewKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  viewSender: TcxGridTableView;
  gridKey: string;
  KeyEvent1, KeyEvent2: TKeyEvent;
begin
  try
    viewSender := ((Sender as TcxControl).Parent as TcxGrid).FocusedView as TcxGridTableView;
    gridKey := viewSender.Owner.Name + viewSender.Name;
    if gridKeyUpEvents.ContainsKey(gridKey) and Assigned(gridKeyUpEvents[gridKey]) then
    begin
      KeyEvent1 := TableViewKeyUp;
      KeyEvent2 := gridKeyUpEvents[gridKey];
      if @KeyEvent1 <> @KeyEvent2 then
        gridKeyUpEvents[gridKey](Sender, Key, Shift);
    end;

    if Key in [VK_PRIOR, VK_NEXT] then
      if gridTimers.ContainsKey(gridKey) then
        gridTimers[gridKey].Enabled := True;
  finally
  end;
end;

procedure TColumnUnboundHelper.TableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  viewSender: TcxGridTableView;
  gridKey: string;
  KeyEvent1, KeyEvent2: TKeyEvent;
begin
  try
    viewSender := ((Sender as TcxControl).Parent as TcxGrid).FocusedView as TcxGridTableView;
    gridKey := viewSender.Owner.Name + viewSender.Name;

    if gridKeyDownEvents.ContainsKey(gridKey) and Assigned(gridKeyDownEvents[gridKey]) then
    begin
      KeyEvent1 := TableViewKeyDown;
      KeyEvent2 := gridKeyDownEvents[gridKey];
      if @KeyEvent1 <> @KeyEvent2 then
        gridKeyDownEvents[gridKey](Sender, Key, Shift);
    end;

    if Key in [VK_PRIOR, VK_NEXT] then
      if gridTimers.ContainsKey(gridKey) then
        gridTimers[gridKey].Enabled := False;
  finally
  end;
end;

procedure TColumnUnboundHelper.TableViewDataControllerCompare(ADataController: TcxCustomDataController;
  ARecordIndex1, ARecordIndex2, AItemIndex: Integer; const V1, V2: Variant; var Compare: Integer);
var
  View: TcxGridTableView;
  gridKey: string;
  cType: TDataTypes;
  f1, f2: double;
  d1, d2: TDateTime;
  Event1, Event2: TcxDataControllerCompareEvent;
  ACompareDone: Boolean;
begin
  View := ADataController.GetOwner as TcxGridTableView;
  gridKey := View.Owner.Name + View.Name;
  ACompareDone := False;
  if isUnbound(View, AItemIndex) then
  begin
    ACompareDone := True;
    cType := unboundColumns[gridKey][View.Items[AItemIndex].Name].cType;
    case cType of
      dtFloat .. dtCurrency:
        begin
          f1 := V1;
          f2 := V2;
          if f1 > f2 then
            Compare := 1
          else if f1 = f2 then
            Compare := 0
          else if f1 < f2 then
            Compare := -1;
        end;
      dtDate:
        begin
          d1 := V1;
          d2 := V2;
          if d1 > d2 then
            Compare := 1
          else if d1 = d2 then
            Compare := 0
          else if d1 < d2 then
            Compare := -1;
        end;
    else
      ACompareDone := False;
    end;
  end
  else
  begin
    if gridDataControllerCompareEvents.ContainsKey(gridKey) and Assigned(gridDataControllerCompareEvents[gridKey]) then
    begin
      Event1 := TableViewDataControllerCompare;
      Event2 := gridDataControllerCompareEvents[gridKey];
      if @Event1 <> @Event2 then
      begin
        ACompareDone := True;
        gridDataControllerCompareEvents[gridKey](ADataController, ARecordIndex1, ARecordIndex2, AItemIndex, V1,
          V2, Compare);
      end;
    end;
  end;

  if not ACompareDone then
  begin
    if V1 > V2 then
      Compare := 1
    else if V1 = V2 then
      Compare := 0
    else if V1 < V2 then
      Compare := -1;
  end;
end;

procedure TColumnUnboundHelper.ColumnGetDataText(Sender: TcxCustomGridTableItem; ARecordIndex: Integer;
  var AText: string);
var
  gridKey, columnKey: string;
  keyValue: Variant;
begin
  if ARecordIndex < 0 then
    Exit;
  columnKey := Sender.Name;
  gridKey := Sender.gridView.Owner.Name + Sender.gridView.Name;
  if not unboundColumnsData[gridKey].ContainsKey(columnKey) then
    Exit;
  if not unboundColumns[gridKey][columnKey].TargetFieldName.IsEmpty then
    Exit;
  keyValue := getKeyValue(TcxCustomGridRow(Sender.gridView.ViewData.GetRecordByRecordIndex(ARecordIndex)),
    TcxGridTableView(Sender.gridView), gridViewKeys[gridKey]);
  if (not VarIsNull(keyValue)) and unboundColumnsData[gridKey][columnKey].ContainsKey(keyValue) then
    AText := VarToStr(unboundColumnsData[gridKey][columnKey][keyValue])
  else
    AText := string.Empty;
end;

procedure TColumnUnboundHelper.ColumnGetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
var
  gridKey, columnKey: string;
  keyValue, Value: Variant;
  Column: TUnboundColumn;
begin
  if not Assigned(ARecord) then
    Exit;
  columnKey := Sender.Name;
  gridKey := Sender.gridView.Owner.Name + Sender.gridView.Name;
  if not unboundColumnsData[gridKey].ContainsKey(columnKey) then
    Exit;
  if not unboundColumns[gridKey][columnKey].TargetFieldName.IsEmpty then
    Exit;
  Column := unboundColumns[gridKey][columnKey];
  if (Column.cType in [dtCurrency, dtString]) and (not Column.Mask.IsEmpty) then
  begin
    keyValue := getKeyValue(TcxCustomGridRow(ARecord), TcxGridTableView(Sender.gridView), gridViewKeys[gridKey]);
    if (not VarIsNull(keyValue)) and unboundColumnsData[gridKey][columnKey].ContainsKey(keyValue) then
    begin
      Value := unboundColumnsData[gridKey][columnKey][keyValue];
      case Column.cType of
        dtCurrency:
          begin
            if not VarIsNull(Value) then
              AText := FormatCurr(Column.Mask, Value)
            else
              AText := string.Empty;
          end;
        dtString:
          begin
            if not VarIsNull(Value) then
              AText := Format(Column.Mask, [Value])
            else
              AText := string.Empty;
          end;
      end;
    end;
  end;
end;

procedure TColumnUnboundHelper.ColumnEditValueChanged(Sender: TObject);
var
  View: TcxGridTableView;
  edit: TcxCustomEdit;
  gridKey, itemName, colName, textStr, expTxt: string;
  doChangeRow: Boolean;
  uColumn, itemColumn: TUnboundColumn;
  params: TDictionary<string, Variant>;
  Column: TcxGridColumn;
  uColumns: TDictionary<string, TUnboundColumn>;
  I, RecordIndex: Integer;
  Rows: TList<TcxCustomGridRow>;
  keyValue: Variant;
begin
  edit := TcxCustomEdit(Sender);
  View := TcxGridTableView(edit.Owner);
  gridKey := View.Owner.Name + View.Name;
  if isEmptyUnbound.ContainsKey(gridKey) and (not isEmptyUnbound[gridKey]) and processRows.ContainsKey(gridKey) and
    processedRows.ContainsKey(gridKey) then
  begin
    itemName := getFocusedItemName(View);
    if itemName.IsEmpty then
      Exit;
    doChangeRow := False;
    RecordIndex := View.ViewData.EditingRecord.RecordIndex;
    keyValue := getKeyValue(TcxCustomGridRow(View.ViewData.EditingRecord), gridViews[gridKey], gridViewKeys[gridKey]);

    if isUnbound(View, itemName) then
    begin
      itemColumn := unboundColumns[gridKey][itemName];
      if itemColumn.Mode in [umSQLEdit, umSQLEditByCondition, umWSEdit, umWSEditByCondition] then
      begin
        if (not isProcessed(gridKey, keyValue)) or isProcess(gridKey, keyValue) then
        begin
          undoEdit(edit);
          Exit;
        end;

        textStr := string.Empty;
        params := TDictionary<string, Variant>.Create;
        if itemColumn.Mode in [umSQLEdit, umSQLEditByCondition] then
          textStr := ColumnUnboundHelperData.prepareSQLUpdateText(edit, itemColumn, params, colName)
        else
          textStr := ColumnUnboundHelperData.prepareWSPostText(edit, itemColumn, colName);
        if not textStr.IsEmpty then
        begin
          if itemColumn.Mode in [umSQLEdit, umSQLEditByCondition] then
            ColumnUnboundHelperData.runSQLRequest(textStr, params, expTxt)
          else
            ColumnUnboundHelperData.runWSRequest(textStr,
              TJSONObject.Create(TJSONPair.Create(colName, VarToStr(edit.EditValue))).ToString, expTxt);
          if not expTxt.IsEmpty then
          begin
            messagedlg(Format('Error while update: %s', [expTxt]), mtError, [mbOK], 0);
            params.Free;
            Exit;
          end
          else
          begin
            Column := ColumnUnboundHelperData.findGridViewColumn(View, colName);
            if (Column <> nil) and params.ContainsKey(colName) then
            begin
              View.DataController.Values[RecordIndex, Column.Index] := params[colName];
            end;
          end;
        end;
        params.Free;
        if textStr.IsEmpty or (not expTxt.IsEmpty) then
          Exit;
        doChangeRow := True;
      end
      else
        Exit;
    end
    else
      postEditor(View, edit);

    uColumns := TDictionary<string, TUnboundColumn>.Create;
    for I := 0 to View.ColumnCount - 1 do
    begin
      if View.Columns[I].Visible and isUnbound(View.Columns[I]) then
      begin
        colName := View.Columns[I].Name;
        uColumn := unboundColumns[gridKey][View.Columns[I].Name];
        uColumns.Add(colName, uColumn);
        if (not doChangeRow) and uColumns[colName].containsField(itemName) then
          doChangeRow := True;
      end
    end;

    if doChangeRow and (not isProcess(gridKey, keyValue)) then
    begin
      Rows := TList<TcxCustomGridRow>.Create;
      Rows.Add(gridViews[gridKey].ViewData.Records[RecordIndex] as TcxCustomGridRow);
      createThread(gridViews[gridKey], Rows, uColumns);
    end
    else
      uColumns.Free;
  end;
end;
{ ****************************************************************************** }

{ Draw styles******************************************************************* }
function TColumnUnboundHelper.TryFindProcessingUnbound(ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo): Boolean;
var
  Sender: TcxCustomGridTableView;
  gridKey, colName: string;
  keyValue: Variant;
begin
  Result := False;
  Sender := AViewInfo.gridView;
  gridKey := Sender.Owner.Name + Sender.Name;
  keyValue := getKeyValue(TcxCustomGridRow(AViewInfo.RecordViewInfo.GridRecord), gridViews[gridKey],
    gridViewKeys[gridKey]);
  colName := AViewInfo.Item.Name;
  if isUnbound(Sender as TcxGridTableView, colName) and isProcess(gridKey, keyValue) and isProcessed(gridKey, keyValue)
  then
  begin
    ACanvas.Brush.Color := unboundProcessingColor;
    Result := True;
  end;
end;
{ ****************************************************************************** }

function TColumnUnboundHelper.addOrSetFieldUnbound(View: TcxGridTableView; FieldName: string;
  fieldValue: Variant): string;
var
  gridKey: string;
  memo: TStringList;
  isNewColumn: Boolean;
begin
  Result := string.Empty;
  gridKey := View.Owner.Name + View.Name;
  if unboundColumns.ContainsKey(gridKey) and (not VarIsNull(fieldValue)) then
  begin
    memo := TStringList.Create;
    memo.text := fieldValue;
    if not string.isNullOrEmpty(memo.text) then
    begin
      isNewColumn := FieldName.IsEmpty;
      if isNewColumn then
      begin
        if (memo.Count > 0) and (not string.isNullOrEmpty(memo.Values['name'])) then
          FieldName := memo.Values['name']
        else
          FieldName := getNewUnboundColumnName(View);
      end;
      unboundColumns[gridKey].AddOrSetValue(FieldName, listToUnboundColumn(View, memo));
      if gridModes[gridKey] = smDict then
        unboundColumnsData[gridKey].AddOrSetValue(FieldName, TDictionary<Variant, Variant>.Create);
      if isNewColumn then
        createUnboundColumn(gridViews[gridKey], unboundColumns[gridKey][FieldName]);
      Result := FieldName;
    end;
    memo.Free;
  end;
end;

function TColumnUnboundHelper.getKeyValue(row: TcxCustomGridRow; View: TcxGridTableView; KeyField: string): Variant;
var
  keyItem: TcxCustomGridTableItem;
  gridKey: string;
begin
  gridKey := View.Owner.Name + View.Name;
  Result := Null;
  if not Assigned(row) then
    Exit;
  if (gridModes[gridKey] = smGrid) or KeyField.IsEmpty then
    Result := row.RecordIndex
  else
  begin
    keyItem := ColumnUnboundHelperData.findGridViewColumn(View, KeyField, False);
    if Assigned(keyItem) then
    begin
      Result := row.Values[keyItem.Index];
    end;
  end;
end;

// Find column caption
function TColumnUnboundHelper.findFieldCaption(View: TcxGridTableView; ColumnName: string): string;
var
  colName: string;
  Column: TcxGridColumn;
  I: Integer;
begin
  Result := string.Empty;
  for I := 0 to View.ColumnCount - 1 do
  begin
    Column := View.Columns[I];
    colName := getFieldName(Column);
    if colName.IsEmpty then
      colName := Column.Name;
    if colName = ColumnName then
    begin
      Result := Column.Caption;
      break;
    end;
  end;
end;

// Update column unbound exp
procedure TColumnUnboundHelper.updateUnbound(View: TcxGridTableView; ColumnName: string; Column: TUnboundColumn);
var
  gridKey: string;
begin
  gridKey := View.Owner.Name + View.Name;
  if unboundColumns.ContainsKey(gridKey) then
  begin
    unboundColumns[gridKey].AddOrSetValue(ColumnName, Column);
    if gridModes[gridKey] = smDict then
      unboundColumnsData[gridKey].AddOrSetValue(ColumnName, TDictionary<Variant, Variant>.Create);
    isEmptyUnbound[gridKey] := False;
    processedRows[gridKey].Clear;
    TryProcessUnbound(View);
    gridViews[gridKey].Painter.Invalidate;
  end;
end;

// Delete column
procedure TColumnUnboundHelper.deleteColumn(View: TcxGridTableView; ColumnName: string);
var
  Column: TcxCustomGridTableItem;
  gridKey: string;
begin
  gridKey := View.Owner.Name + View.Name;
  if unboundColumns.ContainsKey(gridKey) and unboundColumns[gridKey].ContainsKey(ColumnName) then
  begin
    unboundColumns[gridKey].Remove(ColumnName);
    if gridModes[gridKey] = smDict then
      unboundColumnsData[gridKey].Remove(ColumnName);
    Column := gridViews[gridKey].FindItemByName(ColumnName);
    if Column <> nil then
    begin
      gridViews[gridKey].BeginUpdate;
      Column.Destroy;
      gridViews[gridKey].EndUpdate;
    end;
    isEmptyUnbound[gridKey] := not isFoundUnbound(gridViews[gridKey]);
    processedRows[gridKey].Clear;
    TryProcessUnbound(View);
  end;
end;

// Recover column
procedure TColumnUnboundHelper.restoreColumn(View: TcxGridTableView; Column: TcxGridColumn);
var
  unboundColumn: TUnboundColumn;
  gridKey: string;
begin
  gridKey := View.Owner.Name + View.Name;
  if Assigned(Column) and unboundColumns.ContainsKey(gridKey) and unboundColumns[gridKey].ContainsKey(Column.Name) then
  begin
    unboundColumn := unboundColumns[gridKey][Column.Name];
    with Column do
    begin
      case unboundColumn.cType of
        dtInt:
          begin
            DataBinding.ValueTypeClass := TcxIntegerValueType;
            PropertiesClass := TcxSpinEditProperties;
            TcxSpinEditProperties(Properties).ValueType := vtInt;
          end;
        dtFloat:
          begin
            DataBinding.ValueTypeClass := TcxFloatValueType;
            PropertiesClass := TcxSpinEditProperties;
            TcxSpinEditProperties(Properties).ValueType := vtFloat;
          end;
        dtCurrency:
          begin
            DataBinding.ValueTypeClass := TcxCurrencyValueType;
            PropertiesClass := TcxCurrencyEditProperties;;
            if not unboundColumn.Mask.IsEmpty then
            begin
              with TcxCurrencyEditProperties(Properties) do
              begin
                DisplayFormat := unboundColumn.Mask;
              end;
            end;
          end;
        dtDate:
          begin
            DataBinding.ValueTypeClass := TcxDateTimeValueType;
            PropertiesClass := TcxDateEditProperties;
          end;
        dtString:
          begin
            DataBinding.ValueTypeClass := TcxStringValueType;
            if unboundColumn.Mask.IsEmpty then
              PropertiesClass := TcxTextEditProperties
            else
            begin
              PropertiesClass := TcxMaskEditProperties;
              with TcxMaskEditProperties(Properties) do
              begin
                EditMask := unboundColumn.Mask;
              end;
            end;
          end;
      end;
      name := unboundColumn.Name;
      Options.Editing := (unboundColumn.Mode in [umSQLEdit, umSQLEditByCondition, umWSEdit, umWSEditByCondition]);
      if gridModes[gridKey] = smDict then
      begin
        OnGetDataText := ColumnGetDataText;
        OnGetDisplayText := ColumnGetDisplayText;
        if Options.Editing then
          Properties.OnEditValueChanged := ColumnEditValueChanged;
      end;
    end;
    setFieldName(Column, unboundColumn.TargetFieldName);
    unboundColumn.Caption := Column.Caption;
    unboundColumn.Width := Column.Width;
    unboundColumns[gridKey][Column.Name] := unboundColumn;
    isEmptyUnbound[gridKey] := False;
  end;
end;

// Get unbound column
function TColumnUnboundHelper.listToUnboundColumn(View: TcxGridTableView; ColumnList: TStringList): TUnboundColumn;
  function conditionCount(list: TStringList): Integer;
  var
    I: Integer;
  begin
    for I := 4 to list.Count - 1 do
      if (list[I].StartsWith('SQL') or list[I].StartsWith('WS')) then
      begin
        Result := I - 4;
        Exit;
      end;
    Result := list.Count - 4;
  end;

var
  I, countCond: Integer;
  id, strval: string;
  Item: TConditionUnbound;
  varValue: Variant;
begin
  Result.Conditions := TList<TConditionUnbound>.Create;
  Result.sqlStatements := TDictionary<string, string>.Create;
  Result.sqlUpdateStatements := TDictionary<string, string>.Create;
  Result.wsStatements := TDictionary<string, string>.Create;
  Result.wsPostStatements := TDictionary<string, string>.Create;
  Result.updatePeriods := TDictionary<string, Integer>.Create;

  if Assigned(ColumnList) then
  begin
    Result.Name := ColumnList.Values['name'];
    Result.cType := TDataTypes(StrToInt(ColumnList.Values['type']));
    Result.Mask := ColumnList.Values['mask'];
    Result.Mode := TUnboundModes(StrToInt(ColumnList.Values['umode']));
    Result.Expression := ColumnList.Values['uexp'];
    Result.TargetFieldName := ColumnList.Values['targetFN'];

    countCond := conditionCount(ColumnList);
    for I := 1 to (countCond div countConditionUnboundItems) do
    begin
      id := IntToStr(I);
      Item.Mode := TCompareModes(StrToInt(ColumnList.Values['mode' + id]));
      Item.ColumnName1 := ColumnList.Values['column1_' + id];
      if not Item.ColumnName1.IsEmpty then
      begin
        Item.ColumnCaption1 := findFieldCaption(View, Item.ColumnName1);
      end;
      Item.Condition := ColumnList.Values['condition' + id];
      Item.ColumnName2 := ColumnList.Values['column2_' + id];
      if not Item.ColumnName2.IsEmpty then
      begin
        Item.ColumnCaption2 := findFieldCaption(View, Item.ColumnName2);
      end;

      strval := ColumnList.Values['value' + id];
      getStrType(strval, varValue);
      Item.Value := varValue;

      Item.Operand := TOperands(StrToInt(ColumnList.Values['op' + id]));
      Item.Expression := ColumnList.Values['exp' + id];

      Result.Conditions.Add(Item);
    end;

    for I := countCond + 4 to ColumnList.Count - 1 do
    begin
      if ColumnList[I].StartsWith('SQL') then
        Result.sqlStatements.Add(ColumnList.Names[I], ColumnList.Values[ColumnList.Names[I]])
      else if ColumnList[I].StartsWith('WS') then
        Result.wsStatements.Add(ColumnList.Names[I], ColumnList.Values[ColumnList.Names[I]])
      else if ColumnList[I].StartsWith('Update') then
        Result.sqlUpdateStatements.Add(ReplaceStr(ColumnList.Names[I], 'Update', ''),
          ColumnList.Values[ColumnList.Names[I]])
      else if ColumnList[I].StartsWith('Period') then
        Result.updatePeriods.Add(ReplaceStr(ColumnList.Names[I], 'Period', ''),
          StrToInt(ColumnList.Values[ColumnList.Names[I]]))
      else if ColumnList[I].StartsWith('Post') then
        Result.wsPostStatements.Add(ReplaceStr(ColumnList.Names[I], 'Post', ''),
          ColumnList.Values[ColumnList.Names[I]]);
    end;
  end;
end;

function TColumnUnboundHelper.unboundColumnToString(View: TcxGridTableView; ColumnName: string): string;
var
  unboundColumn: TUnboundColumn;
  gridKey, idx: string;
  unboundList: TStringList;
  I: Integer;
  Condition: TConditionUnbound;
  pair: TPair<string, string>;
  period: TPair<string, Integer>;
begin
  Result := string.Empty;
  gridKey := View.Owner.Name + View.Name;
  if unboundColumns.ContainsKey(gridKey) and unboundColumns[gridKey].ContainsKey(ColumnName) then
  begin
    unboundColumn := unboundColumns[gridKey][ColumnName];
    unboundList := TStringList.Create;
    unboundList.Add('name=' + unboundColumn.Name);
    unboundList.Add('type=' + IntToStr(Ord(unboundColumn.cType)));
    unboundList.Add('mask=' + unboundColumn.Mask);
    unboundList.Add('umode=' + IntToStr(Ord(unboundColumn.Mode)));
    unboundList.Add('uexp=' + unboundColumn.Expression);
    unboundList.Add('targetFN=' + unboundColumn.TargetFieldName);

    for I := 0 to unboundColumn.Conditions.Count - 1 do
    begin
      Condition := unboundColumn.Conditions[I];
      idx := IntToStr(I + 1);
      unboundList.Add('mode' + idx + '=' + IntToStr(Ord(Condition.Mode)));
      unboundList.Add('column1_' + idx + '=' + Condition.ColumnName1);
      unboundList.Add('condition' + idx + '=' + Condition.Condition);
      unboundList.Add('column2_' + idx + '=' + Condition.ColumnName2);
      case ValueType(Condition.Value) of
        dtInt:
          unboundList.Add('value' + idx + '=' + IntToStr(Condition.Value));
        dtFloat .. dtCurrency:
          unboundList.Add('value' + idx + '=' + FloatToStr(Condition.Value));
        dtDate:
          unboundList.Add('value' + idx + '=' + DateTimeToStr(Condition.Value));
        dtString:
          unboundList.Add('value' + idx + '=' + Condition.Value);
      else
        unboundList.Add('value' + idx + '=');
      end;
      unboundList.Add('op' + idx + '=' + IntToStr(Ord(Condition.Operand)));
      unboundList.Add('exp' + idx + '=' + Condition.Expression);
    end;
    for pair in unboundColumn.sqlStatements do
      unboundList.Add(pair.Key + '=' + pair.Value);
    for pair in unboundColumn.sqlUpdateStatements do
      unboundList.Add('Update' + pair.Key + '=' + pair.Value);
    for pair in unboundColumn.wsStatements do
      unboundList.Add(pair.Key + '=' + pair.Value);
    for pair in unboundColumn.wsPostStatements do
      unboundList.Add('Post' + pair.Key + '=' + pair.Value);
    for period in unboundColumn.updatePeriods do
      unboundList.Add('Period' + period.Key + '=' + IntToStr(period.Value));
    Result := unboundList.text;
    unboundList.Free;
  end;
end;

// Create after dialog
procedure TColumnUnboundHelper.createUnboundColumn(View: TcxGridTableView; Column: TUnboundColumn;
  parentColumn: TcxGridColumn);
var
  gridKey: string;
  AColumn: TcxGridColumn;
begin
  gridKey := View.Owner.Name + View.Name;
  with View do
  begin
    BeginUpdate;
    AColumn := createGridColumn(View, parentColumn);
    with AColumn do
    begin
      name := Column.Name;
      Options.Sorting := View.OptionsCustomize.ColumnSorting;
      Options.Filtering := False;
      Options.IncSearch := False;
      Options.Editing := (Column.Mode in [umSQLEdit, umSQLEditByCondition, umWSEdit, umWSEditByCondition]);
      Caption := Column.Caption;
      Width := Column.Width;
      case Column.cType of
        dtInt:
          begin
            DataBinding.ValueTypeClass := TcxIntegerValueType;
            PropertiesClass := TcxSpinEditProperties;
            TcxSpinEditProperties(Properties).ValueType := vtInt;
          end;
        dtFloat:
          begin
            DataBinding.ValueTypeClass := TcxFloatValueType;
            PropertiesClass := TcxSpinEditProperties;
            TcxSpinEditProperties(Properties).ValueType := vtFloat;
          end;
        dtCurrency:
          begin
            DataBinding.ValueTypeClass := TcxCurrencyValueType;
            PropertiesClass := TcxCurrencyEditProperties;;
            if not Column.Mask.IsEmpty then
            begin
              with TcxCurrencyEditProperties(Properties) do
              begin
                DisplayFormat := Column.Mask;
              end;
            end;
          end;
        dtDate:
          begin
            DataBinding.ValueTypeClass := TcxDateTimeValueType;
            PropertiesClass := TcxDateEditProperties;
          end;
        dtString:
          begin
            DataBinding.ValueTypeClass := TcxStringValueType;
            if Column.Mask.IsEmpty then
              PropertiesClass := TcxTextEditProperties
            else
            begin
              DataBinding.ValueTypeClass := TcxStringValueType;
              PropertiesClass := TcxMaskEditProperties;
              with TcxMaskEditProperties(Properties) do
              begin
                EditMask := Column.Mask;
              end;
            end;
          end;
      end;
      if gridModes[gridKey] = smDict then
      begin
        OnGetDataText := ColumnGetDataText;
        OnGetDisplayText := ColumnGetDisplayText;
        if Options.Editing then
          Properties.OnEditValueChanged := ColumnEditValueChanged;
      end;
    end;
    setFieldName(AColumn, Column.TargetFieldName);
    EndUpdate;
  end;
  isEmptyUnbound[View.Owner.Name + View.Name] := False;
end;

// Create before load from xml
procedure TColumnUnboundHelper.createUnboundColumn(View: TcxGridTableView; ColumnName: string; idx: Integer);
begin
  with View do
  begin
    BeginUpdate;
    with CreateColumn do
    begin
      Name := ColumnName;
      Options.Sorting := View.OptionsCustomize.ColumnSorting;
      Options.Filtering := False;
      Options.IncSearch := False;
      Options.Editing := False;
      if idx >= 0 then
        Index := idx;
    end;
    EndUpdate;
  end;
end;

procedure TColumnUnboundHelper.saveUnboundColumn(View: TcxGridTableView; Column: TUnboundColumn);
var
  gridColumn: TcxGridColumn;
begin
  // save
  updateUnbound(View, Column.Name, Column);
  gridColumn := View.FindItemByName(Column.Name) as TcxGridColumn;
  if Assigned(gridColumn) then
  begin
    View.BeginUpdate;
    with gridColumn do
    begin
      Caption := Column.Caption;
      Width := Column.Width;
      case Column.cType of
        dtInt:
          begin
            DataBinding.ValueTypeClass := TcxIntegerValueType;
            PropertiesClass := TcxSpinEditProperties;
            TcxSpinEditProperties(Properties).ValueType := vtInt;
          end;
        dtFloat:
          begin
            DataBinding.ValueTypeClass := TcxFloatValueType;
            PropertiesClass := TcxSpinEditProperties;
            TcxSpinEditProperties(Properties).ValueType := vtFloat;
          end;
        dtCurrency:
          begin
            DataBinding.ValueTypeClass := TcxCurrencyValueType;
            PropertiesClass := TcxCurrencyEditProperties;
            if not Column.Mask.IsEmpty then
            begin
              with TcxCurrencyEditProperties(Properties) do
              begin
                DisplayFormat := Column.Mask;
              end;
            end;
          end;
        dtDate:
          begin
            DataBinding.ValueTypeClass := TcxDateTimeValueType;
            PropertiesClass := TcxDateEditProperties;
          end;
        dtString:
          begin
            DataBinding.ValueTypeClass := TcxStringValueType;
            if Column.Mask.IsEmpty then
              PropertiesClass := TcxTextEditProperties
            else
            begin
              DataBinding.ValueTypeClass := TcxStringValueType;
              PropertiesClass := TcxMaskEditProperties;
              with TcxMaskEditProperties(Properties) do
              begin
                EditMask := Column.Mask;
              end;
            end;
          end;
      end;
      Options.Editing := (Column.Mode in [umSQLEdit, umSQLEditByCondition, umWSEdit, umWSEditByCondition]);
      if gridModes[View.Owner.Name + View.Name] = smDict then
      begin
        if Options.Editing then
          Properties.OnEditValueChanged := ColumnEditValueChanged;
      end;
    end;
    setFieldName(gridColumn, Column.TargetFieldName);
    View.EndUpdate;
  end;
end;

function TColumnUnboundHelper.isUnbound(View: TcxCustomGridTableView; ColumnName: string): Boolean;
var
  gridKey: string;
begin
  Result := False;
  gridKey := View.Owner.Name + View.Name;
  if unboundColumns.ContainsKey(gridKey) then
    Result := unboundColumns[gridKey].ContainsKey(ColumnName);
end;

function TColumnUnboundHelper.isUnbound(View: TcxCustomGridTableView; columnIndex: Integer): Boolean;
var
  gridKey: string;
begin
  Result := False;
  if (columnIndex < 0) or (columnIndex > View.ItemCount - 1) then
    Exit;
  gridKey := View.Owner.Name + View.Name;
  if unboundColumns.ContainsKey(gridKey) then
    Result := unboundColumns[gridKey].ContainsKey(View.Items[columnIndex].Name);
end;

function TColumnUnboundHelper.isUnbound(Column: TcxGridColumn): Boolean;
begin
  Result := (Column <> nil) and isUnbound(Column.gridView, Column.Name);
end;

function TColumnUnboundHelper.isFoundUnbound(View: TcxGridTableView): Boolean;
var
  I: Integer;
  Column: TcxGridColumn;
begin
  Result := False;
  for I := 0 to View.ColumnCount - 1 do
  begin
    Column := View.Columns[I];
    if isUnbound(Column) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TColumnUnboundHelper.isProcess(gridKey: string; keyValue: Variant): Boolean;
var
  worker: TThread;
begin
  if processRows.ContainsKey(gridKey) and processRows[gridKey].TryGetValue(keyValue, worker) then
  begin
    if Assigned(worker) then
    begin
      Result := True;
      Exit;
    end;
  end;
  Result := False;
end;

function TColumnUnboundHelper.isProcessed(gridKey: string; keyValue: Variant): Boolean;
begin
  Result := processedRows[gridKey].ContainsKey(keyValue);
end;

function TColumnUnboundHelper.sortColumns(Columns: TArray<TUnboundColumn>): TList<string>;
var
  Column: TUnboundColumn;
  nestedColumns, tColumns: TList<TUnboundColumn>;
  size: Integer;
begin
  Result := TList<string>.Create;
  nestedColumns := TList<TUnboundColumn>.Create;
  nestedColumns.AddRange(Columns);
  for Column in Columns do
  begin
    if (not Column.containsInColumns(Columns)) and (not Column.containsColumns(Columns)) then
    begin
      Result.Add(Column.Name);
      nestedColumns.Remove(Column);
    end;
  end;
  if Result.Count = length(Columns) then
  begin
    nestedColumns.Free;
    Exit;
  end;
  repeat
    size := Result.Count;
    tColumns := TList<TUnboundColumn>.Create;
    tColumns.AddRange(nestedColumns);
    for Column in nestedColumns do
    begin
      if (Column.containsInColumns(nestedColumns.ToArray())) and (not Column.containsColumns(nestedColumns.ToArray()))
      then
      begin
        Result.Add(Column.Name);
        tColumns.Remove(Column);
      end;
    end;
    nestedColumns.Clear;
    nestedColumns.AddRange(tColumns);
    tColumns.Free;
  until size = Result.Count;
  for Column in nestedColumns do
    Result.Add(Column.Name);
  nestedColumns.Free;
end;

{ TUnboundColumn }

procedure TUnboundColumn.setFieldName(FieldName: string);
begin
  Self.TargetFieldName := FieldName;
end;

function TUnboundColumn.containsField(FieldName: string): Boolean;
var
  colTemplate: string;
  pairCond: TConditionUnbound;
  pairData: TPair<string, string>;
begin
  colTemplate := Format('[%s]', [FieldName]);
  Result := False;
  if Self.Expression.Contains(colTemplate) then
  begin
    Result := True;
    Exit;
  end;
  for pairCond in Self.Conditions do
  begin
    if pairCond.Expression.Contains(colTemplate) then
    begin
      Result := True;
      Exit;
    end;
  end;
  for pairData in Self.sqlStatements do
  begin
    if pairData.Value.Contains(colTemplate) then
    begin
      Result := True;
      Exit;
    end;
  end;
  for pairData in Self.wsStatements do
  begin
    if pairData.Value.Contains(colTemplate) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TUnboundColumn.containsInColumns(Columns: TArray<TUnboundColumn>): Boolean;
var
  Column: TUnboundColumn;
begin
  Result := False;
  for Column in Columns do
  begin
    if (Column.Name <> Self.Name) and Column.containsField(Self.Name) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

function TUnboundColumn.containsColumns(Columns: TArray<TUnboundColumn>): Boolean;
var
  Column: TUnboundColumn;
begin
  Result := False;
  for Column in Columns do
  begin
    if (Column.Name <> Self.Name) and Self.containsField(Column.Name) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

procedure TColumnUnboundHelper.terminateThreads;
var
  grids: TPair<string, TDictionary<Variant, TThread>>;
  Rows: TPair<Variant, TThread>;
begin
  for grids in processRows do
  begin
    for Rows in grids.Value do
    begin
      if Assigned(Rows.Value) then
      begin
        Rows.Value.OnTerminate := nil;
        Rows.Value.Terminate;
        Rows.Value.WaitFor;
      end;
    end;
  end;
end;

procedure TColumnUnboundHelper.stoppingTimers;
var
  timer: TPair<string, TViewTimer>;
begin
  for timer in gridTimers do
  begin
    timer.Value.Enabled := False;
  end;
end;

initialization

_columnStyleHelper := TColumnUnboundHelper.Create;
_columnStyleHelper.gridViews := TDictionary<string, TcxGridTableView>.Create;
_columnStyleHelper.gridViewKeys := TDictionary<string, string>.Create;
_columnStyleHelper.gridModes := TDictionary<string, TStoreMode>.Create;
_columnStyleHelper.gridKeyUpEvents := TDictionary<string, TKeyEvent>.Create;
_columnStyleHelper.gridKeyDownEvents := TDictionary<string, TKeyEvent>.Create;
_columnStyleHelper.gridTopRowEvents := TDictionary<string, TNotifyEvent>.Create;
_columnStyleHelper.gridEditValueEvents := TDictionary<string, TcxGridCustomTableItemEvent>.Create;
_columnStyleHelper.gridDataControllerCompareEvents := TDictionary<string, TcxDataControllerCompareEvent>.Create;
_columnStyleHelper.gridTimers := TDictionary<string, TViewTimer>.Create;
_columnStyleHelper.processRows := TDictionary < string, TDictionary < Variant, TThread >>.Create;
_columnStyleHelper.processedRows := TDictionary < string, TDictionary < Variant, TDateTime >>.Create;
_columnStyleHelper.isEmptyUnbound := TDictionary<string, Boolean>.Create;
_columnStyleHelper.unboundColumns := TDictionary < string, TDictionary < string, TUnboundColumn >>.Create;
_columnStyleHelper.unboundColumnsData := TDictionary < string, TDictionary < string,
  TDictionary<Variant, Variant> >>.Create;
_columnStyleHelper.lastVScrollPos := TDictionary<string, Integer>.Create;

finalization

if Assigned(_columnStyleHelper) then
begin
  _columnStyleHelper.terminateThreads;
  FreeAndNil(_columnStyleHelper.gridViews);
  FreeAndNil(_columnStyleHelper.gridViewKeys);
  FreeAndNil(_columnStyleHelper.gridModes);
  FreeAndNil(_columnStyleHelper.gridKeyUpEvents);
  FreeAndNil(_columnStyleHelper.gridKeyDownEvents);
  FreeAndNil(_columnStyleHelper.gridTopRowEvents);
  FreeAndNil(_columnStyleHelper.gridEditValueEvents);
  FreeAndNil(_columnStyleHelper.gridDataControllerCompareEvents);
  FreeAndNil(_columnStyleHelper.processRows);
  _columnStyleHelper.stoppingTimers;
  FreeAndNil(_columnStyleHelper.gridTimers);
  FreeAndNil(_columnStyleHelper.unboundColumns);
  FreeAndNil(_columnStyleHelper.unboundColumnsData);
  FreeAndNil(_columnStyleHelper.lastVScrollPos);
  FreeAndNil(_columnStyleHelper.processedRows);
  FreeAndNil(_columnStyleHelper.isEmptyUnbound);
  FreeAndNil(_columnStyleHelper);
end;

end.
