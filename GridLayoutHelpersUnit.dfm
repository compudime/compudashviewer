object GridLayoutHelpers: TGridLayoutHelpers
  OldCreateOrder = False
  Height = 150
  Width = 126
  object AppXMLFileStorage: TJvAppXMLFileStorage
    StorageOptions.BooleanStringTrueValues = 'TRUE, YES, Y'
    StorageOptions.BooleanStringFalseValues = 'FALSE, NO, N'
    StorageOptions.DefaultIfReadConvertError = True
    StorageOptions.InvalidCharReplacement = '_'
    FlushOnDestroy = False
    RootNodeName = 'Data'
    SubStorages = <>
    Left = 48
    Top = 16
  end
  object XMLLoader: TJvSimpleXML
    IndentString = '  '
    Options = [sxoAutoIndent, sxoAutoEncodeValue, sxoAutoEncodeEntity, sxoTrimPrecedingTextWhitespace, sxoTrimFollowingTextWhitespace]
    Left = 48
    Top = 72
  end
end
