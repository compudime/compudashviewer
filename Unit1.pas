unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Phys.MongoDBDef, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MongoDB, System.Rtti, System.JSON.Types,
  System.JSON.Readers, System.JSON.BSON, System.JSON.Builders, FireDAC.Phys.MongoDBWrapper, FireDAC.VCLUI.Wait,
  FireDAC.Phys.MongoDBDataSet, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet, Vcl.StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, dxBarBuiltInMenu,
  cxGridCustomPopupMenu, cxGridPopupMenu, dxSkinsForm, Vcl.Menus, cxButtons, Vcl.Grids, Vcl.DBGrids, dxDateRanges,
  dxLayoutContainer, dxLayoutControlAdapters, dxLayoutLookAndFeels, dxLayoutControl, cxGridBandedTableView,
  cxGridDBBandedTableView, cxPropertiesStore, caAfterShow, TMS.MQTT.Global, TMS.MQTT.Client,
  System.JSON, System.Generics.Collections, Vcl.ComCtrls, ufrmFilterSettings, System.Notification,
  dxScrollbarAnnotations, cxPC, REST.Response.Adapter, System.ImageList, Vcl.ImgList, cxImageList, dxBar,
  System.DateUtils, Vcl.ExtCtrls, FireDAC.Stan.StorageJSON, DM.ConnectionManager, VisualOptionList, dxCore,
  uNotifyCenter, CDV.Consts, frGrid, frReportGrid, CDV.GridFrameArray, dxUIAClasses, dxSkinWXI;

const
  WM_RESTORCOLUMNCAPTIONS = WM_USER + 1;

type
  TdxLayoutGroupAccess = class(TdxLayoutGroup);

  TcxUserGridColumnHeaderPainter = class(TcxGridColumnHeaderPainter)
  protected
    procedure DrawContent; override;
  end;

  TfrmViewer = class(TForm, IFriendlyFilterSetter)
    FDPhysMongoDriverLink1: TFDPhysMongoDriverLink;
    FDConnection: TFDConnection;
    dsMongoQuery: TDataSource;
    TableView1: TcxGridDBBandedTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    FDMongoQuery1: TFDMongoQuery;
    cxGridPopupMenu1: TcxGridPopupMenu;
    dxSkinController1: TdxSkinController;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutMainTableItem: TdxLayoutItem;
    dsTis01Modules: TDataSource;
    memTis01Modules: TFDMemTable;
    dxLayoutMainSplitterItem: TdxLayoutSplitterItem;
    memTis01ModulesName: TStringField;
    memTis01ModulesValue: TStringField;
    memTis01Modules_id: TStringField;
    dxLayoutBottomGroup: TdxLayoutGroup;
    dsTisWin3Modules: TDataSource;
    memTisWin3Modules: TFDMemTable;
    memTisWin3ModulesName: TStringField;
    memTisWin3ModulesValue: TStringField;
    memTisWin3Modules_id: TStringField;
    dsSyncClients: TDataSource;
    memSyncClients: TFDMemTable;
    memSyncClientsName: TStringField;
    memSyncClientsValue: TStringField;
    memSyncClients_id: TStringField;
    cxPropertiesStore1: TcxPropertiesStore;
    dsRepVers: TDataSource;
    memRepVers: TFDMemTable;
    StringField1: TStringField;
    StringField3: TStringField;
    StatusBar1: TStatusBar;
    MQTTClient: TTMSMQTTClient;
    NotificationCenter1: TNotificationCenter;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarRefreshData: TdxBarButton;
    cxImageList1: TcxImageList;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    dxBarAction: TdxBarButton;
    dxBarDeleteMasterRow: TdxBarButton;
    dxBarAddColumn: TdxBarButton;
    dxBarMQTTRefresh: TdxBarButton;
    dxBarConditionalFormatting: TdxBarButton;
    dxBarExportToExcel: TdxBarButton;
    dxBarFilterSettings: TdxBarButton;
    cxPageControl1: TcxPageControl;
    cxTabViewMain: TcxTabSheet;
    mtblReports: TFDMemTable;
    mtblReportsReport: TStringField;
    mtblReportsDate: TDateTimeField;
    dsReports: TDataSource;
    memRepVersMaxReportDate: TDateTimeField;
    memRepVersHowOld: TIntegerField;
    memRepVersVersion: TDateTimeField;
    FDConnection1: TFDConnection;
    dxSaveCurrentView: TdxBarButton;
    TimerViewNotifications: TTimer;
    pnlNotificationText: TPanel;
    dxViewList: TdxBarCombo;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    dxDeleteView: TdxBarButton;
    dxBarConnections: TdxBarButton;
    dxBarLayoutSetup: TdxBarButton;
    dxBarViews: TdxBarSubItem;
    dxBarSeparator1: TdxBarSeparator;
    dxBarButtonGrid1: TdxBarButton;
    dxBarButtonGrid2: TdxBarButton;
    dxBarButtonGrid3: TdxBarButton;
    dxBarButtonGrid4: TdxBarButton;
    pmGridSelection: TdxBarPopupMenu;
    SelectAll1: TdxBarButton;
    UnselectAll1: TdxBarButton;
    ReversSelection1: TdxBarButton;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarMQTTRefreshCurrent: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dsMongoQueryDataChange(Sender: TObject; Field: TField);
    procedure cbFindBhaviorClick(Sender: TObject);
    procedure dxBarRefreshDataClick(Sender: TObject);
    procedure dxBarActionClick(Sender: TObject);
    procedure dxBarDeleteMasterRowClick(Sender: TObject);
    procedure dxBarAddColumnClick(Sender: TObject);
    procedure dxBarMQTTRefreshClick(Sender: TObject);
    procedure dxBarConditionalFormattingClick(Sender: TObject);
    procedure dxBarExportToExcelClick(Sender: TObject);
    procedure dxBarFilterSettingsClick(Sender: TObject);
    procedure dxSaveCurrentViewClick(Sender: TObject);
    procedure dxRestoreViewClick(Sender: TObject);
    procedure TimerViewNotificationsTimer(Sender: TObject);
    procedure dxDeleteViewClick(Sender: TObject);
    procedure TableView1GetStoredProperties(Sender: TcxCustomGridView; AProperties: TStrings);
    procedure TableView1GetStoredPropertyValue(Sender: TcxCustomGridView; const AName: string; var AValue: Variant);
    procedure TableView1SetStoredPropertyValue(Sender: TcxCustomGridView; const AName: string; const AValue: Variant);
    procedure dxLayoutMainSplitterItemCanResize(Sender: TObject; AItem: TdxCustomLayoutItem; var ANewSize: Integer;
      var AAccept: Boolean);
    procedure dxBarConnectionsClick(Sender: TObject);
    procedure dxBarLayoutSetupClick(Sender: TObject);
    procedure dxViewListCurChange(Sender: TObject);
    procedure MoveBottomGridRight(const AData: TValue);
    procedure MoveBottomGridLeft(const AData: TValue);
    procedure SelectAll1Click(Sender: TObject);
    procedure UnselectAll1Click(Sender: TObject);
    procedure ReversSelection1Click(Sender: TObject);
    procedure TableView1SelectionChanged(Sender: TcxCustomGridTableView);
    procedure dxLayoutControl1GetItemStoredProperties(Sender: TdxCustomLayoutControl; AItem: TdxCustomLayoutItem;
      AProperties: TStrings);
    procedure dxLayoutControl1GetItemStoredPropertyValue(Sender: TdxCustomLayoutControl; AItem: TdxCustomLayoutItem;
      const AName: string; var AValue: Variant);
    procedure dxLayoutControl1SetItemStoredPropertyValue(Sender: TdxCustomLayoutControl; AItem: TdxCustomLayoutItem;
      const AName: string; const AValue: Variant);
    procedure TableView1CustomDrawColumnHeader(Sender: TcxGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean);
    procedure dxBarButton1Click(Sender: TObject);
    procedure dxBarMQTTRefreshCurrentClick(Sender: TObject);
    procedure TableView1Customization(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    DB_NAME: string;
    FUserName: String;
    FPassword: String;
    FStatusLoaded: Boolean;
    FEnv: TMongoEnv;
    FMongoConn: TMongoConnection;
    FBottomFrames: TGridFrameArray;
    FVisualOptionList: TVisualOptionList;

    procedure RefreshData;
    procedure MQTTRefreshCurrent;
    procedure DeleteMasterRow;
    procedure AddColumn;
    procedure ExportToExcel;
    procedure DoRestoreView;
    procedure DoDeleteCurrentView;
    procedure DoSaveCurrentView;
    function StringsIndexOf(ASource: TStrings; const AViewName: string): Integer;
    procedure DoSelectView(const AViewName: string);
    procedure RestoreFromStream(const AStream: TStream; aStoredGrids: String);
    procedure ShowNotification(const AText: String);
    procedure RestoreColumnsCaption(var AMessage: TMessage); message WM_RESTORCOLUMNCAPTIONS;
    procedure DeleteExpressionColumns;
    procedure DoOpenConnectionManager;
    procedure CreateDefaultGridFrames;
    function GetAllSubDocuments(AQuery: TFDMongoQuery): TArray<String>;
    procedure LoadNewFields(IDField: TField);
    function CreateGridPanel(const AName: String; AID: String; AData: TJSONObject): TGridFrame;
    procedure ClearGridPanels;
    procedure DoNotification(const AName, aTitle, aBody: string);
    procedure OpenData;
    procedure OpenDataForClientsAndReports(AMongoQuery: TFDMongoQuery);
    procedure FillReports(AJSON: TJSONObject; ADataSetReports, ADataSetClients, ADataSetClientReports1,
      ADataSetClientReports2: TDataSet; const AID: String; ADict: TDictionary<String, TDateTime>); overload;
    procedure FillReports(AJSON: TJSONObject; ADataSetReports: TDataSet;
      ADict: TDictionary<String, TDateTime>); overload;
    procedure FillReportsDate(ADataSet: TDataSet; ADict: TDictionary<String, TDateTime>; const AFieldName: String);
    procedure ConnectToMongo(AConnectionData: TConnectionData);
    procedure ApplyNewVisibility(const AData: TValue);
    procedure CreateAndConfigureVisualOptionList;
    procedure ToggleSelectionAll(Sender: TObject);
    procedure ScanForNewModules;
    procedure OnNotifyScanNewModules(const AData: TValue);
    procedure CheckGroupButtons(AdxGroup: TdxLayoutGroup);
    procedure dxBarButton2Click(Sender: TObject);
  public
    procedure MQTTRefresh;
    procedure SetFriendlyFilter(const AText: String);
    procedure MQTTPublishToClient(EName, SName: string; const AMess: string);
  end;

var
  frmViewer: TfrmViewer;

implementation

uses
  // GridLayoutHelpersUnit,
{$IFDEF DEBUG}
  System.Diagnostics,
{$ENDIF}
  Utils, Unit2, cxGridExportLink, uCdMongo, uFilter,
  System.IniFiles, View.ConnectionManager, frmLayoutSettings, dmLayouts,
  Unit3, ufrmVisibilityModules, AMR.AutoObject, System.UITypes;

{$R *.dfm}

const
  ADelimiter = ',';
  Collection_Name = 'CompuDashMain';
  MIN_HEIGHT_SPLIT_ITEM = 230;
  OPTION_SELECT_ALL = 'Select All';
  OPTION_UNSELECT_ALL = 'Unselect All';
  OPTION_REVERS_SELECTION = 'Reverse Selection';

procedure TfrmViewer.RestoreColumnsCaption(var AMessage: TMessage);
var
  ASTringList: TStringList;
  ACaption: String;
  i: Integer;
  Lcolumn: TcxGridColumn;
begin
  ASTringList := TStringList(AMessage.lParam);

  for i := 0 to ASTringList.Count - 1 do
    with TcxCustomGridTableView(AMessage.wParam) do
    begin
      Lcolumn := TcxGridColumn(FindItemByName(ASTringList.Names[i]));

      if assigned(Lcolumn) then
      begin
        ACaption := ASTringList.Values[Lcolumn.Name];
        if ACaption <> '' then
          Lcolumn.Caption := ACaption;
        if not Lcolumn.DataBinding.Expression.IsEmpty then
          Lcolumn.DataBinding.ValueTypeClass := TcxStringValueType;
      end;
    end;
end;

procedure TfrmViewer.RestoreFromStream(const AStream: TStream; aStoredGrids: String);
var
  Arr: TArray<String>;
  Str: String;
begin
  // Exit;

  Arr := aStoredGrids.Split([',']);
  if Length(Arr) > 0 then
    FBottomFrames.ClearArray;

  for Str in Arr do
    if not FBottomFrames.ContainsFrame(Str) then
      FBottomFrames.Add(CreateGridPanel(Str, '', nil));

  // Main table
  TableView1.RestoreFromStream(AStream, True, False, [gsoUseFilter, gsoUseSummary]);
  // Bottom Tables
  for Str in Arr do
  begin
    var
    item := FBottomFrames.GetItemByFrameName(Str);
    item.GetTableVew.RestoreFromStream(AStream, True, False, [gsoUseFilter, gsoUseSummary]);
  end;
end;

procedure TfrmViewer.ReversSelection1Click(Sender: TObject);
var
  i: Integer;
begin
  with TableView1 do
    if ColumnCount > 0 then
    begin
      DataController.BeginUpdate;
      try
        for i := 0 to DataController.RecordCount - 1 do
          DataController.ChangeRowSelection(i, not DataController.IsRowSelected(i));

        if Controller.SelectedRecordCount = 0 then
          FVisualOptionList.State := cbUnchecked
        else
          FVisualOptionList.State := cbGrayed;
      finally
        DataController.EndUpdate;
      end;
    end;
end;

function GetColumnNames(Str: WideString; Delimiter: string): TStringList;
var
  tmpStrList: TStringList;
  tmpString, tmpVal: WideString;
  DelimPos: LongInt;
begin
  tmpStrList := TStringList.Create;
  tmpString := Str;
  DelimPos := 1;
  while DelimPos > 0 do
  begin
    DelimPos := LastDelimiter(Delimiter, tmpString);
    tmpVal := Copy(tmpString, DelimPos + 1, Length(tmpString));
    if tmpVal <> '' then
      tmpStrList.Insert(0, (tmpVal));
    Delete(tmpString, DelimPos, Length(tmpString));
  end;
  GetColumnNames := tmpStrList;
end;

procedure TfrmViewer.FormCreate(Sender: TObject);
begin
  cxPageControl1.HideTabs := True;
  cxPageControl1.ActivePageIndex := 0;

  mtblReports.CreateDataSet;

  FBottomFrames := [];

  TNotifyCenter.Instance.Subscribe(NOTIFY_MOVEGRIDLEFT, MoveBottomGridLeft);
  TNotifyCenter.Instance.Subscribe(NOTIFY_MOVEGRIDRIGHT, MoveBottomGridRight);
  TNotifyCenter.Instance.Subscribe(NOTIFY_APPLY_NEW_VISIBILITY, ApplyNewVisibility);
  TNotifyCenter.Instance.Subscribe(NOTIFY_SCAN, OnNotifyScanNewModules);

  dxBarLayoutSetup.Visible := VisibleTodxBarVisible(IsAdministrator);
end;

procedure TfrmViewer.AddColumn;
var
  LColumnName: String;
begin
  if InputQuery('Enter Column Name', 'Column Name:', LColumnName) then
    if LColumnName.IsEmpty then
      ShowMessage('Column name is empty')
    else
    begin
      with TableView1.CreateColumn do
      begin
        Caption := LColumnName;
        Name := TableView1.Name + 'column' + TableView1.ColumnCount.ToString;
        Options.ExpressionEditing := True;
        Options.ShowCaption := True;
        DataBinding.ValueTypeClass := TcxStringValueType;
        Position.BandIndex := 1;
        ShowExpressionEditor;
      end;
    end;
end;

procedure TfrmViewer.ApplyNewVisibility(const AData: TValue);
begin
  var
  Dict := frmVisibilityModules.GetShowingSettingsForWork;
  var
  AutoDict := TAutoFree.Create(Dict);
  for var item in FBottomFrames do
    if Dict.ContainsKey(item.Name) then
    begin
      if (Dict[item.Name] = SHOW_TYPE_NO) and item.IsVisible then
        item.SetVisibleContent(False);

      if (Dict[item.Name] = SHOW_TYPE_ALWAYS) and not item.IsVisible then
        item.SetVisibleContent(True);
    end;

  dsMongoQueryDataChange(FDMongoQuery1, nil);
end;

procedure TfrmViewer.cbFindBhaviorClick(Sender: TObject);
begin
  // if cbFindBhavior.Checked then
  TableView1.FindPanel.Behavior := fcbSearch;
  // else
  TableView1.FindPanel.Behavior := fcbFilter;
end;

procedure TfrmViewer.CheckGroupButtons(AdxGroup: TdxLayoutGroup);
begin
  with AdxGroup do
  begin
    if ButtonOptions.Buttons.Count > 0 then
      ButtonOptions.Buttons[0].Enabled := VisibleIndex > 1;
    if ButtonOptions.Buttons.Count > 1 then
      ButtonOptions.Buttons[1].Enabled := assigned(Parent) and (VisibleIndex <= Parent.VisibleCount - 3);
  end;
end;

procedure TfrmViewer.ClearGridPanels;
begin
  var
  Dict := frmVisibilityModules.GetShowingSettingsForWork;
  var
  AutoDict := TAutoFree.Create(Dict);
  for var item in FBottomFrames do
    if Dict.ContainsKey(item.Name) and (Dict[item.Name] <> SHOW_TYPE_ALWAYS) and item.IsVisible then
      item.SetVisibleContent(False);
end;

procedure TfrmViewer.ConnectToMongo(AConnectionData: TConnectionData);
var
  FConnStr: String;
begin
  if assigned(AConnectionData) then
  begin
    DB_NAME := AConnectionData.database;
    FUserName := AConnectionData.UserName;
    FPassword := AConnectionData.Password;

    if assigned(FDConnection) then
      FreeAndNil(FDConnection);

    if assigned(FDMongoQuery1) then
      FreeAndNil(FDMongoQuery1);

    FDConnection := TFDConnection.Create(nil);

    FDConnection.Params.Clear;
    FDConnection.Params.Add('DriverID=Mongo');

    if AConnectionData.ConnectionType.Equals(DB_ATLAS_TYPE) then
    begin
      { FDConnection.ConnectionString := 'DriverID=Mongo;Server=' + AConnectionData.database + '-shard-00-00.' +
        AConnectionData.Server + '.mongodb.net;User_Name=yona123;Password=yona123;UseSSL=True';
        FDConnection.Connected := True; }

      FConnStr := Format('mongodb+srv://%s:%s@%s.%s.mongodb.net/%s?retryWrites=true&UseSSL=True&w=majority',
        [AConnectionData.UserName, AConnectionData.Password, AConnectionData.database, AConnectionData.Server,
        AConnectionData.database]);

      FDConnection.Params.Add('Server=' + AConnectionData.database + '-shard-00-00.' + AConnectionData.Server +
        '.mongodb.net');
      // FDConnection.Params.Add('MongoAdvanced=ssl=true&authSource=admin&retryWrites=true&w=majority');
      FDConnection.Params.Add('UseSSL=True');

      FMongoConn := TMongoConnection(FDConnection.CliObj);
      FEnv := FMongoConn.Env;

      FMongoConn.Open(FConnStr);
    end
    else if AConnectionData.ConnectionType.Equals(DB_SERVER_TYPE) then
    begin
      FDConnection.Params.Add('Server=' + AConnectionData.Server);
      FDConnection.Params.database := AConnectionData.database;
      // FDConnection.Params.Add('MongoAdvanced=authenticationDatabase=' + AConnectionData.database);
      FDConnection.Params.Add('Port=' + AConnectionData.port.ToString);
      FDConnection.Params.Add('UseSSL=True');

      if not AConnectionData.UserName.IsEmpty then
        FDConnection.Params.UserName := AConnectionData.UserName;
      if not AConnectionData.Password.IsEmpty then
        FDConnection.Params.Password := AConnectionData.Password;

      FDConnection.Connected := True;
      FMongoConn := TMongoConnection(FDConnection.CliObj);
      FEnv := FMongoConn.Env;
    end;

    FDMongoQuery1 := TFDMongoQuery.Create(self);
    FDMongoQuery1.FormatOptions.StrsTrim2Len := True;
    FDMongoQuery1.Connection := FDConnection;
    dsMongoQuery.DataSet := FDMongoQuery1;

    RefreshData;
  end;
end;

procedure TfrmViewer.CreateAndConfigureVisualOptionList;
begin
  FVisualOptionList := TVisualOptionList.Create(self);
  with FVisualOptionList do
    try
      Parent := cxGrid1;
      Left := 5;
      OnClick := ToggleSelectionAll;
      PopupMenu := pmGridSelection;
    except
    end;
end;

procedure TfrmViewer.CreateDefaultGridFrames;
begin
  for var Str in DEFAULT_GRIDS do
  begin
    if FBottomFrames.ContainsFrame(Str) then
      Continue;

    var
    GridFrame := CreateGridPanel(Str, '', nil);
    FBottomFrames.Add(GridFrame);
  end;
end;

function TfrmViewer.CreateGridPanel(const AName: String; AID: String; AData: TJSONObject): TGridFrame;
begin
  if AName <> 'Report_Versions' then
    Result := TGridFrame.Create(Application, AName.DeQuotedString('"'), AID, AData, dxLayoutBottomGroup)
  else
  begin
    Result := TReportGridFrame.Create(Application, AName.DeQuotedString('"'), AID, AData, dxLayoutBottomGroup);
    (Result as TReportGridFrame).ReportsDataSet := mtblReports;
  end;
end;

procedure TfrmViewer.DeleteExpressionColumns;
var
  i: Integer;
begin
  for i := TableView1.ColumnCount - 1 downto 0 do
    if not TableView1.Columns[i].DataBinding.Expression.IsEmpty then
      TableView1.Columns[i].Free;
end;

procedure TfrmViewer.DeleteMasterRow;
var
  EName, SName, Text: String;
begin
  if not FDMongoQuery1.IsEmpty then
  begin
    cdMongo.DoConnect(DB_NAME, Collection_Name);

    try
      EName := FDMongoQuery1.FieldByName('enterprise_name').AsString;
      SName := FDMongoQuery1.FieldByName('store_name').AsString;

      Text := 'Are you sure want to delete record ' + EName + ' ' + SName + '?';

      if MessageDlg(Text, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        if not EName.IsEmpty then
        begin
          if cdMongo.DeleteRecord(Collection_Name, EName, SName) then
            DoNotification('RowDeleted', 'Delete Master Row', EName + ' ' + SName + ' Succesfully Deleted')
          else
            DoNotification('RowDeleted', 'Delete Master Row', EName + ' ' + SName + ' Not Deleted');

          OpenData;
        end
        else
          ShowMessage('Enterprise_name is empty');
      end;
    finally
      cdMongo.DoClose;
    end;
  end;
end;

procedure TfrmViewer.DoDeleteCurrentView;
var
  LCurrentViewIndex: Integer;
  _stream: TStream;
begin
  LCurrentViewIndex := dxViewList.ItemIndex;
  if LCurrentViewIndex <> -1 then
    with LayoutsData do
    begin
      LayoutsData.dsViews.DataSet.Filtered := True; // To filter layouts for current computer or user

      if LayoutsData.dsViews.DataSet.Locate('view_name', dxViewList.Items[LCurrentViewIndex], []) then
      begin
        LayoutsData.dsViews.DataSet.Delete;

        dxViewList.Items.Delete(LCurrentViewIndex);
        if (dxViewList.Items.Count > 1) and (LCurrentViewIndex = 0) then
          dxViewList.ItemIndex := LCurrentViewIndex
        else if (dxViewList.Items.Count > 1) and (LCurrentViewIndex <= dxViewList.Items.Count - 1) then
          dxViewList.ItemIndex := LCurrentViewIndex - 1;

        LCurrentViewIndex := dxViewList.ItemIndex;

        if (dxViewList.ItemIndex <> -1) and (not LayoutsData.dsViews.DataSet.IsEmpty) then
        begin
          LayoutsData.dsViews.DataSet.First;
          while not LayoutsData.dsViews.DataSet.Eof do
          begin
            LayoutsData.dsViews.DataSet.Edit;
            LayoutsData.dsViews.DataSet['view_is_default'] := LayoutsData.dsViews.DataSet['view_name']
              = dxViewList.Items[LCurrentViewIndex];

            if LayoutsData.dsViews.DataSet['view_is_default'] then
              try
                _stream := LayoutsData.dsViews.DataSet.CreateBlobStream
                  (LayoutsData.dsViews.DataSet.FieldByName('view_body'), bmRead);
                var
                StoredGrids := LayoutsData.dsViews.DataSet.FieldByName('view_storedgrids').AsString;
                RestoreFromStream(_stream, StoredGrids);
              finally
                _stream.Free
              end;

            LayoutsData.dsViews.DataSet.Post;
            LayoutsData.dsViews.DataSet.Next;
          end;
        end;

        LayoutsData.dsViews.DataSet.Filtered := False; // Remove filtering for current computer or user

        DoSaveMemViewsToFile;

        ShowNotification('Current view restored!');
      end;
    end;
end;

procedure TfrmViewer.DoNotification(const AName, aTitle, aBody: string);
var
  appNotification: TNotification;
begin
  appNotification := NotificationCenter1.CreateNotification;
  try
    appNotification.Name := AName;
    appNotification.Title := aTitle;
    appNotification.AlertBody := aBody;
    NotificationCenter1.PresentNotification(appNotification);
  finally
    appNotification.Free;
  end;
end;

procedure TfrmViewer.DoOpenConnectionManager;
begin
  TFormConnectionManager.GetInstance.InitAndShow(ConnectToMongo);
end;

procedure TfrmViewer.DoRestoreView;
var
  _stream: TStream;
  _layout: String;
begin
  dxViewList.Items.Clear;

  _layout := LayoutsData.GetUserLayout;
  if _layout.IsEmpty then
    _layout := LayoutsData.GetComputerLayout;

  if not LayoutsData.dsViews.DataSet.IsEmpty then
  begin
    LayoutsData.dsViews.DataSet.Filtered := True;
    LayoutsData.dsViews.DataSet.First;
    while not LayoutsData.dsViews.DataSet.Eof do
    begin
      if LayoutsData.dsViews.DataSet.FieldByName('view_is_default').AsBoolean or
        (not _layout.IsEmpty and SameText(LayoutsData.dsViews.DataSet['view_name'], _layout)) then
      begin
        dxViewList.ItemIndex := dxViewList.Items.Add(LayoutsData.dsViews.DataSet.FieldByName('view_name').AsString);
        try
          var
          ShowingSettings := LayoutsData.dsViews.DataSet.FieldByName('view_showing').AsString;
          frmVisibilityModules.SetShowingSettings(ShowingSettings);

          _stream := LayoutsData.dsViews.DataSet.CreateBlobStream
            (LayoutsData.dsViews.DataSet.FieldByName('view_body'), bmRead);
          var
          StoredGrids := LayoutsData.dsViews.DataSet.FieldByName('view_storedgrids').AsString;
          RestoreFromStream(_stream, StoredGrids);
        finally
          _stream.Free;
        end;
      end
      else
        dxViewList.Items.Add(LayoutsData.dsViews.DataSet.FieldByName('view_name').AsString);

      LayoutsData.dsViews.DataSet.Next;
    end;

    LayoutsData.dsViews.DataSet.Filtered := False;
  end
  else
    CreateDefaultGridFrames;

  TNotifyCenter.Instance.SendMessage(NOTIFY_APPLY_NEW_VISIBILITY, nil);
end;

procedure TfrmViewer.DoSaveCurrentView;
var
  LInputViewName: String;
  LDefViewName: String;
  LViewFileName: String;
  LIndexCurrentView: Integer;
  _stream: TStream;
  _names: array of String;
begin
  if dxViewList.ItemIndex > -1 then
    LDefViewName := dxViewList.Items[dxViewList.ItemIndex]
  else
    LDefViewName := 'New Layout ' + (dxViewList.Items.Count + 1).ToString;

  LInputViewName := LDefViewName;
  SetLength(_names, 1);
  _names[0] := LInputViewName;

  with LayoutsData do
  begin
    if not LayoutsData.dsViews.DataSet.Active then
      LayoutsData.dsViews.DataSet.Active := True;

    LayoutsData.dsViews.DataSet.First;

    if InputQuery('Enter Layout Name', ['Layout Name:'], _names,
      function(const ANames: array of string): Boolean
      begin
        Result := not Trim(ANames[0]).IsEmpty;
        if Result then
        begin
          if LayoutsData.dsViews.DataSet.Locate('view_name', Trim(ANames[0]), []) then
            Result := Application.MessageBox(PChar(Format('A layout with name "%s" exists! Do you want to overwrite?',
              [ANames[0]])), 'Warning', MB_ICONWARNING + MB_YESNO) = IDYES
        end
        else
          ShowMessage('Layout name can''t be empty!');
      end) then
    begin
      LInputViewName := _names[0];

      if LayoutsData.dsViews.DataSet.Locate('view_name', LInputViewName, []) then
      begin
        LayoutsData.dsViews.DataSet.Edit;
        if not UserTags.IsEmpty then
          LayoutsData.dsViews.DataSet['view_tags'] := LayoutsData.dsViews.DataSet['view_tags'] + #32 + UserTags
        else if not ComputerTags.IsEmpty then
          LayoutsData.dsViews.DataSet['view_tags'] := LayoutsData.dsViews.DataSet['view_tags'] + #32 + ComputerTags
      end
      else
      begin
        LayoutsData.dsViews.DataSet.Append;
        if not UserTags.IsEmpty then
          LayoutsData.dsViews.DataSet['view_tags'] := UserTags
        else if not ComputerTags.IsEmpty then
          LayoutsData.dsViews.DataSet['view_tags'] := ComputerTags
      end;

      LayoutsData.dsViews.DataSet['view_name'] := LInputViewName;

      _stream := LayoutsData.dsViews.DataSet.CreateBlobStream
        (LayoutsData.dsViews.DataSet.FieldByName('view_body'), bmWrite);
      try
        // Root layout
        // dxLayoutControl1Group_Root.Container.StoreToStream(_stream);

        // Table views

        // main table
        TableView1.StoreToStream(_stream, [gsoUseFilter, gsoUseSummary]);

        // bottomtables
        var
        StoredGrids := '';
        // FBottomFrames.ReorderByVisibility;
        for var frame in FBottomFrames do
        begin
          frame.GetTableVew.StoreToStream(_stream, [gsoUseFilter, gsoUseSummary]);
          StoredGrids := StoredGrids + frame.Name + ',';
        end;
        StoredGrids := StoredGrids.Remove(StoredGrids.Length - 1);
        LayoutsData.dsViews.DataSet['view_storedgrids'] := StoredGrids;
      finally
        _stream.Free;
      end;

      LayoutsData.dsViews.DataSet['view_showing'] := frmVisibilityModules.GetShowingSettings;

      LayoutsData.dsViews.DataSet.Post;

      LayoutsData.dsViews.DataSet.First;
      while not LayoutsData.dsViews.DataSet.Eof do
      begin
        LayoutsData.dsViews.DataSet.Edit;
        LayoutsData.dsViews.DataSet['view_is_default'] := LayoutsData.dsViews.DataSet.FieldByName('view_name')
          .AsString = LInputViewName;
        LayoutsData.dsViews.DataSet.Post;
        LayoutsData.dsViews.DataSet.Next;
      end;

      if memComps.IsEmpty then
      begin
        if not memComps.Active then
          memComps.Open;

        memComps.Append;

        memCompscomp_name.AsString := ComputerName;
        memCompscomp_tags.AsString := LayoutsData.dsViews.DataSet.FieldByName('view_tags').AsString;
        memCompscomp_layout.AsString := LayoutsData.dsViews.DataSet.FieldByName('view_name').AsString;

        memComps.Post;
        DoSaveMemCompsToFile;
      end;

      if memUsers.IsEmpty then
      begin
        if not memUsers.Active then
          memUsers.Open;

        memUsers.Append;

        memUsersuser_name.AsString := UserName;
        memUsersuser_tags.AsString := LayoutsData.dsViews.DataSet.FieldByName('view_tags').AsString;
        memUsersuser_layout.AsString := LayoutsData.dsViews.DataSet.FieldByName('view_name').AsString;

        memUsers.Post;
        DoSaveMemUsersToFile;
      end;

      DoSaveMemViewsToFile;

      LIndexCurrentView := StringsIndexOf(dxViewList.Items, LInputViewName);
      if LIndexCurrentView <> -1 then
        dxViewList.ItemIndex := LIndexCurrentView
      else
        dxViewList.ItemIndex := dxViewList.Items.Add(LInputViewName);

      ShowNotification('Current layout saved!');
    end;
  end;
end;

procedure TfrmViewer.DoSelectView(const AViewName: string);
var
  _stream: TStream;
begin
  if not AViewName.IsEmpty then
    with LayoutsData do
      if LayoutsData.dsViews.DataSet.Locate('view_name', AViewName, []) then
      begin
        DeleteExpressionColumns;

        dxViewList.ItemIndex := StringsIndexOf(dxViewList.Items, AViewName);
        try
          _stream := LayoutsData.dsViews.DataSet.CreateBlobStream
            (LayoutsData.dsViews.DataSet.FieldByName('view_body'), bmRead);
          var
          StoredGrids := LayoutsData.dsViews.DataSet.FieldByName('view_storedgrids').AsString;
          RestoreFromStream(_stream, StoredGrids);
        finally
          _stream.Free;
          dsMongoQueryDataChange(dsMongoQuery, nil); // To update visibility of dynamic bottom grid panels
        end;

        LayoutsData.dsViews.DataSet.First;
        while not LayoutsData.dsViews.DataSet.Eof do
        begin
          LayoutsData.dsViews.DataSet.Edit;
          LayoutsData.dsViews.DataSet['view_is_default'] := LayoutsData.dsViews.DataSet['view_name'] = AViewName;
          LayoutsData.dsViews.DataSet.Post;
          LayoutsData.dsViews.DataSet.Next;
        end;

        DoSaveMemViewsToFile;
        ShowNotification('Current view changed!');
      end;
end;

procedure TfrmViewer.RefreshData;
var
  LDataChangeEvent: TDataChangeEvent;
begin
  LDataChangeEvent := dsMongoQuery.OnDataChange;
  try
    dsMongoQuery.OnDataChange := nil;
    OpenData;

    // Load layouts, computers and users data
    with LayoutsData do
    begin
      DoLoadMemCompsFromFile;
      DoLoadMemUsersFromFile;
      DoLoadMemViewsFromFile; // Must be the last one!
    end;

    // DoRestoreView;

    if TableView1.ColumnCount = 0 then
    begin
      TableView1.DataController.CreateAllItems;
      TableView1.ApplyBestFit;
      DoRestoreView;
    end;
    FStatusLoaded := True;
  finally
    dsMongoQuery.OnDataChange := LDataChangeEvent;
  end;

  if frmVisibilityModules.IsAutoScanAtStart then
    ScanForNewModules;
end;

procedure TfrmViewer.dsMongoQueryDataChange(Sender: TObject; Field: TField);
var
  IDField: TField;
begin
  ClearGridPanels;

  IDField := FDMongoQuery1.FindField('_id');
  if assigned(IDField) then
    LoadNewFields(IDField);
end;

procedure TfrmViewer.dxBarActionClick(Sender: TObject);
begin
  frmActions.ShowModal;
end;

procedure TfrmViewer.dxBarAddColumnClick(Sender: TObject);
begin
  AddColumn;
end;

procedure TfrmViewer.dxBarButton1Click(Sender: TObject);
begin
  frmEditColumnValue.ShowModal;
end;

procedure TfrmViewer.dxBarButton2Click(Sender: TObject);
begin
  // var Arr := GetAllSubDocuments(FDMongoQuery1);
  // var VisibleArr := FBottomFrames.GetVisibleFrameNameArray;
  frmVisibilityModules.ShowModulesList([]);
end;

procedure TfrmViewer.dxBarConditionalFormattingClick(Sender: TObject);
begin
  TableView1.ConditionalFormatting.ShowRulesManagerDialog;
end;

procedure TfrmViewer.dxBarConnectionsClick(Sender: TObject);
var
  _names: array of String;
begin
  SetLength(_names, 1);
  if InputQuery('Please enter password', ['Password:'], _names,
    function(const ANames: array of string): Boolean
    begin
      Result := ANames[0].Equals('9999');
      if not Result then
        ShowMessage('Password is wrong!');
    end) then
    DoOpenConnectionManager;
end;

procedure TfrmViewer.dxBarDeleteMasterRowClick(Sender: TObject);
begin
  DeleteMasterRow;
end;

procedure TfrmViewer.dxBarExportToExcelClick(Sender: TObject);
begin
  ExportToExcel;
end;

procedure TfrmViewer.dxBarFilterSettingsClick(Sender: TObject);
begin
  frmFilterSettings.ShowForm(FDMongoQuery1, self);
end;

procedure TfrmViewer.dxBarLayoutSetupClick(Sender: TObject);
var
  _names: array of String;
begin
  SetLength(_names, 1);
  if IsAdministrator then
  begin
    if InputQuery('Please enter password', ['Password:'], _names,
      function(const ANames: array of string): Boolean
      begin
        Result := ANames[0].Equals('9999');
        if not Result then
          ShowMessage('Password is wrong!');
      end) then
      with TSetupForm.Create(Application) do
        try
          ShowModal
        finally
          Free
        end
  end
  else
    ShowMessage('You must have Admin privilege!')
end;

procedure TfrmViewer.dxBarMQTTRefreshClick(Sender: TObject);
begin
  MQTTRefresh;
end;

procedure TfrmViewer.dxBarRefreshDataClick(Sender: TObject);
begin
  if FDConnection.Connected then
    RefreshData
  else
    DoOpenConnectionManager;
end;

procedure TfrmViewer.dxDeleteViewClick(Sender: TObject);
begin
  DoDeleteCurrentView;
end;

procedure TfrmViewer.dxLayoutControl1GetItemStoredProperties(Sender: TdxCustomLayoutControl; AItem: TdxCustomLayoutItem;
AProperties: TStrings);
var
  _itemName: String;
begin
  _itemName := AItem.Name;
  if ((AItem is TdxLayoutGroup) or (AItem is TdxLayoutSplitterItem)) and not _itemName.StartsWith('dxLayoutFrame') then
    AProperties.Add('Visible');
end;

procedure TfrmViewer.dxLayoutControl1GetItemStoredPropertyValue(Sender: TdxCustomLayoutControl;
AItem: TdxCustomLayoutItem; const AName: string; var AValue: Variant);
begin
  if (AItem is TdxLayoutGroup) and (AName = 'Visible') then
    AValue := (AItem as TdxLayoutGroup).Visible;
  if (AItem is TdxLayoutSplitterItem) and (AName = 'Visible') then
    AValue := (AItem as TdxLayoutSplitterItem).Visible;
end;

procedure TfrmViewer.dxLayoutControl1SetItemStoredPropertyValue(Sender: TdxCustomLayoutControl;
AItem: TdxCustomLayoutItem; const AName: string; const AValue: Variant);
begin
  if (AItem is TdxLayoutGroup) and (AName = 'Visible') then
    (AItem as TdxLayoutGroup).Visible := AValue;
  if (AItem is TdxLayoutSplitterItem) and (AName = 'Visible') then
    (AItem as TdxLayoutSplitterItem).Visible := AValue;
end;

procedure TfrmViewer.MoveBottomGridLeft(const AData: TValue);
var
  _group: TdxLayoutGroup;
  _splitter: TdxLayoutSplitterItem;
begin
  var
  Sender := AData.AsObject;

  _group := TdxLayoutGroupButtonOptions(TdxLayoutGroupButtons(TdxLayoutGroupButton(Sender).Collection).Owner)
    .Group as TdxLayoutGroup;
  _splitter := TdxLayoutSplitterItem(dxLayoutBottomGroup.VisibleItems[_group.VisibleIndex + 1]);

  if assigned(_group) then
    if _group.VisibleIndex >= 2 then
    begin
      _group.MoveTo(dxLayoutBottomGroup, _group.VisibleIndex - 2, True);
      if assigned(_splitter) then
        _splitter.MoveTo(dxLayoutBottomGroup, _splitter.VisibleIndex - 2, True);
    end;
end;

procedure TfrmViewer.MoveBottomGridRight(const AData: TValue);
var
  _group: TdxLayoutGroup;
  _splitter: TdxLayoutSplitterItem;
begin
  var
  Sender := AData.AsObject;
  _group := TdxLayoutGroupButtonOptions(TdxLayoutGroupButtons(TdxLayoutGroupButton(Sender).Collection).Owner)
    .Group as TdxLayoutGroup;
  _splitter := TdxLayoutSplitterItem(dxLayoutBottomGroup.VisibleItems[_group.VisibleIndex + 1]);

  if assigned(_group) then
    if _group.VisibleIndex <= dxLayoutBottomGroup.VisibleCount - 3 then
    begin
      _group.MoveTo(dxLayoutBottomGroup, _group.VisibleIndex + 3, True);
      _splitter.MoveTo(dxLayoutBottomGroup, _splitter.VisibleIndex + 3, True);
    end;
end;

procedure TfrmViewer.dxLayoutMainSplitterItemCanResize(Sender: TObject; AItem: TdxCustomLayoutItem;
var ANewSize: Integer; var AAccept: Boolean);
begin
  AAccept := (ANewSize > MIN_HEIGHT_SPLIT_ITEM) and (ANewSize < (self.Height - MIN_HEIGHT_SPLIT_ITEM));
end;

procedure TfrmViewer.dxRestoreViewClick(Sender: TObject);
begin
  DoRestoreView;
  ShowNotification('Current view restored!');
end;

procedure TfrmViewer.dxSaveCurrentViewClick(Sender: TObject);
begin
  DoSaveCurrentView;
end;

procedure TfrmViewer.dxViewListCurChange(Sender: TObject);
begin
  if dxViewList.ItemIndex > -1 then
    DoSelectView(dxViewList.Items[dxViewList.CurItemIndex]);
end;

procedure TfrmViewer.ExportToExcel;
var
  AFileName: string;
  ASaveDialog: TSaveDialog;
begin
  ASaveDialog := TSaveDialog.Create(nil);
  try
    ASaveDialog.DefaultExt := 'xlsx';
    ASaveDialog.Filter := 'Excel Workbook (*.xlsx)|*.xlsx';
    ASaveDialog.FileName := 'ExportFile';
    ASaveDialog.InitialDir := ExtractFilePath(Application.ExeName);
    ASaveDialog.Options := ASaveDialog.Options + [ofOverwritePrompt];
    if ASaveDialog.Execute then
      AFileName := ASaveDialog.FileName
    else
      AFileName := '';
  finally
    ASaveDialog.Free;
  end;

  if AFileName <> '' then
    ExportGridToXLSX(AFileName, cxGrid1);
end;

procedure TfrmViewer.LoadNewFields(IDField: TField);
var
  Cur: IMongoCursor;
  SubObj, JObj: TJSONObject;
  JPair: TJSONPair; // Pair: field name : Value
  i: Integer;
  GridFrame: TGridFrame;
begin
  if not self.Showing then
    Exit;

  for var item in FBottomFrames do
    item.ClearData;

  var
  Dict := frmVisibilityModules.GetShowingSettingsForWork;
  var
  AutoDict := TAutoFree.Create(Dict);

  Cur := FMongoConn[DB_NAME][Collection_Name].Find.Match('{_id : {"$oid" : "' + IDField.AsString + '"}}')
    .&End.Project('{_id: 0').&End;
  // .Project('{_id: 0, Tis01_Module : 0, TisWin3_Module : 0, SyncClients : 0, Report_Versions : 0')
  if Cur.Next then
  begin
    JObj := TJSONObject.ParseJSONValue(Cur.Doc.AsJSON) as TJSONObject;

    if not assigned(JObj) then
      Exit;

    try
      for i := 0 to JObj.Count - 1 do
      begin
        JPair := JObj.Pairs[i];
        if assigned(JPair) and (JPair.JsonValue is TJSONObject) then
        begin
          SubObj := JPair.JsonValue.AsType<TJSONObject>;
          var
          gridName := JPair.JsonString.ToString.DeQuotedString('"');
          GridFrame := FBottomFrames.GetItemByFrameName(gridName);
          if Dict.ContainsKey(gridName) and (Dict[gridName] = SHOW_TYPE_NO) then
            Continue;

          if not assigned(GridFrame) then
          begin
            FBottomFrames.Add(CreateGridPanel(gridName, IDField.AsString, SubObj));
            frmVisibilityModules.AddNewShowingSettings(gridName);
          end
          else
          begin
            GridFrame.SetVisibleContent(True);
            GridFrame.UpdateData(SubObj, IDField.AsString);
          end;
        end;
      end;
    finally
      JObj.Free;
    end;
  end;
end;

function FixMqttTopic(ATopic: string): string;
begin
  Result := ATopic;
  Result := StringReplace(Result, ' ', '_', [rfReplaceAll]);
  Result := StringReplace(Result, '/', '', [rfReplaceAll]);
  Result := StringReplace(Result, '.', '', [rfReplaceAll]);
end;

procedure TfrmViewer.MQTTPublishToClient(EName, SName: string; const AMess: string);
begin
  if not MQTTClient.IsConnected then
  begin
    MQTTClient.Connect;
    ShowMessage('MQTT is now connecting, try again');
    Exit;
  end;

  EName := FixMqttTopic(EName);
  SName := FixMqttTopic(SName);
  MQTTClient.Publish('compudime/CompuDash/' + EName + '_' + SName, AMess);
end;

procedure TfrmViewer.MQTTRefresh;
begin
  if not MQTTClient.IsConnected then
  begin
    MQTTClient.Connect;
    ShowMessage('MQTT is now conencting, try again');
    Exit;
  end;

  MQTTClient.Publish('compudime/CompuDash', 'PushValues');
end;

procedure TfrmViewer.MQTTRefreshCurrent;
var
  EName, SName: string;
begin
  EName := FDMongoQuery1.FieldByName('enterprise_name').AsString;
  SName := FDMongoQuery1.FieldByName('store_name').AsString;

  MQTTPublishToClient(EName, SName, 'PushAllValues');
end;

procedure TfrmViewer.ScanForNewModules;
begin
  var
  Arr := GetAllSubDocuments(FDMongoQuery1);
  for var item in Arr do
    frmVisibilityModules.AddNewShowingSettings(item);
end;

procedure TfrmViewer.SelectAll1Click(Sender: TObject);
begin
  with TableView1 do
    if ColumnCount > 0 then
    begin
      DataController.BeginUpdate;
      try
        Controller.SelectAll;
        FVisualOptionList.State := cbChecked;
      finally
        DataController.EndUpdate;
      end;
    end;
end;

procedure TfrmViewer.SetFriendlyFilter(const AText: String);
begin
  StatusBar1.Panels[0].Text := AText;
end;

procedure TfrmViewer.ShowNotification(const AText: string);
begin
  pnlNotificationText.Caption := AText;
  pnlNotificationText.Visible := True;
  pnlNotificationText.BringToFront;
  TimerViewNotifications.Enabled := True;
end;

procedure TcxUserGridColumnHeaderPainter.DrawContent;
var
  _r: TRect;
begin
  with ViewInfo do
  begin
    _r := TextAreaBounds;
    Inc(_r.Left, frmViewer.FVisualOptionList.BoundsRect.Right + 5);

    LookAndFeelPainter.DrawScaledHeader(Canvas, Bounds, _r, Neighbors, Borders, cxbsNormal, AlignmentHorz,
      AlignmentVert, MultiLinePainting, False, Text, Params.Font, Params.TextColor, Params.Color, ScaleFactor, nil,
      Column.IsMostRight, ViewInfo.Container.Kind = ckGroupByBox);
  end;
  DrawAreas
end;

procedure TfrmViewer.dxBarMQTTRefreshCurrentClick(Sender: TObject);
begin
  MQTTRefreshCurrent;
end;

procedure TfrmViewer.TableView1CustomDrawColumnHeader(Sender: TcxGridTableView; ACanvas: TcxCanvas;
AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean);
begin
  if AViewInfo.Column.IsMostLeft then
  begin
    FVisualOptionList.Top := AViewInfo.Bounds.Top + Round((AViewInfo.Height - FVisualOptionList.Height) / 2) +
      AViewInfo.BorderSize[bTop];

    with TcxUserGridColumnHeaderPainter.Create(ACanvas, AViewInfo) do
      try
        DrawContent;
      finally
        Free;
      end;

    ADone := True;
  end;
end;

procedure TfrmViewer.TableView1GetStoredProperties(Sender: TcxCustomGridView; AProperties: TStrings);
begin
  AProperties.Add('ColumnsCaptions');
end;

procedure TfrmViewer.TableView1GetStoredPropertyValue(Sender: TcxCustomGridView; const AName: string;
var AValue: Variant);
var
  i: Integer;
  ASTringList: TStringList;
begin
  if AName = 'ColumnsCaptions' then
  begin
    ASTringList := TStringList.Create;

    try
      AValue := '';

      if Sender is TcxGridTableView then
        with Sender as TcxGridTableView do
          for i := 0 to ColumnCount - 1 do
          begin
            ASTringList.Delimiter := ADelimiter;
            ASTringList.AddPair(Columns[i].Name, Columns[i].Caption);
          end;

      AValue := ASTringList.CommaText;
    finally
      ASTringList.Clear;
      FreeAndNil(ASTringList);
    end;
  end;
end;

procedure TfrmViewer.TableView1SelectionChanged(Sender: TcxCustomGridTableView);
begin
  with TableView1 do
    if Controller.SelectedRecordCount = 0 then
      FVisualOptionList.State := cbUnchecked
    else if Controller.SelectedRecordCount = DataController.RecordCount then
      FVisualOptionList.State := cbChecked
    else
      FVisualOptionList.State := cbGrayed;
end;

procedure TfrmViewer.TableView1SetStoredPropertyValue(Sender: TcxCustomGridView; const AName: string;
const AValue: Variant);
begin
  if AName = 'ColumnsCaptions' then
    PostMessage(Handle, WM_RESTORCOLUMNCAPTIONS, Integer(Sender), Integer(GetColumnNames(AValue, ADelimiter)));
end;

procedure TfrmViewer.TimerViewNotificationsTimer(Sender: TObject);
begin
  TimerViewNotifications.Enabled := False;
  pnlNotificationText.Visible := False;
  pnlNotificationText.Caption := '';
end;

procedure TfrmViewer.ToggleSelectionAll(Sender: TObject);
begin
  with TableView1 do
    if ColumnCount > 0 then
    begin
      DataController.BeginUpdate;
      try
        if TCheckBox(Sender).State = cbChecked then
          Controller.SelectAll;
        if TCheckBox(Sender).State = cbUnchecked then
          Controller.ClearSelection;
      finally
        DataController.EndUpdate;
      end;
    end;
end;

procedure TfrmViewer.UnselectAll1Click(Sender: TObject);
begin
  with TableView1 do
    if ColumnCount > 0 then
    begin
      DataController.BeginUpdate;
      try
        Controller.ClearSelection;
        FVisualOptionList.State := cbUnchecked;
      finally
        DataController.EndUpdate;
      end;
    end;
end;

function TfrmViewer.StringsIndexOf(ASource: TStrings; const AViewName: string): Integer;
begin
  Result := ASource.IndexOf(AViewName);
end;

procedure TfrmViewer.FillReports(AJSON: TJSONObject; ADataSetReports, ADataSetClients, ADataSetClientReports1,
  ADataSetClientReports2: TDataSet; const AID: String; ADict: TDictionary<String, TDateTime>);
var
  i, j: Integer;
  SubJSON: TJSONObject;
  Report: String;
  ReportVer, SavedVer: Variant;
begin
  for i := 0 to AJSON.Count - 1 do
  begin
    if (AJSON.Pairs[i].JsonValue is TJSONObject) and (AJSON.Pairs[i].JsonString.Value = 'Report_Versions') then
    begin
      SubJSON := AJSON.Pairs[i].JsonValue as TJSONObject;
      for j := 0 to SubJSON.Count - 1 do
      begin
        if not ADataSetReports.Locate('Report', SubJSON.Pairs[j].JsonString.Value, []) then
        begin
          ADataSetReports.Insert;
          ADataSetReports['Report'] := SubJSON.Pairs[j].JsonString.Value;
          ADataSetReports.Post;
        end;

        Report := SubJSON.Pairs[j].JsonString.Value;
        ReportVer := OwnStrToDateTime(SubJSON.Pairs[j].JsonValue.Value);
        if ReportVer <> null then
        begin
          if ADict.ContainsKey(Report) then
          begin
            SavedVer := ADict[Report];
            if SavedVer < ReportVer then
              ADict[Report] := ReportVer;
          end
          else
            ADict.Add(Report, ReportVer);
        end;

        if ADataSetClients.Locate('_id', AID, []) then
        begin
          ADataSetClientReports1.Insert;
          ADataSetClientReports1['_id'] := AID;
          ADataSetClientReports1['Report'] := Report;
          ADataSetClientReports1['ClientReportDate'] := ReportVer;
          ADataSetClientReports1['enterprise_name'] := ADataSetClients['enterprise_name'];
          ADataSetClientReports1['store_name'] := ADataSetClients['store_name'];
          ADataSetClientReports1.Post;

          ADataSetClientReports2.Insert;
          ADataSetClientReports2['_id'] := AID;
          ADataSetClientReports2['Report'] := Report;
          ADataSetClientReports2['ClientReportDate'] := ReportVer;
          ADataSetClientReports2['enterprise_name'] := ADataSetClients['enterprise_name'];
          ADataSetClientReports2['store_name'] := ADataSetClients['store_name'];
          ADataSetClientReports2.Post;
        end;
      end;
    end;
  end;
end;

procedure TfrmViewer.FillReports(AJSON: TJSONObject; ADataSetReports: TDataSet; ADict: TDictionary<String, TDateTime>);
var
  i, j: Integer;
  SubJSON: TJSONObject;
  Report: String;
  ReportVer, SavedVer: Variant;
begin
  for i := 0 to AJSON.Count - 1 do
  begin
    if (AJSON.Pairs[i].JsonValue is TJSONObject) and (AJSON.Pairs[i].JsonString.Value = 'Report_Versions') then
    begin
      SubJSON := AJSON.Pairs[i].JsonValue as TJSONObject;
      for j := 0 to SubJSON.Count - 1 do
      begin
        if not ADataSetReports.Locate('Report', SubJSON.Pairs[j].JsonString.Value, []) then
        begin
          ADataSetReports.Insert;
          ADataSetReports['Report'] := SubJSON.Pairs[j].JsonString.Value;
          ADataSetReports.Post;
        end;

        Report := SubJSON.Pairs[j].JsonString.Value;
        ReportVer := OwnStrToDateTime(SubJSON.Pairs[j].JsonValue.Value);
        if ReportVer <> null then
        begin
          if ADict.ContainsKey(Report) then
          begin
            SavedVer := ADict[Report];
            if SavedVer < ReportVer then
              ADict[Report] := ReportVer;
          end
          else
            ADict.Add(Report, ReportVer);
        end;
      end;
    end;
  end;
  FillReportsDate(ADataSetReports, ADict, 'Date');
end;

procedure TfrmViewer.FillReportsDate(ADataSet: TDataSet; ADict: TDictionary<String, TDateTime>;
const AFieldName: String);
var
  Report: String;
begin
  ADataSet.DisableControls;
  try
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      Report := ADataSet['Report'];
      if ADict.ContainsKey(Report) then
      begin
        ADataSet.Edit;
        ADataSet[AFieldName] := ADict[Report];
        ADataSet.Post;
      end;
      ADataSet.Next;
    end;
  finally
    ADataSet.EnableControls;
  end;
end;

procedure TfrmViewer.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  TableView1.DataController.DataSource := nil;
  // should also do for all bottom views
  try
    // Close all dataset connections first
    try
      if assigned(memTis01Modules) and memTis01Modules.Active then
        memTis01Modules.Close;

      if assigned(memTisWin3Modules) and memTisWin3Modules.Active then
        memTisWin3Modules.Close;

      if assigned(memSyncClients) and memSyncClients.Active then
        memSyncClients.Close;

      if assigned(memRepVers) and memRepVers.Active then
        memRepVers.Close;

      if assigned(mtblReports) and mtblReports.Active then
        mtblReports.Close;
    except
      on E: Exception do; // Silently continue if there's an error
    end;

    // Close MongoDB queries
    try
      if assigned(FDMongoQuery1) and FDMongoQuery1.Active then
        FDMongoQuery1.Close;
    except
      on E: Exception do; // Silently continue if there's an error
    end;

    // Close any cdMongo connections if it exists
    try
      if assigned(cdMongo) then
      begin
        if assigned(cdMongo.mqData) and cdMongo.mqData.Active then
          cdMongo.mqData.Close;

        if assigned(cdMongo.FDConnection) and cdMongo.FDConnection.Connected then
          cdMongo.FDConnection.Close;
      end;
    except
      on E: Exception do; // Silently continue if there's an error
    end;

    // Clear all MongoDB related objects
    try
      // Clear the MongoDB connection reference first
      FMongoConn := nil;
      // Clear environment reference
      FEnv := nil;
      // Force application to process messages
      Application.ProcessMessages;
      // Clear connections
      FDConnection := nil;
      FDConnection1 := nil;
    except
      on E: Exception do; // Silently continue if there's an error
    end;

    // Close MQTT connection if active
    try
      if assigned(MQTTClient) and MQTTClient.IsConnected then
        MQTTClient.Disconnect;
    except
      on E: Exception do; // Silently continue if there's an error
    end;

  except
    on E: Exception do
    begin
      OutputDebugString(PChar('Error during close: ' + E.Message));
    end;
  end;

  // Force termination
  CanClose := True;
  try
    // Kill the process if it's still running after a short delay
    TThread.CreateAnonymousThread(
      procedure
      begin
        Sleep(1000); // Wait 1 second
        if Application.Active then
        begin
          OutputDebugString('Forcing application termination');
          TerminateProcess(GetCurrentProcess, 0);
        end;
      end).Start;
  except
    on E: Exception do; // Silently continue if there's an error
  end;
end;

procedure TfrmViewer.FormShow(Sender: TObject);
var
  LConnectionManager: IConnectionManager;
begin
  CreateAndConfigureVisualOptionList;

  LConnectionManager := TDMConnectionManager.Create(Application);
  LConnectionManager.Restore;
  LConnectionManager.SetEventOnConnectTo(ConnectToMongo);
  if not LConnectionManager.ConnectToDefault then
    TFormConnectionManager.GetInstance.InitAndShow(ConnectToMongo);

  MQTTClient.Connect;
end;

function TfrmViewer.GetAllSubDocuments(AQuery: TFDMongoQuery): TArray<String>;
var
  Cur: IMongoCursor;
  JSON: TJSONObject;
  StrList: TStringList;
begin
  Result := [];

  StrList := TStringList.Create;
  try
    Cur := AQuery.Collection.Find();
    while Cur.Next do
    begin
      JSON := TJSONObject.ParseJSONValue(Cur.Doc.AsJSON) as TJSONObject;
      try
        if assigned(JSON) and (JSON.Count > 0) then
          for var i := 0 to JSON.Count - 1 do
            if (JSON.Pairs[i].JsonValue is TJSONObject) and not(JSON.Pairs[i].JsonString.Value = '_id') then
              if StrList.IndexOf(JSON.Pairs[i].JsonString.Value) < 0 then
                StrList.Add(JSON.Pairs[i].JsonString.Value);
      finally
        JSON.Free;
      end;
    end;
    for var Str in StrList do
      Result := Result + [Str];
  finally
    StrList.Free;
  end;
end;

procedure TfrmViewer.OnNotifyScanNewModules(const AData: TValue);
begin
  ScanForNewModules;
end;

procedure TfrmViewer.OpenData;
var
  List: TFilterSettingsList;
  MongoCon: TMongoConnection;
  Cursor: IMongoCursor;
  Str: String;
  LDataChangeEvent: TDataChangeEvent;
begin
  TableView1.BeginUpdate(lsimImmediate);
  try
    if FDMongoQuery1.Active then
      FDMongoQuery1.Close;

    FDMongoQuery1.DatabaseName := DB_NAME;
    FDMongoQuery1.CollectionName := Collection_Name;

    List := GetFilterSettings;
    try
      FDMongoQuery1.QMatch := List.ToMongoFilter;
      StatusBar1.Panels[0].Text := List.ToFriendlyFilter;
    finally
      List.Free;
    end;
    // LDataChangeEvent := dsMongoQuery.OnDataChange;
    // dsMongoQuery.OnDataChange := nil;
    FDMongoQuery1.Open;
    OpenDataForClientsAndReports(FDMongoQuery1);
  finally
    FDMongoQuery1.First;
    TableView1.EndUpdate;
    // dsMongoQuery.OnDataChange := LDataChangeEvent;
  end;
end;

procedure TfrmViewer.OpenDataForClientsAndReports(AMongoQuery: TFDMongoQuery);
var
  Cur: IMongoCursor;
  JSON: TJSONObject;
  Dict: TDictionary<String, TDateTime>;
  ID: String;
  s: String;
begin
  mtblReports.DisableControls;
  try
    mtblReports.EmptyDataSet;
    Dict := nil;
    try
      Dict := TDictionary<string, TDateTime>.Create;

      Cur := AMongoQuery.Collection.Find();
      while Cur.Next do
      begin
        JSON := TJSONObject.ParseJSONValue(Cur.Doc.AsJSON) as TJSONObject;
        s := JSON.ToString;
        if not assigned(JSON) or (JSON.Count = 0) then
          Continue;

        FillReports(JSON, mtblReports, Dict);
      end;
      FillReportsDate(mtblReports, Dict, 'Date');
    finally
      Dict.Free;
    end;

  finally
    mtblReports.EnableControls;
  end;
  Show;
end;

procedure TfrmViewer.TableView1Customization(Sender: TObject);
begin
  if assigned(TableView1.Controller.CustomizationForm) and TableView1.Controller.CustomizationForm.Visible then
    TableView1.Controller.CustomizationForm.Left := Screen.Width -
      (TableView1.Controller.CustomizationForm.Width + 100);
end;

initialization

// dxUseVectorIcons := True;
TdxVisualRefinements.ApplyLightStyle;

end.
