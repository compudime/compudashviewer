unit caAfterShow;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  Forms, ExtCtrls;

type
  TcaAfterShow = class(TComponent)
  private
    FTimer: TTimer;
    FAfterShow: TNotifyEvent;
    procedure TimerEvent(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure StartTimer;
  published
    property AfterShow: TNotifyEvent read FAfterShow write FAfterShow;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TcaAfterShow]);
end;

constructor TcaAfterShow.Create(AOwner: Tcomponent);
begin
  inherited Create(AOwner);
  if not (csDesigning in Owner.ComponentState) then
    begin
      FTimer := TTimer.Create(Self);
      FTimer.OnTimer := TimerEvent;
      FTimer.Interval := 100;
      FTimer.Enabled := True;
    end;
end;

destructor TcaAfterShow.Destroy;
begin
  FTimer.Free;
  inherited Destroy;
end;

procedure TcaAfterShow.StartTimer;
begin
  if not FTimer.Enabled then
    FTimer.Enabled := True;
end;

procedure TcaAfterShow.TimerEvent(Sender: TObject);
begin
  FTimer.Enabled := False;
  if not (Owner is TCustomForm) then Exit;
  if TCustomForm(Owner).Showing then
    begin
      if Assigned(FAfterShow) then
        begin
          FAfterShow(Self);
          Exit;
        end;
    end;
  FTimer.Enabled := True;
end;

end.

