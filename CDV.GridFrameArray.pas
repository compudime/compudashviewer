unit CDV.GridFrameArray;

interface

uses
  System.Classes, System.Generics.Collections, System.Generics.Defaults, System.SysUtils,
  CDV.Consts,
  frGrid;

type
  TGridFrameArray = TArray<TGridFrame>;

  TGridFrameArrayHelper = record helper for TGridFrameArray
    function ContainsFrame(const AName: String): Boolean;
    function GetItemByFrameName(const AName: String): TGridFrame;
    function GetItemIndexByFrame(aItem: TGridFrame): Integer;
    procedure Add(aItem: TGridFrame);
    function Hide(aIndex: Integer): Boolean; overload;
    function Hide(aItem: TGridFrame): Boolean; overload;
    function ArrayLength: Integer;
    function IsDefaultFrame(aIndex: Integer): Boolean;
    function GetVisibleFrameNameArray: TArray<String>;
    procedure ReorderByVisibility;
    procedure ClearArray;
  end;

implementation

{ TGridFrameArrayHelper }

procedure TGridFrameArrayHelper.Add(aItem: TGridFrame);
begin
  var length := Self.ArrayLength;
  SetLength(Self, length + 1);
  Self[length] := aItem;
end;

procedure TGridFrameArrayHelper.ClearArray;
begin
  if Self.ArrayLength = 0 then
    Exit;

  for var Item in Self do
    Item.Free;

  Self := [];
end;

function TGridFrameArrayHelper.ContainsFrame(const AName: String): Boolean;
begin
  for var Frame in Self do
    if Frame.Name = AName then
      Exit(True);

  Result := False;
end;

function TGridFrameArrayHelper.GetItemByFrameName(const AName: String): TGridFrame;
begin
  for var Frame in Self do
    if Frame.Name = AName then
      Exit(Frame);

  Result := nil;
end;

function TGridFrameArrayHelper.GetItemIndexByFrame(aItem: TGridFrame): Integer;
begin
  for var i := 0 to Self.ArrayLength - 1 do
    if aItem.Name = Self[i].Name then
      Exit(i);

  Result := -1;
end;

function TGridFrameArrayHelper.GetVisibleFrameNameArray: TArray<String>;
begin
  Result := [];
  for var item in Self do
    if item.IsVisible then
      Result := Result + [item.Name];
end;

function TGridFrameArrayHelper.IsDefaultFrame(aIndex: Integer): Boolean;
begin
  if (aIndex < 0) or (aIndex >= Self.ArrayLength) then
    Exit(False);

  var name := Self[aIndex].Name;
  for var i := 0 to Length(DEFAULT_GRIDS) - 1 do
    if DEFAULT_GRIDS[i] = name then
      Exit(True);

  Result := False;
end;

procedure TGridFrameArrayHelper.ReorderByVisibility;
var
  F: TComparison<TGridFrame>;
begin
  F := function (const Left, Right: TGridFrame): Integer
        begin
          if Left.IsVisible and Right.IsVisible then
            Result := 0
          else if Left.IsVisible then
            Result := -1
          else Result := 1;
        end;

  TArray.Sort<TGridFrame>(Self, TComparer<TGridFrame>.Construct(F));

  F := function (const Left, Right: TGridFrame): Integer
        begin
          Result := Left.GetDataGroup.VisibleIndex - Right.GetDataGroup.VisibleIndex;
        end;

  TArray.Sort<TGridFrame>(Self, TComparer<TGridFrame>.Construct(F));
end;

function TGridFrameArrayHelper.ArrayLength: Integer;
begin
  Result := Length(Self);
end;

function TGridFrameArrayHelper.Hide(aItem: TGridFrame): Boolean;
begin
  var index := GetItemIndexByFrame(aItem);

  Exit(Hide(index));
end;

function TGridFrameArrayHelper.Hide(aIndex: Integer): Boolean;
begin
  if (aIndex < 0) or (aIndex >= Self.ArrayLength) then
    Exit(False);

  Self[aIndex].SetVisibleContent(false);

  Result := True;
end;

end.
