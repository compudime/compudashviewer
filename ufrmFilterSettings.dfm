object frmFilterSettings: TfrmFilterSettings
  Left = 0
  Top = 0
  Caption = 'Filter settings'
  ClientHeight = 476
  ClientWidth = 992
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 992
    Height = 476
    Align = alClient
    TabOrder = 0
    object lboxMasterFields: TcxListBox
      Left = 22
      Top = 51
      Width = 226
      Height = 372
      ItemHeight = 13
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = cbs3D
      TabOrder = 1
      OnClick = lboxMasterFieldsClick
    end
    object lblText: TcxLabel
      Left = 22
      Top = 28
      Caption = 'Master fields'
      Style.HotTrack = False
      TabOrder = 0
      Transparent = True
    end
    object gridSubFields: TcxGrid
      Left = 278
      Top = 28
      Width = 692
      Height = 395
      TabOrder = 2
      object gridSubFieldsDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dtsrcSubFields
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        object clmSubFieldName: TcxGridDBColumn
          DataBinding.FieldName = 'FieldName'
          Options.Editing = False
          Width = 287
        end
        object clmSubFieldExists: TcxGridDBColumn
          DataBinding.FieldName = 'Exists'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.Items.Strings = (
            'Not Set'
            'Exists'
            'Not Exists')
          Width = 202
        end
        object clmSubFieldsValue: TcxGridDBColumn
          DataBinding.FieldName = 'Value'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.DropDownListStyle = lsFixedList
          Properties.Items.Strings = (
            'Not Set'
            'T'
            'F')
          Width = 201
        end
      end
      object gridSubFieldsLevel1: TcxGridLevel
        GridView = gridSubFieldsDBTableView1
      end
    end
    object btnScan: TcxButton
      Left = 10
      Top = 441
      Width = 75
      Height = 25
      Caption = 'Scan'
      TabOrder = 3
      OnClick = btnScanClick
    end
    object btnCancel: TcxButton
      Left = 499
      Top = 441
      Width = 75
      Height = 25
      Caption = 'Cancel'
      TabOrder = 6
      OnClick = btnCancelClick
    end
    object btnApply: TcxButton
      Left = 418
      Top = 441
      Width = 75
      Height = 25
      Caption = 'Apply'
      TabOrder = 5
      OnClick = btnApplyClick
    end
    object btnRemoveAllFilters: TcxButton
      Left = 91
      Top = 441
      Width = 102
      Height = 25
      Caption = 'Remove All Filters'
      TabOrder = 4
      OnClick = btnRemoveAllFiltersClick
    end
    object dxLayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      Hidden = True
      ItemIndex = 1
      ShowBorder = False
      Index = -1
    end
    object laylboxMasterFields: TdxLayoutItem
      Parent = laygrpLeft
      AlignVert = avClient
      CaptionOptions.Text = 'cxListBox1'
      CaptionOptions.Visible = False
      Control = lboxMasterFields
      ControlOptions.OriginalHeight = 97
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laylblText: TdxLayoutItem
      Parent = laygrpLeft
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblText
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 65
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygridSubFields: TdxLayoutItem
      Parent = laygrpRight
      AlignVert = avClient
      CaptionOptions.Text = 'cxGrid1'
      CaptionOptions.Visible = False
      Control = gridSubFields
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpLeft: TdxLayoutGroup
      Parent = laygrpTop
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      SizeOptions.Width = 250
      Index = 0
    end
    object laygrpRight: TdxLayoutGroup
      Parent = laygrpTop
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      Index = 1
    end
    object laygrpTop: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object laygrpBottom: TdxLayoutGroup
      Parent = dxLayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object laybtnScan: TdxLayoutItem
      Parent = laygrpBottom
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnScan
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object laygrpControlButtons: TdxLayoutGroup
      Parent = laygrpBottom
      AlignHorz = ahCenter
      CaptionOptions.Text = 'New Group'
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object laybtnCancel: TdxLayoutItem
      Parent = laygrpControlButtons
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object laybtnApply: TdxLayoutItem
      Parent = laygrpControlButtons
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnApply
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = laygrpBottom
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnRemoveAllFilters
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 102
      ControlOptions.ShowBorder = False
      Index = 1
    end
  end
  object mtblSubFields: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 376
    Top = 192
    object mtblSubFieldsFieldName: TStringField
      FieldName = 'FieldName'
      Size = 100
    end
    object mtblSubFieldsExists: TStringField
      FieldName = 'Exists'
      OnGetText = mtblSubFieldsExistsGetText
    end
    object mtblSubFieldsValue: TStringField
      FieldName = 'Value'
      OnGetText = mtblSubFieldsExistsGetText
    end
  end
  object dtsrcSubFields: TDataSource
    DataSet = mtblSubFields
    Left = 368
    Top = 272
  end
end
