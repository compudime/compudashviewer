unit uCdMongo;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Phys.MongoDBDef, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.MongoDB, System.Rtti, System.JSON.Types,
  System.JSON.Readers, System.JSON.BSON, System.JSON.Builders, FireDAC.Phys.MongoDBWrapper, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, Data.DB, FireDAC.Comp.Client, FireDAC.Comp.DataSet, FireDAC.Phys.MongoDBDataSet,
  DM.ConnectionManager;

type
  TcdMongo = class(TDataModule)
    FDPhysMongoDriverLink1: TFDPhysMongoDriverLink;
    mqData: TFDMongoQuery;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDConnection: TFDConnection;
    procedure DataModuleDestroy(Sender: TObject);
  private
    FCollection_Name: string;
    FDB_Name: string;
    FMongoConn: TMongoConnection;
    FEnv: TMongoEnv;

    procedure StartConnection;
    procedure ConnectToMongo(AConnectionData: TConnectionData);
  public
    property DB_Name: string read FDB_Name;
    property Collection_Name: string read FCollection_Name;

    procedure DoClose;

    function DeleteRecord(const ACollection, AEnterprise_ID, AStore_ID: String): Boolean;
    function DeleteKey(const ACollection, AEnterprise_ID, AStore_ID, AFieldName: string): Boolean;
    procedure DoConnect(const ADB_Name, ACollection_Name: string);

    function MyValueSetGet(const ACollection, AEnterprise_ID, AStore_ID, AFieldName, AFieldValue: string)
      : string; overload;
    function MyValueSetGet(const ACollection, AEnterprise_ID, AStore_ID, AFieldName: string): string; overload;
    function MyValueGet(const ACollection, AEnterprise_ID, AStore_ID, AFieldName: string): string;

    function DeviceSetGet(const ACollection, ADevice_ID, AFieldName, AFieldValue: string): string; overload;
    function DeviceSetGet(const ACollection, ADevice_ID, AFieldName: string): string; overload;

    function GetJSONByID(const DBName, Collection, ID: string): string;
    procedure Refresh(const ACollection_Name: string);
  end;

var
  cdMongo: TcdMongo;

implementation

uses System.JSON, System.IniFiles, VCL.Forms;

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
{ TDataModule3 }

procedure TcdMongo.ConnectToMongo(AConnectionData: TConnectionData);
var
  FConnStr: String;
begin
  if not Assigned(AConnectionData) then
    exit;

  if Assigned(FDConnection) then
    FreeAndNil(FDConnection);

  if Assigned(mqData) then
    FreeAndNil(mqData);

  FDConnection := TFDConnection.Create(nil);

  FDConnection.Params.Clear;
  FDConnection.Params.Add('DriverID=Mongo');

  if AConnectionData.ConnectionType.Equals(DB_ATLAS_TYPE) then
  begin
    FConnStr := Format('mongodb+srv://%s:%s@%s.%s.mongodb.net/%s?retryWrites=true&UseSSL=True&w=majority',
      [AConnectionData.UserName, AConnectionData.Password, AConnectionData.Database, AConnectionData.Server,
      AConnectionData.Database]);

    FDConnection.Params.Add('Server=' + AConnectionData.Database + '-shard-00-00.' + AConnectionData.Server +
      '.mongodb.net');
    // FDConnection.Params.Add('MongoAdvanced=ssl=true&authSource=admin&retryWrites=true&w=majority');
    FDConnection.Params.Add('UseSSL=True');

    FMongoConn := TMongoConnection(FDConnection.CliObj);
    FEnv := FMongoConn.Env;

    FMongoConn.Open(FConnStr);
  end
  else if AConnectionData.ConnectionType.Equals(DB_SERVER_TYPE) then
  begin
    FDConnection.Params.Add('Server=' + AConnectionData.Server);
    FDConnection.Params.Database := AConnectionData.Database;
    FDConnection.Params.Add('MongoAdvanced=authenticationDatabase=' + AConnectionData.Database);
    FDConnection.Params.Add('Port=' + AConnectionData.port.toString);

    if not AConnectionData.UserName.isEmpty then
      FDConnection.Params.UserName := AConnectionData.UserName;
    if not AConnectionData.Password.isEmpty then
      FDConnection.Params.Password := AConnectionData.Password;

    FDConnection.Connected := True;
    FMongoConn := TMongoConnection(FDConnection.CliObj);
    FEnv := FMongoConn.Env;
  end;

  mqData := TFDMongoQuery.Create(self);
  mqData.FormatOptions.StrsTrim2Len := True;
  mqData.Connection := FDConnection;
end;

procedure TcdMongo.DataModuleDestroy(Sender: TObject);
begin
  if mqData.Active then
    mqData.Active := False;

  if FDConnection.Connected then
    FDConnection.Connected := False;
end;

function TcdMongo.DeleteKey(const ACollection, AEnterprise_ID, AStore_ID, AFieldName: string): Boolean;
var
  MongoUpdate: TMongoUpdate;
  MongoColl: TMongoCollection;
begin
  MongoColl := FMongoConn.Collections[DB_Name, ACollection];
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match().Add('enterprise_name', AEnterprise_ID);
    MongoUpdate.Match().Add('store_name', AStore_ID);
    MongoUpdate.Modify().Unset().Field(AFieldName);
    MongoColl.Update(MongoUpdate);
    Result := True;
  finally
    FreeAndNil(MongoUpdate);
  end;
end;

function TcdMongo.DeleteRecord(const ACollection, AEnterprise_ID, AStore_ID: String): Boolean;
var
  MongoSel: TMongoSelector;
  MongoColl: TMongoCollection;
  EncEnterprise_ID, EncStore_ID: String;
begin
  // URL encode the IDs to handle special characters
  EncEnterprise_ID := AEnterprise_ID;
  EncStore_ID := AStore_ID;

  MongoColl := FMongoConn.Collections[DB_Name, ACollection];
  MongoSel := TMongoSelector.Create(FEnv);
  try
    MongoSel.Match().Add('enterprise_name', EncEnterprise_ID);
    MongoSel.Match().Add('store_name', EncStore_ID);
    MongoColl.Remove(MongoSel, [TMongoCollection.TRemoveFlag.SingleRemove]);
    Result := True;
  finally
    FreeAndNil(MongoSel);
  end;
end;

procedure TcdMongo.DoClose;
begin
  mqData.Close;
  // FDConnection.Connected := True;
end;

procedure TcdMongo.DoConnect(const ADB_Name, ACollection_Name: string);
var
  LConnectionManager: IConnectionManager;
begin
  if not FDConnection.Connected then
  begin
    FDB_Name := ADB_Name;
    FCollection_Name := ACollection_Name;

    LConnectionManager := TDMConnectionManager.Create(Application);
    LConnectionManager.Restore;
    LConnectionManager.SetEventOnConnectTo(ConnectToMongo);
    if LConnectionManager.ConnectToDefault then
      StartConnection;
  end;
end;

function TcdMongo.GetJSONByID(const DBName, Collection, ID: string): string;
var
  Cur: IMongoCursor;
begin
  Cur := FMongoConn[DBName][Collection].Find().Match('{_id : {"$oid" : "' + ID + '"}}').&End;

  if Cur.Next then
    Result := Cur.Doc.AsJSON
  else
    Result := '';
end;

function TcdMongo.MyValueSetGet(const ACollection, AEnterprise_ID, AStore_ID, AFieldName, AFieldValue: string): string;
var
  MongoColl: TMongoCollection;
  MongoUpdate: TMongoUpdate;
begin
  MongoColl := FMongoConn.Collections[DB_Name, ACollection];
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match(Format('{"enterprise_name" : "%s", "store_name": "%s"}', [AEnterprise_ID, AStore_ID]));
    MongoUpdate.Modify().&Set().Field(AFieldName, AFieldValue);
    MongoColl.Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
  finally
    FreeAndNil(MongoUpdate);
  end;
  Result := AFieldValue;
end;

function TcdMongo.MyValueGet(const ACollection, AEnterprise_ID, AStore_ID, AFieldName: string): string;
var
  Field: TField;
begin
  mqData.Close;
  mqData.CollectionName := ACollection;
  mqData.QMatch := Format('{"enterprise_name" : "%s", "store_name": "%s"}', [AEnterprise_ID, AStore_ID]);
  mqData.Open;
  Field := mqData.FindField(AFieldName);

  if not Assigned(Field) or (mqData.FieldByName('enterprise_name').AsString.Length = 0) then
    Result := ''
  else
    Result := Field.AsString;
end;

function TcdMongo.MyValueSetGet(const ACollection, AEnterprise_ID, AStore_ID, AFieldName: string): string;
var
  // Field: TField;
  Cur: IMongoCursor;
  JSON: TJSONObject;
begin
  // mqData.Close;
  // mqData.CollectionName := ACollection;
  // mqData.QMatch := Format('{"enterprise_name" : "%s", "store_name": "%s"}', [AEnterprise_ID, AStore_ID]);
  // mqData.Open;
  // Field := mqData.FindField(AFieldName);

  { if Assigned(Field) then
    Result := Field.AsString
    else
    Result := '';

    if (mqData.FieldByName('enterprise_name').AsString.Length = 0) or (Result = '') then
    Result := MyValueSetGet(ACollection, AEnterprise_ID, AStore_ID, AFieldName, ''); }
  JSON := nil;

  Cur := FMongoConn[FDB_Name][ACollection].Find().Match(Format('{"enterprise_name" : "%s", "store_name": "%s"}',
    [AEnterprise_ID, AStore_ID])).&End;

  if Cur.Next then
    JSON := TJSONObject.ParseJSONValue(Cur.Doc.AsJSON) as TJSONObject
  else
    exit(MyValueSetGet(ACollection, AEnterprise_ID, AStore_ID, AFieldName, ''));

  if not Assigned(JSON) then
    exit('');

  try
    if Assigned(JSON.Values[AFieldName]) then
      Result := JSON.Values[AFieldName].Value
    else
      Result := MyValueSetGet(ACollection, AEnterprise_ID, AStore_ID, AFieldName, '');
  finally
    JSON.Free;
  end;
end;

procedure TcdMongo.Refresh(const ACollection_Name: string);
begin
  mqData.Close;
  mqData.CollectionName := ACollection_Name;
  mqData.Open;
end;

procedure TcdMongo.StartConnection;
begin
  mqData.DatabaseName := DB_Name;
  mqData.CollectionName := Collection_Name;
  mqData.Open;
end;

function TcdMongo.DeviceSetGet(const ACollection, ADevice_ID, AFieldName, AFieldValue: string): string;
var
  MongoColl: TMongoCollection;
  MongoUpdate: TMongoUpdate;
begin
  MongoColl := FMongoConn.Collections[DB_Name, ACollection];
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match(Format('{"device_id" : "%s"}', [ADevice_ID]));
    MongoUpdate.Modify().&Set().Field(AFieldName, AFieldValue);
    MongoColl.Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
  finally
    FreeAndNil(MongoUpdate);
  end;
  Result := AFieldValue;
end;

function TcdMongo.DeviceSetGet(const ACollection, ADevice_ID, AFieldName: string): string;
var
  Field: TField;
begin

  mqData.Close;
  mqData.CollectionName := ACollection;
  mqData.QMatch := Format('{"device_id" : "%s"}', [ADevice_ID]);
  mqData.Open;
  Field := mqData.FindField(AFieldName);
  if not Assigned(Field) or (mqData.FieldByName('device_id').AsString.Length = 0) then
    Result := DeviceSetGet(ACollection, ADevice_ID, AFieldName, '')
  else
    Result := Field.AsString;
end;

end.
