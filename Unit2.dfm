object frmActions: TfrmActions
  Left = 0
  Top = 0
  Caption = 'Apply Action to Selected Rows'
  ClientHeight = 220
  ClientWidth = 517
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnShow = FormShow
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 517
    Height = 220
    Align = alClient
    TabOrder = 0
    AutoSize = True
    ExplicitWidth = 541
    ExplicitHeight = 304
    object cmbAction: TcxComboBox
      Left = 50
      Top = 10
      Properties.DropDownListStyle = lsEditFixedList
      Properties.Items.Strings = (
        'DownloadFTP'
        'ExecSQL'
        'ExecSQL_RepDefs'
        'GetDataValue'
        'PushEizer'
        'RunScript'
        'SetDTOS'
        'SetKeyValue'
        'ShellExecute'
        'UpdateWicAPL')
      Properties.OnEditValueChanged = cmbActionPropertiesEditValueChanged
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 3
      Width = 121
    end
    object btnApply: TcxButton
      Left = 432
      Top = 185
      Width = 75
      Height = 25
      Caption = 'Apply'
      TabOrder = 2
      OnClick = btnApplyClick
    end
    object txtParams: TcxTextEdit
      Left = 50
      Top = 151
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      TextHint = 'Parameters for Execute Method'
      Width = 449
    end
    object btnExtract: TcxButton
      Left = 263
      Top = 185
      Width = 163
      Height = 25
      Caption = 'Extract Command to Clipboard'
      TabOrder = 1
      OnClick = btnExtractClick
    end
    object txtActionValue: TcxMemo
      Left = 50
      Top = 64
      Properties.ScrollBars = ssVertical
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Height = 81
      Width = 449
    end
    object cmbScripts: TcxComboBox
      Left = 50
      Top = 37
      Properties.DropDownListStyle = lsEditFixedList
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      Style.ButtonStyle = bts3D
      Style.PopupBorderStyle = epbsFrame3D
      TabOrder = 4
      Width = 239
    end
    object chkPostMQTT: TcxCheckBox
      Left = 10
      Top = 185
      Caption = 'Post MQTT Refresh'
      Style.BorderColor = clWindowFrame
      Style.BorderStyle = ebs3D
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      Transparent = True
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahParentManaged
      AlignVert = avParentManaged
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object dxLayoutGroup3: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahLeft
      CaptionOptions.Text = 'Hidden Group'
      Hidden = True
      ItemIndex = 1
      ShowBorder = False
      Index = 1
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = dxLayoutGroup3
      AlignHorz = ahLeft
      CaptionOptions.Text = 'Action'
      Control = cmbAction
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object liTxtActionValue: TdxLayoutItem
      Parent = dxLayoutGroup3
      AlignHorz = ahLeft
      CaptionOptions.AlignVert = tavTop
      CaptionOptions.Text = 'Value'
      Control = txtActionValue
      ControlOptions.OriginalHeight = 81
      ControlOptions.OriginalWidth = 449
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object liTxtParams: TdxLayoutItem
      Parent = dxLayoutGroup3
      AlignHorz = ahLeft
      Visible = False
      CaptionOptions.Text = 'Params'
      Control = txtParams
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 449
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object dxLayoutItem4: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup2
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'btnApply'
      CaptionOptions.Visible = False
      Control = btnApply
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object liBtnExtract: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup2
      AlignVert = avClient
      Visible = False
      CaptionOptions.Text = 'btnExtract'
      CaptionOptions.Visible = False
      Control = btnExtract
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 163
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object liCmbScripts: TdxLayoutItem
      Parent = dxLayoutGroup3
      AlignHorz = ahLeft
      Visible = False
      CaptionOptions.Text = 'Script'
      Control = cmbScripts
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 239
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutAutoCreatedGroup2: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutAutoCreatedGroup1
      AlignHorz = ahRight
      AlignVert = avBottom
      LayoutDirection = ldHorizontal
      Index = 1
    end
    object liChkPostMQTT: TdxLayoutItem
      Parent = dxLayoutAutoCreatedGroup1
      AlignVert = avClient
      CaptionOptions.Visible = False
      Control = chkPostMQTT
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 111
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutAutoCreatedGroup1: TdxLayoutAutoCreatedGroup
      Parent = dxLayoutControl1Group_Root
      AlignVert = avBottom
      LayoutDirection = ldHorizontal
      Index = 0
    end
  end
  object IdFTP1: TIdFTP
    ConnectTimeout = 0
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 336
    Top = 120
  end
end
