object cdMongo: TcdMongo
  OnDestroy = DataModuleDestroy
  Height = 174
  Width = 233
  object FDPhysMongoDriverLink1: TFDPhysMongoDriverLink
    Left = 100
    Top = 84
  end
  object mqData: TFDMongoQuery
    FormatOptions.AssignedValues = [fvStrsTrim2Len]
    FormatOptions.StrsTrim2Len = True
    Connection = FDConnection
    Left = 44
    Top = 84
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 100
    Top = 28
  end
  object FDConnection: TFDConnection
    Params.Strings = (
      'Port=63700'
      'DriverID=Mongo'
      'Database=radomirdb'
      'UseSSL=True')
    LoginPrompt = False
    Left = 44
    Top = 28
  end
end
