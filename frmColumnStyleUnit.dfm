object frmColumnStyle: TfrmColumnStyle
  Left = 0
  Top = 0
  AlphaBlend = True
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Custom Style'
  ClientHeight = 160
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PopupMode = pmAuto
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 454
    Height = 160
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = dxLayoutSkinLookAndFeel1
    OptionsItem.AllowFloatingGroups = True
    OptionsItem.SizableHorz = True
    OptionsItem.SizableVert = True
    object dxColorEdit1: TdxColorEdit
      Left = 56
      Top = 74
      Properties.OnEditValueChanged = cxComboBox1PropertiesEditValueChanged
      Style.HotTrack = False
      TabOrder = 2
      Width = 85
    end
    object dxColorEdit2: TdxColorEdit
      Left = 204
      Top = 74
      Properties.OnEditValueChanged = cxComboBox1PropertiesEditValueChanged
      Style.HotTrack = False
      TabOrder = 3
      Width = 85
    end
    object btnSave: TcxButton
      Left = 288
      Top = 117
      Width = 75
      Height = 25
      Caption = 'Save'
      TabOrder = 6
      OnClick = btnSaveClick
    end
    object btnClose: TcxButton
      Left = 369
      Top = 117
      Width = 75
      Height = 25
      Caption = 'Close'
      TabOrder = 7
      OnClick = btnCloseClick
    end
    object cxLabel2: TcxLabel
      Left = 10
      Top = 10
      Caption = 'Set style of cells where:'
      Style.HotTrack = False
      Transparent = True
    end
    object btnReset: TcxButton
      Left = 10
      Top = 117
      Width = 75
      Height = 25
      Caption = 'Reset'
      TabOrder = 5
      OnClick = btnResetClick
    end
    object lblColumn: TcxLabel
      Left = 10
      Top = 33
      AutoSize = False
      Caption = 'ColumnName'
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.LookAndFeel.SkinName = 'Office2013DarkGray'
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.SkinName = 'Office2013DarkGray'
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.SkinName = 'Office2013DarkGray'
      StyleHot.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.SkinName = 'Office2013DarkGray'
      Properties.LineOptions.Visible = True
      Transparent = True
      Height = 17
      Width = 434
    end
    object cxButton1: TcxButton
      Left = 295
      Top = 74
      Width = 53
      Height = 25
      Caption = 'Set'
      TabOrder = 4
      OnClick = cxButton1Click
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahParentManaged
      AlignVert = avParentManaged
      LayoutLookAndFeel = dxLayoutSkinLookAndFeel1
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = True
      SizeOptions.SizableVert = True
      AllowQuickCustomize = True
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 1
      ShowBorder = False
      Index = -1
    end
    object lcgStyle: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'Style'
      Visible = False
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      Padding.AssignedValues = [lpavTop]
      Index = 2
    end
    object dxLayoutControl1Item1: TdxLayoutItem
      Parent = lcgStyle
      AlignHorz = ahLeft
      CaptionOptions.Text = 'Color:'
      LayoutLookAndFeel = dxLayoutSkinLookAndFeel1
      Control = dxColorEdit1
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 85
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item10: TdxLayoutItem
      Parent = lcgStyle
      CaptionOptions.Text = 'Text color:'
      Control = dxColorEdit2
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 85
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Group5: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      Padding.AssignedValues = [lpavBottom, lpavTop]
      ShowBorder = False
      Index = 3
    end
    object dxLayoutControl1Item13: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton4'
      CaptionOptions.Visible = False
      Control = btnSave
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item12: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnClose
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object dxLayoutControl1Item8: TdxLayoutItem
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxLabel2'
      CaptionOptions.Visible = False
      Control = cxLabel2
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1Item9: TdxLayoutItem
      Parent = dxLayoutControl1Group5
      AlignHorz = ahLeft
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnReset
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutControl1SpaceItem4: TdxLayoutEmptySpaceItem
      Parent = dxLayoutControl1Group5
      AlignHorz = ahClient
      CaptionOptions.Text = 'Empty Space Item'
      SizeOptions.Height = 10
      SizeOptions.Width = 10
      Index = 3
    end
    object dxLayoutControl1Item11: TdxLayoutItem
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      CaptionOptions.Text = 'cxLabel1'
      CaptionOptions.Visible = False
      Control = lblColumn
      ControlOptions.OriginalHeight = 17
      ControlOptions.OriginalWidth = 350
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutControl1Item2: TdxLayoutItem
      Parent = lcgStyle
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = cxButton1
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 53
      ControlOptions.ShowBorder = False
      Index = 2
    end
  end
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    Left = 448
    Top = 144
    object dxLayoutSkinLookAndFeel1: TdxLayoutSkinLookAndFeel
      PixelsPerInch = 96
    end
  end
  object cxImageList1: TcxImageList
    SourceDPI = 96
    FormatVersion = 1
    DesignInfo = 272
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000777777FF7777
          77FF777777FF777777FF777777FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000777777FF0000
          00000000000000000000777777FF000000000000000000000000777777FF0000
          0000000000000000000000000000000000000000000000000000777777FF0000
          0000777777FF00000000777777FF0000000000000000777777FF777777FF7777
          77FF777777FF777777FF00000000000000000000000000000000777777FF0000
          00000000000000000000777777FF00000000777777FF777777FF777777FF7777
          77FF777777FF777777FF00000000000000000000000000000000777777FF0000
          0000777777FF00000000777777FF0000000000000000777777FF777777FF7777
          77FF777777FF777777FF00000000000000000000000000000000777777FF0000
          00000000000000000000777777FF000000000000000000000000777777FF0000
          0000000000000000000000000000000000000000000000000000777777FF0000
          0000777777FF00000000777777FF000000000000000000000000000000000000
          000000000000777777FF777777FF777777FF777777FF777777FF777777FF0000
          00000000000000000000777777FF000000000000000000000000000000000000
          000000000000777777FF000000000000000000000000777777FF777777FF7777
          77FF777777FF777777FF777777FF000000000000000000000000000000000000
          000000000000777777FF00000000777777FF00000000777777FF777777FF7777
          77FF777777FF777777FF777777FF000000000000000000000000000000000000
          000000000000777777FF000000000000000000000000777777FF000000000000
          00000000000000000000000000000000000000000000777777FF000000000000
          000000000000777777FF00000000777777FF00000000777777FF000000000000
          00000000000000000000777777FF777777FF777777FF777777FF777777FF0000
          000000000000777777FF000000000000000000000000777777FF000000000000
          00000000000000000000777777FF777777FF777777FF777777FF777777FF7777
          77FF00000000777777FF00000000777777FF00000000777777FF000000000000
          00000000000000000000777777FF777777FF777777FF777777FF777777FF0000
          000000000000777777FF000000000000000000000000777777FF000000000000
          00000000000000000000000000000000000000000000777777FF000000000000
          000000000000777777FF777777FF777777FF777777FF777777FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000777777FF777777FF777777FF777777FF777777FF}
      end>
  end
end
