unit uNotifyCenter;

interface

uses
  System.RTTI,
  System.SysUtils,
  System.Generics.Collections;

type
  TValue = System.RTTI.TValue;

//  TAppEventNotify = procedure (const Data: TValue) of object;
  TAppEventNotify = reference to procedure (const Data: TValue);

  TNotifyCenter = class sealed
  private
    FSubscribers: TDictionary<String, TList<TAppEventNotify>>;

    constructor Create;

    class constructor CreateNew;
    class destructor DestroyNew;

    class var FInstance: TNotifyCenter;
  public
    procedure SendMessage(const ACommand: String; const AData: TValue);
    procedure Subscribe(const ACommand: String; AEvent: TAppEventNotify);
    procedure UnSubscribe(const ACommand: String; AEvent: TAppEventNotify);

    destructor Destroy; override;

    class function Instance: TNotifyCenter;
  end;

implementation

constructor TNotifyCenter.Create;
begin
  FSubscribers := TDictionary<String, TList<TAppEventNotify>>.Create;
end;

class constructor TNotifyCenter.CreateNew;
begin
  FInstance := nil;
end;

destructor TNotifyCenter.Destroy;
var
  Pair: TPair<String, TList<TAppEventNotify>>;
begin
  for Pair in FSubscribers do
    Pair.Value.Free;

  FSubscribers.Free;
  inherited;
end;

class destructor TNotifyCenter.DestroyNew;
begin
  if FInstance <> nil then
    FInstance.Free;
end;

class function TNotifyCenter.Instance: TNotifyCenter;
begin
  if FInstance = nil then
    FInstance := TNotifyCenter.Create;

  Result := FInstance;
end;

procedure TNotifyCenter.SendMessage(const ACommand: String; const AData: TValue);
var
  Pair: TPair<String, TList<TAppEventNotify>>;
  Item: TAppEventNotify;
begin
  for Pair in FSubscribers do
    if SameText(Pair.Key, ACommand) then
      for Item in Pair.Value do
        Item(AData);
end;

procedure TNotifyCenter.Subscribe(const ACommand: String; AEvent: TAppEventNotify);
begin
  if not FSubscribers.ContainsKey(ACommand) then
    FSubscribers.Add(ACommand, TList<TAppEventNotify>.Create);

  if FSubscribers.Items[ACommand].IndexOf(AEvent) < 0 then
    FSubscribers.Items[ACommand].Add(AEvent);
end;

procedure TNotifyCenter.UnSubscribe(const ACommand: String; AEvent: TAppEventNotify);
var
  Index: Integer;
begin
  if FSubscribers.ContainsKey(ACommand) then begin
    Index := FSubscribers.Items[ACommand].IndexOf(AEvent);
    if Index >= 0 then begin
      FSubscribers.Items[ACommand].Delete(Index);
      if FSubscribers.Items[ACommand].Count = 0 then begin
        FSubscribers.Items[ACommand].Free;
        FSubscribers.Remove(ACommand);
      end;
    end;
  end;
end;

end.
