unit CDV.Consts;

interface

uses
  System.Classes;

const
  NOTIFY_MOVEGRIDLEFT  = 'NOTIFY_MOVEGRIDLEFT';
  NOTIFY_MOVEGRIDRIGHT = 'NOTIFY_MOVEGRIDRIGHT';
  NOTIFY_APPLY_NEW_VISIBILITY = 'NOTIFY_APPLY_NEW_VISIBILITY';
  NOTIFY_SCAN = 'NOTIFY_SCAN';

  DEFAULT_GRIDS: array[0..3] of string = ('Tis01_Module', 'TisWin3_Module', 'SyncClients', 'Report_Versions');
  DEFAULT_STORED_GRIDS = 'Tis01_Module,TisWin3_Module,SyncClients,Report_Versions';

  SHOW_TYPE_ALWAYS = 'Show always';
  SHOW_TYPE_EXISTS = 'Show when exists';
  SHOW_TYPE_NO     = 'Not show';

implementation

end.
