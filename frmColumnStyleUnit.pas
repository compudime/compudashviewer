unit frmColumnStyleUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinscxPCPainter, cxContainer, cxEdit, dxCore, Vcl.Menus, dxLayoutcxEditAdapters, dxLayoutControlAdapters,
  Vcl.ImgList, dxLayoutLookAndFeels, dxLayoutContainer, cxClasses, cxLabel, Vcl.StdCtrls, cxButtons, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, dxColorEdit, dxLayoutControl, System.Generics.Collections, cxGridTableView,
  ColumnStyleHelperUnit, ColumnHelperTypesUnit, cxColorComboBox, cxSpinEdit, cxRadioGroup, dxGalleryControl,
  dxColorGallery, Vcl.ComCtrls, cxDateUtils, cxCalendar, cxGrid, Math, cxCheckBox, cxGridCustomTableView,
  cxGridCustomView, DB, System.ImageList, cxImageList;

type
  TfrmColumnStyle = class(TForm)
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutControl1Group5: TdxLayoutGroup;
    dxColorEdit1: TdxColorEdit;
    dxLayoutControl1Item1: TdxLayoutItem;
    dxLayoutControl1SpaceItem4: TdxLayoutEmptySpaceItem;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutSkinLookAndFeel1: TdxLayoutSkinLookAndFeel;
    dxColorEdit2: TdxColorEdit;
    dxLayoutControl1Item10: TdxLayoutItem;
    lcgStyle: TdxLayoutGroup;
    lblColumn: TcxLabel;
    dxLayoutControl1Item11: TdxLayoutItem;
    btnClose: TcxButton;
    dxLayoutControl1Item12: TdxLayoutItem;
    btnSave: TcxButton;
    dxLayoutControl1Item13: TdxLayoutItem;
    cxLabel2: TcxLabel;
    dxLayoutControl1Item8: TdxLayoutItem;
    btnReset: TcxButton;
    dxLayoutControl1Item9: TdxLayoutItem;
    cxImageList1: TcxImageList;
    cxButton1: TcxButton;
    dxLayoutControl1Item2: TdxLayoutItem;
    procedure OperandRadioButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OperandNoneRadioButtonClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure cxComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CompareModeRadioButtonClick(Sender: TObject);
    procedure editorFocusChanged(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure setCheckBoxColor(index: Integer);
    procedure setConditionColors();
    function getColumnType(columnName: String): TDataTypes;
    procedure setNoneEnable(index: Integer; locking: Boolean);
  public
    { Public declarations }
    cellValue: Variant;
    columnName: String;
    columnType: TDataTypes;
    isUnboundColumn: Boolean;
    gridView: TcxGridTableView;
    dataFields: TStringList;
    allFieldTypes: TDictionary<String, TDataTypes>;
    allFieldNames: TDictionary<String, String>;
    allFieldCapts: TDictionary<String, String>;

    function indexOfGroup(control: TControl): Integer;
    procedure deleteGroupItems(ALayoutGroup: TdxLayoutGroup);
    procedure addGroupItems(gr: TdxLayoutGroup; condition: TConditionStyle);
    function getGroupCondition(ALayoutGroup: TdxLayoutGroup): TConditionStyle;
    procedure initFields(viewInfo: TcxGridTableDataCellViewInfo);
  end;

var
  frmDialog: TfrmColumnStyle;
  conditionColors: TList<TConditionColors>;
  conditionOperands: TList<TOperands>;
  currentIdxCondition: Integer;
  defaultColor: TConditionColors;

implementation

{$R *.dfm}

// Get column type
function TfrmColumnStyle.getColumnType(columnName: String): TDataTypes;
begin
  Result := dtUnknown;
  if allFieldTypes.ContainsKey(columnName) then
    Result := allFieldTypes[columnName]
  else if self.columnName = columnName then
    Result := columnType;
end;

procedure TfrmColumnStyle.initFields(viewInfo: TcxGridTableDataCellViewInfo);
var
  I, idx: Integer;
  ftype: TDataTypes;
  column: TcxGridColumn;
  colCapt, fieldName: String;
begin
  allFieldNames := TDictionary<String, String>.Create;
  allFieldCapts := TDictionary<String, String>.Create;
  dataFields := TStringList.Create;
  allFieldTypes := TDictionary<String, TDataTypes>.Create;
  isUnboundColumn := false;
  for I := 0 to viewInfo.GridRecord.ValueCount - 1 do
  begin
    column := viewInfo.gridView.Items[I] as TcxGridColumn;
    if not column.Visible then
      continue;
    if (viewInfo.Item.index <> I) then
    begin
      ftype := valueType(viewInfo.GridRecord.Values[I], column.DataBinding.valueType);
      if checkType(ftype) then
        allFieldTypes.Add(column.Name, ftype);
    end;
    fieldName := getFieldName(column);
    if not fieldName.IsEmpty then
    begin
      allFieldNames.Add(column.Name, fieldName);
      if column.Caption.IsEmpty then
        allFieldCapts.Add(column.Name, fieldName)
      else
      begin
        colCapt := column.Caption;
        idx := 1;
        while containsValue(colCapt, allFieldCapts) do
        begin
          colCapt := Format('%s_%d', [column.Caption, idx]);
          Inc(idx);
        end;
        allFieldCapts.Add(column.Name, colCapt);
      end;
    end
    else
    begin
      allFieldNames.Add(column.Name, column.Name); // unbound
      if column.Caption.IsEmpty then
        allFieldCapts.Add(column.Name, column.Name)
      else
      begin
        colCapt := column.Caption;
        idx := 1;
        while containsValue(colCapt, allFieldCapts) do
        begin
          colCapt := Format('%s_%d', [column.Caption, idx]);
          Inc(idx);
        end;
        allFieldCapts.Add(column.Name, colCapt);
      end;
      if viewInfo.Item.index = I then
        isUnboundColumn := true;
    end;
    if (viewInfo.Item.index <> I) then
    begin
      ftype := valueType(viewInfo.GridRecord.Values[I], column.DataBinding.valueType);
      if checkType(ftype) and (ftype = columnType) then
        dataFields.Add(allFieldCapts[column.Name]);
    end;
  end;
end;

// delete group
procedure TfrmColumnStyle.deleteGroupItems(ALayoutGroup: TdxLayoutGroup);
var
  I: Integer;
  AItem: TdxCustomLayoutItem;
begin
  for I := ALayoutGroup.Count - 1 downto 0 do
  begin
    AItem := ALayoutGroup.Items[I];
    if (AItem is TdxLayoutItem) and (TdxLayoutItem(AItem).control <> nil) then
      TdxLayoutItem(AItem).control.Free
    else if (AItem is TdxLayoutGroup) then
      deleteGroupItems(TdxLayoutGroup(AItem));
  end;
  while (ALayoutGroup.Count > 0) do
    ALayoutGroup.Items[0].Free;
end;

// set colors
procedure TfrmColumnStyle.setConditionColors();
var
  condClr: TConditionColors;
  I, maxIdx, minIdx: Integer;
begin
  maxIdx := conditionColors.Count - 1;
  minIdx := 0;
  condClr := conditionColors[currentIdxCondition];
  if conditionOperands[currentIdxCondition] = opAnd_Op then
  begin
    for I := currentIdxCondition + 1 to maxIdx do
    begin
      conditionColors[I] := condClr;
      setCheckBoxColor(I);
      if (conditionOperands[I] = opNone) or (conditionOperands[I] = opOr_Op) then
        break;
    end;
  end;
  for I := currentIdxCondition - 1 downto minIdx do
  begin
    if conditionOperands[I] = opOr_Op then
      break;
    conditionColors[I] := condClr;
    setCheckBoxColor(I);
  end;
end;

procedure TfrmColumnStyle.setCheckBoxColor(index: Integer);
var
  gr, gr1, gr2: TdxLayoutGroup;
  checkBox: TcxCheckBox;
begin
  gr := TdxLayoutGroup(dxLayoutControl1.Items.Items[index + itemsStyleShiftLength]);
  gr1 := gr.Items[0] as TdxLayoutGroup;
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  checkBox := TcxCheckBox((gr2.Items[4] as TdxLayoutItem).control);
  checkBox.Style.Color := conditionColors[index].Color;
  checkBox.Style.TextColor := conditionColors[index].TextColor;
  if checkBox.Checked then
    checkBox.Checked := false;
end;

procedure TfrmColumnStyle.cxButton1Click(Sender: TObject);
var
  gr: TdxLayoutGroup;
  idxOfGroup: Integer;
  condClr: TConditionColors;
begin
  idxOfGroup := indexOfGroup(TcxButton(Sender));
  gr := TdxLayoutGroup(dxLayoutControl1.Items.Items[idxOfGroup]);
  condClr.Color := TdxColorEdit((gr.Items[0] as TdxLayoutItem).control).ColorValue;
  condClr.TextColor := TdxColorEdit((gr.Items[1] as TdxLayoutItem).control).ColorValue;
  if currentIdxCondition <> -1 then
  begin
    conditionColors[currentIdxCondition] := condClr;
    setCheckBoxColor(currentIdxCondition);
    setConditionColors;
  end;
  dxLayoutControl1.BeginUpdate;
  lcgStyle.Visible := false;
  dxLayoutControl1.EndUpdate;
end;

// show color editors
procedure TfrmColumnStyle.cxCheckBox1Click(Sender: TObject);
var
  control: TcxCheckBox;
begin
  control := TcxCheckBox(Sender);
  editorFocusChanged(Sender);
  dxLayoutControl1.BeginUpdate;
  lcgStyle.Visible := control.Checked;
  Height := Height - IfThen(lcgStyle.Visible, -70, 70);
  dxLayoutControl1.EndUpdate;
end;

// change other column value
procedure TfrmColumnStyle.cxComboBox1PropertiesChange(Sender: TObject);
var
  idxOfGroup: Integer;
  gr1, gr2, gr3: TdxLayoutGroup;
  conditionType: TDataTypes;
begin
  idxOfGroup := indexOfGroup(TcxComboBox(Sender));
  gr1 := TdxLayoutGroup(dxLayoutControl1.Items.Items[idxOfGroup]);
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  gr3 := gr2.Items[1] as TdxLayoutGroup;

  conditionType := getColumnType(getKeyByValue(TcxComboBox((gr3.Items[1] as TdxLayoutItem).control).Text,
    allFieldCapts));
  dxLayoutControl1.BeginUpdate;
  TcxComboBox((gr3.Items[2] as TdxLayoutItem).control).Properties.Items := ColumnStyleHelper.listConditions;
  if conditionType = dtString then
    TcxComboBox((gr3.Items[2] as TdxLayoutItem).control).Properties.Items.Add(containsCondition);
  (gr3.Items[4] as TdxLayoutItem).Visible := conditionType = dtDate; // value
  if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) then
  begin
    (gr3.Items[5] as TdxLayoutItem).Visible := true;
    if (conditionType = dtInt) then
      TcxSpinEdit((gr3.Items[5] as TdxLayoutItem).control).Properties.valueType := vtInt
    else
      TcxSpinEdit((gr3.Items[5] as TdxLayoutItem).control).Properties.valueType := vtFloat;
  end
  else
    (gr3.Items[5] as TdxLayoutItem).Visible := false;
  (gr3.Items[6] as TdxLayoutItem).Visible := conditionType = dtString;
  dxLayoutControl1.EndUpdate;

  editorFocusChanged(Sender);
end;

// change focus of condition
procedure TfrmColumnStyle.editorFocusChanged(Sender: TObject);
var
  control: TControl;
  condGroupIndex: Integer;
  gr, gr1, gr2: TdxLayoutGroup;
  checkBox: TcxCheckBox;
begin
  control := TControl(Sender);
  if control <> nil then
  begin
    condGroupIndex := indexOfGroup(control);
    if (condGroupIndex <> -1) and ((condGroupIndex - itemsStyleShiftLength) <> currentIdxCondition) then
    begin
      gr := TdxLayoutGroup(dxLayoutControl1.Items.Items[currentIdxCondition + itemsStyleShiftLength]);
      gr1 := gr.Items[0] as TdxLayoutGroup;
      gr2 := gr1.Items[0] as TdxLayoutGroup;
      checkBox := TcxCheckBox((gr2.Items[4] as TdxLayoutItem).control);
      if checkBox.Checked then
        checkBox.Checked := false;
      currentIdxCondition := condGroupIndex - itemsStyleShiftLength;
      dxColorEdit1.ColorValue := conditionColors[currentIdxCondition].Color;
      dxColorEdit2.ColorValue := conditionColors[currentIdxCondition].TextColor;
    end;
  end;
end;

function TfrmColumnStyle.indexOfGroup(control: TControl): Integer;
var
  I: Integer;
  Item: TdxLayoutItem;
  group: TdxCustomLayoutGroup;
begin
  Result := -1;
  if control <> nil then
  begin
    Item := dxLayoutControl1.FindItem(control);
    if Item <> nil then
    begin
      group := Item.Parent;
      while group.Parent <> dxLayoutControl1.Items do
        group := group.Parent;
      for I := 0 to dxLayoutControl1.Items.Count - 1 do
      begin
        if dxLayoutControl1.Items.Items[I] = group then
        begin
          Result := I;
          break;
        end;
      end;
    end;
  end;
end;

// set enable None
procedure TfrmColumnStyle.setNoneEnable(index: Integer; locking: Boolean);
var
  gr1, gr2: TdxLayoutGroup;
begin
  gr1 := dxLayoutControl1.Items.Items[index] as TdxLayoutGroup;
  gr2 := gr1.Items[1] as TdxLayoutGroup;
  (gr2.Items[0] as TdxLayoutItem).Enabled := not locking;
end;

// getting condition values
function TfrmColumnStyle.getGroupCondition(ALayoutGroup: TdxLayoutGroup): TConditionStyle;
var
  condition: TConditionStyle;
  gr1, gr2, gr3, gr4: TdxLayoutGroup;
  conditionType: TDataTypes;
begin
  gr1 := ALayoutGroup.Items[0] as TdxLayoutGroup;

  gr2 := gr1.Items[0] as TdxLayoutGroup;
  if TcxRadioButton((gr2.Items[1] as TdxLayoutItem).control).Checked then
    condition.mode := cmColumns
  else if TcxRadioButton((gr2.Items[2] as TdxLayoutItem).control).Checked then
    condition.mode := cmOtherColumn
  else if TcxRadioButton((gr2.Items[3] as TdxLayoutItem).control).Checked then
    condition.mode := cmUpdateTime
  else
    condition.mode := cmValue;

  gr3 := gr1.Items[1] as TdxLayoutGroup;
  conditionType := columnType;
  if condition.mode = cmOtherColumn then
  begin
    condition.columnCaption := TcxComboBox((gr3.Items[1] as TdxLayoutItem).control).Text;
    conditionType := getColumnType(getKeyByValue(condition.columnCaption, allFieldCapts));
  end;
  condition.condition := TcxComboBox((gr3.Items[2] as TdxLayoutItem).control).Text;
  if condition.mode = cmColumns then
    condition.columnCaption := TcxComboBox((gr3.Items[3] as TdxLayoutItem).control).Text;

  if condition.mode = cmUpdateTime then
    condition.value := TcxSpinEdit((gr3.Items[5] as TdxLayoutItem).control).value
  else if conditionType = dtDate then
    condition.value := TcxDateEdit((gr3.Items[4] as TdxLayoutItem).control).Date
  else if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) then
    condition.value := TcxSpinEdit((gr3.Items[5] as TdxLayoutItem).control).value
  else if conditionType = dtString then
    condition.value := TcxTextEdit((gr3.Items[6] as TdxLayoutItem).control).Text;

  gr4 := ALayoutGroup.Items[1] as TdxLayoutGroup;
  if TcxRadioButton((gr4.Items[0] as TdxLayoutItem).control).Checked then
    condition.operand := opNone
  else if TcxRadioButton((gr4.Items[1] as TdxLayoutItem).control).Checked then
    condition.operand := opAnd_Op
  else
    condition.operand := opOr_Op;
  Result := condition;
end;

// add controls according to condition
procedure TfrmColumnStyle.addGroupItems(gr: TdxLayoutGroup; condition: TConditionStyle);
var
  gr1, gr2, gr3, gr4: TdxLayoutGroup;
  it: TdxLayoutItem;
  ctr11: TcxRadioButton;
  ctr12: TcxRadioButton;
  ctr13: TcxRadioButton;
  ctr14: TcxRadioButton;
  ctr15: TcxCheckBox;
  ctr1: TcxLabel;
  ctr2: TcxComboBox;
  ctr3: TcxComboBox;
  ctr4: TcxComboBox;
  ctr51: TcxSpinEdit;
  ctr52: TcxDateEdit;
  ctr53: TcxTextEdit;
  ctr6: TcxRadioButton;
  ctr7: TcxRadioButton;
  ctr8: TcxRadioButton;
  conditionType: TDataTypes;
  columnCaption: String;
  ACheckedRadio: TcxRadioButton;
begin
  ACheckedRadio := nil;
  conditionType := columnType;
  columnCaption := allFieldCapts[columnName];
  if condition.mode = cmOtherColumn then
    conditionType := getColumnType(getKeyByValue(condition.columnCaption, allFieldCapts));

  gr1 := gr.CreateGroup;
  gr1.ShowBorder := false;
  gr1.LayoutDirection := ldVertical;

  gr2 := gr1.CreateGroup;
  gr2.ShowBorder := false;
  gr2.LayoutDirection := ldHorizontal;

  ctr11 := TcxRadioButton.Create(dxLayoutControl1);
  ctr11.Caption := 'by value';
  ctr11.GroupIndex := gr.index + 1;
  ctr11.Checked := condition.mode = cmValue;
  ctr11.Width := 79;
  ctr11.OnClick := CompareModeRadioButtonClick;
  ctr11.Transparent := true;
  gr2.CreateItemForControl(ctr11);
  if ctr11.Checked then
    ACheckedRadio := ctr11;

  ctr12 := TcxRadioButton.Create(dxLayoutControl1);
  ctr12.Caption := 'two columns';
  ctr12.GroupIndex := gr.index + 1;
  ctr12.Checked := condition.mode = cmColumns;
  ctr12.Width := 97;
  ctr12.OnClick := CompareModeRadioButtonClick;
  ctr12.Transparent := true;
  gr2.CreateItemForControl(ctr12).Enabled := dataFields.Count > 0;
  if ctr12.Checked then
    ACheckedRadio := ctr12;

  ctr13 := TcxRadioButton.Create(dxLayoutControl1);
  ctr13.Caption := 'other column';
  ctr13.GroupIndex := gr.index + 1;
  ctr13.Checked := condition.mode = cmOtherColumn;
  ctr13.Width := 99;
  ctr13.OnClick := CompareModeRadioButtonClick;
  ctr13.Transparent := true;
  gr2.CreateItemForControl(ctr13).Enabled := allFieldTypes.Count > 0;
  if ctr13.Checked then
    ACheckedRadio := ctr13;

  ctr14 := TcxRadioButton.Create(dxLayoutControl1);
  ctr14.Caption := 'update';
  ctr14.GroupIndex := gr.index + 1;
  ctr14.Checked := condition.mode = cmUpdateTime;
  ctr14.Width := 75;
  ctr14.OnClick := CompareModeRadioButtonClick;
  ctr14.Transparent := true;
  gr2.CreateItemForControl(ctr14).Enabled := isUnboundColumn;
  if ctr14.Checked then
    ACheckedRadio := ctr14;

  ctr15 := TcxCheckBox.Create(dxLayoutControl1);
  ctr15.Caption := 'Style';
  ctr15.ParentBackground := false;
  ctr15.ParentColor := false;
  ctr15.Style.Color := conditionColors[currentIdxCondition].Color;
  ctr15.Style.TextColor := conditionColors[currentIdxCondition].TextColor;
  ctr15.Width := 50;
  ctr15.OnClick := cxCheckBox1Click;
  gr2.CreateItemForControl(ctr15).AlignHorz := ahRight;

  gr3 := gr1.CreateGroup;
  gr3.ShowBorder := false;
  gr3.LayoutDirection := ldHorizontal;

  ctr1 := TcxLabel.Create(dxLayoutControl1); // first item
  ctr1.Caption := 'Time From Update (sec)';
  it := gr3.CreateItemForControl(ctr1);
  it.Visible := condition.mode = cmUpdateTime;
  it.AlignHorz := ahClient;

  ctr2 := TcxComboBox.Create(dxLayoutControl1);
  ctr2.Properties.DropDownListStyle := lsEditFixedList;
  ctr2.Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
  if ctr2.Properties.Items.IndexOf(columnCaption) >= 0 then
    ctr2.Properties.Items.Delete(ctr2.Properties.Items.IndexOf(columnCaption));
  if (condition.columnCaption = '') and (allFieldTypes.Count > 0) then
    ctr2.Text := allFieldCapts.Values.ToArray()[0]
  else
    ctr2.Text := condition.columnCaption;
  ctr2.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr2.Properties.OnChange := cxComboBox1PropertiesChange;
  ctr2.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr2);
  it.Visible := condition.mode = cmOtherColumn;
  it.AlignHorz := ahClient;

  ctr3 := TcxComboBox.Create(dxLayoutControl1); // cond item
  ctr3.Properties.DropDownListStyle := lsEditFixedList;
  ctr3.Properties.Items := ColumnStyleHelper.listConditions;
  if conditionType = dtString then
    ctr3.Properties.Items.Add(containsCondition);
  ctr3.Text := condition.condition;
  ctr3.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr3.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr3);
  it.AlignHorz := ahClient;

  ctr4 := TcxComboBox.Create(dxLayoutControl1); // second item
  ctr4.Properties.DropDownListStyle := lsEditFixedList;
  ctr4.Properties.Items := dataFields;
  if (condition.columnCaption = '') and (dataFields.Count > 0) then
    ctr4.Text := dataFields[0]
  else
    ctr4.Text := condition.columnCaption;
  ctr4.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr4.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr4);
  it.Visible := condition.mode = cmColumns;
  it.AlignHorz := ahClient;

  ctr52 := TcxDateEdit.Create(dxLayoutControl1); // value items
  if conditionType = dtDate then
    ctr52.Date := condition.value
  else
    ctr52.Date := Date;
  ctr52.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr52.OnFocusChanged := editorFocusChanged;
  if condition.mode = cmColumns then
    ctr52.Properties.DisplayFormat := dateFormat
  else
    ctr52.Properties.DisplayFormat := '';
  it := gr3.CreateItemForControl(ctr52);
  it.Visible := (conditionType = dtDate) and (condition.mode <> cmUpdateTime);
  it.AlignHorz := ahClient;

  ctr51 := TcxSpinEdit.Create(dxLayoutControl1);
  if ((conditionType = dtInt) or (condition.mode = cmUpdateTime)) then
    ctr51.Properties.valueType := vtInt
  else if (conditionType in [dtFloat, dtCurrency]) then
    ctr51.Properties.valueType := vtFloat;
  if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) or (condition.mode = cmUpdateTime) then
    ctr51.value := condition.value;
  ctr51.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr51.OnFocusChanged := editorFocusChanged;
  if condition.mode = cmColumns then
    ctr51.Properties.DisplayFormat := numFormat
  else
    ctr51.Properties.DisplayFormat := '';
  it := gr3.CreateItemForControl(ctr51);
  it.Visible := ((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) or
    (condition.mode = cmUpdateTime));
  it.AlignHorz := ahClient;

  ctr53 := TcxTextEdit.Create(dxLayoutControl1);
  if conditionType = dtString then
    ctr53.Text := condition.value;
  ctr53.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr53.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr53);
  it.Visible := (conditionType = dtString) and (condition.mode <> cmUpdateTime);
  it.AlignHorz := ahClient;

  gr4 := gr.CreateGroup;
  gr4.ShowBorder := false;
  gr4.LayoutDirection := ldHorizontal;
  gr4.AlignHorz := ahRight;

  ctr6 := TcxRadioButton.Create(dxLayoutControl1);
  ctr6.Caption := 'None';
  ctr6.GroupIndex := gr.index + 3;
  ctr6.Checked := condition.operand = opNone;
  ctr6.Width := 50;
  ctr6.OnClick := OperandNoneRadioButtonClick;
  ctr6.Transparent := true;
  gr4.CreateItemForControl(ctr6);

  ctr7 := TcxRadioButton.Create(dxLayoutControl1);
  ctr7.Caption := 'AND';
  ctr7.GroupIndex := gr.index + 3;
  ctr7.Checked := condition.operand = opAnd_Op;
  ctr7.Width := 50;
  ctr7.OnClick := OperandRadioButtonClick;
  ctr7.Transparent := true;
  gr4.CreateItemForControl(ctr7);

  ctr8 := TcxRadioButton.Create(dxLayoutControl1);
  ctr8.Caption := 'OR';
  ctr8.GroupIndex := gr.index + 3;
  ctr8.Checked := condition.operand = opOr_Op;
  ctr8.Width := 50;
  ctr8.OnClick := OperandRadioButtonClick;
  ctr8.Transparent := true;
  gr4.CreateItemForControl(ctr8);

  if Assigned(ACheckedRadio) then
    CompareModeRadioButtonClick(ACheckedRadio);
end;

// reset styles
procedure TfrmColumnStyle.btnResetClick(Sender: TObject);
begin
  ColumnStyleHelper.clearCondition(gridView, allFieldNames[columnName]);

  close;
end;

// close form
procedure TfrmColumnStyle.btnCloseClick(Sender: TObject);
begin
  close;
end;

// save condition for column
procedure TfrmColumnStyle.btnSaveClick(Sender: TObject);
var
  I: Integer;
  gr: TdxCustomLayoutItem;
  condition: TConditionStyle;
  styleList: TStringList;
  conditionType: TDataTypes;
begin
  // save
  ColumnStyleHelper.clearCondition(gridView, allFieldNames[columnName]);
  styleList := TStringList.Create;
  for I := 2 to dxLayoutControl1.Items.Count - 3 do
  begin
    gr := dxLayoutControl1.Items.Items[I];
    condition := getGroupCondition(gr as TdxLayoutGroup);
    styleList.Add('mode' + IntToStr(I - 1) + '=' + IntToStr(Ord(condition.mode)));
    if not condition.columnCaption.IsEmpty then
      styleList.Add('column' + IntToStr(I - 1) + '=' + allFieldNames[getKeyByValue(condition.columnCaption,
        allFieldCapts)])
    else
      styleList.Add('column' + IntToStr(I - 1) + '=');
    styleList.Add('condition' + IntToStr(I - 1) + '=' + condition.condition);
    conditionType := columnType;
    if condition.mode = cmOtherColumn then
      conditionType := getColumnType(getKeyByValue(condition.columnCaption, allFieldCapts));
    if condition.mode = cmUpdateTime then
      styleList.Add('value' + IntToStr(I - 1) + '=' + IntToStr(condition.value))
    else if conditionType = dtDate then
      styleList.Add('value' + IntToStr(I - 1) + '=' + DateTimeToStr(condition.value))
    else if conditionType = dtInt then
      styleList.Add('value' + IntToStr(I - 1) + '=' + IntToStr(condition.value))
    else if conditionType in [dtFloat, dtCurrency] then
      styleList.Add('value' + IntToStr(I - 1) + '=' + FloatToStr(condition.value))
    else
      styleList.Add('value' + IntToStr(I - 1) + '=' + condition.value);
    styleList.Add('op' + IntToStr(I - 1) + '=' + IntToStr(Ord(condition.operand)));
    condition.Color := conditionColors[I - itemsStyleShiftLength];
    styleList.Add('color' + IntToStr(I - 1) + '=' + IntToStr(condition.Color.Color));
    styleList.Add('textcolor' + IntToStr(I - 1) + '=' + IntToStr(condition.Color.TextColor));

  end;

  ColumnStyleHelper.updateStyle(gridView, allFieldNames[columnName], styleList);
  styleList.Free;

  close;
end;

procedure TfrmColumnStyle.cxComboBox1PropertiesEditValueChanged(Sender: TObject);
begin
  btnSave.Enabled := true;
end;

// change compare mode
procedure TfrmColumnStyle.CompareModeRadioButtonClick(Sender: TObject);
var
  idxOfItem, idxOfGroup, idxOfValue: Integer;
  gr1, gr2, gr3: TdxLayoutGroup;
  Item: TdxLayoutItem;
  conditionType: TDataTypes;
begin
  Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
  idxOfItem := Item.index;
  idxOfGroup := indexOfGroup(TcxRadioButton(Sender));
  gr1 := TdxLayoutGroup(dxLayoutControl1.Items.Items[idxOfGroup]);
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  gr3 := gr2.Items[1] as TdxLayoutGroup;

  conditionType := columnType;
  if idxOfItem = 2 then
    conditionType := getColumnType(getKeyByValue(TcxComboBox((gr3.Items[1] as TdxLayoutItem).control).Text,
      allFieldCapts));
  idxOfValue := IfThen(conditionType = dtDate, 4,
    IfThen((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]), 5, 6));
  dxLayoutControl1.BeginUpdate;
  (gr3.Items[0] as TdxLayoutItem).Visible := idxOfItem = 3; // update
  (gr3.Items[1] as TdxLayoutItem).Visible := idxOfItem = 2; // other column
  TcxComboBox((gr3.Items[2] as TdxLayoutItem).control).Properties.Items := ColumnStyleHelper.listConditions;
  if (conditionType = dtString) and (idxOfItem <> 3) then
    TcxComboBox((gr3.Items[2] as TdxLayoutItem).control).Properties.Items.Add(containsCondition);
  (gr3.Items[3] as TdxLayoutItem).Visible := idxOfItem = 1; // second column

  (gr3.Items[4] as TdxLayoutItem).Visible := (conditionType = dtDate) and (idxOfItem <> 3); // value
  if ((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency])) and (idxOfItem <> 3) then
  begin
    (gr3.Items[5] as TdxLayoutItem).Visible := true;
    if (conditionType = dtInt) then
      TcxSpinEdit((gr3.Items[5] as TdxLayoutItem).control).Properties.valueType := vtInt
    else
      TcxSpinEdit((gr3.Items[5] as TdxLayoutItem).control).Properties.valueType := vtFloat;
  end
  else
    (gr3.Items[5] as TdxLayoutItem).Visible := false;
  (gr3.Items[5] as TdxLayoutItem).Visible := idxOfItem = 3;
  (gr3.Items[6] as TdxLayoutItem).Visible := (conditionType = dtString) and (idxOfItem <> 3);

  if (conditionType = dtDate) and (idxOfItem = 1) then
    TcxDateEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := dateFormat
  else if ((gr3.Items[idxOfValue] as TdxLayoutItem).control is TcxDateEdit) then
    TcxDateEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := '';

  if ((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency])) and (idxOfItem = 1) then
    TcxSpinEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := numFormat
  else
    TcxSpinEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := '';
  dxLayoutControl1.EndUpdate;

  editorFocusChanged(Sender);
  btnSave.Enabled := true;
end;

// delete condition (None)
procedure TfrmColumnStyle.OperandNoneRadioButtonClick(Sender: TObject);
var
  idx, h: Integer;
  gr: TdxLayoutGroup;
begin
  idx := indexOfGroup(TcxRadioButton(Sender));
  if idx = (dxLayoutControl1.Items.Count - 4) then
  begin
    dxLayoutControl1.BeginUpdate;
    if dxLayoutControl1.Items.Count >= 7 then
      setNoneEnable(idx - 1, false);
    gr := TdxLayoutGroup(dxLayoutControl1.Items.Items[idx + 1]);
    h := gr.viewInfo.ClientBounds.Height;
    deleteGroupItems(gr);
    gr.Free;
    dxLayoutControl1.EndUpdate;
    conditionOperands.Remove(conditionOperands[idx + 1 - itemsStyleShiftLength]);
    conditionColors.Remove(conditionColors[idx + 1 - itemsStyleShiftLength]);
    Height := Height - h - 8;
    btnSave.Enabled := true;
  end;
end;

// add new condition (click OR,AND)
procedure TfrmColumnStyle.OperandRadioButtonClick(Sender: TObject);
var
  gr: TdxLayoutGroup;
  Item: TdxLayoutItem;
  idx: Integer;
  condition: TConditionStyle;
begin
  idx := indexOfGroup(TcxRadioButton(Sender));
  if dxLayoutControl1.Items.Count = (idx + 3) then
  begin
    dxLayoutControl1.BeginUpdate;
    gr := dxLayoutControl1.Items.CreateGroup;
    gr.ShowBorder := false;
    gr.index := idx + 1;
    condition.mode := cmValue;
    condition.columnCaption := '';
    condition.condition := '='; // equals
    condition.value := cellValue;
    condition.operand := opNone;
    Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
    if Item.index = 1 then
      conditionOperands[idx - itemsStyleShiftLength] := opAnd_Op
    else
      conditionOperands[idx - itemsStyleShiftLength] := opOr_Op;
    conditionOperands.Add(condition.operand);
    if condition.operand = opOr_Op then
      conditionColors.Add(defaultColor)
    else
      conditionColors.Add(conditionColors[idx - itemsStyleShiftLength]);
    currentIdxCondition := gr.index - itemsStyleShiftLength;
    addGroupItems(gr, condition);
    if dxLayoutControl1.Items.Count >= 7 then
      setNoneEnable(idx - 1, true);
    dxLayoutControl1.EndUpdate;

    dxColorEdit1.ColorValue := conditionColors[currentIdxCondition].Color;
    dxColorEdit2.ColorValue := conditionColors[currentIdxCondition].TextColor;

    Height := Height + gr.viewInfo.ClientBounds.Height + 8;
  end
  else
  begin
    currentIdxCondition := idx - itemsStyleShiftLength;
    Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
    if Item.index = 1 then
      conditionOperands[currentIdxCondition] := opAnd_Op
    else
      conditionOperands[currentIdxCondition] := opOr_Op;
    setConditionColors;
  end;
  btnSave.Enabled := true;
end;

procedure TfrmColumnStyle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ColumnStyleHelper.removeDialog(gridView);
  FreeAndNil(dataFields);
  FreeAndNil(allFieldTypes);
  FreeAndNil(allFieldNames);
  FreeAndNil(allFieldCapts);
  FreeAndNil(conditionColors);
  FreeAndNil(conditionOperands);
end;

procedure TfrmColumnStyle.FormCreate(Sender: TObject);
begin
  conditionColors := TList<TConditionColors>.Create;
  conditionOperands := TList<TOperands>.Create;
  currentIdxCondition := -1;
end;

procedure TfrmColumnStyle.FormShow(Sender: TObject);
var
  I, idx: Integer;
  gr: TdxLayoutGroup;
  condition: TConditionStyle;
  Items: TList<TConditionStyle>;
  firstRun: Boolean;
begin
  defaultColor.Color := defaultStyleColors[IfThen(columnType = dtDate, 2, 0)];
  defaultColor.TextColor := defaultStyleColors[IfThen(columnType = dtDate, 3, 1)];
  Items := ColumnStyleHelper.getConditionsByColumn(gridView, allFieldNames[columnName]);
  idx := itemsStyleShiftLength;
  firstRun := not ColumnStyleHelper.isStyledColumn(gridView, allFieldNames[columnName]);
  if not firstRun then
  begin
    for I := 0 to Items.Count - 1 do
    begin
      condition := Items[I];
      gr := dxLayoutControl1.Items.CreateGroup;
      gr.ShowBorder := false;
      gr.index := idx + I;
      conditionOperands.Add(condition.operand);
      conditionColors.Add(condition.Color);
      currentIdxCondition := gr.index - itemsStyleShiftLength;
      addGroupItems(gr, condition);
      if dxLayoutControl1.Items.Count >= 7 then
        setNoneEnable(gr.index - 2, true);
      Height := Height + gr.viewInfo.ClientBounds.Height + 8;
    end;
  end
  else
  begin
    condition.mode := cmValue;
    condition.columnCaption := '';
    condition.condition := '=';
    condition.operand := opNone;
    condition.value := cellValue;
    condition.Color := defaultColor;

    gr := dxLayoutControl1.Items.CreateGroup;
    gr.ShowBorder := false;
    gr.index := idx;
    conditionOperands.Add(condition.operand);
    conditionColors.Add(condition.Color);
    currentIdxCondition := gr.index - itemsStyleShiftLength;
    addGroupItems(gr, condition);
    Height := Height + gr.viewInfo.ClientBounds.Height + 8;
  end;

  dxColorEdit1.ColorValue := conditionColors[currentIdxCondition].Color;
  dxColorEdit2.ColorValue := conditionColors[currentIdxCondition].TextColor;

  btnReset.Enabled := not firstRun;
  btnSave.Enabled := firstRun;
end;

end.
