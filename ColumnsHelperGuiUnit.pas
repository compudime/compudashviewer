unit ColumnsHelperGuiUnit;

interface

uses System.UITypes, System.Classes, System.SysUtils, Vcl.Forms, Vcl.Controls, System.Generics.Collections,
  ColumnHelperTypesUnit, dxBar, cxGridCustomTableView, cxGraphics, cxGridTableView;

type
  TColumnsGuiHelper = class
  private
    gridDrawCellEvents: TDictionary<String, TcxGridTableDataCellCustomDrawEvent>;
    gridCellClickEvents: TDictionary<String, TcxGridCellClickEvent>;
    dxBarManager: TdxBarManager;
    dxBarPopupMenu: TdxBarPopupMenu;
    menuCellViewInfo: TcxGridTableDataCellViewInfo;
    isDesignMode: Boolean;

    procedure addMenuItem(itemCaption: string; itemEvent: TNotifyEvent);
    procedure brbSetColumnColorClick(Sender: TObject);
    procedure brbAddUnboundColumnClick(Sender: TObject);
    procedure brbEditUnboundColumnClick(Sender: TObject);

    procedure TableViewCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure TableViewCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
      AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);

    procedure UnboundUpdateComplete(view: TcxGridTableView);
  public
    OnCompleted: TWorkCompetedEvent;

    property DesignMode: Boolean read isDesignMode write isDesignMode;

    procedure addTableView(gridView: TcxGridTableView; keyField: String);
    procedure prepareExport(gridView: TcxGridTableView);
  end;

function ColumnsGuiHelper: TColumnsGuiHelper;

implementation

uses ColumnUnboundHelperUnit, ColumnStyleHelperUnit, ColumnHelperDataUnit{$IFDEF TISWIN3}, Modules{$ENDIF};

var
  _columnsGuiHelper: TColumnsGuiHelper;

function ColumnsGuiHelper: TColumnsGuiHelper;
begin
  if not Assigned(_columnsGuiHelper) then
    _columnsGuiHelper := TColumnsGuiHelper.Create;
  Result := _columnsGuiHelper;
end;

function CompareMethods(aMethod1, aMethod2: TMethod; ACheckData: Boolean): Boolean;
begin
  Result := aMethod1.Code = aMethod2.Code;
  if Result and ACheckData then
    Result := aMethod1.Data = aMethod2.Data;
end;

procedure TColumnsGuiHelper.addTableView(gridView: TcxGridTableView; keyField: String);
var
  gridKey: String;
  ClickEvent: TcxGridCellClickEvent;
  DrawEvent: TcxGridTableDataCellCustomDrawEvent;
  I: Integer;
begin
  gridKey := gridView.Owner.Name + gridView.Name;

  if not Assigned(Self.dxBarPopupMenu) then
  begin
    {$IFDEF TISWIN3}
    Self.dxBarManager := ModuleInfoManager.MainBarManager;
    {$ELSE}
    for I := 0 to Application.MainForm.ComponentCount - 1 do
      if Application.MainForm.Components[I] is TdxBarManager then
      begin
        Self.dxBarManager := Application.MainForm.Components[I] as TdxBarManager;
        break;
      end;
    if not Assigned(Self.dxBarManager) then
      Self.dxBarManager := TdxBarManager.Create(Application.MainForm);
    {$ENDIF}
    Self.dxBarPopupMenu := TdxBarPopupMenu.Create(Application.MainForm);
    Self.dxBarPopupMenu.BarManager := dxBarManager;
    Self.addMenuItem('Set Column Color', brbSetColumnColorClick);
    Self.addMenuItem('Add Unbound Column', brbAddUnboundColumnClick);
    Self.addMenuItem('Edit Unbound Column', brbEditUnboundColumnClick);
  end;

  ColumnUnboundHelper.addTableView(gridView, keyField);
  ColumnStyleHelper.addTableView(gridView);

  if Assigned(gridView.OnCellClick) then
  begin
    ClickEvent := TableViewCellClick;
    if not CompareMethods(TMethod(gridView.OnCellClick), TMethod(ClickEvent), False) then
      Self.gridCellClickEvents.AddOrSetValue(gridKey, gridView.OnCellClick);
  end;
  gridView.OnCellClick := TableViewCellClick;

  if Assigned(gridView.OnCustomDrawCell) then
  begin
    DrawEvent := TableViewCustomDrawCell;
    if not CompareMethods(TMethod(gridView.OnCustomDrawCell), TMethod(DrawEvent), False) then
      Self.gridDrawCellEvents.AddOrSetValue(gridKey, gridView.OnCustomDrawCell);
  end;
  gridView.OnCustomDrawCell := TableViewCustomDrawCell;
end;

procedure TColumnsGuiHelper.TableViewCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  column: TcxGridColumn;
  gridKey, fieldName: String;
  editItem: TdxBarItem;
begin
  if ACellViewInfo is TcxGridTableViewInplaceEditFormDataCellViewInfo then
    Exit;
  gridKey := Sender.Owner.Name + Sender.Name;
  if gridCellClickEvents.ContainsKey(gridKey) and Assigned(gridCellClickEvents[gridKey]) then
    gridCellClickEvents[gridKey](Sender, ACellViewInfo, AButton, AShift, AHandled);

  if AButton = mbRight then
  begin
    if AShift = [ssCtrl] then
    begin
      if isDesignMode then
      begin
        column := ACellViewInfo.Item as TcxGridColumn;
        fieldName := getFieldName(column);
        editItem := dxBarPopupMenu.ItemLinks[dxBarPopupMenu.ItemLinks.Count - 1].Item;
        if fieldName.IsEmpty then
          editItem.Visible := ivAlways
        else
          editItem.Visible := ivNever;

        menuCellViewInfo := ACellViewInfo;
        dxBarPopupMenu.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
      end;
    end;
  end
  else
    ColumnStyleHelper.closeDialog(ACellViewInfo);
end;

procedure TColumnsGuiHelper.TableViewCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  gridKey: String;
begin
  if AViewInfo is TcxGridTableViewInplaceEditFormDataCellViewInfo then
    Exit;
  if not(Sender is TcxGridTableView) then
    Exit;
  if not(ColumnUnboundHelper.HasUnboundColumns(Sender as TcxGridTableView) or
    ColumnStyleHelper.HasStyleColumns(Sender as TcxGridTableView)) then
    Exit;

  gridKey := Sender.Owner.Name + Sender.Name;
  if gridDrawCellEvents.ContainsKey(gridKey) and Assigned(gridDrawCellEvents[gridKey]) then
    gridDrawCellEvents[gridKey](Sender, ACanvas, AViewInfo, ADone);

  if not ColumnUnboundHelper.TryFindProcessingUnbound(ACanvas, AViewInfo) then
    ColumnStyleHelper.TrySetColumnStyles(ACanvas, AViewInfo)
end;

procedure TColumnsGuiHelper.brbSetColumnColorClick(Sender: TObject);
begin
  if Assigned(menuCellViewInfo) then
    ColumnStyleHelper.openDialog(menuCellViewInfo)
end;

procedure TColumnsGuiHelper.brbAddUnboundColumnClick(Sender: TObject);
begin
  if Assigned(menuCellViewInfo) then
    ColumnUnboundHelper.openDialog(menuCellViewInfo, True)
end;

procedure TColumnsGuiHelper.brbEditUnboundColumnClick(Sender: TObject);
begin
  if Assigned(menuCellViewInfo) then
    ColumnUnboundHelper.openDialog(menuCellViewInfo, False)
end;

procedure TColumnsGuiHelper.addMenuItem(itemCaption: string; itemEvent: TNotifyEvent);
var
  link: TdxBarItemLink;
begin
  link := dxBarPopupMenu.ItemLinks.AddButton;
  link.Item.Caption := itemCaption;
  link.Item.OnClick := itemEvent;
end;

procedure TColumnsGuiHelper.prepareExport(gridView: TcxGridTableView);
begin
  ColumnUnboundHelper.TryProcessUnboundAll(gridView);
  ColumnUnboundHelper.OnCompleted := UnboundUpdateComplete;
end;

procedure TColumnsGuiHelper.UnboundUpdateComplete(view: TcxGridTableView);
begin
  view.LayoutChanged;
  if Assigned(OnCompleted) then
    OnCompleted;
end;

initialization

_columnsGuiHelper := TColumnsGuiHelper.Create;
_columnsGuiHelper.gridDrawCellEvents := TDictionary<String, TcxGridTableDataCellCustomDrawEvent>.Create;
_columnsGuiHelper.gridCellClickEvents := TDictionary<String, TcxGridCellClickEvent>.Create;
_columnsGuiHelper.isDesignMode := True;

finalization

if Assigned(_columnsGuiHelper) then
begin
  FreeAndNil(_columnsGuiHelper.gridDrawCellEvents);
  FreeAndNil(_columnsGuiHelper.gridCellClickEvents);
  FreeAndNil(_columnsGuiHelper);
end;

end.
