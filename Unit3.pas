unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer,
  cxEdit, dxSkinsCore, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
  dxUIAClasses, dxSkinWXI;

type
  TfrmEditColumnValue = class(TForm)
    cxLabel1: TcxLabel;
    cmbColumnName: TcxComboBox;
    cxLabel2: TcxLabel;
    txtValue: TcxTextEdit;
    btnApply: TcxButton;
    procedure btnApplyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEditColumnValue: TfrmEditColumnValue;

implementation

uses
  Unit1, uCdMongo, cxGridTableView;

{$R *.dfm}

procedure TfrmEditColumnValue.btnApplyClick(Sender: TObject);
var
  I: Integer;
  AColumn: TcxGridColumn;
  AEnterpriseI, AStoreI: Integer;
  AEnterprise, AStore, ACollectionName: string;
begin
  ACollectionName := 'CompuDashMain';

  AEnterpriseI := 0;
  AStoreI := 0;
  AColumn := frmViewer.TableView1.GetColumnByFieldName('enterprise_name');
  if Assigned(AColumn) then
    AEnterpriseI := AColumn.Index;

  AColumn := frmViewer.TableView1.GetColumnByFieldName('store_name');
  if Assigned(AColumn) then
    AStoreI := AColumn.Index;

  if (AEnterpriseI = 0) or (AStoreI = 0) then
    raise Exception.Create('Column Enterprise_name and store_name not found.');

  if cmbColumnName.Text = '' then
    raise Exception.Create('No Column Name selected.');

  cdMongo.DoConnect('compudime', ACollectionName);
  for I := 0 to frmViewer.TableView1.Controller.SelectedRecordCount - 1 do
  begin
    AEnterprise := frmViewer.TableView1.Controller.SelectedRecords[I].Values[AEnterpriseI];
    AStore := frmViewer.TableView1.Controller.SelectedRecords[I].Values[AStoreI];
    cdMongo.MyValueSetGet(ACollectionName, AEnterprise, AStore, cmbColumnName.Text, txtValue.Text);
  end;
  cdMongo.DoClose;
end;

end.
