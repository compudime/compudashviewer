object SetupForm: TSetupForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Setup layouts, computers and users'
  ClientHeight = 533
  ClientWidth = 848
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 848
    Height = 533
    Align = alClient
    TabOrder = 0
    AutoSize = True
    object gdLayouts: TcxGrid
      Left = 17
      Top = 38
      Width = 242
      Height = 278
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      object tvLayouts: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = LayoutsData.dsViews
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object tvLayoutsName: TcxGridDBColumn
          Caption = 'Name'
          DataBinding.FieldName = 'view_name'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.OnValidate = tvLayoutsNamePropertiesValidate
          Options.Filtering = False
          Options.Grouping = False
          Width = 259
        end
      end
      object gdLayoutsLevel1: TcxGridLevel
        GridView = tvLayouts
      end
    end
    object edtLayoutTag: TDBTagEditor
      Left = 17
      Top = 376
      TabOrder = 2
      Color = clWhite
      EditorColor = clWhite
      Properties.MultiLine = True
      Properties.ShowIncludeOptions = False
      Properties.PopupTokens = <>
      DataBinding.DataField = 'view_tags'
      DataBinding.DataSource = LayoutsData.dsViews
      Height = 111
      Width = 242
    end
    object edtCompTag: TDBTagEditor
      Left = 293
      Top = 376
      TabOrder = 6
      Color = clWhite
      EditorColor = clWhite
      Properties.MultiLine = True
      Properties.ShowIncludeOptions = False
      Properties.PopupTokens = <>
      DataBinding.DataField = 'comp_tags'
      DataBinding.DataSource = LayoutsData.dsComps
      Height = 111
      Width = 269
    end
    object edtUserTag: TDBTagEditor
      Left = 596
      Top = 376
      TabOrder = 10
      Color = clWhite
      EditorColor = clWhite
      Properties.MultiLine = True
      Properties.ShowIncludeOptions = False
      Properties.PopupTokens = <>
      DataBinding.DataField = 'user_tags'
      DataBinding.DataSource = LayoutsData.dsUsers
      Height = 111
      Width = 235
    end
    object gdComps: TcxGrid
      Left = 293
      Top = 38
      Width = 269
      Height = 278
      TabOrder = 3
      object tvComps: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = LayoutsData.dsComps
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object tvCompsName: TcxGridDBColumn
          Caption = 'Name'
          DataBinding.FieldName = 'comp_name'
          OnCustomDrawCell = tvCompsNameCustomDrawCell
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Grouping = False
          Width = 94
        end
        object tvCompsLayout: TcxGridDBColumn
          Caption = 'Default layout'
          DataBinding.FieldName = 'comp_layout'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'view_name'
          Properties.ListColumns = <
            item
              FieldName = 'view_name'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = LayoutsData.dsViews
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Grouping = False
          Width = 128
        end
      end
      object gdCompsLevel1: TcxGridLevel
        GridView = tvComps
      end
    end
    object gdUsers: TcxGrid
      Left = 596
      Top = 38
      Width = 235
      Height = 278
      TabOrder = 7
      object tvUsers: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        OnCustomDrawCell = tvUsersCustomDrawCell
        DataController.DataSource = LayoutsData.dsUsers
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object tvUsersName: TcxGridDBColumn
          Caption = 'Name'
          DataBinding.FieldName = 'user_name'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Grouping = False
        end
        object tvUsersLayout: TcxGridDBColumn
          Caption = 'Default layout'
          DataBinding.FieldName = 'user_layout'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'view_name'
          Properties.ListColumns = <
            item
              FieldName = 'view_name'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = LayoutsData.dsViews
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.AutoWidthSizable = False
          Options.Grouping = False
          Width = 132
        end
      end
      object gdUsersLevel1: TcxGridLevel
        GridView = tvUsers
      end
    end
    object btnSave: TcxButton
      Left = 687
      Top = 501
      Width = 75
      Height = 25
      Caption = 'Save'
      ModalResult = 1
      TabOrder = 11
      OnClick = btnSaveClick
    end
    object btnCancel: TcxButton
      Left = 766
      Top = 501
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 12
    end
    object btnAddComp: TcxButton
      Left = 293
      Top = 320
      Width = 50
      Height = 25
      Caption = 'Add'
      TabOrder = 4
      OnClick = btnAddCompClick
    end
    object btnDelComp: TcxButton
      Left = 347
      Top = 320
      Width = 50
      Height = 25
      Caption = 'Delete'
      TabOrder = 5
      OnClick = btnDelCompClick
    end
    object btnAddUser: TcxButton
      Left = 596
      Top = 320
      Width = 50
      Height = 25
      Caption = 'Add'
      TabOrder = 8
      OnClick = btnAddUserClick
    end
    object btnDelUser: TcxButton
      Left = 650
      Top = 320
      Width = 50
      Height = 25
      Caption = 'Delete'
      TabOrder = 9
      OnClick = btnDelUserClick
    end
    object btnDelLayout: TcxButton
      Left = 17
      Top = 320
      Width = 50
      Height = 25
      Caption = 'Delete'
      TabOrder = 1
      OnClick = btnDelLayoutClick
    end
    object dxLayoutControl1Group_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avParentManaged
      CaptionOptions.Visible = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel1
      Hidden = True
      ShowBorder = False
      Index = -1
      Buttons = <
        item
          Glyph.SourceDPI = 96
          Glyph.Data = {
            89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
            6100000023744558745469746C650043616E63656C3B53746F703B457869743B
            426172733B526962626F6E3B4C9696B20000038849444154785E1D906B4C5367
            18C7FF3DBD40CB1A2E32B55C9D598B4CA675D8D13836652E9B0359B67D589665
            3259E644A52571644474CB4CB6ECC23770C4449DD38D2885005E4683AB69C616
            8DA12384264EC8AAAC0C9149A1175ACEE9E939CFDE9EE7E477F2CBFFB924E720
            E6E943CC3B8895D12B00A0FEE3D08167A75A5BBAEEB71D9D081E6B4DA549FBDD
            A3CEEFDD1F3658016818AA98A71FD1915E202DE980A19D741E3EF6E0F8A7FC7F
            673B6979E002C5BC43B4C2581EB8480BE7BA68E6441BEF3B72F03300990C8E1D
            5016554E7B55D6C1ED9543C6C2B5BB739FDF025988838424E4240F10A0D2EAA0
            D26540AD37203CFE17C2C187A3EDBFDE7CF3DAD4748403A06EA8A8E830AC5FB3
            3B7BAB1901B717AE23DFE1CEC5EBEC90A0E0EB71A3CFD981C0B017C6F252180B
            D6BD74BCFA856E003A0CBDFD966DF250532AD4FF038DB734D18557DF21CFB08F
            2E37B5D370ED5E72D7D52BEEF9654CE9F91C1FD392EB0C4D3A0E4BE7F6ECD909
            CFDEFAD381AF4ED0A3D35FD399E272BA3F3D478F971234FD2044BDCE930AF798
            CF2FAED0DF5373CACCFCA92F2970B29DDCAFD7F56B48945E918201C41738945A
            2D581C7461ADA3192AB50AD64F9A010272730CC8D4AA313BE44289D58CF85D3F
            2411504BB28D93845489145E041F9CC1863C09A11BD7E1EFEA86240339463DB2
            B3F59025C0DFD98DD0C83594E6886C360831F408523265D208BC0021B20A35A7
            82B8BC0429C2239A10D812417988007088B14C8A8421EA75A094044A8A48F200
            17E78587629220B370E69F2884EA3750F07E23245946868E43A64EA3B8695F23
            F8EA7A046763EC780AC9640AF155FEB1269AE0BD91AC8CFDF910108E26F15A5B
            33788D1E860CF6CDE7CF225D45FB3F02A0C7CE36076E5CBD84825C3562A20E4B
            097E0CAD051B5FFCA97C9BE4ABAEA05B2FDBE9E6BE0F880F8568FCDB0E1AA9AA
            646C579C654AEF564D15FDB96333FDBCC94A8E751B6A0140DF5168B9E42A7B86
            266AB6D2ED1A1BF559CAC853B58DFCB576F2D7D9D3AE64B777D96862D716EA2F
            2BA76F4CE62B008C1A00C2F9C57F9D8DA2C99212C5E72C85323699F320A77FD2
            72040021DF9885F56BF2204457706F9EC74C4CF2F744169A012430DBF21E00A8
            2B754F98BEC82EEEED7AF2291A306FA451EBD3346633938FF13BF341969D62BD
            CF738AAF6ED6EA4B006882CE77A14ABFD255D2799903606830E4EF28E274070C
            1C67D74255041044C25C9CE43B4149F8B16735F41B8038DB9300E07F6924ECFB
            01D589CC0000000049454E44AE426082}
          Height = 16
          Tag = 16
        end>
    end
    object dxLayoutGroup1: TdxLayoutGroup
      Parent = dxLayoutGroup4
      AlignHorz = ahLeft
      AlignVert = avClient
      CaptionOptions.Text = 'Layouts'
      SizeOptions.AssignedValues = [sovSizableHorz]
      SizeOptions.SizableHorz = True
      SizeOptions.Width = 262
      ItemIndex = 1
      Index = 0
    end
    object dxLayoutSplitterItem1: TdxLayoutSplitterItem
      Parent = dxLayoutGroup1
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
    object dxLayoutItem3: TdxLayoutItem
      Parent = dxLayoutGroup9
      AlignVert = avClient
      SizeOptions.AssignedValues = [sovSizableVert]
      SizeOptions.SizableVert = True
      SizeOptions.Height = 278
      CaptionOptions.Text = 'cxGrid1'
      CaptionOptions.Visible = False
      Control = gdLayouts
      ControlOptions.OriginalHeight = 278
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem1: TdxLayoutItem
      Parent = dxLayoutGroup10
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxMemo1'
      CaptionOptions.Visible = False
      Control = edtLayoutTag
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutLabeledItem1: TdxLayoutLabeledItem
      Parent = dxLayoutGroup10
      AlignVert = avTop
      CaptionOptions.Text = 'TAGs'
      Index = 0
    end
    object dxLayoutGroup2: TdxLayoutGroup
      Parent = dxLayoutGroup4
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'Computers'
      Index = 2
    end
    object dxLayoutGroup3: TdxLayoutGroup
      Parent = dxLayoutGroup4
      AlignHorz = ahLeft
      AlignVert = avClient
      CaptionOptions.Text = 'Users'
      SizeOptions.AssignedValues = [sovSizableHorz]
      SizeOptions.SizableHorz = True
      SizeOptions.Width = 255
      Index = 4
    end
    object dxLayoutSplitterItem2: TdxLayoutSplitterItem
      Parent = dxLayoutGroup4
      AlignVert = avClient
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
    object dxLayoutSplitterItem3: TdxLayoutSplitterItem
      Parent = dxLayoutGroup4
      AlignVert = avClient
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 3
    end
    object dxLayoutItem2: TdxLayoutItem
      Parent = dxLayoutGroup11
      AlignVert = avClient
      CaptionOptions.Text = 'cxGrid2'
      CaptionOptions.Visible = False
      Control = gdComps
      ControlOptions.OriginalHeight = 278
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem4: TdxLayoutItem
      Parent = dxLayoutGroup13
      AlignVert = avClient
      CaptionOptions.Text = 'cxGrid3'
      CaptionOptions.Visible = False
      Control = gdUsers
      ControlOptions.OriginalHeight = 278
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem5: TdxLayoutItem
      Parent = dxLayoutGroup12
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxMemo2'
      CaptionOptions.Visible = False
      Control = edtCompTag
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutItem6: TdxLayoutItem
      Parent = dxLayoutGroup14
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'cxMemo3'
      CaptionOptions.Visible = False
      Control = edtUserTag
      ControlOptions.OriginalHeight = 89
      ControlOptions.OriginalWidth = 185
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutSplitterItem4: TdxLayoutSplitterItem
      Parent = dxLayoutGroup2
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
    object dxLayoutSplitterItem5: TdxLayoutSplitterItem
      Parent = dxLayoutGroup3
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
    object dxLayoutLabeledItem2: TdxLayoutLabeledItem
      Parent = dxLayoutGroup12
      AlignVert = avTop
      CaptionOptions.Text = 'TAGs'
      Index = 0
    end
    object dxLayoutLabeledItem3: TdxLayoutLabeledItem
      Parent = dxLayoutGroup14
      AlignVert = avTop
      CaptionOptions.Text = 'TAGs'
      Index = 0
    end
    object dxLayoutGroup4: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      LayoutDirection = ldHorizontal
      ScrollOptions.Horizontal = smAuto
      ScrollOptions.Vertical = smIndependent
      ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup5: TdxLayoutGroup
      Parent = dxLayoutControl1Group_Root
      AlignHorz = ahClient
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutItem7: TdxLayoutItem
      Parent = dxLayoutGroup5
      AlignHorz = ahRight
      AlignVert = avCenter
      CaptionOptions.Text = 'cxButton1'
      CaptionOptions.Visible = False
      Control = btnSave
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem8: TdxLayoutItem
      Parent = dxLayoutGroup5
      AlignHorz = ahRight
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      Control = btnCancel
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutItem9: TdxLayoutItem
      Parent = dxLayoutGroup7
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnAddComp
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem11: TdxLayoutItem
      Parent = dxLayoutGroup7
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnDelComp
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutGroup7: TdxLayoutGroup
      Parent = dxLayoutGroup11
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutGroup6: TdxLayoutGroup
      Parent = dxLayoutGroup13
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutItem12: TdxLayoutItem
      Parent = dxLayoutGroup6
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnAddUser
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutItem14: TdxLayoutItem
      Parent = dxLayoutGroup6
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnDelUser
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutGroup8: TdxLayoutGroup
      Parent = dxLayoutGroup9
      AlignVert = avBottom
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object dxLayoutItem16: TdxLayoutItem
      Parent = dxLayoutGroup8
      CaptionOptions.Text = 'cxButton3'
      CaptionOptions.Visible = False
      Control = btnDelLayout
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 50
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup9: TdxLayoutGroup
      Parent = dxLayoutGroup1
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ItemIndex = 1
      ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup10: TdxLayoutGroup
      Parent = dxLayoutGroup1
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ItemIndex = 1
      ShowBorder = False
      Index = 2
    end
    object dxLayoutGroup11: TdxLayoutGroup
      Parent = dxLayoutGroup2
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup12: TdxLayoutGroup
      Parent = dxLayoutGroup2
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ItemIndex = 1
      ShowBorder = False
      Index = 2
    end
    object dxLayoutGroup13: TdxLayoutGroup
      Parent = dxLayoutGroup3
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ShowBorder = False
      Index = 0
    end
    object dxLayoutGroup14: TdxLayoutGroup
      Parent = dxLayoutGroup3
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      CaptionOptions.Visible = False
      ItemIndex = 1
      ShowBorder = False
      Index = 2
    end
  end
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    Left = 404
    Top = 116
    object dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
      PixelsPerInch = 96
    end
  end
end
