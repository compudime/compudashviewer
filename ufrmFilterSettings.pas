unit ufrmFilterSettings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, cxClasses, dxLayoutContainer, dxLayoutControl, dxLayoutcxEditAdapters,
  cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  dxDateRanges, Data.DB, cxDBData, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxLabel, cxCustomListBox, cxListBox, cxDropDownEdit,
  System.Generics.Collections, System.IOUtils, FireDAC.Phys.MongoDBDataSet,
  FireDAC.Phys.MongoDBWrapper, dxLayoutControlAdapters, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, System.JSON, uFilter, dxScrollbarAnnotations, dxUIAClasses, dxSkinWXI;

type
  IFriendlyFilterSetter = interface
    procedure SetFriendlyFilter(const AText: String);
  end;

  TfrmFilterSettings = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    lboxMasterFields: TcxListBox;
    laylboxMasterFields: TdxLayoutItem;
    lblText: TcxLabel;
    laylblText: TdxLayoutItem;
    gridSubFieldsDBTableView1: TcxGridDBTableView;
    gridSubFieldsLevel1: TcxGridLevel;
    gridSubFields: TcxGrid;
    laygridSubFields: TdxLayoutItem;
    mtblSubFields: TFDMemTable;
    dtsrcSubFields: TDataSource;
    laygrpLeft: TdxLayoutGroup;
    laygrpRight: TdxLayoutGroup;
    mtblSubFieldsFieldName: TStringField;
    mtblSubFieldsExists: TStringField;
    mtblSubFieldsValue: TStringField;
    clmSubFieldName: TcxGridDBColumn;
    clmSubFieldExists: TcxGridDBColumn;
    clmSubFieldsValue: TcxGridDBColumn;
    laygrpTop: TdxLayoutGroup;
    laygrpBottom: TdxLayoutGroup;
    btnScan: TcxButton;
    laybtnScan: TdxLayoutItem;
    laygrpControlButtons: TdxLayoutGroup;
    btnCancel: TcxButton;
    laybtnCancel: TdxLayoutItem;
    btnApply: TcxButton;
    laybtnApply: TdxLayoutItem;
    btnRemoveAllFilters: TcxButton;
    dxLayoutItem1: TdxLayoutItem;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lboxMasterFieldsClick(Sender: TObject);
    procedure btnScanClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnRemoveAllFiltersClick(Sender: TObject);
    procedure mtblSubFieldsExistsGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    FList: TFilterSettingsList;
    FPrevSelectedMasterList: String;
    FMongoQuery: TFDMongoQuery;
    FFriendlyFilterSetter: IFriendlyFilterSetter;

    procedure LoadFilterListToListBox(AList: TFilterSettingsList);
    function GridToFilterList(const AMasterField: String): TFilterSettingsList;
    procedure DeleteMasterFieldFromList(AList: TFilterSettingsList; const AMasterField: String);

    function ScanQuery(AQuery: TFDMongoQuery): TFilterSettingsList;
  public
    procedure ShowForm(AMongoQuery: TFDMongoQuery; AFriendlyFilterSetter: IFriendlyFilterSetter);
  end;

var
  frmFilterSettings: TfrmFilterSettings;

implementation

{$R *.dfm}

{ TfrmFilterSettings }

procedure TfrmFilterSettings.btnApplyClick(Sender: TObject);
begin
  lboxMasterFieldsClick(nil);
  SetFilterSettings(FList);

  if Assigned(FMongoQuery) then begin
    FMongoQuery.Close;
    FMongoQuery.QMatch := FList.ToMongoFilter;
    FMongoQuery.Open;

    if Assigned(FFriendlyFilterSetter) then
      FFriendlyFilterSetter.SetFriendlyFilter(FList.ToFriendlyFilter);
  end;
  FormShow(nil);
end;

procedure TfrmFilterSettings.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFilterSettings.btnRemoveAllFiltersClick(Sender: TObject);
var
  i: integer;
  Filter: TFilterSettings;
begin
  for i:=0 to FList.Count - 1 do begin
    Filter := FList[i];
    Filter.FilterExists := FILTER_NOT_SET;
    Filter.FilterValue := FILTER_NOT_SET;
    FList[i] := Filter;
  end;

  LoadFilterListToListBox(FList);
  FPrevSelectedMasterList := '';
  if mtblSubFields.Active then
    mtblSubFields.EmptyDataSet;
end;

procedure TfrmFilterSettings.btnScanClick(Sender: TObject);
var
  i, j: integer;
  ScanList: TFilterSettingsList;
begin
  FPrevSelectedMasterList := '';
  ScanList := ScanQuery(FMongoQuery);
  try
    if Assigned(FList) then begin
      for i := 0 to ScanList.Count - 1 do begin
        j := FList.IndexOfFields(ScanList[i].MasterFieldName, ScanList[i].SubFieldName);
        if j < 0 then
          FList.Add(ScanList[i]);
      end;
    end;
  finally
    ScanList.Free;
  end;
  LoadFilterListToListBox(FList);
  if mtblSubFields.Active then
    mtblSubFields.EmptyDataSet;
end;

procedure TfrmFilterSettings.DeleteMasterFieldFromList(AList: TFilterSettingsList; const AMasterField: String);
var
  i: integer;
begin
  i := 0;
  while i < AList.Count do
    if AList[i].MasterFieldName = AMasterField then
      AList.Delete(i)
    else
      i := i + 1;
end;

procedure TfrmFilterSettings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FPrevSelectedMasterList := '';
  if mtblSubFields.Active then
    mtblSubFields.EmptyDataSet;
  if Assigned(FList) then
    FreeAndNil(FList);
end;

procedure TfrmFilterSettings.FormShow(Sender: TObject);
begin
  FList := GetFilterSettings;
  LoadFilterListToListBox(FList);
  FPrevSelectedMasterList := '';
  if mtblSubFields.Active then
    mtblSubFields.EmptyDataSet;
end;

function TfrmFilterSettings.GridToFilterList(const AMasterField: String): TFilterSettingsList;
begin
  Result := TFilterSettingsList.Create;

  mtblSubFields.DisableControls;
  try
    mtblSubFields.First;
    while not mtblSubFields.Eof do begin
      Result.Add(TFilterSettings.Create(AMasterField,
                                        mtblSubFields.FieldByName('FieldName').AsString,
                                        mtblSubFields.FieldByName('Exists').AsString,
                                        mtblSubFields.FieldByName('Value').AsString));

      mtblSubFields.Next;
    end;
  finally
    mtblSubFields.EnableControls;
  end;
end;

procedure TfrmFilterSettings.lboxMasterFieldsClick(Sender: TObject);
var
  Item: TFilterSettings;
  List: TFilterSettingsList;
begin
  if not mtblSubFields.Active then
    mtblSubFields.CreateDataSet;

  if lboxMasterFields.ItemIndex < 0 then
    Exit;

  if not FPrevSelectedMasterList.IsEmpty then begin
    DeleteMasterFieldFromList(FList, FPrevSelectedMasterList);
    List := GridToFilterList(FPrevSelectedMasterList);
    FList.AddRange(List);
    List.Free;
  end;

  mtblSubFields.DisableControls;
  try
    mtblSubFields.EmptyDataSet;
    FPrevSelectedMasterList := lboxMasterFields.Items[lboxMasterFields.ItemIndex];
    for Item in FList do
      if Item.MasterFieldName = FPrevSelectedMasterList then begin
        mtblSubFields.Append;
        mtblSubFields.FieldByName('FieldName').Value := Item.SubFieldName;
        mtblSubFields.FieldByName('Exists').Value := Item.FilterExists;
        mtblSubFields.FieldByName('Value').Value := Item.FilterValue;
        mtblSubFields.Post;
      end;
  finally
    mtblSubFields.EnableControls;
  end;
end;

procedure TfrmFilterSettings.LoadFilterListToListBox(AList: TFilterSettingsList);
var
  Item: TFilterSettings;
begin
  lboxMasterFields.Items.BeginUpdate;
  try
    lboxMasterFields.Items.Clear;

    if not Assigned(AList) or (AList.Count = 0) then
      Exit;

    for Item in AList do
      if lboxMasterFields.Items.IndexOf(Item.MasterFieldName) < 0 then
        lboxMasterFields.Items.Add(Item.MasterFieldName);
  finally
    lboxMasterFields.Items.EndUpdate;
  end;

  lboxMasterFields.ItemIndex := -1;
end;

procedure TfrmFilterSettings.mtblSubFieldsExistsGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  if SameText(Sender.AsString, FILTER_NOT_SET) then
    Text := ''
  else
    Text := Sender.AsString;
end;

function TfrmFilterSettings.ScanQuery(AQuery: TFDMongoQuery): TFilterSettingsList;
var
  Cur: IMongoCursor;
  JSON, SubJSON: TJSONObject;
  i, j: integer;
begin
  Result := TFilterSettingsList.Create;

  Cur := AQuery.Collection.Find();
  while Cur.Next do begin
    JSON := TJSONObject.ParseJSONValue(Cur.Doc.AsJSON) as TJSONObject;
    try
      if Assigned(JSON) and (JSON.Count > 0) then
        for i := 0 to JSON.Count - 1 do
          if (JSON.Pairs[i].JsonValue is TJSONObject) and not (JSON.Pairs[i].JsonString.Value = '_id') then begin
            SubJSON := JSON.Pairs[i].JsonValue as TJSONObject;
            for j := 0 to SubJSON.Count - 1 do
              if Result.IndexOfFields(JSON.Pairs[i].JsonString.Value, SubJSON.Pairs[j].JsonString.Value) < 0 then
                Result.Add(TFilterSettings.Create(JSON.Pairs[i].JsonString.Value,
                                                  SubJSON.Pairs[j].JsonString.Value));
          end;
    finally
      JSON.Free;
    end;
  end;
end;

procedure TfrmFilterSettings.ShowForm(AMongoQuery: TFDMongoQuery; AFriendlyFilterSetter: IFriendlyFilterSetter);
begin
  FFriendlyFilterSetter := AFriendlyFilterSetter;
  FMongoQuery := AMongoQuery;
  Show;
end;

end.
