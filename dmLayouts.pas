unit dmLayouts;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TLayoutsData = class(TDataModule)
    memComps: TFDMemTable;
    memCompscomp_name: TStringField;
    memCompscomp_tags: TMemoField;
    memCompscomp_layout: TStringField;
    memUsers: TFDMemTable;
    memUsersuser_name: TStringField;
    memUsersuser_tags: TMemoField;
    memUsersuser_layout: TStringField;
    dsComps: TDataSource;
    dsUsers: TDataSource;
    dsViews: TDataSource;
    memViewsNew: TFDMemTable;
    StringField1: TStringField;
    BooleanField1: TBooleanField;
    BlobField1: TBlobField;
    MemoField1: TMemoField;
    memViewsNewview_showing: TStringField;
    memViewsNewview_storedgrids: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure memViewsOldFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FCompName: String;
    FUserName: String;
    FCompTags: TStringList;
    FUserTags: TStringList;
    FLayoutTags: TStringList;
    function GetComputerTags: TStringList;
    function GetUserTags: TStringList;
    function GetLayoutTags(const ALayoutName: String): TStringList;
    function GetUserTagsAsString: String;
    function GetCompTagsAsString: String;
    function GetDefaultShowing: String;
  public
    property ComputerName: String read FCompName;
    property UserName: String read FUserName;
    property UserTags: String read GetUserTagsAsString;
    property ComputerTags: String read GetCompTagsAsString;
    procedure DoSaveMemViewsToFile;
    procedure DoLoadMemViewsFromFile;
    procedure DoSaveMemUsersToFile;
    procedure DoLoadMemUsersFromFile;
    procedure DoSaveMemCompsToFile;
    procedure DoLoadMemCompsFromFile;
    function GetComputerLayout: String;
    function GetUserLayout: String;
  end;

var
  LayoutsData: TLayoutsData;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses Utils, CDV.Consts;

{$R *.dfm}

{ TLayoutsData }

procedure TLayoutsData.DataModuleCreate(Sender: TObject);
begin
  FCompName := GetOSComputerName;
  FUserName := GetOSUserName;

  FLayoutTags := TStringList.Create;
  FLayoutTags.Delimiter := #32;
end;

procedure TLayoutsData.DataModuleDestroy(Sender: TObject);
begin
  if Assigned(FLayoutTags) then
  begin
    FLayoutTags.Clear;
    FreeAndNil(FLayoutTags);
  end;

  if Assigned(FCompTags) then
  begin
    FCompTags.Clear;
    FreeAndNil(FCompTags);
  end;

  if Assigned(FUserTags) then
  begin
    FUserTags.Clear;
    FreeAndNil(FUserTags);
  end;
end;

procedure TLayoutsData.DoLoadMemCompsFromFile;
var
  LViewSettingsPath: String;
begin
  LViewSettingsPath := GetCurrentDir + '\CompSettings_v2.json';
  if FileExists(LViewSettingsPath) then
  begin
    try
      memComps.LoadFromFile(LViewSettingsPath, TFDStorageFormat.sfJSON);
    except
    end;

    memComps.Open;

    // Tags for current computer
    if Assigned(FCompTags) then
    begin
      FCompTags.Clear;
      FreeAndNil(FCompTags);
    end;
    FCompTags := GetComputerTags;
  end;
end;

procedure TLayoutsData.DoLoadMemUsersFromFile;
var
  LViewSettingsPath: String;
begin
  LViewSettingsPath := GetCurrentDir + '\UserSettings_v2.json';
  if FileExists(LViewSettingsPath) then
  begin
    try
      memUsers.LoadFromFile(LViewSettingsPath, TFDStorageFormat.sfJSON);
    except
    end;

    memUsers.Open;

    // Tags for current user
    if Assigned(FUserTags) then
    begin
      FUserTags.Clear;
      FreeAndNil(FUserTags);
    end;
    FUserTags := GetUserTags;
  end;
end;

procedure TLayoutsData.DoLoadMemViewsFromFile;
var
  LViewSettingsPath: String;
begin
  LViewSettingsPath := GetCurrentDir + '\ViewSettings_v2.json';
  if FileExists(LViewSettingsPath) then
  try
    (dsViews.DataSet as TFDMemTable).LoadFromFile(LViewSettingsPath, TFDStorageFormat.sfJSON);
  except
  end;
end;

procedure TLayoutsData.DoSaveMemCompsToFile;
begin
  memComps.SaveToFile(GetCurrentDir + '\CompSettings_v2.json', TFDStorageFormat.sfJSON);
end;

procedure TLayoutsData.DoSaveMemUsersToFile;
begin
  memUsers.SaveToFile(GetCurrentDir + '\UserSettings_v2.json', TFDStorageFormat.sfJSON);
end;

procedure TLayoutsData.DoSaveMemViewsToFile;
begin
  (dsViews.DataSet as TFDMemTable).SaveToFile(GetCurrentDir + '\ViewSettings_v2.json', TFDStorageFormat.sfJSON);
end;

function TLayoutsData.GetCompTagsAsString: String;
begin
  if Assigned(FCompTags) then
    Result := FCompTags.DelimitedText
  else
    Result := '';
end;

function TLayoutsData.GetComputerLayout: String;
begin
  with memComps do
    if Active and Locate(memCompscomp_name.FieldName, FCompName, [loCaseInsensitive]) then
      Result := memCompscomp_layout.Value
    else
      Result := '';
end;

function TLayoutsData.GetComputerTags: TStringList;
begin
  with memComps do
    if Active and Locate(memCompscomp_name.FieldName, FCompName, [loCaseInsensitive]) then
    begin
      Result := TStringList.Create;
      Result.Delimiter := ' ';
      Result.DelimitedText := memCompscomp_tags.Value;
    end
    else
      Result := nil;
end;

function TLayoutsData.GetDefaultShowing: String;
var
  json: String;
begin
  json := '[';
  for var Str in DEFAULT_GRIDS do
    json := json + '{ "' + Str + '" : "' + SHOW_TYPE_ALWAYS + '"},';

  json.Remove(json.Length);
  json := json + ']';
end;

function TLayoutsData.GetLayoutTags(const ALayoutName: String): TStringList;
begin
  if dsViews.DataSet.Active and dsViews.DataSet.Locate('view_name', ALayoutName, [loCaseInsensitive]) then
  begin
    Result := TStringList.Create;
    Result.Delimiter := ' ';
    Result.DelimitedText := dsViews.DataSet['view_tags'];
  end else
    Result := nil;
end;

function TLayoutsData.GetUserLayout: String;
begin
  with memUsers do
    if Active and Locate(memUsersuser_name.FieldName, FUserName, [loCaseInsensitive]) then
      Result := memUsersuser_layout.Value
    else
      Result := '';
end;

function TLayoutsData.GetUserTags: TStringList;
begin
  with memUsers do
    if Active and Locate(memUsersuser_name.FieldName, FUserName, [loCaseInsensitive]) then
    begin
      Result := TStringList.Create;
      Result.Delimiter := ' ';
      Result.DelimitedText := memUsersuser_tags.Value;
    end
    else
      Result := nil;
end;

function TLayoutsData.GetUserTagsAsString: String;
begin
  if Assigned(FUserTags) then
    Result := FUserTags.DelimitedText
  else
    Result := '';
end;

procedure TLayoutsData.memViewsOldFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
var
  i: Integer;
begin
  Accept := False;

  FLayoutTags.DelimitedText := DataSet.FieldByName('view_tags').AsString;

  Accept := SameText(DataSet['view_name'], GetUserLayout) or
    SameText(DataSet['view_name'], GetComputerLayout) or
    (FLayoutTags.IndexOf('GLOBAL') >= 0);

  if not Accept and (FLayoutTags.Count > 0) then
  begin
    if Assigned(FCompTags) and (FCompTags.Count > 0) then
      for i := 0 to FLayoutTags.Count - 1 do
        if FCompTags.IndexOf(FLayoutTags[i]) >= 0 then
        begin
          Accept := True;
          break;
        end;

    if not Accept and Assigned(FUserTags) and (FUserTags.Count > 0) then
      for i := 0 to FLayoutTags.Count - 1 do
        if FUserTags.IndexOf(FLayoutTags[i]) >= 0 then
        begin
          Accept := True;
          break;
        end;
  end;
end;

end.
