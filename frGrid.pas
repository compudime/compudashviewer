unit frGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData, dxLayoutContainer,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxLayoutControl,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uNotifyCenter, CDV.Consts, System.JSON, dxUIAClasses, dxSkinWXI;

type
  TdxLayoutGroupAccess = class(TdxLayoutGroup);

  TGridFrame = class(TFrame)
    dxLayoutControl1: TdxLayoutControl;
    cxDataGrid: TcxGrid;
    tvData: TcxGridDBTableView;
    cxDataGridLevel1: TcxGridLevel;
    dxLayoutFrameRoot: TdxLayoutGroup;
    dxLayoutFrameGridItem: TdxLayoutItem;
    dxLayoutFrameDataGroup: TdxLayoutGroup;
    dxLayoutFrameGridSplitter: TdxLayoutSplitterItem;
    memData: TFDMemTable;
    dsData: TDataSource;
    memDataName: TStringField;
    memDataValue: TStringField;
    memData_id: TStringField;
    tvDataColumn1: TcxGridDBColumn;
    tvDataColumn2: TcxGridDBColumn;
    tvDataColumn3: TcxGridDBColumn;
    procedure dxLayoutFrameDataGroupButton0Click(Sender: TObject);
    procedure dxLayoutFrameDataGroupButton1Click(Sender: TObject);
  private
    FDataGroup: TdxLayoutGroup;
    FID: String;
  public
    constructor Create(AOwner: TComponent; const AName, aID: String; AJObj: TJSONObject;
      AParentLayout: TdxLayoutGroup); reintroduce;

    function GetDataGroup: TdxLayoutGroup;
    procedure UpdateData(aJObj: TJSONObject; const aID: String); virtual;
    procedure ClearData;
    procedure SetVisibleContent(aVisible: Boolean);
    function IsVisible: Boolean;
    function GetTableVew: TcxGridDBTableView;

    property ID: String read FID;
  end;

implementation

{$R *.dfm}

{ TGridFrame }

procedure TGridFrame.ClearData;
begin
  memData.EmptyDataSet;
end;

constructor TGridFrame.Create(AOwner: TComponent; const AName, aID: String; AJObj: TJSONObject; AParentLayout: TdxLayoutGroup);
begin
  inherited Create(AOwner);
  FDataGroup := dxLayoutFrameDataGroup;
  Self.Name := AName;
  dxLayoutFrameDataGroup.CaptionOptions.Text := AName.Replace('_', ' ');

  memData.CreateDataSet;
  UpdateData(AJObj, aID);

  dxLayoutFrameDataGroup.MoveTo(AParentLayout, AParentLayout.VisibleCount, True);
  dxLayoutFrameGridSplitter.MoveTo(AParentLayout, AParentLayout.VisibleCount, True);
end;

procedure TGridFrame.dxLayoutFrameDataGroupButton0Click(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(NOTIFY_MOVEGRIDLEFT, Sender);
end;

procedure TGridFrame.dxLayoutFrameDataGroupButton1Click(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(NOTIFY_MOVEGRIDRIGHT, Sender);
end;

function TGridFrame.GetDataGroup: TdxLayoutGroup;
begin
  Result := FDataGroup;
end;

function TGridFrame.GetTableVew: TcxGridDBTableView;
begin
  Result := tvData;
end;

function TGridFrame.IsVisible: Boolean;
begin
  Result := dxLayoutFrameGridSplitter.Visible;
end;

procedure TGridFrame.SetVisibleContent(aVisible: Boolean);
begin
  with TdxLayoutGroupAccess(dxLayoutFrameDataGroup) do
    if FIsFloat then begin
      if aVisible then
        ShowFloat(True)
      else
        HideFloat;
    end else
      Visible := aVisible;

  dxLayoutFrameGridSplitter.Visible := aVisible;
end;

procedure TGridFrame.UpdateData(aJObj: TJSONObject; const aID: String);
begin
  FID := aID;

  memData.EmptyDataSet;

  if Assigned(AJObj) then begin
    memData.DisableControls;
    try
      for var i := 0 to AJObj.Count - 1 do
        begin
          memData.Append;
          memData_id.AsString := aID;
          memDataName.AsString := AJObj.Pairs[i].JsonString.Value;
          memDataValue.AsString := AJObj.Pairs[i].JsonValue.Value;
        end;
    finally
      memData.EnableControls;
    end;
    memData.First;
  end;
end;

end.
