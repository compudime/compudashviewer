unit HelperMongoUnit;

interface

implementation
procedure TfrmMain.btnApplyClick(Sender: TObject);
var
  IDField: TField;
  MongoUpdate: TMongoUpdate;
begin
  IDField := mqData.FindField('_id');
  if not Assigned(IDField) then
    Exit;

  ClientDataSet.First;
  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match('{_id : {"$oid" : "' + IDField.AsString + '"}}');
    while not ClientDataSet.Eof do begin
      MongoUpdate.Modify().&Set().Field('Tis01_Module.' + ClientDataSet.FieldByName('Name').AsString,
        ClientDataSet.FieldByName('Value').AsString);
      ClientDataSet.Next;
    end;
    FMongoConn[DB_NAME][vCollection_Name].Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
  finally
    FreeAndNil(MongoUpdate);
  end;
end;

procedure TfrmMain.btnDeleteClick(Sender: TObject);
var
  IDField: TField;
  MongoUpdate: TMongoUpdate;
  Name, Value: String;
begin
  IDField := mqData.FindField('_id');
  if not Assigned(IDField) then
    Exit;

  Name := ClientDataSet.FieldByName('Name').AsString;
  Value := ClientDataSet.FieldByName('Value').AsString;
  if MessageDlg('Are you sure want delete record "' + Name + ' : ' + Value + '" ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
    Exit;

  if ClientDataSet.FieldByName('_id').AsString.IsEmpty then begin
    DSstoreDataChange(nil, nil);
    Exit;
  end;

  MongoUpdate := TMongoUpdate.Create(FEnv);
  try
    MongoUpdate.Match('{_id : {"$oid" : "' + IDField.AsString + '"}}');
    MongoUpdate.Modify().&UnSet().Field('Tis01_Module.' + ClientDataSet.FieldByName('Name').AsString);

    FMongoConn[DB_NAME][vCollection_Name].Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
  finally
    FreeAndNil(MongoUpdate);
  end;

  DSstoreDataChange(nil, nil);
end;

procedure TfrmMain.dbnvgrDetailClick(Sender: TObject; Button: TNavigateBtn);
var
  IDField: TField;
  MongoUpdate: TMongoUpdate;
begin
  Exit;
  if Button = TNavigateBtn.nbPost then begin
    IDField := mqData.FindField('_id');
    if not Assigned(IDField) then
      Exit;

    ClientDataSet.First;
    MongoUpdate := TMongoUpdate.Create(FEnv);
    try
      MongoUpdate.Match('{_id : {"$oid" : "' + IDField.AsString + '"}}');
      while not ClientDataSet.Eof do begin
        MongoUpdate.Modify().&Set().Field('Tis01_Module.' + ClientDataSet.FieldByName('Name').AsString,
          ClientDataSet.FieldByName('Value').AsString);
        ClientDataSet.Next;
      end;
      FMongoConn[DB_NAME][vCollection_Name].Update(MongoUpdate, [TMongoCollection.TUpdateFlag.Upsert]);
    finally
      FreeAndNil(MongoUpdate);
    end;
  end;
end;

//prevent editing
procedure TfrmMain.DBGrid2KeyPress(Sender: TObject; var Key: Char);
begin
  if (not ClientDataSet.FieldByName('_id').AsString.IsEmpty) and (DBGrid2.SelectedField.FieldName = 'Name') then
    Key := #0;
end;

end.
