unit frmColumnUnboundUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls,
  Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinscxPCPainter, dxLayoutContainer, dxLayoutControl, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxDropDownEdit,
  dxColorEdit, cxColorComboBox, cxSpinEdit, cxTextEdit, cxMaskEdit, dxLayoutControlAdapters, Vcl.StdCtrls, cxRadioGroup,
  dxGalleryControl, dxColorGallery, Vcl.ComCtrls, dxCore, cxDateUtils, cxCalendar, Vcl.Menus, cxButtons, cxLabel,
  dxLayoutLookAndFeels, cxClasses, System.Generics.Collections, cxGrid, Vcl.ImgList, Math, cxCheckBox, cxGridTableView,
  cxGridCustomTableView, cxGridCustomView, DB, ColumnUnboundHelperUnit, ColumnHelperTypesUnit, dxBarBuiltInMenu,
  cxButtonEdit, cxPC, System.Actions, Vcl.ActnList, cxMemo, cxRichEdit, cxListBox, System.UITypes, ColumnHelperDataUnit,
  System.StrUtils, System.JSON, cxCustomListBox;

type
  TfrmColumnUnbound = class(TForm)
    dxLayoutControlGroup_Root: TdxLayoutGroup;
    dxLayoutControl: TdxLayoutControl;
    layoutBottom: TdxLayoutGroup;
    dxLayoutControlSpaceItem4: TdxLayoutEmptySpaceItem;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    btnClose: TcxButton;
    dxLayoutControlItem12: TdxLayoutItem;
    btnSave: TcxButton;
    dxLayoutControlItem13: TdxLayoutItem;
    btnReset: TcxButton;
    dxLayoutControlItem9: TdxLayoutItem;
    cxPageControl: TcxPageControl;
    dxLayoutControlItem1: TdxLayoutItem;
    tshExpressions: TcxTabSheet;
    tshExpEditor: TcxTabSheet;
    tshSQLEditor: TcxTabSheet;
    tshWSEditor: TcxTabSheet;
    dxLayoutControl2: TdxLayoutControl;
    dxLayoutControl2Group_Root: TdxLayoutGroup;
    dxLayoutControl3: TdxLayoutControl;
    dxLayoutControl3Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    editCaption: TcxTextEdit;
    editType: TcxComboBox;
    editWidth: TcxSpinEdit;
    editExpModeValue: TcxRadioButton;
    editExpModeCondition: TcxRadioButton;
    lblExpressions1: TcxLabel;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutItem1: TdxLayoutItem;
    dxLayoutControl1Item2: TdxLayoutItem;
    dxLayoutControl1Item5: TdxLayoutItem;
    dxLayoutGroup1: TdxLayoutGroup;
    dxLayoutControl1Item4: TdxLayoutItem;
    dxLayoutControl1Item3: TdxLayoutItem;
    dxLayoutControl1Item7: TdxLayoutItem;
    editExp: TcxRichEdit;
    dxLayoutControl2Item1: TdxLayoutItem;
    layoutSQLExp: TdxLayoutItem;
    editTypeValue: TcxComboBox;
    dxLayoutControl2Item2: TdxLayoutItem;
    layoutExpFunc: TdxLayoutGroup;
    layoutAddValue: TdxLayoutGroup;
    dxLayoutControl2Group3: TdxLayoutAutoCreatedGroup;
    btnPasteValue: TcxButton;
    editTextValue: TcxTextEdit;
    editNumberValue: TcxSpinEdit;
    editDateValue: TcxDateEdit;
    layoutText: TdxLayoutItem;
    layoutNumber: TdxLayoutItem;
    layoutDate: TdxLayoutItem;
    dxLayoutControl2Item8: TdxLayoutItem;
    btnPasteField: TcxButton;
    btnPasteSQL: TcxButton;
    dxLayoutControl2Group2: TdxLayoutGroup;
    editFieldName: TcxComboBox;
    editSQLName: TcxComboBox;
    btnEditSQL: TcxButton;
    btnBackExp: TcxButton;
    btnSetExp: TcxButton;
    dxLayoutControl2Item9: TdxLayoutItem;
    dxLayoutControl2Item10: TdxLayoutItem;
    dxLayoutControl2Group4: TdxLayoutGroup;
    layoutAddField: TdxLayoutGroup;
    dxLayoutControl2Group5: TdxLayoutGroup;
    layoutEditSQL: TdxLayoutItem;
    layoutPasteSQL: TdxLayoutItem;
    layoutSQLName: TdxLayoutItem;
    layoutAddSQL: TdxLayoutGroup;
    dxLayoutControl2Group7: TdxLayoutGroup;
    dxLayoutControl2Item17: TdxLayoutItem;
    dxLayoutControl2Item18: TdxLayoutItem;
    dxLayoutControl2Group6: TdxLayoutGroup;
    btnPasteAdd: TcxButton;
    btnPasteMin: TcxButton;
    bntPasteMul: TcxButton;
    btnPasteBrace: TcxButton;
    btnPasteDate: TcxButton;
    dxLayoutControl2Item19: TdxLayoutItem;
    dxLayoutControl2Item20: TdxLayoutItem;
    dxLayoutControl2Group9: TdxLayoutGroup;
    dxLayoutControl2Item21: TdxLayoutItem;
    dxLayoutControl2Item22: TdxLayoutItem;
    dxLayoutControl2Group10: TdxLayoutGroup;
    dxLayoutControl2Item23: TdxLayoutItem;
    lstSQL: TcxListBox;
    editSQLAsstField: TcxComboBox;
    btnAddSQL: TcxButton;
    btnDelSQL: TcxButton;
    btnSetSQLName: TcxButton;
    btnPasteFieldToSQL: TcxButton;
    editFieldNameForSQL: TcxComboBox;
    dxLayoutControl3Item2: TdxLayoutItem;
    dxLayoutControl3Item3: TdxLayoutItem;
    dxLayoutControl3Group1: TdxLayoutGroup;
    layoutPasteToSQL: TdxLayoutItem;
    layoutTopSQL: TdxLayoutGroup;
    dxLayoutControl3Item5: TdxLayoutItem;
    layoutSQLSet: TdxLayoutItem;
    layoutAddSQLButton: TdxLayoutItem;
    layoutDeleteSQLButton: TdxLayoutItem;
    btnBackFromSQL: TcxButton;
    btnSetSQL: TcxButton;
    layoutSetSQL: TdxLayoutItem;
    dxLayoutControl3Item10: TdxLayoutItem;
    dxLayoutControl4: TdxLayoutControl;
    lstWS: TcxListBox;
    btnPasteFieldToWS: TcxButton;
    editFieldNameForWS: TcxComboBox;
    editWSAsstField: TcxComboBox;
    btnSetWSName: TcxButton;
    btnAddWS: TcxButton;
    btnDelWS: TcxButton;
    btnBackFromWS: TcxButton;
    btnSetWS: TcxButton;
    dxLayoutGroup2: TdxLayoutGroup;
    layoutWSExp: TdxLayoutItem;
    dxLayoutItem3: TdxLayoutItem;
    dxLayoutGroup3: TdxLayoutGroup;
    layoutPasteToWS: TdxLayoutItem;
    dxLayoutItem5: TdxLayoutItem;
    layoutTopWS: TdxLayoutGroup;
    dxLayoutItem6: TdxLayoutItem;
    layoutWSSet: TdxLayoutItem;
    dxLayoutItem8: TdxLayoutItem;
    layoutDeleteWSButton: TdxLayoutItem;
    dxLayoutGroup5: TdxLayoutGroup;
    dxLayoutGroup6: TdxLayoutGroup;
    dxLayoutItem10: TdxLayoutItem;
    dxLayoutItem11: TdxLayoutItem;
    dxLayoutControl1Group1: TdxLayoutGroup;
    rbSQL: TcxRadioButton;
    rbWS: TcxRadioButton;
    layoutSelectSQL: TdxLayoutItem;
    layoutSelectWS: TdxLayoutItem;
    btnClearMainExp: TcxButton;
    btnEditMainExp: TcxButton;
    editMainExpression: TcxTextEdit;
    layoutExpression: TdxLayoutGroup;
    dxLayoutControl1Item1: TdxLayoutItem;
    dxLayoutControl1Item6: TdxLayoutItem;
    dxLayoutControl1Item9: TdxLayoutItem;
    btnTrySQL: TcxButton;
    btnTryWS: TcxButton;
    layoutTrySQL: TdxLayoutItem;
    layoutTryWS: TdxLayoutItem;
    editSQLUpdateLimit: TcxSpinEdit;
    editWSUpdateLimit: TcxSpinEdit;
    layoutStaleData: TdxLayoutItem;
    layoutBottomSQL: TdxLayoutGroup;
    dxLayoutControl3Group7: TdxLayoutGroup;
    dxLayoutControl4Item1: TdxLayoutItem;
    layoutBottomWS: TdxLayoutGroup;
    dxLayoutControl4Group2: TdxLayoutGroup;
    editExpModeSQLValue: TcxRadioButton;
    editExpModeSQLCondition: TcxRadioButton;
    dxLayoutControl1Item8: TdxLayoutItem;
    dxLayoutControl1Item10: TdxLayoutItem;
    SQLPageControl: TcxPageControl;
    tshSQLSelect: TcxTabSheet;
    tshSQLUpdate: TcxTabSheet;
    editSQLExp: TcxRichEdit;
    editSQLUpdate: TcxRichEdit;
    editExpModeWSValue: TcxRadioButton;
    editExpModeWSCondition: TcxRadioButton;
    dxLayoutControl1Item11: TdxLayoutItem;
    dxLayoutControl1Item12: TdxLayoutItem;
    WSPageControl: TcxPageControl;
    tshWSGet: TcxTabSheet;
    tshWSPost: TcxTabSheet;
    editWSExp: TcxRichEdit;
    editWSPost: TcxRichEdit;
    editMask: TcxTextEdit;
    layoutMask: TdxLayoutItem;
    editTargetField: TcxComboBox;
    layoutTargetField: TdxLayoutItem;
    dxLayoutControl1Group3: TdxLayoutGroup;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutAutoCreatedGroup4: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup2: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup3: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup5: TdxLayoutAutoCreatedGroup;
    dxLayoutLabeledItem1: TdxLayoutLabeledItem;
    dxLayoutAutoCreatedGroup7: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup1: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup6: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup10: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup11: TdxLayoutAutoCreatedGroup;
    dxLayoutAutoCreatedGroup8: TdxLayoutAutoCreatedGroup;
    procedure cxRadioButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure cxComboBox1PropertiesEditValueChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxRadioButton3Click(Sender: TObject);
    procedure editorFocusChanged(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnClearMainExpClick(Sender: TObject);
    procedure btnEditMainExpClick(Sender: TObject);
    procedure editExpModeValueClick(Sender: TObject);
    procedure btnBackExpClick(Sender: TObject);
    procedure btnSetExpClick(Sender: TObject);
    procedure editTypeValuePropertiesChange(Sender: TObject);
    procedure rbSQLClick(Sender: TObject);
    procedure btnPasteFieldClick(Sender: TObject);
    procedure btnPasteValueClick(Sender: TObject);
    procedure btnPasteSQLClick(Sender: TObject);
    procedure btnPasteBraceClick(Sender: TObject);
    procedure btnPasteDateClick(Sender: TObject);
    procedure btnPasteAddClick(Sender: TObject);
    procedure btnPasteMinClick(Sender: TObject);
    procedure bntPasteMulClick(Sender: TObject);
    procedure btnEditSQLClick(Sender: TObject);
    procedure btnBackFromSQLClick(Sender: TObject);
    procedure btnSetSQLClick(Sender: TObject);
    procedure btnSetWSClick(Sender: TObject);
    procedure btnAddSQLClick(Sender: TObject);
    procedure btnDelSQLClick(Sender: TObject);
    procedure btnPasteFieldToSQLClick(Sender: TObject);
    procedure btnSetSQLNameClick(Sender: TObject);
    procedure lstSQLClick(Sender: TObject);
    procedure editSQLExpPropertiesChange(Sender: TObject);
    procedure editTypePropertiesChange(Sender: TObject);
    procedure btnTrySQLClick(Sender: TObject);
    procedure editSQLUpdateLimitPropertiesChange(Sender: TObject);
    procedure tshSQLUpdateShow(Sender: TObject);
  private
    { Private declarations }
    textCurrent: TcxTextEdit;
    changing: Boolean;

    function editDataOnly(text: String): Boolean;
    procedure setButtonEditExp(index: Integer);
    procedure setConditionColors();
    function getColumnType(columnName: String): TDataTypes;
    procedure setNoneEnable(index: Integer; locking: Boolean);
    procedure addEmptyCondition(expression: String = '');
    procedure setItemsComboBoxForTwoColumns(firstBox: TcxComboBox; secondBox: TcxComboBox);
    function getExpressionBox(gr: TdxLayoutGroup): TcxTextEdit;
  public
    { Public declarations }

    unboundColumn: TUnboundColumn;
    gridView: TcxGridTableView;

    allFieldTypes: TDictionary<String, TDataTypes>;
    allFieldNames: TDictionary<String, String>;
    allFieldCapts: TDictionary<String, String>;
    allFieldValues: TDictionary<String, Variant>;

    function indexOfGroup(control: TControl): Integer;
    procedure deleteGroupItems(ALayoutGroup: TdxLayoutGroup);
    procedure addGroupItems(gr: TdxLayoutGroup; condition: TConditionUnbound);
    function getGroupCondition(ALayoutGroup: TdxLayoutGroup): TConditionUnbound;
    procedure initFields(viewInfo: TcxGridTableDataCellViewInfo);
  end;

var
  frmDialog: TfrmColumnUnbound;
  conditionExps: TList<String>;
  conditionOperands: TList<TOperands>;
  currentIdxCondition: Integer;

implementation

{$R *.dfm}

// Get column type
function TfrmColumnUnbound.getColumnType(columnName: String): TDataTypes;
begin
  Result := dtUnknown;
  if allFieldTypes.ContainsKey(columnName) then
    Result := allFieldTypes[columnName];
end;

procedure TfrmColumnUnbound.initFields(viewInfo: TcxGridTableDataCellViewInfo);
var
  I, idx: Integer;
  ftype: TDataTypes;
  column: TcxGridColumn;
  fvalue: Variant;
  colCapt, fieldName: String;
begin
  if (not Assigned(viewInfo)) or (not Assigned(viewInfo.GridRecord)) then
    exit;
  allFieldNames := TDictionary<String, String>.Create;
  allFieldCapts := TDictionary<String, String>.Create;
  allFieldTypes := TDictionary<String, TDataTypes>.Create;
  allFieldValues := TDictionary<String, Variant>.Create;
  for I := 0 to viewInfo.GridRecord.ValueCount - 1 do
  begin
    column := viewInfo.gridView.Items[I] as TcxGridColumn;
    if (viewInfo.Item.index <> I) {and column.Visible} then //Question we might need fields that are loaded through Column Chooser
    begin
      fvalue := viewInfo.GridRecord.Values[I];
      ftype := valueType(fvalue, column.DataBinding.valueType);
      if checkType(ftype) then
      begin
        allFieldTypes.Add(column.Name, ftype);
        fieldName := getFieldName(column);
        if not fieldName.IsEmpty then
        begin
          allFieldNames.Add(column.Name, fieldName);
          if column.Caption.IsEmpty then
            allFieldCapts.Add(column.Name, fieldName)
          else
          begin
            colCapt := column.Caption;
            idx := 1;
            while containsValue(colCapt, allFieldCapts) do
            begin
              colCapt := Format('%s_%d', [column.Caption, idx]);
              Inc(idx);
            end;
            allFieldCapts.Add(column.Name, colCapt);
          end;
        end
        else
        begin
          allFieldNames.Add(column.Name, column.Name); // unbound
          if column.Caption.IsEmpty then
            allFieldCapts.Add(column.Name, column.Name)
          else
          begin
            colCapt := column.Caption;
            idx := 1;
            while containsValue(colCapt, allFieldCapts) do
            begin
              colCapt := Format('%s_%d', [column.Caption, idx]);
              Inc(idx);
            end;
            allFieldCapts.Add(column.Name, colCapt);
          end;
        end;
        if valueType(fvalue) = dtUnknown then
        begin
          case getColumnType(column.Name) of
            dtInt:
              allFieldValues.Add(column.Name, 0);
            dtFloat .. dtCurrency:
              allFieldValues.Add(column.Name, Double.NaN);
            dtDate:
              allFieldValues.Add(column.Name, Date);
            dtString:
              allFieldValues.Add(column.Name, String.Empty);
          end;
        end
        else
          allFieldValues.Add(column.Name, fvalue);
      end;
    end;
  end;
end;

// delete group
procedure TfrmColumnUnbound.deleteGroupItems(ALayoutGroup: TdxLayoutGroup);
var
  I: Integer;
  AItem: TdxCustomLayoutItem;
begin
  for I := ALayoutGroup.Count - 1 downto 0 do
  begin
    AItem := ALayoutGroup.Items[I];
    if (AItem is TdxLayoutItem) and (TdxLayoutItem(AItem).control <> nil) then
      TdxLayoutItem(AItem).control.Free
    else if (AItem is TdxLayoutGroup) then
      deleteGroupItems(TdxLayoutGroup(AItem));
  end;
  while (ALayoutGroup.Count > 0) do
    ALayoutGroup.Items[0].Free;
end;

// set exp
procedure TfrmColumnUnbound.setConditionColors();
var
  condExp: String;
  I, maxIdx, minIdx: Integer;
begin
  maxIdx := conditionExps.Count - 1;
  minIdx := 0;
  condExp := conditionExps[currentIdxCondition];
  if conditionOperands[currentIdxCondition] = opAnd_Op then
  begin
    for I := currentIdxCondition + 1 to maxIdx do
    begin
      conditionExps[I] := condExp;
      setButtonEditExp(I);
      if (conditionOperands[I] = opNone) or (conditionOperands[I] = opOr_Op) then
        break;
    end;
  end;
  for I := currentIdxCondition - 1 downto minIdx do
  begin
    if conditionOperands[I] = opOr_Op then
      break;
    conditionExps[I] := condExp;
    setButtonEditExp(I);
  end;
end;

procedure TfrmColumnUnbound.setButtonEditExp(index: Integer);
var
  gr, gr1, gr2: TdxLayoutGroup;
  buttonEdit: TcxTextEdit;
begin
  gr := TdxLayoutGroup(dxLayoutControl1.Items.Items[index + itemsUConditionShiftLength]);
  gr1 := gr.Items[0] as TdxLayoutGroup;
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  buttonEdit := TcxTextEdit((gr2.Items[2] as TdxLayoutItem).control);
  buttonEdit.text := conditionExps[index];
end;

// change other column value
procedure TfrmColumnUnbound.cxComboBox1PropertiesChange(Sender: TObject);
var
  idxOfGroup, idxOfItem: Integer;
  gr1, gr2, gr3: TdxLayoutGroup;
  conditionType: TDataTypes;
  Name, Item: String;
  fisrtBox, secondBox, conditionBox: TcxComboBox;
begin
  idxOfGroup := indexOfGroup(TcxComboBox(Sender));
  idxOfItem := dxLayoutControl1.FindItem(TcxComboBox(Sender)).index;
  gr1 := TdxLayoutGroup(dxLayoutControl1.Items.Items[idxOfGroup]);
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  gr3 := gr2.Items[1] as TdxLayoutGroup;
  fisrtBox := TcxComboBox((gr3.Items[0] as TdxLayoutItem).control);
  conditionBox := TcxComboBox((gr3.Items[1] as TdxLayoutItem).control);
  secondBox := TcxComboBox((gr3.Items[2] as TdxLayoutItem).control);

  name := fisrtBox.text;
  conditionType := getColumnType(getKeyByValue(name, allFieldCapts));

  conditionBox.Properties.Items.Clear;
  conditionBox.Properties.Items.AddStrings(conditions);
  if conditionType = dtString then
    conditionBox.Properties.Items.Add(containsCondition);
  if (idxOfItem = 0) and (gr3.Items[2] as TdxLayoutItem).Visible then
  begin
    secondBox.Properties.Items.Clear;
    for Item in fisrtBox.Properties.Items do
    begin
      if Item <> name then
        secondBox.Properties.Items.Add(Item);
    end;
  end;

  dxLayoutControl1.BeginUpdate;

  (gr3.Items[3] as TdxLayoutItem).Visible := conditionType = dtDate; // value
  if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) then
  begin
    (gr3.Items[4] as TdxLayoutItem).Visible := True;
    if (conditionType = dtInt) then
      TcxSpinEdit((gr3.Items[4] as TdxLayoutItem).control).Properties.valueType := vtInt
    else
      TcxSpinEdit((gr3.Items[4] as TdxLayoutItem).control).Properties.valueType := vtFloat;
  end
  else
    (gr3.Items[4] as TdxLayoutItem).Visible := False;
  (gr3.Items[5] as TdxLayoutItem).Visible := conditionType = dtString;

  dxLayoutControl1.EndUpdate;

  editorFocusChanged(Sender);
end;

// change unbound expression mode
procedure TfrmColumnUnbound.editExpModeValueClick(Sender: TObject);
var
  Item: TdxLayoutItem;
  gr: TdxLayoutGroup;
  I, Count: Integer;
  condition: TConditionUnbound;
  sqlName: String;
begin
  if changing then
    exit;

  dxLayoutControl1.BeginUpdate;
  Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
  if (Item.index = 0) or (Item.index = 2) or (Item.index = 4) then // value or sql
  begin
    I := itemsUConditionShiftLength;
    Count := dxLayoutControl1.Items.Count;
    while I < Count do
    begin
      gr := dxLayoutControl1.Items.Items[itemsUConditionShiftLength] as TdxLayoutGroup;
      if I = itemsUConditionShiftLength then
      begin
        condition := getGroupCondition(gr);
        if (Item.index = 2) or (Item.index = 4) then
        begin
          sqlName := Copy(condition.expression, 2, length(condition.expression) - 2);
          sqlName := Copy(sqlName, sqlName.IndexOf('_') + 2, sqlName.length);
          if unboundColumn.sqlStatements.ContainsKey(sqlName) or unboundColumn.wsStatements.ContainsKey(sqlName) then
            editMainExpression.text := condition.expression;
        end
        else
          editMainExpression.text := condition.expression;
      end;
      deleteGroupItems(gr);
      Height := Height - gr.viewInfo.ClientBounds.Height;
      gr.Free;
      Inc(I);
    end;
    conditionOperands.Clear;
    conditionExps.Clear;
    currentIdxCondition := -1;
    layoutExpression.Visible := True;
    if Count <> itemsUConditionShiftLength then
      Height := Height + 50;
  end
  else if (Item.index = 1) or (Item.index = 3) then // condition or sql condition
  begin
    if layoutExpression.Visible then
    begin
      Height := Height - 50;
      layoutExpression.Visible := False;
    end;
    I := itemsUConditionShiftLength;
    Count := dxLayoutControl1.Items.Count;
    while I < Count do
    begin
      if Item.index = 1 then
        break;
      gr := dxLayoutControl1.Items.Items[I] as TdxLayoutGroup;
      condition := getGroupCondition(gr);
      sqlName := Copy(condition.expression, 2, length(condition.expression) - 2);
      sqlName := Copy(sqlName, sqlName.IndexOf('_') + 2, sqlName.length);
      if (not sqlName.IsEmpty) and (not unboundColumn.sqlStatements.ContainsKey(sqlName)) and
        (not unboundColumn.wsStatements.ContainsKey(sqlName)) then
      begin
        deleteGroupItems(gr);
        Height := Height - gr.viewInfo.ClientBounds.Height;
        gr.Free;
        Dec(Count);
      end
      else
        Inc(I);
    end;
    if dxLayoutControl1.Items.Count = itemsUConditionShiftLength then
    begin
      if (Item.index = 3) or (Item.index = 5) then
      begin
        sqlName := Copy(editMainExpression.text, 2, length(editMainExpression.text) - 2);
        sqlName := Copy(sqlName, sqlName.IndexOf('_') + 2, sqlName.length);
        if unboundColumn.sqlStatements.ContainsKey(sqlName) or unboundColumn.wsStatements.ContainsKey(sqlName) then
          addEmptyCondition(editMainExpression.text)
        else
          addEmptyCondition;
      end
      else
        addEmptyCondition(editMainExpression.text);
    end;
  end;
  dxLayoutControl1.EndUpdate;
  if ((Item.index = 0) or (Item.index = 1)) and (unboundColumn.sqlUpdateStatements.Count > 0) then
    unboundColumn.sqlUpdateStatements.Clear
  else if ((Item.index = 0) or (Item.index = 1)) and (unboundColumn.wsPostStatements.Count > 0) then
    unboundColumn.wsPostStatements.Clear;
end;

procedure TfrmColumnUnbound.addEmptyCondition(expression: String);
var
  condition: TConditionUnbound;
  gr: TdxLayoutGroup;
begin
  condition.mode := cmOtherColumn;
  condition.columnCaption1 := allFieldCapts.Values.ToArray()[0];
  condition.condition := '=';
  condition.operand := opNone;
  condition.value := allFieldValues.Values.ToArray()[0];
  condition.expression := expression;

  gr := dxLayoutControl1.Items.CreateGroup;
  gr.ShowBorder := False;
  gr.index := itemsUConditionShiftLength;
  conditionOperands.Add(condition.operand);
  conditionExps.Add(condition.expression);
  currentIdxCondition := gr.index - itemsUConditionShiftLength;
  addGroupItems(gr, condition);
  if gr.viewInfo = nil then
    Height := Height + 75
  else
    Height := Height + gr.viewInfo.ClientBounds.Height;
end;

// change focus of condition
procedure TfrmColumnUnbound.editorFocusChanged(Sender: TObject);
var
  control: TControl;
  condGroupIndex: Integer;
begin
  control := TControl(Sender);
  if control <> nil then
  begin
    condGroupIndex := indexOfGroup(control);
    if (condGroupIndex <> -1) and ((condGroupIndex - itemsUConditionShiftLength) <> currentIdxCondition) then
    begin
      currentIdxCondition := condGroupIndex - itemsUConditionShiftLength;
    end;
  end;
end;

function TfrmColumnUnbound.indexOfGroup(control: TControl): Integer;
var
  I: Integer;
  Item: TdxLayoutItem;
  group: TdxCustomLayoutGroup;
begin
  Result := -1;
  if control <> nil then
  begin
    Item := dxLayoutControl1.FindItem(control);
    if Item <> nil then
    begin
      group := Item.Parent;
      while group.Parent <> dxLayoutControl1.Items do
        group := group.Parent;
      for I := 0 to dxLayoutControl1.Items.Count - 1 do
      begin
        if dxLayoutControl1.Items.Items[I] = group then
        begin
          Result := I;
          break;
        end;
      end;
    end;
  end;
end;

// set enable None
procedure TfrmColumnUnbound.setNoneEnable(index: Integer; locking: Boolean);
var
  gr1, gr2: TdxLayoutGroup;
begin
  gr1 := dxLayoutControl1.Items.Items[index] as TdxLayoutGroup;
  gr2 := gr1.Items[1] as TdxLayoutGroup;
  (gr2.Items[0] as TdxLayoutItem).Enabled := not locking;
end;

function TfrmColumnUnbound.getExpressionBox(gr: TdxLayoutGroup): TcxTextEdit;
var
  gr1, gr2: TdxLayoutGroup;
begin
  gr1 := gr.Items[0] as TdxLayoutGroup;
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  Result := TcxTextEdit((gr2.Items[2] as TdxLayoutItem).control);
end;

// getting condition values
function TfrmColumnUnbound.getGroupCondition(ALayoutGroup: TdxLayoutGroup): TConditionUnbound;
var
  condition: TConditionUnbound;
  gr1, gr2, gr3, gr4: TdxLayoutGroup;
  conditionType: TDataTypes;
begin
  gr1 := ALayoutGroup.Items[0] as TdxLayoutGroup;

  gr2 := gr1.Items[0] as TdxLayoutGroup;
  if TcxRadioButton((gr2.Items[0] as TdxLayoutItem).control).Checked then
    condition.mode := cmOtherColumn
  else
    condition.mode := cmColumns;

  condition.expression := TcxTextEdit((gr2.Items[2] as TdxLayoutItem).control).text;

  gr3 := gr1.Items[1] as TdxLayoutGroup;
  conditionType := dtUnknown;
  if (gr3.Items[0] as TdxLayoutItem).Visible then
  begin
    condition.columnCaption1 := TcxComboBox((gr3.Items[0] as TdxLayoutItem).control).text;
    conditionType := getColumnType(getKeyByValue(condition.columnCaption1, allFieldCapts));
  end;
  condition.condition := TcxComboBox((gr3.Items[1] as TdxLayoutItem).control).text;
  if (gr3.Items[2] as TdxLayoutItem).Visible then
    condition.columnCaption2 := TcxComboBox((gr3.Items[2] as TdxLayoutItem).control).text;
  if conditionType = dtDate then
    condition.value := TcxDateEdit((gr3.Items[3] as TdxLayoutItem).control).Date
  else if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) then
    condition.value := TcxSpinEdit((gr3.Items[4] as TdxLayoutItem).control).value
  else if conditionType = dtString then
    condition.value := TcxTextEdit((gr3.Items[5] as TdxLayoutItem).control).text;

  gr4 := ALayoutGroup.Items[1] as TdxLayoutGroup;
  if TcxRadioButton((gr4.Items[0] as TdxLayoutItem).control).Checked then
    condition.operand := opNone
  else if TcxRadioButton((gr4.Items[1] as TdxLayoutItem).control).Checked then
    condition.operand := opAnd_Op
  else
    condition.operand := opOr_Op;
  Result := condition;
end;

// add controls according to condition
procedure TfrmColumnUnbound.addGroupItems(gr: TdxLayoutGroup; condition: TConditionUnbound);
var
  gr1, gr2, gr3, gr4: TdxLayoutGroup;
  it: TdxLayoutItem;
  ctr12: TcxRadioButton;
  ctr13: TcxRadioButton;
  ctr14: TcxTextEdit;
  ctr141: TcxButton;
  ctr142: TcxButton;
  ctr2: TcxComboBox;
  ctr3: TcxComboBox;
  ctr4: TcxComboBox;
  ctr51: TcxSpinEdit;
  ctr52: TcxDateEdit;
  ctr53: TcxTextEdit;
  ctr6: TcxRadioButton;
  ctr7: TcxRadioButton;
  ctr8: TcxRadioButton;
  conditionType: TDataTypes;
begin
  dxLayoutControl1.BeginUpdate;

  gr1 := gr.CreateGroup;
  gr1.ShowBorder := False;
  gr1.LayoutDirection := ldVertical;

  gr2 := gr1.CreateGroup;
  gr2.ShowBorder := False;
  gr2.LayoutDirection := ldHorizontal;

  ctr13 := TcxRadioButton.Create(dxLayoutControl1);
  ctr13.Caption := 'other column';
  ctr13.GroupIndex := gr.index + 3;
  ctr13.Checked := condition.mode = cmOtherColumn;
  ctr13.Width := 85;
  ctr13.OnClick := cxRadioButton3Click;
  gr2.CreateItemForControl(ctr13).Enabled := allFieldNames.Count > 0;

  ctr12 := TcxRadioButton.Create(dxLayoutControl1);
  ctr12.Caption := 'two columns';
  ctr12.GroupIndex := gr.index + 3;
  ctr12.Checked := condition.mode = cmColumns;
  ctr12.Width := 82;
  ctr12.OnClick := cxRadioButton3Click;
  gr2.CreateItemForControl(ctr12).Enabled := allFieldNames.Count > 1;

  ctr14 := TcxTextEdit.Create(dxLayoutControl1);
  ctr14.text := condition.expression;
  ctr14.OnFocusChanged := editorFocusChanged;
  ctr14.Width := 250;
  ctr14.Properties.ReadOnly := True;
  gr2.CreateItemForControl(ctr14).AlignHorz := ahRight;

  ctr141 := TcxButton.Create(dxLayoutControl1);
  ctr141.Caption := 'Edit';
  ctr141.OnClick := btnEditMainExpClick;
  ctr141.Width := 33;
  gr2.CreateItemForControl(ctr141).AlignHorz := ahRight;

  ctr142 := TcxButton.Create(dxLayoutControl1);
  ctr142.Caption := 'Clear';
  ctr142.OnClick := btnClearMainExpClick;
  ctr142.Width := 33;
  gr2.CreateItemForControl(ctr142).AlignHorz := ahRight;

  gr3 := gr1.CreateGroup;
  gr3.ShowBorder := False;
  gr3.LayoutDirection := ldHorizontal;

  ctr2 := TcxComboBox.Create(dxLayoutControl1); // first column
  ctr2.Properties.DropDownListStyle := lsEditFixedList;
  ctr2.Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
  if (condition.columnCaption1 = '') and (allFieldNames.Count > 0) then
    ctr2.text := allFieldCapts.Values.ToArray()[0]
  else
    ctr2.text := condition.columnCaption1;
  conditionType := getColumnType(getKeyByValue(ctr2.text, allFieldCapts));
  ctr2.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr2.Properties.OnChange := cxComboBox1PropertiesChange;
  ctr2.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr2);
  it.Visible := (condition.mode = cmOtherColumn) or (condition.mode = cmColumns);
  it.AlignHorz := ahClient;

  ctr3 := TcxComboBox.Create(dxLayoutControl1); // condition
  ctr3.Properties.DropDownListStyle := lsEditFixedList;
  ctr3.Properties.Items.AddStrings(conditions);
  if conditionType = dtString then
    ctr3.Properties.Items.Add(containsCondition);
  ctr3.text := condition.condition;
  ctr3.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr3.OnFocusChanged := editorFocusChanged;
  gr3.CreateItemForControl(ctr3).AlignHorz := ahClient;

  ctr4 := TcxComboBox.Create(dxLayoutControl1); // second column
  ctr4.Properties.DropDownListStyle := lsEditFixedList;
  ctr4.Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
  if allFieldNames.Count > 1 then
  begin
    if condition.columnCaption1.IsEmpty then
      ctr4.Properties.Items.Delete(ctr4.Properties.Items.IndexOf(condition.columnName1))
    else
      ctr4.Properties.Items.Delete(ctr4.Properties.Items.IndexOf(condition.columnCaption1))
  end;
  if (condition.columnCaption2 = '') and (allFieldNames.Count > 1) then
    ctr4.text := allFieldCapts.Values.ToArray()[1]
  else
    ctr4.text := condition.columnCaption2;
  ctr4.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr4.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr4);
  it.Visible := condition.mode = cmColumns;
  it.AlignHorz := ahClient;

  ctr52 := TcxDateEdit.Create(dxLayoutControl1);
  if conditionType = dtDate then
    ctr52.Date := condition.value
  else
    ctr52.Date := Date;
  ctr52.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr52.OnFocusChanged := editorFocusChanged;
  if condition.mode = cmColumns then
    ctr52.Properties.DisplayFormat := dateFormat
  else
    ctr52.Properties.DisplayFormat := '';
  it := gr3.CreateItemForControl(ctr52);
  it.Visible := conditionType = dtDate;
  it.AlignHorz := ahClient;

  ctr51 := TcxSpinEdit.Create(dxLayoutControl1);
  if (conditionType = dtInt) then
    ctr51.Properties.valueType := vtInt
  else if (conditionType in [dtFloat, dtCurrency]) then
    ctr51.Properties.valueType := vtFloat;
  if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) then
    ctr51.value := condition.value;
  ctr51.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr51.OnFocusChanged := editorFocusChanged;
  if condition.mode = cmColumns then
    ctr51.Properties.DisplayFormat := numFormat
  else
    ctr51.Properties.DisplayFormat := '';
  it := gr3.CreateItemForControl(ctr51);
  it.Visible := ((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]));
  it.AlignHorz := ahClient;

  ctr53 := TcxTextEdit.Create(dxLayoutControl1);
  if conditionType = dtString then
    ctr53.text := condition.value;
  ctr53.Properties.OnEditValueChanged := cxComboBox1PropertiesEditValueChanged;
  ctr53.OnFocusChanged := editorFocusChanged;
  it := gr3.CreateItemForControl(ctr53);
  it.Visible := conditionType = dtString;
  it.AlignHorz := ahClient;

  gr4 := gr.CreateGroup;
  gr4.ShowBorder := False;
  gr4.LayoutDirection := ldHorizontal;
  gr4.AlignHorz := ahRight;

  ctr6 := TcxRadioButton.Create(dxLayoutControl1);
  ctr6.Caption := 'None';
  ctr6.GroupIndex := gr.index + 4;
  ctr6.Checked := condition.operand = opNone;
  ctr6.Width := 50;
  ctr6.OnClick := cxRadioButton1Click;
  gr4.CreateItemForControl(ctr6);

  ctr7 := TcxRadioButton.Create(dxLayoutControl1);
  ctr7.Caption := 'AND';
  ctr7.GroupIndex := gr.index + 4;
  ctr7.Checked := condition.operand = opAnd_Op;
  ctr7.Width := 50;
  ctr7.OnClick := cxRadioButton2Click;
  gr4.CreateItemForControl(ctr7);

  ctr8 := TcxRadioButton.Create(dxLayoutControl1);
  ctr8.Caption := 'OR';
  ctr8.GroupIndex := gr.index + 4;
  ctr8.Checked := condition.operand = opOr_Op;
  ctr8.Width := 50;
  ctr8.OnClick := cxRadioButton2Click;
  gr4.CreateItemForControl(ctr8);

  dxLayoutControl1.EndUpdate;
end;

{ ****************************************************************************** }

// Clear expression
procedure TfrmColumnUnbound.btnClearMainExpClick(Sender: TObject);
var
  textEdit: TcxTextEdit;
  Item: TdxLayoutItem;
  gr: TdxLayoutGroup;
  sqlName: String;
begin
  Item := dxLayoutControl1.FindItem(TcxButton(Sender));
  gr := Item.Parent as TdxLayoutGroup;
  Item := gr.Items[Item.index - 2] as TdxLayoutItem;
  textEdit := TcxTextEdit(Item.control);
  if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked then
  begin
    sqlName := Copy(textEdit.text, 2, length(textEdit.text) - 2);
    if unboundColumn.sqlUpdateStatements.ContainsKey(sqlName) then
      unboundColumn.sqlUpdateStatements.Remove(sqlName);
  end;
  textEdit.text := '';
end;

// Edit expression
function TfrmColumnUnbound.editDataOnly(text: String): Boolean;
var
  sqlName: String;
begin
  Result := False;
  if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked then
  begin
    lstSQL.Items.Clear;
    lstSQL.Items.AddStrings(unboundColumn.sqlStatements.Keys.ToArray());
    if (not string.IsNullOrEmpty(text)) and (length(text) > 2) then
    begin
      sqlName := Copy(text, 2, length(text) - 2);
      if unboundColumn.sqlStatements.ContainsKey(sqlName) then
        editSQLExp.text := unboundColumn.sqlStatements[sqlName];
      if unboundColumn.sqlUpdateStatements.ContainsKey(sqlName) then
        editSQLUpdate.text := unboundColumn.sqlUpdateStatements[sqlName];
      if unboundColumn.updatePeriods.ContainsKey(sqlName) then
        editSQLUpdateLimit.value := unboundColumn.updatePeriods[sqlName];
      lstSQL.ItemIndex := lstSQL.Items.IndexOf(sqlName);
    end;
    dxLayoutControl.BeginUpdate;
    layoutTopSQL.Enabled := lstSQL.Items.Count > 0;
    //layoutBottomSQL.Enabled := layoutTopSQL.Enabled;
    layoutStaleData.Enabled := layoutTopSQL.Enabled;
    layoutSetSQL.Enabled := layoutTopSQL.Enabled;
    layoutDeleteSQLButton.Enabled := layoutTopSQL.Enabled;
    layoutSQLExp.Enabled := layoutTopSQL.Enabled;
    SQLPageControl.Properties.HideTabs := False;
    layoutBottom.Visible := False;
    tshSQLEditor.Show;
    tshSQLSelect.Show;
    dxLayoutControl.EndUpdate;
    Result := True;
  end
  else if editExpModeWSValue.Checked or editExpModeWSCondition.Checked then
  begin
    lstWS.Items.Clear;
    lstWS.Items.AddStrings(unboundColumn.wsStatements.Keys.ToArray());
    if (not string.IsNullOrEmpty(text)) and (length(text) > 2) then
    begin
      sqlName := Copy(text, 2, length(text) - 2);
      if unboundColumn.wsStatements.ContainsKey(sqlName) then
        editWSExp.text := unboundColumn.wsStatements[sqlName];
      if unboundColumn.wsPostStatements.ContainsKey(sqlName) then
        editWSPost.text := unboundColumn.wsPostStatements[sqlName];
      if unboundColumn.updatePeriods.ContainsKey(sqlName) then
        editWSUpdateLimit.value := unboundColumn.updatePeriods[sqlName];
      lstWS.ItemIndex := lstWS.Items.IndexOf(sqlName);
    end;
    dxLayoutControl.BeginUpdate;
    layoutTopWS.Enabled := lstWS.Items.Count > 0;
    layoutBottomWS.Enabled := layoutTopWS.Enabled;
    layoutDeleteWSButton.Enabled := layoutTopWS.Enabled;
    layoutWSExp.Enabled := layoutTopWS.Enabled;
    WSPageControl.Properties.HideTabs := False;
    layoutBottom.Visible := False;
    tshWSEditor.Show;
    tshWSGet.Show;
    dxLayoutControl.EndUpdate;
    Result := True;
  end;
end;

procedure TfrmColumnUnbound.btnEditMainExpClick(Sender: TObject);
var
  textEdit: TcxTextEdit;
  Item: TdxLayoutItem;
  gr: TdxLayoutGroup;
  expType: TDataTypes;
  varValue: Variant;
  strType: TDataTypes;
  Values: TArray<Variant>;
  types: TArray<TDataTypes>;
  names: TArray<String>;
  I: Integer;
begin
  Item := dxLayoutControl1.FindItem(TcxButton(Sender));
  gr := Item.Parent as TdxLayoutGroup;
  Item := gr.Items[Item.index - 1] as TdxLayoutItem;
  textEdit := TcxTextEdit(Item.control);
  textCurrent := textEdit;

  if editDataOnly(textEdit.text) then
    exit;

  editExp.text := textEdit.text;
  expType := TDataTypes(editType.ItemIndex + 1);
  Values := allFieldValues.Values.ToArray();
  types := allFieldTypes.Values.ToArray();
  names := allFieldCapts.Values.ToArray();
  editFieldName.Properties.Items.Clear;
  for I := Low(names) to High(names) do
  begin
    strType := dtUnknown;
    if types[I] = dtString then
      strType := getStrType(Values[I], varValue);
    if (expType = dtInt) and ((types[I] = dtInt) or ((types[I] = dtString) and (strType = dtInt))) then
      editFieldName.Properties.Items.Add(names[I])
    else if (expType in [dtFloat, dtCurrency]) and ((types[I] in [dtFloat, dtCurrency]) or (types[I] = dtInt) or
      ((types[I] = dtString) and ((strType in [dtFloat, dtCurrency]) or (strType = dtInt)))) then
      editFieldName.Properties.Items.Add(names[I])
    else if (expType = dtDate) and ((types[I] = dtDate) or (types[I] = dtInt) or
      ((types[I] = dtString) and ((strType = dtDate) or (strType = dtInt)))) then
      editFieldName.Properties.Items.Add(names[I])
    else if expType = dtString then
      editFieldName.Properties.Items.Add(names[I]);
  end;
  editFieldName.ItemIndex := 0;
  editTypeValue.Properties.Items.Clear;
  Case expType of
    dtInt:
      editTypeValue.Properties.Items.AddStrings(['integer']);
    dtFloat .. dtCurrency:
      editTypeValue.Properties.Items.AddStrings(['double', 'integer']);
    dtDate:
      editTypeValue.Properties.Items.AddStrings(['date', 'integer']);
    dtString:
      editTypeValue.Properties.Items.AddStrings(['string', 'integer', 'double', 'date']);
  End;
  editTypeValue.ItemIndex := 0;

  btnPasteDate.Enabled := (expType = dtDate) or (expType = dtString);
  btnPasteMin.Enabled := expType <> dtString;
  bntPasteMul.Enabled := expType <> dtString;

  dxLayoutControl.BeginUpdate;
  layoutBottom.Visible := False;
  tshExpEditor.Show;
  dxLayoutControl.EndUpdate;
  if editExp.Enabled then
    editExp.SetFocus;
end;

// Back from edit
procedure TfrmColumnUnbound.btnBackExpClick(Sender: TObject);
begin
  dxLayoutControl.BeginUpdate;
  layoutBottom.Visible := True;
  tshExpressions.Show;
  dxLayoutControl.EndUpdate;
  textCurrent.SetFocus;
end;

// Change expression edit
procedure TfrmColumnUnbound.btnSetExpClick(Sender: TObject);
begin
  textCurrent.text := editExp.text;
  dxLayoutControl.BeginUpdate;
  layoutBottom.Visible := True;
  tshExpressions.Show;
  dxLayoutControl.EndUpdate;
  textCurrent.SetFocus;
end;

procedure TfrmColumnUnbound.editTypePropertiesChange(Sender: TObject);
var
  I: Integer;
  IsEmpty: Boolean;
begin
  if changing then
    exit;
  if editExpModeValue.Checked and String.IsNullOrEmpty(editMainExpression.text) then
    exit
  else if editExpModeCondition.Checked then
  begin
    IsEmpty := True;
    for I := itemsUConditionShiftLength to dxLayoutControl1.Items.Count - 1 do
    begin
      if not String.IsNullOrEmpty(getExpressionBox(dxLayoutControl1.Items.Items[I] as TdxLayoutGroup).text) then
      begin
        IsEmpty := False;
        break;
      end;
    end;
    if IsEmpty then
      exit;
  end;
  if messagedlg('Are you sure, do change type? After change type all expressions will lost', mtConfirmation,
    [mbYes, mbNo], 0) = mrYes then
  begin
    if editExpModeValue.Checked then
      editMainExpression.text := String.Empty
    else
    begin
      for I := itemsUConditionShiftLength to dxLayoutControl1.Items.Count - 1 do
        getExpressionBox(dxLayoutControl1.Items.Items[I] as TdxLayoutGroup).text := String.Empty;
    end;
  end
  else
  begin
    changing := True;
    editType.text := dataTypeNames[Ord(unboundColumn.ctype) - 1];
    changing := False;
    exit;
  end;
  unboundColumn.ctype := TDataTypes(editType.ItemIndex + 1);
  layoutMask.Visible := unboundColumn.ctype in [dtString, dtCurrency];
end;

procedure TfrmColumnUnbound.editTypeValuePropertiesChange(Sender: TObject);
var
  idx: Integer;
  expType: TDataTypes;
begin
  idx := indexOfArray(dataTypeNames, editTypeValue.text);
  if idx <> -1 then
  begin
    expType := TDataTypes(idx + 1);
    dxLayoutControl2.BeginUpdate;
    layoutText.Visible := expType = dtString;
    layoutNumber.Visible := ((expType = dtInt) or (expType in [dtFloat, dtCurrency]));
    layoutDate.Visible := expType = dtDate;
    dxLayoutControl2.EndUpdate;
    Case expType of
      dtInt:
        editNumberValue.Properties.valueType := vtInt;
      dtFloat .. dtCurrency:
        editNumberValue.Properties.valueType := vtFloat;
    End;
  end;
end;

procedure TfrmColumnUnbound.rbSQLClick(Sender: TObject);
var
  Item: TdxLayoutItem;
begin
  Item := dxLayoutControl2.FindItem(TcxRadioButton(Sender));
  editSQLName.Properties.Items.Clear;
  if Item.index = 2 then
  begin
    layoutAddSQL.Caption := 'Add SQL';
    layoutSQLName.Caption := 'SQL name:';
    if unboundColumn.sqlStatements.Count > 0 then
    begin
      editSQLName.Properties.Items.AddStrings(unboundColumn.sqlStatements.Keys.ToArray());
      editSQLName.ItemIndex := 0;
    end;
  end
  else
  begin
    layoutAddSQL.Caption := 'Add WS';
    layoutSQLName.Caption := 'WS name:';
    if unboundColumn.wsStatements.Count > 0 then
    begin
      editSQLName.Properties.Items.AddStrings(unboundColumn.wsStatements.Keys.ToArray());
      editSQLName.ItemIndex := 0;
    end;
  end;
  layoutSQLName.Enabled := editSQLName.Properties.Items.Count > 0;
  layoutPasteSQL.Enabled := layoutSQLName.Enabled;
end;

procedure TfrmColumnUnbound.btnPasteFieldClick(Sender: TObject);
var
  fldType, ubcType: TDataTypes;
begin
  ubcType := TDataTypes(editType.ItemIndex + 1);
  fldType := getColumnType(getKeyByValue(editFieldName.text, allFieldCapts));

  if (ubcType = dtString) then
  begin
    if fldType = dtInt then
      editExp.SelText := Format('IntToStr([%s])', [editFieldName.text])
    else if fldType in [dtFloat, dtCurrency] then
      editExp.SelText := Format('FloatToStr([%s])', [editFieldName.text])
    else if fldType = dtDate then
      editExp.SelText := Format('DateToStr([%s])', [editFieldName.text])
    else
      editExp.SelText := Format('[%s]', [editFieldName.text]);
  end
  else if (ubcType = dtDate) then
  begin
    if fldType = dtString then
      editExp.SelText := Format('StrToDate([%s])', [editFieldName.text])
    else
      editExp.SelText := Format('[%s]', [editFieldName.text]);
  end
  else if (ubcType in [dtFloat, dtCurrency]) then
  begin
    if fldType = dtString then
      editExp.SelText := Format('StrToFloat([%s])', [editFieldName.text])
    else
      editExp.SelText := Format('[%s]', [editFieldName.text]);
  end
  else if (ubcType = dtInt) then
  begin
    if fldType = dtString then
      editExp.SelText := Format('StrToInt([%s])', [editFieldName.text])
    else
      editExp.SelText := Format('[%s]', [editFieldName.text]);
  end;
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.btnPasteValueClick(Sender: TObject);
var
  vlType, ubcType: TDataTypes;
  idx: Integer;
begin
  idx := indexOfArray(dataTypeNames, editTypeValue.text);
  ubcType := TDataTypes(editType.ItemIndex + 1);
  vlType := TDataTypes(idx + 1);

  if (ubcType = dtString) then
  begin
    if vlType = dtInt then
      editExp.SelText := Format('IntToStr(%s)', [IntToStr(editNumberValue.value)])
    else if vlType in [dtFloat, dtCurrency] then
      editExp.SelText := Format('FloatToStr(%s)', [FloatToStr(editNumberValue.value)])
    else if vlType = dtDate then
      editExp.SelText := Format('Date(%s)', [DateToStr(editDateValue.Date)])
    else
      editExp.SelText := Format('"%s"', [editTextValue.text]);
  end
  else
  begin
    if vlType = dtDate then
      editExp.SelText := Format('Date(%s)', [DateToStr(editDateValue.Date)])
    else if vlType in [dtFloat, dtCurrency] then
      editExp.SelText := Format('%s', [FloatToStr(editNumberValue.value)])
    else if vlType = dtInt then
      editExp.SelText := Format('%s', [IntToStr(editNumberValue.value)]);
  end;
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.btnPasteSQLClick(Sender: TObject);
begin
  editExp.SelText := Format('[%s]', [editSQLName.text]);
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.btnPasteBraceClick(Sender: TObject);
var
  pos: TPoint;
begin
  pos := TPoint.Create(editExp.SelStart + editExp.SelLength + 2, 0);
  editExp.SelText := Format('(%s)', [editExp.SelText]);
  editExp.CaretPos := pos;
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.btnPasteDateClick(Sender: TObject);

begin
  editExp.SelText := 'Date()';
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.btnPasteAddClick(Sender: TObject);
begin
  editExp.SelText := '+';
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.btnPasteMinClick(Sender: TObject);
begin
  editExp.SelText := '-';
  editExp.SetFocus;
end;

procedure TfrmColumnUnbound.bntPasteMulClick(Sender: TObject);
begin
  editExp.SelText := '*';
  editExp.SetFocus;
end;

// Show SQL/WS editor
procedure TfrmColumnUnbound.btnEditSQLClick(Sender: TObject);
begin
  if rbSQL.Checked then // SQL
  begin
    SQLPageControl.Properties.HideTabs := True;
    tshSQLEditor.Show;
    layoutTopSQL.Enabled := editSQLName.Properties.Items.Count > 0;
    //layoutBottomSQL.Enabled := layoutTopSQL.Enabled;
    layoutStaleData.Enabled := layoutTopSQL.Enabled;
    layoutSetSQL.Enabled := layoutTopSQL.Enabled;
    layoutDeleteSQLButton.Enabled := layoutTopSQL.Enabled;
    layoutSQLExp.Enabled := layoutTopSQL.Enabled;
    lstSQL.ItemIndex := editSQLName.ItemIndex;
    if layoutTopSQL.Enabled then
    begin
      if unboundColumn.sqlStatements.ContainsKey(editSQLName.text) then
        editSQLExp.text := unboundColumn.sqlStatements[editSQLName.text];
      if unboundColumn.updatePeriods.ContainsKey(editSQLName.text) then
        editSQLUpdateLimit.value := unboundColumn.updatePeriods[editSQLName.text];
      lstSQL.Items := editSQLName.Properties.Items;
      lstSQL.ItemIndex := editSQLName.ItemIndex;
    end;
  end
  else
  begin
    tshWSEditor.Show;
    layoutTopWS.Enabled := editSQLName.Properties.Items.Count > 0;
    layoutBottomWS.Enabled := layoutTopWS.Enabled;
    layoutDeleteWSButton.Enabled := layoutTopWS.Enabled;
    layoutWSExp.Enabled := layoutTopWS.Enabled;
    lstWS.ItemIndex := editSQLName.ItemIndex;
    if layoutTopWS.Enabled then
    begin
      if unboundColumn.wsStatements.ContainsKey(editSQLName.text) then
        editWSExp.text := unboundColumn.wsStatements[editSQLName.text];
      if unboundColumn.updatePeriods.ContainsKey(editSQLName.text) then
        editWSUpdateLimit.value := unboundColumn.updatePeriods[editSQLName.text];
      lstWS.Items := editSQLName.Properties.Items;
      lstWS.ItemIndex := editSQLName.ItemIndex;
    end;
  end;
end;

procedure TfrmColumnUnbound.btnBackFromSQLClick(Sender: TObject);
begin
  if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked or editExpModeWSValue.Checked or
    editExpModeWSCondition.Checked then
  begin
    dxLayoutControl.BeginUpdate;
    layoutBottom.Visible := True;
    tshExpressions.Show;
    dxLayoutControl.EndUpdate;
    textCurrent.SetFocus;
  end
  else
    tshExpEditor.Show;
end;

procedure TfrmColumnUnbound.btnSetSQLClick(Sender: TObject);
begin
  tshExpEditor.Show;
end;

procedure TfrmColumnUnbound.btnSetWSClick(Sender: TObject);
begin
  tshExpEditor.Show;
end;

procedure TfrmColumnUnbound.tshSQLUpdateShow(Sender: TObject);
var
  expText, sqlName, updateField: String;
  posFrom, posWhere: Integer;
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    if SQLPageControl.ActivePage = tshSQLSelect then
    begin
      layoutTrySQL.Enabled := not string.IsNullOrEmpty(editSQLExp.text);
    end
    else if SQLPageControl.ActivePage = tshSQLUpdate then
    begin
      layoutTrySQL.Enabled := not string.IsNullOrEmpty(editSQLUpdate.text);
      expText := editSQLExp.text;
      if (not layoutTrySQL.Enabled) and (not expText.IsEmpty) and ContainsText(expText, 'select') and
        ContainsText(expText, 'from') and ContainsText(expText, 'where') and (lstSQL.ItemIndex >= 0) then
      begin
        sqlName := lstSQL.Items[lstSQL.ItemIndex];
        updateField := Copy(sqlName, sqlName.IndexOf('_') + 2, sqlName.length);
        posFrom := ansipos('from', expText.ToLower());
        expText := expText.Substring(posFrom - 1);
        expText := StringReplace(expText, 'from', 'Update', [rfIgnoreCase]);
        posWhere := ansipos('where', expText.ToLower());
        expText := expText.Insert(posWhere - 1, Format('Set %s=[%s] ', [updateField, updateField]));
        if lstSQL.ItemIndex >= 0 then
          unboundColumn.sqlUpdateStatements.AddOrSetValue(lstSQL.Items[lstSQL.ItemIndex], expText);
        editSQLUpdate.text := expText;
      end;
    end;
  end
  else if cxPageControl.ActivePage = tshWSEditor then
  begin
    if WSPageControl.ActivePage = tshWSGet then
    begin
      layoutTrySQL.Enabled := not string.IsNullOrEmpty(editWSExp.text);
    end
    else if WSPageControl.ActivePage = tshWSPost then
    begin
      layoutTrySQL.Enabled := not string.IsNullOrEmpty(editWSPost.text);
      expText := editWSExp.text;
      if (not layoutTrySQL.Enabled) and (not expText.IsEmpty) and (lstWS.ItemIndex >= 0) then
      begin
        editWSPost.text := expText;
      end;
    end;
  end;
end;

procedure TfrmColumnUnbound.btnTrySQLClick(Sender: TObject);
var
  params: TDictionary<String, Variant>;
  Values: TDictionary<String, Variant>;
  pair: TPair<String, Variant>;
  exception, text, Name, updateValue: String;
  resValue: Variant;
  fields: TList<String>;
begin
  fields := TList<String>.Create;
  Values := TDictionary<String, Variant>.Create;
  for pair in allFieldValues do
  begin
    if allFieldNames.ContainsKey(pair.Key) then
      Values.Add(allFieldNames[pair.Key], pair.value);
  end;
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    name := lstSQL.Items[lstSQL.ItemIndex];
    name := Copy(name, name.IndexOf('_') + 2, name.length);
    params := TDictionary<String, Variant>.Create;

    if SQLPageControl.ActivePage = tshSQLSelect then
    begin
      text := ColumnUnboundHelperData.prepareSQLText(editSQLExp.text, Values, params);
      resValue := ColumnUnboundHelperData.runSQLRequest(text, name, params, fields, exception);

      if not exception.IsEmpty then
        messagedlg(Format('Wrong SQL statement. %s', [exception]), mtInformation, [mbOK], 0)
      else
        messagedlg(Format('SQL statement is OK. Return value is %s', [VarToStr(resValue)]), mtInformation, [mbOK], 0);

      if fields.Count > 0 then
      begin
        editSQLAsstField.Properties.Items.Clear;
        editSQLAsstField.Properties.Items.AddStrings(fields.ToArray());
      end;
    end
    else if SQLPageControl.ActivePage = tshSQLUpdate then
    begin
      if Values.ContainsKey(name) then
        updateValue := VarToStr(Values[name]);
      if InputQuery('Test update statement', Format('Please type value for %s', [name]), updateValue) then
      begin
        Values.AddOrSetValue(name, updateValue);
        text := ColumnUnboundHelperData.prepareSQLText(editSQLUpdate.text, Values, params);
        ColumnUnboundHelperData.runSQLRequest(text, params, exception);

        if not exception.IsEmpty then
          messagedlg(Format('Wrong SQL statement. %s', [exception]), mtInformation, [mbOK], 0)
        else
          messagedlg('SQL statement is OK', mtInformation, [mbOK], 0);
      end;
    end;
    params.Free;
  end
  else
  begin
    name := lstWS.Items[lstWS.ItemIndex];
    name := Copy(name, name.IndexOf('_') + 2, name.length);

    if WSPageControl.ActivePage = tshWSGet then
    begin
      text := ColumnUnboundHelperData.prepareWSText(editWSExp.text, Values);
      resValue := ColumnUnboundHelperData.runWSRequest(text, name, fields, exception);

      if not exception.IsEmpty then
        messagedlg(Format('Wrong WS url. %s', [exception]), mtInformation, [mbOK], 0)
      else
        messagedlg(Format('WS url is OK. Return value is %s', [VarToStr(resValue)]), mtInformation, [mbOK], 0);

      if fields.Count > 0 then
      begin
        editWSAsstField.Properties.Items.Clear;
        editWSAsstField.Properties.Items.AddStrings(fields.ToArray());
      end;
    end
    else if WSPageControl.ActivePage = tshWSPost then
    begin
      if Values.ContainsKey(name) then
        updateValue := VarToStr(Values[name]);
      if InputQuery('Test post url', Format('Please type value for %s', [name]), updateValue) then
      begin
        text := ColumnUnboundHelperData.prepareWSText(editWSPost.text, Values);
        ColumnUnboundHelperData.runWSRequest(text, TJSONObject.Create(TJSONPair.Create(name, updateValue)).ToString,
          exception);

        if not exception.IsEmpty then
          messagedlg(Format('Wrong WS url. %s', [exception]), mtInformation, [mbOK], 0)
        else
          messagedlg('WS statement is OK', mtInformation, [mbOK], 0);
      end;
    end;
  end;
  fields.Free;
  Values.Free;
end;

procedure TfrmColumnUnbound.btnAddSQLClick(Sender: TObject);
var
  Name: String;
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    name := editSQLAsstField.text;
    if name.IsEmpty then
      lstSQL.ItemIndex := lstSQL.Items.Add(Format('SQL%d', [lstSQL.Count + 1]))
    else
      lstSQL.ItemIndex := lstSQL.Items.Add(Format('SQL%d_%s', [lstSQL.Count + 1, name]));
    editSQLName.Properties.Items := lstSQL.Items;
    editSQLName.ItemIndex := lstSQL.ItemIndex;
    unboundColumn.sqlStatements.Add(lstSQL.Items[lstSQL.ItemIndex], editSQLExp.text);
    unboundColumn.updatePeriods.Add(lstSQL.Items[lstSQL.ItemIndex], 60);
    if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked then
    begin
      textCurrent.text := Format('[%s]', [lstSQL.Items[lstSQL.ItemIndex]]);
      unboundColumn.sqlUpdateStatements.Add(lstSQL.Items[lstSQL.ItemIndex], editSQLUpdate.text);
    end;
    layoutTopSQL.Enabled := True;
    //layoutBottomSQL.Enabled := True;
    layoutStaleData.Enabled := True;
    layoutSetSQL.Enabled := True;
    layoutDeleteSQLButton.Enabled := True;
    layoutSQLExp.Enabled := True;
  end
  else
  begin
    name := editWSAsstField.text;
    if name.IsEmpty then
      lstWS.ItemIndex := lstWS.Items.Add(Format('WS%d', [lstWS.Count + 1]))
    else
      lstWS.ItemIndex := lstWS.Items.Add(Format('WS%d_%s', [lstWS.Count + 1, name]));
    editSQLName.Properties.Items := lstWS.Items;
    editSQLName.ItemIndex := lstWS.ItemIndex;
    unboundColumn.wsStatements.Add(lstWS.Items[lstWS.ItemIndex], editWSExp.text);
    unboundColumn.updatePeriods.Add(lstWS.Items[lstWS.ItemIndex], 60);
    if editExpModeWSValue.Checked or editExpModeWSCondition.Checked then
    begin
      textCurrent.text := Format('[%s]', [lstWS.Items[lstWS.ItemIndex]]);
      unboundColumn.wsPostStatements.Add(lstWS.Items[lstWS.ItemIndex], editWSPost.text);
    end;
    layoutTopWS.Enabled := True;
    layoutBottomWS.Enabled := True;
    layoutWSExp.Enabled := True;
    layoutDeleteWSButton.Enabled := True;
  end;
  layoutPasteSQL.Enabled := True;
  layoutSQLName.Enabled := True;
end;

// Delete SQL/WS
procedure TfrmColumnUnbound.btnDelSQLClick(Sender: TObject);
var
  Key: String;
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    Key := lstSQL.Items[lstSQL.ItemIndex];
    unboundColumn.sqlStatements.Remove(Key);
    unboundColumn.updatePeriods.Remove(Key);
    lstSQL.DeleteSelected;
    editSQLName.Properties.Items := lstSQL.Items;
    layoutPasteSQL.Enabled := lstSQL.Count > 0;
    layoutSQLName.Enabled := lstSQL.Count > 0;
    if lstSQL.Count > 0 then
    begin
      lstSQL.ItemIndex := lstSQL.Count - 1;
      editSQLName.ItemIndex := lstSQL.ItemIndex;
    end
    else
    begin
      layoutTopSQL.Enabled := False;
      //layoutBottomSQL.Enabled := False;
      layoutStaleData.Enabled := False;
      layoutSetSQL.Enabled := False;
      layoutDeleteSQLButton.Enabled := False;
      layoutSQLExp.Enabled := False;
      editSQLAsstField.text := '';
      editSQLExp.text := '';
    end;
    if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked then
    begin
      if lstSQL.Count > 0 then
        textCurrent.text := Format('[%s]', [lstSQL.Items[lstSQL.ItemIndex]])
      else
        textCurrent.text := string.Empty;
      unboundColumn.sqlUpdateStatements.Remove(Key);
    end;
  end
  else
  begin
    Key := lstWS.Items[lstWS.ItemIndex];
    unboundColumn.wsStatements.Remove(Key);
    unboundColumn.updatePeriods.Remove(Key);
    lstWS.DeleteSelected;
    editSQLName.Properties.Items := lstWS.Items;
    layoutPasteSQL.Enabled := lstWS.Count > 0;
    layoutSQLName.Enabled := lstWS.Count > 0;
    if lstWS.Count > 0 then
    begin
      lstWS.ItemIndex := lstWS.Count - 1;
      editSQLName.ItemIndex := lstWS.ItemIndex;
    end
    else
    begin
      layoutTopWS.Enabled := False;
      layoutBottomWS.Enabled := False;
      layoutWSExp.Enabled := False;
      layoutDeleteWSButton.Enabled := False;
      editWSAsstField.text := '';
      editWSExp.text := '';
    end;
    if editExpModeWSValue.Checked or editExpModeWSCondition.Checked then
    begin
      if lstWS.Count > 0 then
        textCurrent.text := Format('[%s]', [lstWS.Items[lstWS.ItemIndex]])
      else
        textCurrent.text := string.Empty;
      unboundColumn.wsPostStatements.Remove(Key);
    end;
  end;
end;

procedure TfrmColumnUnbound.btnPasteFieldToSQLClick(Sender: TObject);
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    editSQLExp.SelText := Format('[%s]', [editFieldNameForSQL.text]);
    editSQLExp.SetFocus;
  end
  else
  begin
    editWSExp.SelText := Format('[%s]', [editFieldNameForWS.text]);
    editWSExp.SetFocus;
  end;
end;

// Set new field
procedure TfrmColumnUnbound.btnSetSQLNameClick(Sender: TObject);
var
  Name, oldexp, oldname: String;
begin
  oldexp := editExp.text;
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    name := StringReplace(editSQLAsstField.text, ' ', '_', [rfReplaceAll]);
    oldname := lstSQL.Items[lstSQL.ItemIndex];
    unboundColumn.sqlStatements.Remove(oldname);
    unboundColumn.updatePeriods.Remove(oldname);
    if name.IsEmpty then
      lstSQL.Items[lstSQL.ItemIndex] := Format('SQL%d', [lstSQL.ItemIndex + 1])
    else
      lstSQL.Items[lstSQL.ItemIndex] := Format('SQL%d_%s', [lstSQL.ItemIndex + 1, name]);
    unboundColumn.sqlStatements.Add(lstSQL.Items[lstSQL.ItemIndex], editSQLExp.text);
    unboundColumn.updatePeriods.Add(lstSQL.Items[lstSQL.ItemIndex], editSQLUpdateLimit.value);
    editSQLName.Properties.Items := lstSQL.Items;
    editSQLName.ItemIndex := lstSQL.ItemIndex;
    if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked then
    begin
      textCurrent.text := Format('[%s]', [lstSQL.Items[lstSQL.ItemIndex]]);
      unboundColumn.sqlUpdateStatements.Remove(oldname);
      unboundColumn.sqlUpdateStatements.Add(lstSQL.Items[lstSQL.ItemIndex], editSQLUpdate.text);
    end;
  end
  else
  begin
    name := StringReplace(editWSAsstField.text, ' ', '_', [rfReplaceAll]);
    oldname := lstWS.Items[lstWS.ItemIndex];
    unboundColumn.wsStatements.Remove(oldname);
    unboundColumn.updatePeriods.Remove(oldname);
    if name.IsEmpty then
      lstWS.Items[lstWS.ItemIndex] := Format('WS%d', [lstWS.ItemIndex + 1])
    else
      lstWS.Items[lstWS.ItemIndex] := Format('WS%d_%s', [lstWS.ItemIndex + 1, name]);
    name := lstWS.Items[lstWS.ItemIndex];
    unboundColumn.wsStatements.Add(lstWS.Items[lstWS.ItemIndex], editWSExp.text);
    unboundColumn.updatePeriods.Add(lstWS.Items[lstWS.ItemIndex], editWSUpdateLimit.value);
    editSQLName.Properties.Items := lstWS.Items;
    editSQLName.ItemIndex := lstWS.ItemIndex;
    if editExpModeWSValue.Checked or editExpModeWSCondition.Checked then
    begin
      textCurrent.text := Format('[%s]', [lstWS.Items[lstWS.ItemIndex]]);
      unboundColumn.wsPostStatements.Remove(oldname);
      unboundColumn.wsPostStatements.Add(lstWS.Items[lstWS.ItemIndex], editWSPost.text);
    end;
  end;
  editExp.text := StringReplace(editExp.text, Format('[%s]', [oldname]), Format('[%s]', [name]), [rfReplaceAll]);
end;

// Select SQL/WS
procedure TfrmColumnUnbound.lstSQLClick(Sender: TObject);
var
  Key: String;
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    editSQLName.ItemIndex := lstSQL.ItemIndex;
    Key := lstSQL.Items[lstSQL.ItemIndex];
    if unboundColumn.sqlStatements.ContainsKey(Key) then
      editSQLExp.text := unboundColumn.sqlStatements[Key];
    if unboundColumn.updatePeriods.ContainsKey(Key) then
      editSQLUpdateLimit.value := unboundColumn.updatePeriods[Key];
    if editExpModeSQLValue.Checked or editExpModeSQLCondition.Checked then
    begin
      textCurrent.text := Format('[%s]', [Key]);
      if unboundColumn.sqlUpdateStatements.ContainsKey(Key) then
        editSQLUpdate.text := unboundColumn.sqlUpdateStatements[Key];
    end;
  end
  else
  begin
    editSQLName.ItemIndex := lstWS.ItemIndex;
    Key := lstWS.Items[lstWS.ItemIndex];
    if unboundColumn.wsStatements.ContainsKey(Key) then
      editWSExp.text := unboundColumn.wsStatements[Key];
    if unboundColumn.updatePeriods.ContainsKey(Key) then
      editWSUpdateLimit.value := unboundColumn.updatePeriods[Key];
    if editExpModeWSValue.Checked or editExpModeWSCondition.Checked then
    begin
      textCurrent.text := Format('[%s]', [Key]);
      if unboundColumn.wsPostStatements.ContainsKey(Key) then
        editWSPost.text := unboundColumn.wsPostStatements[Key];
    end;
  end;
end;

procedure TfrmColumnUnbound.editSQLExpPropertiesChange(Sender: TObject);
var
  expText: String;
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    if SQLPageControl.ActivePage = tshSQLSelect then
    begin
      expText := editSQLExp.text;
      if lstSQL.ItemIndex >= 0 then
        unboundColumn.sqlStatements[lstSQL.Items[lstSQL.ItemIndex]] := expText;
      layoutTrySQL.Enabled := not expText.IsEmpty;
      if not layoutTrySQL.Enabled then
        editSQLAsstField.Properties.Items.Clear;
    end
    else if SQLPageControl.ActivePage = tshSQLUpdate then
    begin
      expText := editSQLUpdate.text;
      if lstSQL.ItemIndex >= 0 then
        unboundColumn.sqlUpdateStatements[lstSQL.Items[lstSQL.ItemIndex]] := expText;
      layoutTrySQL.Enabled := not expText.IsEmpty;
    end;
  end
  else
  begin
    if WSPageControl.ActivePage = tshWSGet then
    begin
      expText := editWSExp.text;
      if lstWS.ItemIndex >= 0 then
        unboundColumn.wsStatements[lstWS.Items[lstWS.ItemIndex]] := expText;
      layoutTryWS.Enabled := not expText.IsEmpty;
      if not layoutTryWS.Enabled then
        editWSAsstField.Properties.Items.Clear;
    end
    else if WSPageControl.ActivePage = tshWSPost then
    begin
      expText := editWSPost.text;
      if lstWS.ItemIndex >= 0 then
        unboundColumn.wsPostStatements[lstWS.Items[lstWS.ItemIndex]] := expText;
      layoutTrySQL.Enabled := not expText.IsEmpty;
    end;
  end;
end;

procedure TfrmColumnUnbound.editSQLUpdateLimitPropertiesChange(Sender: TObject);
begin
  if cxPageControl.ActivePage = tshSQLEditor then
  begin
    if (lstSQL.ItemIndex >= 0) then
      if unboundColumn.updatePeriods.ContainsKey(lstSQL.Items[lstSQL.ItemIndex]) then
        unboundColumn.updatePeriods[lstSQL.Items[lstSQL.ItemIndex]] := editSQLUpdateLimit.value
      else
        unboundColumn.updatePeriods.Add(lstSQL.Items[lstSQL.ItemIndex], editSQLUpdateLimit.value);
  end
  else
  begin
    if (lstWS.ItemIndex >= 0) then
      if unboundColumn.updatePeriods.ContainsKey(lstWS.Items[lstWS.ItemIndex]) then
        unboundColumn.updatePeriods[lstWS.Items[lstWS.ItemIndex]] := editWSUpdateLimit.value
      else
        unboundColumn.updatePeriods.Add(lstWS.Items[lstWS.ItemIndex], editWSUpdateLimit.value);
  end;
end;

procedure TfrmColumnUnbound.cxComboBox1PropertiesEditValueChanged(Sender: TObject);
begin
  // btnSave.Enabled:=true;
end;

procedure TfrmColumnUnbound.setItemsComboBoxForTwoColumns(firstBox: TcxComboBox; secondBox: TcxComboBox);
type
  TIntArray = Array of Integer;
var
  includeCaptions: TStringList;
  typePair: TPair<String, TDataTypes>;
  countArray: TIntArray;
begin
  if (firstBox = nil) or (secondBox = nil) then
    exit;
  firstBox.Properties.Items.Clear;
  secondBox.Properties.Items.Clear;
  countArray := TIntArray.Create(0, 0, 0, 0);
  for typePair in allFieldTypes do
  begin
    if typePair.value = dtInt then
      Inc(countArray[0])
    else if typePair.value in [dtFloat, dtCurrency] then
      Inc(countArray[1])
    else if typePair.value = dtDate then
      Inc(countArray[2])
    else if typePair.value = dtString then
      Inc(countArray[3]);
  end;
  includeCaptions := TStringList.Create;
  for typePair in allFieldTypes do
  begin
    if (typePair.value = dtInt) and (countArray[0] > 1) then
      includeCaptions.Add(allFieldCapts[typePair.Key])
    else if (typePair.value in [dtFloat, dtCurrency]) and (countArray[1] > 1) then
      includeCaptions.Add(allFieldCapts[typePair.Key])
    else if (typePair.value = dtDate) and (countArray[2] > 1) then
      includeCaptions.Add(allFieldCapts[typePair.Key])
    else if (typePair.value = dtString) and (countArray[3] > 1) then
      includeCaptions.Add(allFieldCapts[typePair.Key]);
  end;
  firstBox.Properties.Items := includeCaptions;
  secondBox.Properties.Items := includeCaptions;
  firstBox.text := includeCaptions[0];
  secondBox.Properties.Items.Delete(0);
  includeCaptions.Free;
end;

// change compare mode
procedure TfrmColumnUnbound.cxRadioButton3Click(Sender: TObject);
var
  idxOfItem, idxOfGroup, idxOfValue: Integer;
  gr1, gr2, gr3: TdxLayoutGroup;
  Item: TdxLayoutItem;
  conditionType: TDataTypes;
begin
  Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
  idxOfItem := Item.index;
  idxOfGroup := indexOfGroup(TcxRadioButton(Sender));
  gr1 := TdxLayoutGroup(dxLayoutControl1.Items.Items[idxOfGroup]);
  gr2 := gr1.Items[0] as TdxLayoutGroup;
  gr3 := gr2.Items[1] as TdxLayoutGroup;

  if idxOfItem = 1 then
    setItemsComboBoxForTwoColumns(TcxComboBox((gr3.Items[0] as TdxLayoutItem).control),
      TcxComboBox((gr3.Items[2] as TdxLayoutItem).control))
  else
  begin
    TcxComboBox((gr3.Items[0] as TdxLayoutItem).control).Properties.Items.Clear;
    TcxComboBox((gr3.Items[0] as TdxLayoutItem).control).Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
    TcxComboBox((gr3.Items[0] as TdxLayoutItem).control).text := allFieldNames.Keys.ToArray()[0];
  end;

  conditionType := getColumnType(getKeyByValue(TcxComboBox((gr3.Items[0] as TdxLayoutItem).control).text,
    allFieldCapts));
  idxOfValue := IfThen(conditionType = dtDate, 3,
    IfThen((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]), 4, 5));

  dxLayoutControl1.BeginUpdate;
  (gr3.Items[2] as TdxLayoutItem).Visible := idxOfItem = 1; // second column

  (gr3.Items[3] as TdxLayoutItem).Visible := conditionType = dtDate; // value
  if (conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency]) then
  begin
    (gr3.Items[4] as TdxLayoutItem).Visible := True;
    if (conditionType = dtInt) then
      TcxSpinEdit((gr3.Items[4] as TdxLayoutItem).control).Properties.valueType := vtInt
    else
      TcxSpinEdit((gr3.Items[4] as TdxLayoutItem).control).Properties.valueType := vtFloat;
  end
  else
    (gr3.Items[4] as TdxLayoutItem).Visible := False;
  (gr3.Items[5] as TdxLayoutItem).Visible := conditionType = dtString;
  if (conditionType = dtDate) and (idxOfItem = 1) then
    TcxDateEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := dateFormat
  else
    TcxDateEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := '';
  if ((conditionType = dtInt) or (conditionType in [dtFloat, dtCurrency])) and (idxOfItem = 1) then
    TcxSpinEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := numFormat
  else
    TcxSpinEdit((gr3.Items[idxOfValue] as TdxLayoutItem).control).Properties.DisplayFormat := '';
  dxLayoutControl1.EndUpdate;

  editorFocusChanged(Sender);
  btnSave.Enabled := True;
end;

// delete condition (None)
procedure TfrmColumnUnbound.cxRadioButton1Click(Sender: TObject);
var
  idx, h: Integer;
  gr: TdxLayoutGroup;
begin
  idx := indexOfGroup(TcxRadioButton(Sender));
  if idx = (dxLayoutControl1.Items.Count - 2) then
  begin
    dxLayoutControl1.BeginUpdate;
    if dxLayoutControl1.Items.Count >= 8 then
      setNoneEnable(idx - 1, False);
    gr := TdxLayoutGroup(dxLayoutControl1.Items.Items[idx + 1]);
    h := gr.viewInfo.ClientBounds.Height;
    deleteGroupItems(gr);
    gr.Free;
    dxLayoutControl1.EndUpdate;
    conditionOperands.Remove(conditionOperands[idx + 1 - itemsUConditionShiftLength]);
    conditionExps.Remove(conditionExps[idx + 1 - itemsUConditionShiftLength]);

    Height := Height - h;
    btnSave.Enabled := True;
  end;
end;

// add new condition (click OR,AND)
procedure TfrmColumnUnbound.cxRadioButton2Click(Sender: TObject);
var
  gr: TdxLayoutGroup;
  Item: TdxLayoutItem;
  idx: Integer;
  condition: TConditionUnbound;
begin
  idx := indexOfGroup(TcxRadioButton(Sender));
  if idx = dxLayoutControl1.Items.Count - 1 then
  begin
    dxLayoutControl1.BeginUpdate;
    gr := dxLayoutControl1.Items.CreateGroup;
    gr.ShowBorder := False;
    gr.index := idx + 1;
    condition.mode := cmOtherColumn;
    condition.columnCaption1 := allFieldCapts.Values.ToArray()[0];
    condition.columnCaption2 := '';
    condition.condition := '='; // equals
    condition.value := allFieldValues.Values.ToArray()[0];
    condition.operand := opNone;
    Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
    if Item.index = 1 then
      conditionOperands[idx - itemsUConditionShiftLength] := opAnd_Op
    else
      conditionOperands[idx - itemsUConditionShiftLength] := opOr_Op;
    conditionOperands.Add(condition.operand);
    if condition.operand = opOr_Op then
      conditionExps.Add('')
    else
      conditionExps.Add(conditionExps[idx - itemsUConditionShiftLength]);
    currentIdxCondition := gr.index - itemsUConditionShiftLength;
    addGroupItems(gr, condition);
    if dxLayoutControl1.Items.Count >= 8 then
      setNoneEnable(idx - 1, True);
    dxLayoutControl1.EndUpdate;

    Height := Height + gr.viewInfo.ClientBounds.Height;
  end
  else
  begin
    currentIdxCondition := idx - itemsUConditionShiftLength;
    Item := dxLayoutControl1.FindItem(TcxRadioButton(Sender));
    if Item.index = 1 then
      conditionOperands[currentIdxCondition] := opAnd_Op
    else
      conditionOperands[currentIdxCondition] := opOr_Op;
    setConditionColors;
  end;
  btnSave.Enabled := True;
end;

procedure TfrmColumnUnbound.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(allFieldTypes);
  FreeAndNil(allFieldNames);
  FreeAndNil(allFieldValues);
  FreeAndNil(allFieldCapts);
  FreeAndNil(conditionOperands);
  FreeAndNil(conditionExps);
end;

procedure TfrmColumnUnbound.FormCreate(Sender: TObject);
begin
  conditionExps := TList<String>.Create;
  conditionOperands := TList<TOperands>.Create;
  currentIdxCondition := -1;
end;

// save condition for column
procedure TfrmColumnUnbound.btnSaveClick(Sender: TObject);
  function replaceExpCaption(exp: String): String;
  var
    c: TPair<String, String>;
  begin
    Result := exp;
    if not exp.IsEmpty then
      for c in allFieldNames do
        Result := StringReplace(Result, '['+allFieldCapts[c.Key]+']', '['+c.value+']', [rfReplaceAll]);
  end;
  function getTargetFieldName(fieldCaption: String): String;
  var
    c: TPair<String, String>;
  begin
    Result := fieldCaption;
    if not Result.IsEmpty then
      for c in allFieldCapts do
      begin
        if c.value = fieldCaption then
        begin
          Result := allFieldNames[c.Key];
          break;
        end;
      end;
  end;

var
  I: Integer;
  gr: TdxCustomLayoutItem;
  condition: TConditionUnbound;
  pair: TPair<String, String>;
begin
  // save
  unboundColumn.Caption := editCaption.text;
  unboundColumn.ctype := TDataTypes(editType.Properties.Items.IndexOf(editType.text) + 1);
  unboundColumn.mask := editMask.text;
  unboundColumn.Width := editWidth.value;
  unboundColumn.targetFieldName := getTargetFieldName(editTargetField.text);
  if editExpModeValue.Checked then
    unboundColumn.mode := umValue
  else if editExpModeCondition.Checked then
    unboundColumn.mode := umValueByCondition
  else if editExpModeSQLValue.Checked then
    unboundColumn.mode := umSQLEdit
  else if editExpModeSQLCondition.Checked then
    unboundColumn.mode := umSQLEditByCondition
  else if editExpModeWSValue.Checked then
    unboundColumn.mode := umWSEdit
  else if editExpModeWSCondition.Checked then
    unboundColumn.mode := umWSEditByCondition;

  if editExpModeValue.Checked or editExpModeSQLValue.Checked or editExpModeWSValue.Checked then
  begin
    if String.IsNullOrEmpty(editMainExpression.text) then
    begin
      messagedlg('Empty unbound column expression', mtInformation, [mbOK], 0);
      editMainExpression.SetFocus;
      ModalResult := mrNone;
      exit;
    end;
    unboundColumn.expression := replaceExpCaption(editMainExpression.text);
  end
  else
  begin
    unboundColumn.conditions.Clear;
    for I := itemsUConditionShiftLength to dxLayoutControl1.Items.Count - 1 do
    begin
      gr := dxLayoutControl1.Items.Items[I];
      condition := getGroupCondition(gr as TdxLayoutGroup);
      if condition.expression.IsEmpty then
      begin
        messagedlg('Empty unbound column expression', mtInformation, [mbOK], 0);
        getExpressionBox(gr as TdxLayoutGroup).SetFocus;
        ModalResult := mrNone;
        exit;
      end;
      condition.expression := replaceExpCaption(condition.expression);
      if not condition.columnCaption1.IsEmpty then
        condition.columnName1 := allFieldNames[condition.columnCaption1];
      if not condition.columnCaption2.IsEmpty then
        condition.columnName2 := allFieldNames[condition.columnCaption2];
      unboundColumn.conditions.Add(condition);
    end;
  end;
  for pair in unboundColumn.sqlStatements do
    unboundColumn.sqlStatements[pair.Key] := replaceExpCaption(pair.value);
  for pair in unboundColumn.sqlUpdateStatements do
    unboundColumn.sqlUpdateStatements[pair.Key] := replaceExpCaption(pair.value);
  for pair in unboundColumn.wsStatements do
    unboundColumn.wsStatements[pair.Key] := replaceExpCaption(pair.value);
  for pair in unboundColumn.wsPostStatements do
    unboundColumn.wsPostStatements[pair.Key] := replaceExpCaption(pair.value);
end;

// Show form and load unbound data
procedure TfrmColumnUnbound.FormShow(Sender: TObject);
  function newColumnName: String;
  const
    columnFormat = 'New Column %d';
  var
    I, Count: Integer;
    found: Boolean;
  begin
    Count := 1;
    Result := Format(columnFormat, [Count]);
    while True do
    begin
      found := False;
      for I := 0 to gridView.ColumnCount - 1 do
        if gridView.Columns[I].Caption = Result then
        begin
          found := True;
          break
        end;
      if not found then
        exit
      else
      begin
        Inc(Count);
        Result := Format(columnFormat, [Count]);
      end;
    end;
  end;
  function revertExpCaption(exp: String): String;
  var
    c: TPair<String, String>;
  begin
    Result := exp;
    if not exp.IsEmpty then
      for c in allFieldNames do
        Result := StringReplace(Result, '['+c.value+']', '['+allFieldCapts[c.Key]+']', [rfReplaceAll]);
  end;

var
  I, h: Integer;
  gr: TdxLayoutGroup;
  condition: TConditionUnbound;
  newColumnRun: Boolean;
  pair: TPair<String, String>;
begin
  changing := True;
  tshExpressions.Show;
  editType.Properties.Items.AddStrings(dataTypeNames);
  newColumnRun := unboundColumn.Name.IsEmpty;
  if not newColumnRun then
  begin
    editCaption.text := unboundColumn.Caption;
    editType.text := dataTypeNames[Ord(unboundColumn.ctype) - 1];
    layoutMask.Visible := unboundColumn.ctype in [dtString, dtCurrency];
    editMask.text := unboundColumn.mask;
    editWidth.value := unboundColumn.Width;
    editMainExpression.text := revertExpCaption(unboundColumn.expression);
    for pair in unboundColumn.sqlStatements do
      unboundColumn.sqlStatements[pair.Key] := revertExpCaption(pair.value);
    for pair in unboundColumn.sqlUpdateStatements do
      unboundColumn.sqlUpdateStatements[pair.Key] := revertExpCaption(pair.value);
    for pair in unboundColumn.wsStatements do
      unboundColumn.wsStatements[pair.Key] := revertExpCaption(pair.value);
    for pair in unboundColumn.wsPostStatements do
      unboundColumn.wsPostStatements[pair.Key] := revertExpCaption(pair.value);

    rbSQL.Checked := (unboundColumn.sqlStatements.Count > 0) or
      ((unboundColumn.sqlStatements.Count = 0) and (unboundColumn.wsStatements.Count = 0));
    rbWS.Checked := unboundColumn.wsStatements.Count > 0;
    if unboundColumn.sqlStatements.Count > 0 then
      rbSQLClick(rbSQL)
    else if unboundColumn.wsStatements.Count > 0 then
      rbSQLClick(rbWS);

    if unboundColumn.mode = umValue then
      editExpModeValue.Checked := True
    else if unboundColumn.mode = umValueByCondition then
      editExpModeCondition.Checked := True
    else if unboundColumn.mode = umSQLEdit then
      editExpModeSQLValue.Checked := True
    else if unboundColumn.mode = umSQLEditByCondition then
      editExpModeSQLCondition.Checked := True
    else if unboundColumn.mode = umWSEdit then
      editExpModeWSValue.Checked := True
    else if unboundColumn.mode = umWSEditByCondition then
      editExpModeWSCondition.Checked := True;

    layoutExpression.Visible := unboundColumn.conditions.Count = 0;
    h := Height;
    for I := 0 to unboundColumn.conditions.Count - 1 do
    begin
      condition := unboundColumn.conditions[I];
      condition.expression := revertExpCaption(condition.expression);

      gr := dxLayoutControl1.Items.CreateGroup;
      gr.ShowBorder := False;
      gr.index := itemsUConditionShiftLength + I;
      conditionOperands.Add(condition.operand);
      conditionExps.Add(condition.expression);
      currentIdxCondition := gr.index - itemsUConditionShiftLength;
      addGroupItems(gr, condition);
      if dxLayoutControl1.Items.Count >= 8 then
        setNoneEnable(gr.index - 2, True);

      h := h + gr.viewInfo.ClientBounds.Height * I;
    end;
    Height := h;
    Caption := 'Editing ' + Caption;
    btnSave.Caption := 'Save';
  end
  else
  begin
    Caption := 'Adding ' + Caption;
    btnSave.Caption := 'Add';
    editCaption.text := newColumnName;
    editType.text := dataTypeNames[high(dataTypeNames)];
    unboundColumn.ctype := dtString;
    layoutMask.Visible := True;
  end;
  layoutPasteSQL.Enabled := ((not newColumnRun) and ((unboundColumn.sqlStatements.Count > 0) or
    (unboundColumn.wsStatements.Count > 0)));
  layoutSQLName.Enabled := layoutPasteSQL.Enabled;
  editTargetField.Properties.Items.Add(string.Empty);
  editTargetField.Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
  editTargetField.text := unboundColumn.targetFieldName;
  editFieldNameForSQL.Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
  editFieldNameForSQL.ItemIndex := 0;
  editFieldNameForWS.Properties.Items.AddStrings(allFieldCapts.Values.ToArray());
  editFieldNameForWS.ItemIndex := 0;

  btnReset.Enabled := not newColumnRun;
  // btnSave.Enabled:=newColumnRun;
  changing := False;
end;

end.
