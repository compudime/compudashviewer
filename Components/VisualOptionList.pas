unit VisualOptionList;

interface

uses
  System.Classes,
  System.Types,
  System.SysUtils,
  System.Generics.Collections,
  Vcl.ExtCtrls,
  Vcl.Forms,
  Vcl.Buttons,
  Vcl.Menus,
  Vcl.Graphics,
  Vcl.StdCtrls,
  Vcl.Controls,
  dxBar,
  PNGImage;

type
//  TBaseVisualOptionList = class(TWinControl)
  TBaseVisualOptionList = class(TPanel)
  strict private
    FMasterButton: TCheckBox;
    FDropDownButton: TSpeedButton;
    function BuildMasterButton: TCheckBox;
    function BuildDropDownButton: TSpeedButton;
    function GetMasterButton: TCheckBox;
    function GetBackgroundColor: TColor;
    procedure SetBackgroundColor(const Value: TColor);
  private
    function GetDropDownButton: TSpeedButton;
  protected
    property MasterButton: TCheckBox read GetMasterButton;
    property DropDownButton: TSpeedButton read GetDropDownButton;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    property BackgroundColor: TColor read GetBackgroundColor write SetBackgroundColor;
  end;

  TVisualOptionList = class(TBaseVisualOptionList)
  private
    FRootParent: TWinControl;
    FOnClick: TNotifyEvent;
    FPopup: TdxBarPopupMenu;
    procedure SetDefPositionMasterButton;
    procedure SetDefPositionOptionList;
    function GetRootParent: TWinControl;
    procedure MasterButtonClick(Sender: TObject);
    procedure DropDownClick(Sender: TObject);
    function GetCheckState: TCheckBoxState;
    procedure SetCheckState(const Value: TCheckBoxState);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetParent(AParent: TWinControl); override;
    property State: TCheckBoxState read GetCheckState write SetCheckState;
  published
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    property PopupMenu: TdxBarPopupMenu read FPopup write FPopup;
  end;

implementation

const
  MASTER_BUTTON_NAME = 'VisualOptionMasterButton';
  MASTER_BUTTON_TOP_OFFSET = 0;
  MASTER_BUTTON_LEFT_OFFSET = 0;
  MASTER_BUTTON_HEIGHT = 16;
  MASTER_BUTTON_WIDTH = 16;
  LIST_OPTION_NAME = 'VisualOptionListBox';
  OPTION_LIST_LEFT_OFFSET = 5;
  OPTION_LIST_TOP_OFFSET = 5;
//  OPTION_LIST_ITEM_HEIGHT = 25;
  OPTION_LIST_ITEM_HEIGHT = 16;
//  OPTION_LIST_ITEM_FONT_SIZE = 11;
  OPTION_LIST_ITEM_FONT_SIZE = 8;
//  OPTION_LIST_HEIGHT = 75;
  OPTION_LIST_HEIGHT = 3 * OPTION_LIST_ITEM_HEIGHT;
  DEF_PATH_TO_DROP_DOWN_IMG = 'Icons\drop-down-arrow.png';
  DROP_DOWN_IMG_NAME = 'VisualOptionDropDownImg';
//  DROP_DOWN_IMG_TOP_OFFSET = 5;
  DROP_DOWN_IMG_TOP_OFFSET = 0;
  DROP_DOWN_IMG_LEFT_OFFSET = MASTER_BUTTON_WIDTH;
  DROP_DOWN_IMG_HEIGHT = 16;
  DROP_DOWN_IMG_WIDTH = 16;

{ TVisualOptionList }

constructor TBaseVisualOptionList.Create(AOwner: TComponent);
begin
  inherited;

  ShowCaption := False;
  BevelOuter := bvNone;
  BorderStyle := bsNone;
  BorderWidth := 0;

  FMasterButton := BuildMasterButton;
  FMasterButton.Visible := True;

  FDropDownButton := BuildDropDownButton;

  AutoSize := True;
end;

destructor TBaseVisualOptionList.Destroy;
begin
  FreeAndNil(FDropDownButton);

  inherited;
end;

constructor TVisualOptionList.Create(AOwner: TComponent);
begin
  inherited;
{  Color := clWhite;
  ParentColor := False;}
  MasterButton.OnClick := MasterButtonClick;
  DropDownButton.OnClick := DropDownClick;
end;

destructor TVisualOptionList.Destroy;
begin

  inherited;
end;

function TVisualOptionList.GetCheckState: TCheckBoxState;
begin
  if Assigned(MasterButton) then
    Result := MasterButton.State
  else
    Result := cbGrayed;
end;

function TVisualOptionList.GetRootParent: TWinControl;
begin
  Result := GetParentForm(Self);
end;

procedure TVisualOptionList.DropDownClick(Sender: TObject);
var
  _point: TPoint;
begin
  if Assigned(FPopup) then
  begin
    _point.Create(Left, Top + Height);
    _point := Parent.ClientToScreen(_point);
    FPopup.Popup(_point.X, _point.Y);
  end;
end;

procedure TVisualOptionList.MasterButtonClick(Sender: TObject);
begin
  if Assigned(FOnClick) then
    FOnClick(MasterButton);
end;

procedure TVisualOptionList.SetCheckState(const Value: TCheckBoxState);
begin
  if Assigned(MasterButton) then
    MasterButton.State := Value;
end;

procedure TVisualOptionList.SetDefPositionMasterButton;
begin
  MasterButton.Top := MASTER_BUTTON_TOP_OFFSET;
  MasterButton.Left := MASTER_BUTTON_LEFT_OFFSET;
end;

procedure TVisualOptionList.SetDefPositionOptionList;
var
  LMasterButtonPoint: TPoint;
  LClientToScreenPoint: TPoint;
  LOptionListPoint: TPoint;
begin
  LMasterButtonPoint := TPoint.Create(MasterButton.Left, MasterButton.Top);

  LClientToScreenPoint := ClientToScreen(LMasterButtonPoint);
  LOptionListPoint := FRootParent.ScreenToClient(LClientToScreenPoint);
end;

procedure TVisualOptionList.SetParent(AParent: TWinControl);
begin
  inherited;

  if AParent <> nil then
  begin
    MasterButton.Parent := Self;
    SetDefPositionMasterButton;
    FRootParent := GetRootParent;
  end;
end;

function TBaseVisualOptionList.BuildDropDownButton: TSpeedButton;
begin
  Result := TSpeedButton.Create(Owner);

  with Result do
  begin
    Name := DROP_DOWN_IMG_NAME;
    Parent := Self;

    with TPngImage.Create do
    try
      LoadFromFile(DEF_PATH_TO_DROP_DOWN_IMG);
//      Transparent := True;
      AssignTo(Glyph);
    finally
      Free
    end;

    Height := DROP_DOWN_IMG_HEIGHT;
    Width := DROP_DOWN_IMG_WIDTH;
    Top := DROP_DOWN_IMG_TOP_OFFSET;
    Left := DROP_DOWN_IMG_LEFT_OFFSET;
  //  Transparent := True;
    Flat := True;
  end;
end;

function TBaseVisualOptionList.BuildMasterButton: TCheckBox;
begin
  Result := TCheckBox.Create(Owner);
  Result.Parent := Self;

  Result.Height := MASTER_BUTTON_HEIGHT;
  Result.Width := MASTER_BUTTON_WIDTH;
  Result.Name := MASTER_BUTTON_NAME;
  Result.Caption := '';
end;

function TBaseVisualOptionList.GetBackgroundColor: TColor;
begin
  Result := Color;
end;

function TBaseVisualOptionList.GetDropDownButton: TSpeedButton;
begin
  Result := FDropDownButton;
end;

function TBaseVisualOptionList.GetMasterButton: TCheckBox;
begin
  Result := FMasterButton;
end;

procedure TBaseVisualOptionList.SetBackgroundColor(const Value: TColor);
begin
  Color := Value;
end;

end.

