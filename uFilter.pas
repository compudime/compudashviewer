unit uFilter;

interface

uses
  System.SysUtils, System.JSON, System.Generics.Collections,
  VCL.Forms;

type
  TFilterSettings = record
    MasterFieldName: String;
    SubFieldName: String;
    FilterExists: String;
    FilterValue: String;

    constructor Create(AJSON: TJSONObject); overload;
    constructor Create(const AMasterFieldName, ASubFieldName: String;
                       const AFilterExists: String = '';
                       const AFilterValue: String = ''); overload;
    function ToMongoFilter: String;
    function ToFriendlyFilter: String;
    function ToJSON: TJSONObject;
  end;

  TFilterSettingsList = class(TList<TFilterSettings>)
  public
    function IndexOfFields(const AMasterField, ASubField: String): Integer;
    function ToMongoFilter: String;
    function ToFriendlyFilter: String;
  end;

  procedure SetFilterSettings(AList: TFilterSettingsList);
  function GetFilterSettings: TFilterSettingsList;

const
  FILTER_NOT_SET = 'Not Set';
  FILTER_EXISTS = 'Exists';
  FILTER_NOT_EXISTS = 'Not Exists';
  FILTER_SETTINGS_FILENAME = 'FilterSettings.json';

implementation

uses
  System.Classes;

var
  GList: TFilterSettingsList;

procedure SaveFilterSettingsToFile(AList: TFilterSettingsList; const AFileName: String);
var
  JArr: TJSONArray;
  StrList: TStringList;
  Item: TFilterSettings;
begin
  StrList := nil;
  JArr := TJSONArray.Create;
  try
    for Item in AList do
      JArr.Add(Item.ToJSON);
    StrList := TStringList.Create;
    StrList.Text := JArr.ToString;
    StrList.SaveToFile(ExtractFilePath(Application.ExeName) + AFileName);
  finally
    JArr.Free;
    StrList.Free;
  end;
end;

procedure SetFilterSettings(AList: TFilterSettingsList);
begin
  if not Assigned(AList) then
    Exit;

  SaveFilterSettingsToFile(AList, FILTER_SETTINGS_FILENAME);
  GList.Free;
  GList := TFilterSettingsList.Create;
  GList.AddRange(AList);
end;

function GetFilterSettings: TFilterSettingsList;
begin
  Result := TFilterSettingsList.Create;
  Result.AddRange(GList);
end;

{ TFilterSettings }

constructor TFilterSettings.Create(const AMasterFieldName, ASubFieldName,  AFilterExists, AFilterValue: String);
begin
  Self.MasterFieldName := AMasterFieldName;
  Self.SubFieldName := ASubFieldName;
  if AFilterExists.IsEmpty then
    Self.FilterExists := FILTER_NOT_SET
  else
    Self.FilterExists := AFilterExists;
  if AFilterValue.IsEmpty then
    Self.FilterValue := FILTER_NOT_SET
  else
    Self.FilterValue := AFilterValue;
end;

constructor TFilterSettings.Create(AJSON: TJSONObject);
begin
  Self.MasterFieldName := AJSON.GetValue<String>('MasterField', '');
  Self.SubFieldName := AJSON.GetValue<String>('SubField', '');
  Self.FilterExists := AJSON.GetValue<String>('Exists', FILTER_NOT_SET);
  Self.FilterValue := AJSON.GetValue<String>('Value', FILTER_NOT_SET);
end;

function TFilterSettings.ToFriendlyFilter: String;
begin
  Result := '';
  if Self.FilterExists = FILTER_NOT_EXISTS then begin
    Result := '"' + Self.MasterFieldName + '.' + Self.SubFieldName + '": "Not Exists"';
  end else begin
    if Self.FilterValue <> FILTER_NOT_SET then
      Result := '"' + Self.MasterFieldName + '.' + Self.SubFieldName + '": value is "' + Self.FilterValue + '"'
    else if Self.FilterExists = FILTER_EXISTS then
      Result := '"' + Self.MasterFieldName + '.' + Self.SubFieldName + '" : "Exists"';
  end;
end;

function TFilterSettings.ToJSON: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.AddPair('MasterField', Self.MasterFieldName);
  Result.AddPair('SubField', Self.SubFieldName);
  Result.AddPair('Exists', Self.FilterExists);
  Result.AddPair('Value', Self.FilterValue);
end;

function TFilterSettings.ToMongoFilter: String;
begin
  Result := '';
  if Self.FilterExists = FILTER_NOT_EXISTS then begin
    Result := '{ "' + Self.MasterFieldName + '.' + Self.SubFieldName + '" : {$exists: false} }';
  end else begin
    if Self.FilterValue <> FILTER_NOT_SET then
      Result := '{ "' + Self.MasterFieldName + '.' + Self.SubFieldName + '" : {$exists: true, $eq: "' + Self.FilterValue + '"} }'
    else if Self.FilterExists = FILTER_EXISTS then
      Result := '{ "' + Self.MasterFieldName + '.' + Self.SubFieldName + '" : {$exists: true} }';
  end;
end;

{ TFilterSettingsList }

function TFilterSettingsList.IndexOfFields(const AMasterField, ASubField: String): Integer;
var
  i: integer;
begin
  Result := -1;
  for i := 0 to Self.Count - 1 do
    if (Self[i].MasterFieldName = AMasterField) and (Self[i].SubFieldName = ASubField) then
      Exit(i);
end;

function TFilterSettingsList.ToFriendlyFilter: String;
var
  ItemMongoFilter: String;
  Item: TFilterSettings;
begin
  Result := '';

  for Item in Self do begin
    ItemMongoFilter := Item.ToFriendlyFilter;
    if not ItemMongoFilter.IsEmpty then
      Result := Result + ItemMongoFilter + ' and ';
  end;

  if not Result.IsEmpty then
    Result := Result.Remove(Result.Length - 5);
end;

function TFilterSettingsList.ToMongoFilter: String;
var
  ItemMongoFilter: String;
  Item: TFilterSettings;
begin
  Result := '';

  for Item in Self do begin
    ItemMongoFilter := Item.ToMongoFilter;
    if not ItemMongoFilter.IsEmpty then
      Result := Result + ItemMongoFilter + ', ';
  end;

  if not Result.IsEmpty then begin
    Result := '{ $and: [' + Result + ']}';
    Result := Result.Replace(', ]', ']');
  end;
end;

function LoadFilterSettingsFromFile(const AFileName: String): TFilterSettingsList;
var
  FullFileName: String;
  StrList: TStringList;
  JArr: TJSONArray;
  i: integer;
begin
  Result := TFilterSettingsList.Create;

  FullFileName := ExtractFilePath(Application.ExeName) + AFileName;

  if FileExists(FullFileName) then begin
    JArr := nil;
    StrList := TStringList.Create;
    try
      StrList.LoadFromFile(FullFileName);
      JArr := TJSONObject.ParseJSONValue(StrList.Text) as TJSONArray;
      if not Assigned(JArr) then
        Exit;

      for i:=0 to JArr.Count - 1 do
        Result.Add(TFilterSettings.Create(JArr.Items[i] as TJSONObject));
    finally
      StrList.Free;
      JArr.Free;
    end;
  end;
end;

initialization
  GList := LoadFilterSettingsFromFile(FILTER_SETTINGS_FILENAME);

finalization
  GList.Free;

end.
