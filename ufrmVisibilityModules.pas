unit ufrmVisibilityModules;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxCheckBox, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxContainer, cxEdit,
  cxCustomListBox, cxCheckListBox,
  FireDAC.Phys.MongoDBWrapper, FireDAC.Phys.MongoDBDataSet,
  System.JSON, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxDropDownEdit, System.Generics.Collections, uNotifyCenter,
  AMR.AutoObject, dxSkinWXI, dxUIAClasses;

type
  TfrmVisibilityModules = class(TForm)
    btnOK: TcxButton;
    btnApply: TcxButton;
    btnCancel: TcxButton;
    chkAutoStartScanForModules: TcxCheckBox;
    btnScanModules: TcxButton;
    gridModulesDBTableView1: TcxGridDBTableView;
    gridModulesLevel1: TcxGridLevel;
    gridModules: TcxGrid;
    mtblModules: TFDMemTable;
    dsModules: TDataSource;
    clmModuleName: TcxGridDBColumn;
    clmShowingSetting: TcxGridDBColumn;
    mtblModulesModuleName: TStringField;
    mtblModulesShowingType: TStringField;
    procedure btnOKClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnScanModulesClick(Sender: TObject);
  private
    FLastSettedSettings: String;

    function ContainsInDefaultArray(const aValue: String): Boolean;
    procedure Apply;
    procedure SaveSettings;
    procedure LoadSettings;
  private const
    SETTING_FILENAME = 'VisibilitySettings.json';
  public
    procedure ShowModulesList(aArrAll: TArray<String>);

    function GetShowingSettings: String;
    function GetShowingSettingsForWork: TDictionary<String, String>;
    procedure SetShowingSettings(const AStr: String);
    procedure AddNewShowingSettings(const AStr: String);
    function IsAutoScanAtStart: Boolean;
  end;

var
  frmVisibilityModules: TfrmVisibilityModules;

implementation

uses
  CDV.Consts;

{$R *.dfm}

{ TfrmVisibilityModules }

procedure TfrmVisibilityModules.AddNewShowingSettings(const AStr: String);
begin
  if not mtblModules.Active then
    mtblModules.CreateDataSet;

  if not mtblModules.Locate('ModuleName', AStr) then begin
    mtblModules.Append;
    mtblModules.FieldByName('ModuleName').Value := AStr;
    mtblModules.FieldByName('ShowingType').Value := SHOW_TYPE_EXISTS;
    mtblModules.Post;
  end;
end;

procedure TfrmVisibilityModules.Apply;
begin
  SaveSettings;
  TNotifyCenter.Instance.SendMessage(NOTIFY_APPLY_NEW_VISIBILITY, nil);
end;

procedure TfrmVisibilityModules.btnApplyClick(Sender: TObject);
begin
  Apply;
end;

procedure TfrmVisibilityModules.btnCancelClick(Sender: TObject);
begin
  SetShowingSettings(FLastSettedSettings);
  Close;
end;

procedure TfrmVisibilityModules.btnOKClick(Sender: TObject);
begin
  Apply;
  Close;
end;

procedure TfrmVisibilityModules.btnScanModulesClick(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(NOTIFY_SCAN, nil);
end;

function TfrmVisibilityModules.ContainsInDefaultArray(const aValue: String): Boolean;
begin
  for var Str in DEFAULT_GRIDS do
    if Str = aValue then
      Exit(True);

  Result := False;
end;

procedure TfrmVisibilityModules.FormCreate(Sender: TObject);
begin
  FLastSettedSettings := '';
  LoadSettings;
end;

function TfrmVisibilityModules.GetShowingSettings: String;
begin
  Result := '[';

  if not mtblModules.Active then
    Exit('[]');

  mtblModules.First;
  while not mtblModules.Eof do begin
    Result := Result + '{"' + mtblModules.FieldByName('ModuleName').AsString + '": "' +
               mtblModules.FieldByName('ShowingType').AsString + '"},';
    mtblModules.Next;
  end;
  if Result.Length > 1 then
    Result := Result.Remove(Result.Length - 1);

  Result := Result + ']';
end;

function TfrmVisibilityModules.GetShowingSettingsForWork: TDictionary<String, String>;
begin
  Result := TDictionary<String, String>.Create;

  if not mtblModules.Active then
    Exit;

  mtblModules.First;
  while not mtblModules.Eof do begin
    Result.Add(mtblModules.FieldByName('ModuleName').AsString,
               mtblModules.FieldByName('ShowingType').AsString);
    mtblModules.Next;
  end;
end;

function TfrmVisibilityModules.IsAutoScanAtStart: Boolean;
begin
  Result := chkAutoStartScanForModules.Checked;
end;

procedure TfrmVisibilityModules.LoadSettings;
begin
  if not FileExists(GetCurrentDir + '\' + SETTING_FILENAME) then
    Exit;

  var StrList := TStringList.Create;
  var AutoStrList := TAutoFree.Create(StrList);

  StrList.LoadFromFile(GetCurrentDir + '\' + SETTING_FILENAME);

  var JObj := TJSONObject.ParseJSONValue(StrList.Text) as TJSONObject;
  if not Assigned(JObj) then
    Exit;
  var AutoObj := TAutoFree.Create(JObj);

  chkAutoStartScanForModules.Checked := JObj.GetValue<Boolean>('IsAutoScanAtAppStart', False);
end;

procedure TfrmVisibilityModules.SaveSettings;
begin
  var JObj := TJSONObject.Create;
  var AutoObj := TAutoFree.Create(JObj);

  JObj.AddPair('IsAutoScanAtAppStart', TJSONBool.Create(chkAutoStartScanForModules.Checked));

  var StrList := TStringList.Create;
  var AutoStrList := TAutoFree.Create(StrList);

  StrList.Text := JObj.ToString;

  StrList.SaveToFile(GetCurrentDir + '\' + SETTING_FILENAME);
end;

procedure TfrmVisibilityModules.SetShowingSettings(const AStr: String);
var
  JArr: TJSONArray;
begin
  JArr := TJSONObject.ParseJSONValue(AStr) as TJSONArray;

  if not Assigned(JArr) then
    Exit;

  try
    FLastSettedSettings := AStr;

    if mtblModules.Active then
      mtblModules.EmptyDataSet
    else
      mtblModules.CreateDataSet;

    for var JValue in JArr do begin
      mtblModules.Append;
      mtblModules.FieldByName('ModuleName').Value := (JValue as TJSONObject).Pairs[0].JsonString.Value;
      mtblModules.FieldByName('ShowingType').Value := (JValue as TJSONObject).Pairs[0].JsonValue.Value;
      mtblModules.Post;
    end;
  finally
    JArr.Free;
  end;
end;

procedure TfrmVisibilityModules.ShowModulesList(aArrAll: TArray<String>);
begin
  if not mtblModules.Active then
    mtblModules.CreateDataSet;

  if (mtblModules.RecordCount = 0) and (Length(aArrAll) > 0) then
    for var Str in aArrAll do begin
      mtblModules.Append;
      mtblModulesModuleName.asString := Str;
      if ContainsInDefaultArray(Str) then
        mtblModulesShowingType.asString := SHOW_TYPE_ALWAYS
      else
        mtblModulesShowingType.asString := SHOW_TYPE_EXISTS;
      mtblModules.Post;
    end;

  Show;
end;

end.
