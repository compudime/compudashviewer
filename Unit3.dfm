object frmEditColumnValue: TfrmEditColumnValue
  Left = 0
  Top = 0
  Caption = 'Edit Column Values'
  ClientHeight = 119
  ClientWidth = 571
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel1: TcxLabel
    Left = 8
    Top = 9
    Caption = 'Column Name'
    TabOrder = 0
    Transparent = True
  end
  object cmbColumnName: TcxComboBox
    Left = 88
    Top = 8
    Properties.Items.Strings = (
      'Segment')
    TabOrder = 1
    Width = 121
  end
  object cxLabel2: TcxLabel
    Left = 8
    Top = 32
    Caption = 'Value'
    TabOrder = 2
    Transparent = True
  end
  object txtValue: TcxTextEdit
    Left = 88
    Top = 32
    TabOrder = 3
    Width = 449
  end
  object btnApply: TcxButton
    Left = 8
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Apply'
    ModalResult = 8
    TabOrder = 4
    OnClick = btnApplyClick
  end
end
