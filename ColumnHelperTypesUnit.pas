unit ColumnHelperTypesUnit;

interface

uses System.Generics.Collections, Vcl.Graphics, System.Variants, System.SysUtils, System.Classes, Vcl.ExtCtrls,
  cxGridTableView, cxGridDBTableView, cxGridDBBandedTableView, cxGridCustomtableView, cxTextEdit, cxSpinEdit,
  cxCalendar, cxEdit, dxCore, cxGridDBDataDefinitions;

const
  numFormat = '+0';
  dateFormat = '"+"dd/MM/yyyy';
  conditions: TArray<String> = ['=', '<>', '<', '<=', '>', '>='];
  containsCondition = 'contains';
  dataTypeNames: TArray<String> = ['integer', 'double', 'currency', 'date', 'string'];

type
  TWorkCompetedEvent = procedure of object;
  TWorkGridCompetedEvent = procedure(view: TcxGridTableView) of object;

  TOperands = (opNone, opAnd_Op, opOr_Op);
  TCompareModes = (cmValue, cmColumns, cmOtherColumn, cmUpdateTime);
  TDataTypes = (dtUnknown, dtInt, dtFloat, dtCurrency, dtDate, dtString);
  TStoreMode = (smGrid, smDict);

  TConditionColors = record
    color: TColor;
    textColor: TColor;
  end;

  TViewTimer = class(TTimer)
  private
    view: TcxGridTableView;
    procedure TimerTimer(Sender: TObject);
  public
    constructor Create(gridView: TcxGridTableView);
  end;

function checkType(vtype: TDataTypes): Boolean;
function valueType(value: Variant; valueType: String = ''): TDataTypes;
function checkTypeByValue(value: Variant): Boolean;
function getStrType(strValue: String; var varValue: Variant): TDataTypes;
function containsString(value1: Variant; value2: Variant): Boolean;
function indexOfArray(sarray: TArray<String>; svalue: String): Integer;
function containsValue(value: String; items: TDictionary<String, String>): Boolean;
function getKeyByValue(value: String; items: TDictionary<String, String>): String;
function getFocusedItemName(view: TcxGridTableView): String;

function getFieldName(column: TcxGridColumn;isUseUnbound: Boolean=false): String;
procedure setFieldName(column: TcxGridColumn; fieldName: String);
procedure setFieldValue(view: TcxGridTableView;recordIndex: Integer;fieldName: String;fieldValue: Variant;keyName: String);
function createGridColumn(view: TcxGridTableView; parentColumn: TcxGridColumn): TcxGridColumn;
function getColumnStoreMode(view: TcxGridTableView): TStoreMode;

procedure undoEdit(edit: TcxCustomEdit);

implementation

uses ColumnUnboundHelperUnit, DB {$IFNDEF NOADSGRIDS}, adscnnct, adstable{$ENDIF};

function valueType(value: Variant; valueType: String): TDataTypes;
var
  vtype: Word;
begin
  Result := dtUnknown;
  if (value = Null) then
  begin
    if valueType = 'DateTime' then
      Result := dtDate
    else if (valueType = 'Integer') or (valueType = 'Smallint') then
      Result := dtInt
    else if valueType = 'Float' then
      Result := dtFloat
    else if valueType = 'Currency' then
      Result := dtCurrency
    else if valueType = 'String' then
      Result := dtString;
  end
  else
  begin
    vtype := TVarData(value).vtype;
    if (vtype = varDate) then
      Result := dtDate
    else if (vtype = varInteger) or (vtype = varSmallint) then
      Result := dtInt
    else if vtype = varDouble then
      Result := dtFloat
    else if vtype = varCurrency then
      Result := dtCurrency
    else if (vtype = varUString) or (vtype = varString) then
      Result := dtString;
  end;
end;

function checkType(vtype: TDataTypes): Boolean;
begin
  Result := ((vtype = dtDate) or (vtype = dtInt) or (vtype = dtFloat) or (vtype = dtCurrency) or (vtype = dtString));
end;

function checkTypeByValue(value: Variant): Boolean;
begin
  Result := checkType(valueType(value));
end;

function getStrType(strValue: String; var varValue: Variant): TDataTypes;
var
  intValue: Integer;
  dateValue: TDateTime;
  floatValue: Double;
begin
  if TryStrToInt(strValue, intValue) then
  begin
    if strValue.Length <> IntToStr(intValue).Length then
    begin
      varValue := strValue;
      Result := dtString;
    end
    else
    begin
      varValue := intValue;
      Result := dtInt;
    end;
  end
  else if TryStrToDateTime(strValue, dateValue) then
  begin
    varValue := dateValue;
    Result := dtDate;
  end
  else if TryStrToFloat(strValue, floatValue) then
  begin
    varValue := floatValue;
    Result := dtFloat;
  end
  else
  begin
    varValue := strValue;
    Result := dtString;
  end;
end;

function containsString(value1: Variant; value2: Variant): Boolean;
var
  str1, str2: String;
begin
  if value1 = Null then
  begin
    Result := false;
    exit;
  end;
  str1 := VarToStr(value1);
  str2 := VarToStr(value2);
  // contains case insensitive
  str1 := str1.ToLowerInvariant;
  str2 := str2.ToLowerInvariant;
  Result := str1.Contains(str2)
end;

function indexOfArray(sarray: TArray<String>; svalue: String): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := Low(sarray) to High(sarray) do
  begin
    if sarray[I] = svalue then
    begin
      Result := I;
      break;
    end;
  end;
end;

function containsValue(value: String; items: TDictionary<String, String>): Boolean;
var
  pair: TPair<String, String>;
begin
  Result := false;
  for pair in items do
  begin
    if pair.value = value then
    begin
      Result := true;
      break;
    end;
  end;
end;

function getKeyByValue(value: String; items: TDictionary<String, String>): String;
var
  pair: TPair<String, String>;
begin
  Result := string.Empty;
  for pair in items do
  begin
    if pair.value = value then
    begin
      Result := pair.Key;
      break;
    end;
  end;
end;

function getFocusedItemName(view: TcxGridTableView): String;
var
  I: Integer;
begin
  Result := string.Empty;
  for I := 0 to view.ItemCount - 1 do
  begin
    if view.items[I].Visible and view.items[I].Focused then
    begin
      Result := view.items[I].Name;
      break;
    end;
  end;
end;

function getFieldName(column: TcxGridColumn;isUseUnbound: Boolean): String;
begin
  Result := string.Empty;
  if ColumnUnboundHelper.isUnbound(column) and (not isUseUnbound) then
    exit;
  if column is TcxGridDBColumn then
    Result := (column as TcxGridDBColumn).DataBinding.fieldName
  else if column is TcxGridDBBandedColumn then
    Result := (column as TcxGridDBBandedColumn).DataBinding.fieldName;
end;

procedure setFieldName(column: TcxGridColumn; fieldName: String);
begin
  if fieldName.IsEmpty then
    exit;
  if column is TcxGridDBColumn then
    (column as TcxGridDBColumn).DataBinding.fieldName := fieldName
  else if column is TcxGridDBBandedColumn then
    (column as TcxGridDBBandedColumn).DataBinding.fieldName := fieldName;
end;

procedure setFieldValue(view: TcxGridTableView;recordIndex: Integer;fieldName: String;fieldValue: Variant;keyName: String);
var
  connection: TAdsConnection;
  params: TParams;
  tableName: String;
  item: TcxCustomGridTableItem;
  controller: TcxGridDBDataController;
  keyValue: Variant;
  oAdsDatasetOptions : TAdsDatasetOptions;
begin
  controller:=nil;
  if view is TcxGridDBTableView then
    controller:=(view as TcxGridDBTableView).DataController
  else if view is TcxGridDBBandedTableView then
    controller:=(view as TcxGridDBBandedTableView).DataController;

  if Assigned(controller) and Assigned(controller.DataSet) then
  begin
    item:=controller.GetItemByFieldName(keyName);
    if Assigned(item) then
    begin
      keyValue:=controller.GetValue(recordIndex,item.Index);
      connection:=nil;
      if controller.DataSet is TAdsTable then
      begin
        connection:=(controller.DataSet as TAdsTable).AdsConnection;
        tableName:=(controller.DataSet as TAdsTable).TableName;
      end;
      if Assigned(connection) and (not VarIsNull(keyValue)) then
      begin
        params:=TParams.Create;
        params.AddParameter.Name:='fieldvalue';
        params.AddParameter.Name:='keyvalue';
        params.ParamByName('fieldvalue').Value:=fieldValue;
        params.ParamByName('keyvalue').Value:=keyValue;
        oAdsDatasetOptions.musAdsTableType:=3;
        oAdsDatasetOptions.musAdsLockType:=1;
        oAdsDatasetOptions.musAdsCharType:=1;
        oAdsDatasetOptions.musAdsRightsCheck:=1;
        connection.Execute(oAdsDatasetOptions,Format('Update %s Set %s=:fieldvalue Where %s=:keyvalue;',[tableName,fieldName,keyName]),params,true);
        params.Free;
      end;
    end;
  end;
end;

function createGridColumn(view: TcxGridTableView; parentColumn: TcxGridColumn): TcxGridColumn;
begin
  if view is TcxGridDBTableView then
  begin
    Result := (view as TcxGridDBTableView).CreateColumn;
    if Assigned(parentColumn) then
      Result.Index := parentColumn.Index + 1;
  end
  else if view is TcxGridDBBandedTableView then
  begin
    Result := (view as TcxGridDBBandedTableView).CreateColumn;
    if Assigned(parentColumn) then
    begin
      (Result as TcxGridDBBandedColumn).Position.BandIndex := (parentColumn as TcxGridDBBandedColumn)
        .Position.BandIndex;
      (Result as TcxGridDBBandedColumn).Position.ColIndex := (parentColumn as TcxGridDBBandedColumn)
        .Position.ColIndex + 1;
    end;
  end
  else
    Result := view.CreateColumn;
end;

function getColumnStoreMode(view: TcxGridTableView): TStoreMode;
  function hasSorted(): Boolean;
  var
    i: Integer;
    item: TcxGridColumn;
  begin
    Result:=false;
    for i := 0 to view.ItemCount-1 do
    begin
      item:=TcxGridColumn(view.Items[i]);
      if Assigned(item) and item.Visible and item.Options.Editing and item.Options.Sorting then
      begin
        Result:=true;
        exit;
      end;
    end;
  end;
begin
  Result := smGrid;
  if view is TcxGridDBTableView then
  begin
    Result := smGrid;
    if (view as TcxGridDBTableView).DataController.IsGridMode or hasSorted then
      Result := smDict;
    //else if Assigned((view as TcxGridDBTableView).DataController.DataSet) and (view as TcxGridDBTableView)
    //  .DataController.DataSet.CanModify then
    //  Result := smDict;
  end
  else if view is TcxGridDBBandedTableView then
  begin
    Result := smGrid;
    if (view as TcxGridDBBandedTableView).DataController.IsGridMode or hasSorted then
      Result := smDict;
    //else if Assigned((view as TcxGridDBBandedTableView).DataController.DataSet) and (view as TcxGridDBBandedTableView)
    //  .DataController.DataSet.CanModify then
    //  Result := smDict;
  end;
end;

procedure undoEdit(edit: TcxCustomEdit);
begin
  if edit is TcxTextEdit then
    (edit as TcxTextEdit).Undo
  else if edit is TcxSpinEdit then
    (edit as TcxSpinEdit).Undo
  else if edit is TcxDateEdit then
    (edit as TcxDateEdit).Undo;
end;

{ TViewTimer }
constructor TViewTimer.Create(gridView: TcxGridTableView);
begin
  inherited Create(gridView.Owner);

  self.view := gridView;
  self.Enabled := false;
  self.OnTimer := TimerTimer;
  self.Interval := 2000;
end;

procedure TViewTimer.TimerTimer(Sender: TObject);
begin
  ColumnUnboundHelper.TryProcessUnbound(view);
end;

end.
