object GridFrame: TGridFrame
  Left = 0
  Top = 0
  Width = 271
  Height = 298
  TabOrder = 0
  object dxLayoutControl1: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 271
    Height = 298
    Align = alClient
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    AutoSize = True
    object cxDataGrid: TcxGrid
      Left = 22
      Top = 30
      Width = 218
      Height = 246
      TabOrder = 0
      object tvData: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmManual
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsData
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        object tvDataColumn1: TcxGridDBColumn
          DataBinding.FieldName = '_id'
          Visible = False
        end
        object tvDataColumn2: TcxGridDBColumn
          DataBinding.FieldName = 'Name'
          Width = 155
        end
        object tvDataColumn3: TcxGridDBColumn
          DataBinding.FieldName = 'Value'
          Width = 61
        end
      end
      object cxDataGridLevel1: TcxGridLevel
        GridView = tvData
      end
    end
    object dxLayoutFrameRoot: TdxLayoutGroup
      AlignHorz = ahParentManaged
      AlignVert = avParentManaged
      Hidden = True
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = -1
    end
    object dxLayoutFrameGridItem: TdxLayoutItem
      Parent = dxLayoutFrameDataGroup
      AlignVert = avClient
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = True
      SizeOptions.SizableVert = True
      CaptionOptions.Text = 'Tis01 Modules'
      CaptionOptions.Visible = False
      CaptionOptions.Layout = clTop
      Control = cxDataGrid
      ControlOptions.OriginalHeight = 96
      ControlOptions.OriginalWidth = 150
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object dxLayoutFrameDataGroup: TdxLayoutGroup
      Parent = dxLayoutFrameRoot
      CaptionOptions.Text = 'Data'
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = True
      SizeOptions.SizableVert = True
      SizeOptions.Width = 242
      SizeOptions.MaxHeight = 500
      SizeOptions.MaxWidth = 1000
      Index = 0
      Buttons = <
        item
          Glyph.SourceDPI = 96
          Glyph.SourceHeight = 16
          Glyph.SourceWidth = 16
          Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2241
            72726F77344C656674223E0D0A09093C706F6C79676F6E20636C6173733D2247
            7265656E2220706F696E74733D2232382C31342031312E372C31342031392E37
            2C362031342C3620342C31362031342C32362031392E372C32362031312E372C
            31382032382C3138202623393B222F3E0D0A093C2F673E0D0A3C2F7376673E0D
            0A}
          Hint = 'Move left'
          Tag = 1
          OnClick = dxLayoutFrameDataGroupButton0Click
        end
        item
          Glyph.SourceDPI = 96
          Glyph.SourceHeight = 16
          Glyph.SourceWidth = 16
          Glyph.Data = {
            3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554
            462D38223F3E0D0A3C7376672076657273696F6E3D22312E31222069643D224C
            617965725F312220786D6C6E733D22687474703A2F2F7777772E77332E6F7267
            2F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F77
            77772E77332E6F72672F313939392F786C696E6B2220783D223070782220793D
            22307078222076696577426F783D2230203020333220333222207374796C653D
            22656E61626C652D6261636B67726F756E643A6E657720302030203332203332
            3B2220786D6C3A73706163653D227072657365727665223E262331333B262331
            303B3C7374796C6520747970653D22746578742F6373732220786D6C3A737061
            63653D227072657365727665223E2E426C75657B66696C6C3A23313137374437
            3B7D262331333B262331303B2623393B2E59656C6C6F777B66696C6C3A234646
            423131353B7D262331333B262331303B2623393B2E426C61636B7B66696C6C3A
            233732373237323B7D262331333B262331303B2623393B2E477265656E7B6669
            6C6C3A233033394332333B7D262331333B262331303B2623393B2E5265647B66
            696C6C3A234431314331433B7D262331333B262331303B2623393B2E7374307B
            6F7061636974793A302E37353B7D262331333B262331303B2623393B2E737431
            7B6F7061636974793A302E353B7D3C2F7374796C653E0D0A3C672069643D2241
            72726F77345269676874223E0D0A09093C706F6C79676F6E20636C6173733D22
            477265656E2220706F696E74733D2231382C362031322E332C362032302E332C
            313420342C313420342C31382032302E332C31382031322E332C32362031382C
            32362032382C3136202623393B222F3E0D0A093C2F673E0D0A3C2F7376673E0D
            0A}
          Hint = 'Move right'
          Tag = 1
          OnClick = dxLayoutFrameDataGroupButton1Click
        end>
    end
    object dxLayoutFrameGridSplitter: TdxLayoutSplitterItem
      Parent = dxLayoutFrameRoot
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
  end
  object memData: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 72
    Top = 88
    object memDataName: TStringField
      DisplayWidth = 50
      FieldName = 'Name'
      Visible = False
      Size = 50
    end
    object memDataValue: TStringField
      FieldName = 'Value'
      Visible = False
      Size = 1000
    end
    object memData_id: TStringField
      FieldName = '_id'
      Visible = False
    end
  end
  object dsData: TDataSource
    DataSet = memData
    Left = 144
    Top = 88
  end
end
