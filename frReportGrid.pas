unit frReportGrid;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, frGrid, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData,
  dxLayoutContainer, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxLayoutControl, cxDataControllerConditionalFormattingRulesManagerDialog,
  System.JSON, System.Generics.Collections, uNotifyCenter, CDV.Consts, dxUIAClasses, dxSkinWXI;

type
  TReportGridFrame = class(TGridFrame)
    tvDataColumn4: TcxGridDBColumn;
    memDataMaxReportDate: TDateTimeField;
    memDataHowOldDays: TIntegerField;
    tvDataColumn5: TcxGridDBColumn;
    procedure dxLayoutFrameDataGroupButton0Click(Sender: TObject);
    procedure dxLayoutFrameDataGroupButton1Click(Sender: TObject);
  private
    FReportsDataSet: TFDMemTable;
    procedure FillOtherReports(aAllReports, AClientReports: TDataSet);
  public
    procedure UpdateData(aJObj: TJSONObject; const aID: String); override;

    property ReportsDataSet: TFDMemTable read FReportsDataSet write FReportsDataSet;
  end;

var
  ReportGridFrame: TReportGridFrame;

implementation

{$R *.dfm}

uses
  Utils, System.DateUtils;

{ TReportGridFrame }

procedure TReportGridFrame.dxLayoutFrameDataGroupButton0Click(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(NOTIFY_MOVEGRIDLEFT, Sender);
end;

procedure TReportGridFrame.dxLayoutFrameDataGroupButton1Click(Sender: TObject);
begin
  TNotifyCenter.Instance.SendMessage(NOTIFY_MOVEGRIDRIGHT, Sender);
end;

procedure TReportGridFrame.FillOtherReports(aAllReports, AClientReports: TDataSet);
begin
  aAllReports.First;
  while not AAllReports.Eof do
  begin
    if not AClientReports.Locate('Name', AAllReports.FieldByName('Report').AsString, []) then
    begin
      AClientReports.Append;
      AClientReports.FieldByName('_id').AsString := '0';
      AClientReports.FieldByName('Name').AsString := AAllReports.FieldByName('Report').AsString;
      AClientReports.FieldByName('MaxReportDate').AsDateTime := AAllReports.FieldByName('Date').AsDateTime;
      AClientReports.Post;
    end;
    AAllReports.Next;
  end;
end;

procedure TReportGridFrame.UpdateData(aJObj: TJSONObject; const aID: String);
begin
  if not Assigned(aJObj) then
    Exit;

  memData.DisableControls;
  try
    for var i := 0 to aJObj.Count - 1 do begin
      memData.Append;
      memData.FieldByName('_id').AsString := aID;
      var s := aJObj.ToString;
      memData.FieldByName('Name').AsString := aJObj.Pairs[i].JsonString.Value;
      var DataVersion: Variant;
      DataVersion := OwnStrToDateTime(aJObj.Pairs[i].JsonValue.Value);
      memData.FieldByName('Value').AsDateTime := iif(DataVersion = null, 0, DataVersion);
      if Assigned(ReportsDataSet) and ReportsDataSet.Locate('Report', aJObj.Pairs[i].JsonString.Value, []) then
      begin
        memData.FieldByName('MaxReportDate').AsDateTime :=  ReportsDataSet.FieldByName('Date').AsDateTime;
        memData.FieldByName('HowOldDays').AsInteger := DaysBetween(memData.FieldByName('Value').AsDateTime, ReportsDataSet.FieldByName('Date').AsDateTime);
      end;
      memData.Post;
    end;
    memData.First;

    if Assigned(ReportsDataSet) then
      FillOtherReports(ReportsDataSet, memData);
  finally
    memData.EnableControls;
  end;
end;

end.
