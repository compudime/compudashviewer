object LayoutsData: TLayoutsData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 160
  Width = 331
  object memComps: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    StoreDefs = True
    Left = 16
    Top = 16
    object memCompscomp_name: TStringField
      FieldName = 'comp_name'
      Size = 1024
    end
    object memCompscomp_tags: TMemoField
      FieldName = 'comp_tags'
      BlobType = ftMemo
    end
    object memCompscomp_layout: TStringField
      FieldName = 'comp_layout'
      Size = 255
    end
  end
  object memUsers: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    StoreDefs = True
    Left = 80
    Top = 16
    object memUsersuser_name: TStringField
      FieldName = 'user_name'
      Size = 255
    end
    object memUsersuser_tags: TMemoField
      FieldName = 'user_tags'
      BlobType = ftMemo
    end
    object memUsersuser_layout: TStringField
      FieldName = 'user_layout'
      Size = 255
    end
  end
  object dsComps: TDataSource
    DataSet = memComps
    Left = 16
    Top = 80
  end
  object dsUsers: TDataSource
    DataSet = memUsers
    Left = 80
    Top = 80
  end
  object dsViews: TDataSource
    DataSet = memViewsNew
    Left = 144
    Top = 80
  end
  object memViewsNew: TFDMemTable
    OnFilterRecord = memViewsOldFilterRecord
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    StoreDefs = True
    Left = 159
    Top = 16
    object StringField1: TStringField
      FieldName = 'view_name'
      Size = 100
    end
    object BooleanField1: TBooleanField
      FieldName = 'view_is_default'
    end
    object BlobField1: TBlobField
      FieldName = 'view_body'
    end
    object MemoField1: TMemoField
      FieldName = 'view_tags'
      BlobType = ftMemo
    end
    object memViewsNewview_showing: TStringField
      FieldName = 'view_showing'
      Size = 5000
    end
    object memViewsNewview_storedgrids: TStringField
      FieldName = 'view_storedgrids'
      Size = 5000
    end
  end
end
